﻿using PROST_Integration.Util;
using PROST_Integration.View;
using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace PROST_Integration
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>


    public partial class App : System.Windows.Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                // DEBUG 
                /*string[] strArg = new string[4];
                strArg[1] = "Manual";
                strArg[2] = @"C:\Users\CSA_DEV\Documents\Working\MANDOIVS\WorkSpace\Temp\IVSConfig.csa";*/

                string[] strArg = Environment.GetCommandLineArgs();
                ViewModelLocator.SetAndReg();
                base.OnStartup(e);
                InitializeComponent();

                SetupExceptionHandling();

                CommonUtil.InitWorkspace();


                if (strArg.Length > 1 || Common.Common.IS_TESTMODE)
                {
                    string StartMode = strArg[1];
                    ViewModelLocator.MainVM.MainModel.GUIStartup = false;

                    AllocConsole(); // Create Console
                    Console.Title = "IVS Console App";

                    Task.Factory.StartNew(() =>
                    {
                        while (true)
                        {
                            switch (Console.ReadKey().Key)
                            {
                                case ConsoleKey.Oem2:
                                    ViewModelLocator.MainVM.PauseTest();
                                    break;
                                case ConsoleKey.OemPeriod:
                                    ViewModelLocator.MainVM.RestartTest();
                                    break;

                            }
                        }
                    });

                    if (StartMode == "Auto")
                    {
                        if (strArg.Length < 3)
                        {
                            IVSLog.getInstance().Log("Please Check the Arguments..", Common.Common.MODULE_VIEW, ConsoleColor.Red);
                            IVSLog.getInstance().Log("Press any key to continue..", Common.Common.MODULE_VIEW, ConsoleColor.White);
                            Console.ReadKey(false);
                            Shutdown(1);

                        }
                        else
                        {
                            ViewModelLocator.MainVM.MainModel.ConsoleAppMode = Common.Common.AUTO_START;
                            
                            ViewModelLocator.MainVM.ReadConfigFile(strArg[2]);

                            IVSLog.getInstance().Log("Make WorkSpace Folder", Common.Common.MODULE_VIEW, ConsoleColor.Yellow);
                            DirectoryInfo di = new DirectoryInfo(Common.Common.WORKSPACE_PATH);
                            if (!di.Exists)
                            {
                                di.Create();
                                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\TestCase");
                                di.Create();
                                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\Report");
                                di.Create();
                                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\T1Report");
                                di.Create();
                                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\Temp");
                                di.Create();
                            }

                            FunctionWorkHandler.getInstance().ParseStart();
                        }
                    }
                    else
                    {
                        ViewModelLocator.MainVM.MainModel.ConsoleAppMode = Common.Common.MANUAL_START;
                        ViewModelLocator.MainVM.ConsoleMode_ConfigMode();
                    }

                    // 0 - 정상종료 , 1 - 에러종료
                    //Shutdown(0);
                }
                else
                {
                    //FreeConsole();
                    ViewModelLocator.MainVM.MainModel.GUIStartup = true;
                    this.MainWindow = new MainWindow();
                    this.MainWindow.Show();

                    if (File.Exists(Common.Common.WORKSPACE_PATH + "\\Temp\\IVSConfig.csa"))
                    {
                        ViewModelLocator.MainVM.ReadConfigFile(Common.Common.WORKSPACE_PATH + "\\Temp\\IVSConfig.csa");
                    }
                    else
                        File.Create(Common.Common.WORKSPACE_PATH + "\\Temp\\IVSConfig.csa");
                }

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("[Error] " +  ex.Message +" " +CommonUtil.GetExceptionLineNumber(ex));
            }


        }
        private void SetupExceptionHandling()
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
                LogUnhandledException((Exception)e.ExceptionObject, "AppDomain.CurrentDomain.UnhandledException");

            DispatcherUnhandledException += (s, e) =>
            {
                LogUnhandledException(e.Exception, "Application.Current.DispatcherUnhandledException");
                e.Handled = true;
            };

            TaskScheduler.UnobservedTaskException += (s, e) =>
            {
                LogUnhandledException(e.Exception, "TaskScheduler.UnobservedTaskException");
                e.SetObserved();
            };
        }

        private void LogUnhandledException(Exception exception, string source)
        {
            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, exception.Message);
            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, "Line :: " + CommonUtil.GetExceptionLineNumber(exception));
        }
        private const int ATTACH_PARENT_PROCESS = -1;

        [DllImport("kernel32", SetLastError = true)]
        private static extern bool AttachConsole(int dwProcessId);

        [DllImport("kernel32.dll")]
        private static extern bool FreeConsole();
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();



    }
}
