﻿using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PROST_Integration.Dialog
{
    /// <summary>
    /// SelectTestSheetDialogView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class SelectTestSheetDialogView : UserControl
    {
        public static SelectTestSheetDialogView Instance
        {
            get; private set;
        }
        public SelectTestSheetDialogView()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = ViewModelLocator.MainVM;
            UpdateView();
        }
        public void UpdateView()
        {
            GridTestSheet.RowDefinitions.Clear();
            GridTestSheet.Children.Clear();


            for (int i = 0; i < ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Count; i++)
            {
                RowDefinition c1 = new RowDefinition();
                c1.Height = new GridLength(30, GridUnitType.Pixel);

                GridTestSheet.RowDefinitions.Add(c1);
            }
            

            for (int i = 0; i < ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Count; i++)
            {
                CheckBox checkbox = new CheckBox();
                checkbox.Content = ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[i];
                checkbox.Margin = new Thickness(10, 10, 0, 0);
                checkbox.VerticalAlignment = VerticalAlignment.Center;
                checkbox.Background = new SolidColorBrush(Colors.Black);
                checkbox.Foreground = new SolidColorBrush(Colors.Black);

                /*checkbox.SetValue(Control.ForegroundProperty, App.Current.Resources["StandardTextForeground"]);
                checkbox.SetValue(Control.BackgroundProperty, App.Current.Resources["CheckBoxBackGroundColor"]);*/

                Binding myBinding = new Binding("SelectTestcaseSheetNameList[" + i + "]");

                checkbox.SetBinding(CheckBox.IsCheckedProperty, myBinding);

                Grid.SetRow(checkbox, i);
                GridTestSheet.Children.Add(checkbox);
            }
        }
    }
}
