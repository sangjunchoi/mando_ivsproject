﻿using GalaSoft.MvvmLight;
using PROST_Integration.Popup;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PROST_Integration.Common
{
    public class Common
    {
        public static string INSTALL_PATH = Directory.GetParent(Environment.CurrentDirectory).FullName;
        public const string PARSER_APP = @"\parser\src\Main.py";
        public const string T1MANAGER_APP = @"\t1manager\Main.py";
        public const string T1PROJECTINFO_APP = @"\t1manager\T1Pparser.py";
        public const string PROJECT_INFO_DATA = "C_PROJECT_INFO_DATA_dictionary.txt";
        public const string TRUE = "Visible";
        public const string FALSE = "Collapsed";


        private static string _WORKSPACE_PATH;
        public static string WORKSPACE_PATH
        {
            get
            {
                return _WORKSPACE_PATH;
            }
            set
            {
                _WORKSPACE_PATH = value;
            }
        }
        public static string _SRC_PATH = Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).FullName + "\\src";
        public static string SRC_PATH
        {
            get
            {
                return _SRC_PATH;
            }
            set
            {
                _SRC_PATH = value;
            }
        }
        public const int CET = 0;
        public const int DT = 1;
        public const int GET = 2;
        public const int IPT = 3;
        public const int RT = 4;
        public const int ST = 5;
        public const int PER = 6;

        public const int TIMING_PARAM_START = 0;
        public const int TIMING_PARAM_ACTIVATION = 1;
        public const int TIMING_PARAM_STOP = 2;

        public const int MODULE_VIEW = 0;
        public const int MODULE_PARSE = 1;
        public const int MODULE_REPORT = 2;
        public const int MODULE_TRACE32 = 3;
        public const int MODULE_CANLINK = 4;
        public const int MODULE_T1MANAGER = 5;

        public const int LOGTYPE_INF = 0;
        public const int LOGTYPE_PGR = 1;
        public const int LOGTYPE_PFF = 2;
        public const int LOGTYPE_ERR = 3;
        public const int LOGTYPE_CPL = 4;
        public const int LOGTYPE_TINFO = 5;

        public const int TESTDIALOG_SHOW = -1;

        public const bool IS_TESTMODE = false;

        public const int RUNNING = 0;
        public const int PAUSE = 1;
        public const int STEP_RUNNING = 2;
        public const int STOP = 3;

        public const int AUTO_START = 0;
        public const int MANUAL_START = 1;

        public const int MANUAL_TEST_MODE = 0;
        public const int AUTO_TEST_MODE = 1;

        public const int WORKSTATE_NORMAL = 0;
        public const int WORKSTATE_ERROR = 1;

        public static PopupTestStatusView m_PopupTestStatusView;
        public static PopupParsingStatusView m_PopupParsingView;
        public static PopupPerformanceTestStatusView m_PopupPerformanceTestStatusView;
        public static PopupConstraintSettingView m_PopupConstraintSettingView;
        public static PopupScenarioSettingView m_PopupScenarioSettingView;

        public const int SAVEMODE_STANDARD = 0;
        public const int SAVEMODE_BEFORE_END = 1;

    }
}
