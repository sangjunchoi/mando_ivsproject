﻿using PROST_Integration.CustomControl;
using PROST_Integration.Model;
using PROST_Integration.Util;
using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PROST_Integration.Popup
{
    /// <summary>
    /// PopupConstraintSettingView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupConstraintSettingView : Window
    {
    

        public PopupConstraintSettingView(string Header, object context)
        {
            InitializeComponent();
            Title_TB.Text = Header;
            DataContext = DataContext = context;
        }
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox comboBox = (ComboBox)sender;

            ConstrateModel model = (ConstrateModel)comboBox.Tag;

            if (model == null)
                return;

            if (comboBox.SelectedValue == null)
                return;

            
            int index = ViewModelLocator.MainVM.MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList.IndexOf(model);


            foreach (T1ProjectInfoModel info in ViewModelLocator.MainVM.T1ProjectInfoList)
            {
                if (info.CoreName.Equals(comboBox.SelectedValue))
                {
                    ViewModelLocator.MainVM.MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList[index].TaskISRList = info.DataList;
                    break;
                }
            }
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void NumericSpinner_LostFocus(object sender, RoutedEventArgs e)
        {
            NumericSpinner spinner = (NumericSpinner)sender;

            T1ProjectInfoDataModel model = (T1ProjectInfoDataModel)spinner.Tag;

            if (model.DataType.Equals("CPU Load"))
            {
                if (spinner.Value > 100)
                    spinner.Value = 100;
            }
            else
            {
                if (spinner.Value > 10000000000)
                    spinner.Value = 10000000000;
            }
    }
    }
}
