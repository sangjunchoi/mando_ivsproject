﻿using HwDriverAPI;
using PROST_Integration.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using static HwDriverAPI.CANMsg;

namespace PROST_Integration.Util
{
    class CANlink
    {
        HpAPI hwAPI;
        bool runRxThread;

        public UInt16 ChannelSelet;
        public Dictionary<string, string> rxMessage;
        public string HexResultData;
        public bool rxMessageCheck;

        List<CANMsg> arrMsg;
        UInt16 rxStartbit;
        UInt16 rxSignalLenght;
        UInt16 rxMessageID;

        string rxByteOrder;

        bool rxRead;

        private static class LazyHolder
        {
            public static readonly CANlink INSTANCE = new CANlink();
        }

        public static CANlink getInstance()
        {
            return LazyHolder.INSTANCE;
        }
        public CANlink()
        {
            rxMessage = new Dictionary<string, string>();
            runRxThread = false;
            rxRead = false;
            rxMessageCheck = false;
            ChannelSelet = 0;
            rxMessageID = 0;
            rxStartbit = 0;
            rxSignalLenght = 0;
            rxByteOrder = "";
            HexResultData = "";
        }
        ~CANlink()
        {

        }
        public void rxThreadVariableInit()
        {
            rxThreadVariableSet(0, 0, 0, "", false);
        }
        public void rxVariableReadInit()
        {
            HexResultData = "";
        }
        public void rxThreadVariableSet(UInt16 MessageID, UInt16 StartBit, UInt16 SignalLenght, string ByteOrder, bool Read)
        {
            rxMessageID = MessageID;
            rxStartbit = StartBit;
            rxSignalLenght = SignalLenght;
            rxByteOrder = ByteOrder;
            rxRead = Read;
            rxMessageCheck = false;
        }

        public List<object> MessageSignalLinking(string MessageDatabasePath, string SignalDatabasePath)
        {
            List<object> MergeDatabase = new List<object>();
            List<object> MessageDataList = new List<object>();

            Dictionary<string, string> MessageDictionary = new Dictionary<string, string>();
            StreamReader FullMessageDatabase = new StreamReader(MessageDatabasePath);

            if (FullMessageDatabase == null)
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_TINFO, "MSG Database is null (" + MessageDatabasePath + ")");

            while (!FullMessageDatabase.EndOfStream)
            {
                string ReadFullMessageDatabase = FullMessageDatabase.ReadToEnd();
                string[] ParsedMessageData = ReadFullMessageDatabase.Split(',');

                for (int i = 0; i < ParsedMessageData.Length; i++)
                {
                    switch (i % 20)
                    {
                        case 0:
                            if (ParsedMessageData[i].Contains("\n"))
                            {
                                string[] RemoveText = ParsedMessageData[i].Split('\n');
                                MessageDictionary["Message name"] = RemoveText[1];
                            }
                            else
                            {
                                MessageDictionary["Message name"] = ParsedMessageData[i];
                            }

                            break;
                        case 1: MessageDictionary["Message ID"] = ParsedMessageData[i]; break;
                        case 2: MessageDictionary["ID_Format"] = ParsedMessageData[i]; break;
                        case 3: MessageDictionary["DLC [Byte]"] = ParsedMessageData[i]; break;
                        case 5: MessageDictionary["Comment"] = ParsedMessageData[i]; break;
                        case 13:
                            MessageDictionary["GenMsgCycleTimeFast"] = ParsedMessageData[i];
                            MessageDataList.Add(new Dictionary<string, string>(MessageDictionary));
                            break;
                    }
                }
            }

            List<object> SignalDataList = new List<object>();
            Dictionary<string, string> SignalDictionary = new Dictionary<string, string>();
            StreamReader FullSignalDatabase = new StreamReader(SignalDatabasePath);

            if (FullSignalDatabase == null)
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_TINFO, "SIG Database is null (" + SignalDatabasePath + ")");

            while (!FullSignalDatabase.EndOfStream)
            {
                string ReadFullSignalDatabase = FullSignalDatabase.ReadToEnd();
                string[] ParsedMessageData = ReadFullSignalDatabase.Split(',');

                for (int i = 0; i < ParsedMessageData.Length; i++)
                {
                    switch (i % 35)
                    {
                        case 0:
                            if (ParsedMessageData[i].Contains("\n"))
                            {
                                string[] RemoveText = ParsedMessageData[i].Split('\n');
                                SignalDictionary["Signal name"] = RemoveText[1];
                            }
                            else
                            {
                                SignalDictionary["Signal name"] = ParsedMessageData[i];
                            }

                            break;
                        case 2: SignalDictionary["Message ID"] = ParsedMessageData[i]; break;
                        case 4: SignalDictionary["Startbit"] = ParsedMessageData[i]; break;
                        case 5: SignalDictionary["SignalLength"] = ParsedMessageData[i]; break;
                        case 6: SignalDictionary["Byte order"] = ParsedMessageData[i]; break;
                        case 8: SignalDictionary["Factor"] = ParsedMessageData[i]; break;
                        case 9:
                            SignalDictionary["Offset"] = ParsedMessageData[i];
                            SignalDataList.Add(new Dictionary<string, string>(SignalDictionary));
                            break;
                    }
                }
            }

            for (int i = 0; i < SignalDataList.Count; i++)
            {
                Dictionary<string, string> SignalDataDictionary = (Dictionary<string, string>)SignalDataList[i];
                for (int j = 0; j < MessageDataList.Count; j++)
                {
                    Dictionary<string, string> MessageDataDictionary = (Dictionary<string, string>)MessageDataList[j];

                    if (MessageDataDictionary["Message ID"].Equals(SignalDataDictionary["Message ID"]))
                    {
                        var MergeDictionary = SignalDataDictionary.Union(MessageDataDictionary).ToDictionary(k => k.Key, v => v.Value);
                        MergeDatabase.Add(MergeDictionary);
                        break;
                    }
                }
            }
            return MergeDatabase;
        }

        public void LibraryOpen()
        {
            try
            {
                hwAPI = new HpAPI(new HwAPI._FuncPrint(Print), "PROST_Integration");
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_CANLINK, Common.Common.LOGTYPE_ERR, typeof(CANlink).Name + " :: " + ex.Message + " Line :: " + (ex));
            }

            return;
        }

        public void DBCtoCSV(string dbc_path)
        {
            ProcessStartInfo cmd = new ProcessStartInfo();
            Process process = new Process();
            string convert_dbc_Path = Path.GetDirectoryName(dbc_path) + "\\" + Path.GetFileNameWithoutExtension(dbc_path);
            FileInfo CSVFileList = new FileInfo(convert_dbc_Path + "_MSG.csv");
            if (CSVFileList.Exists)
            {
                CSVFileList = new FileInfo(convert_dbc_Path + "_SIG.csv");
                if (CSVFileList.Exists) return;
            }


            cmd.FileName = @"cmd";
            cmd.WindowStyle = ProcessWindowStyle.Hidden;
            cmd.CreateNoWindow = true;

            cmd.UseShellExecute = false;
            cmd.RedirectStandardOutput = true;
            cmd.RedirectStandardInput = true;
            cmd.RedirectStandardError = true;

            process.EnableRaisingEvents = false;
            process.StartInfo = cmd;
            process.Start();

            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_TINFO, "DBDesigner.exe " + convert_dbc_Path + " " + convert_dbc_Path + "_MSG.csv /OBJ:MSG");
            process.StandardInput.Write("DBDesigner.exe " + convert_dbc_Path + ".dbc" + " " + convert_dbc_Path + "_MSG.csv /OBJ:MSG" + Environment.NewLine);
            Thread.Sleep(5000);

            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_TINFO, "DBDesigner.exe " + convert_dbc_Path + " " + convert_dbc_Path + "_SIG.csv /OBJ:SIG");
            process.StandardInput.Write("DBDesigner.exe " + convert_dbc_Path + ".dbc" + " " + convert_dbc_Path + "_SIG.csv /OBJ:SIG" + Environment.NewLine);
            Thread.Sleep(5000);

            process.StandardInput.Close();
        }
        public void Print(string strLog, bool IsAlway)
        {
            IVSLog.getInstance().Log(strLog, Common.Common.MODULE_CANLINK);
        }

        public bool CANInterfaceConnect()
        {
            bool isCanFd = true;
            bool isStdId = true;
            bool bSuccess;
            uint baudrate = (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CAN_BaudRate;
            uint datarate = (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CANFD_BaudRate;
            string errLog = "";

            //hwAPI.OpenConfigWindow();
            bSuccess = hwAPI.ConnectMultipleChannel(isCanFd, isStdId, baudrate, datarate, ref errLog, (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CAN_TSEG1,
                (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CAN_TSEG2, (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CAN_SJW,
                (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CANFD_TSEG1, (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CANFD_TSEG2,
                (uint)ViewModelLocator.MainVM.CANPropertyModelList[0].CANFD_SJW);


            IVSLog.getInstance().Log(errLog, Common.Common.MODULE_CANLINK);
            runRxThread = true;

            return bSuccess;
        }

        public void CANDisConnect()
        {
            hwAPI.DisConnectCAN();
        }
        public bool SendCANMsg(int MessageID, int Startbit, int DLC, int data, string ID_Format, int SignalLenght, int Ch, string ByteOrder)
        {
            CANMsg msg = new CANMsg();
            msg.Id = (uint)MessageID;
            msg.Dir = (Msg_Direction)1;
            #region DLC Converting

            UInt16 MSgDLC = (UInt16)DLC;

            if (DLC == 12)
                MSgDLC = 9;
            else if (DLC == 16)
                MSgDLC = 10;
            else if (DLC == 20)
                MSgDLC = 11;
            else if (DLC == 24)
                MSgDLC = 12;
            else if (DLC == 32)
                MSgDLC = 13;
            else if (DLC == 48)
                MSgDLC = 14;
            else if (DLC == 64)
                MSgDLC = 15;
            else
            {
                ;
            }

            #endregion
            msg.DLC = MSgDLC;


            #region CAN-FD Message Setting                
            if (ID_Format == "CAN FD Standard")
                msg.FDF = true;
            else
                msg.FDF = false;
            #endregion

            char[] MsgData = new char[DLC * 8];
            for (int i = 0; i < DLC * 8; i++)
                MsgData[i] = '0';


            string binary = Convert.ToString(data, 2);

            if (binary.Length > SignalLenght)
            {
                return true;
            }



            string[] InsertMsgData = new string[DLC];

            if (ByteOrder == "Intel")
            {
                for (int i = 0; i < binary.Length; i++)
                    MsgData[Startbit + binary.Length - 1 - i] = binary[i];
            }
            else if (ByteOrder == "Motorola")
            {
                for (int i = 0; i < binary.Length; i++)
                    MsgData[Startbit + i] = binary[i];
            }



            for (int i = 0; i < DLC; i++)
                for (int j = 0; j < 8; j++)
                {
                    //for (int j = 0 + i * 8; j < i * 8 + 8; j++)
                    InsertMsgData[i] += MsgData[(i * 8 + 7) - j];
                }

            for (int i = 0; i < DLC; i++)
                msg.Data[i] = Convert.ToByte(InsertMsgData[i], 2);

            if (ViewModelLocator.MainVM.CANPropertyModelList[0].CANFD_BRS)
                hwAPI.SendMsg(msg);
            else
                hwAPI.SendMsgBrsOff(msg, (byte)Ch);

            return false;
        }

        public void ReceiveBufferClear()
        {
            arrMsg = null;
        }

        public void ReceiveThread()
        {
            arrMsg = hwAPI.ReceiveMsg();

            foreach (CANMsg msg in arrMsg)
            {
                ushort DLC = 0;
                if (msg.DLC == 9)
                    DLC = 12;
                else if (msg.DLC == 10)
                    DLC = 16;
                else if (msg.DLC == 11)
                    DLC = 20;
                else if (msg.DLC == 12)
                    DLC = 24;
                else if (msg.DLC == 13)
                    DLC = 32;
                else if (msg.DLC == 14)
                    DLC = 48;
                else if (msg.DLC == 15)
                    DLC = 64;
                else
                {
                    DLC = msg.DLC;
                }



                if (msg.Id == rxMessageID)
                {
                    List<string> BinaryData = new List<string>();
                    string binary = "";
                    string SaveBinaryData = "";
                    string ResultBinary = "";

                    if (rxByteOrder == "Intel")
                    {
                        for (int i = DLC - 1; i >= 0; i--)
                        {
                            binary = Convert.ToString(msg.Data[i], 2);
                            binary = binary.PadLeft(8, '0');
                            SaveBinaryData += binary;
                        }

                        SaveBinaryData = SaveBinaryData.Substring(DLC * 8 - rxStartbit - rxSignalLenght, rxSignalLenght);
                        string[] LocalBinary = new string[(int)SaveBinaryData.Length / 8 + 1];

                        for (int i = 0; i < (int)SaveBinaryData.Length / 8 + 1; i++)
                        {
                            if (i < (int)SaveBinaryData.Length / 8)
                                LocalBinary[SaveBinaryData.Length / 8 - i] = SaveBinaryData.Substring(SaveBinaryData.Length - (i + 1) * 8, 8).PadLeft(8, '0');

                            else if (i == (int)SaveBinaryData.Length / 8)
                            {
                                int SignalLenght = (int)rxSignalLenght - i * 8;
                                LocalBinary[0] = SaveBinaryData.Substring(0, SignalLenght).PadLeft(8, '0');
                            }

                        }

                        for (int i = 0; i < LocalBinary.Count(); i++)
                        {
                            ResultBinary += LocalBinary[i];
                        }
                    }

                    else if (rxByteOrder == "Motorola")
                    {
                        for (int i = 0; i < DLC; i++)
                        {
                            binary = Convert.ToString(msg.Data[i], 2);
                            binary = binary.PadLeft(8, '0');
                            SaveBinaryData += binary;
                        }

                        SaveBinaryData = SaveBinaryData.Substring(rxStartbit, rxSignalLenght);
                        string[] LocalBinary = new string[(int)SaveBinaryData.Length / 8 + 1];

                        for (int i = 0; i < (int)SaveBinaryData.Length / 8 + 1; i++)
                        {
                            if (i < (int)SaveBinaryData.Length / 8)
                                LocalBinary[i] = SaveBinaryData.Substring(i * 8, 8).PadLeft(8, '0');
                            else if (i >= (int)SaveBinaryData.Length / 8)
                                LocalBinary[i] = SaveBinaryData.Substring(i * 8, SaveBinaryData.Length - i * 8).PadLeft(8, '0');
                        }

                        for (int i = 0; i < LocalBinary.Count(); i++)
                        {
                            ResultBinary += LocalBinary[i];
                        }
                    }
                    else
                    {

                    }

                    rxMessage["Message DLC"] = DLC.ToString();
                    UInt32 DecResultData = Convert.ToUInt32(ResultBinary, 2);
                    HexResultData = "0x" + Convert.ToString(DecResultData, 16);
                    rxRead = false;
                    rxMessageCheck = true;
                    break;
                }
            }
        }
    }
}
