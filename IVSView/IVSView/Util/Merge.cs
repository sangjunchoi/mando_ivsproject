﻿using PROST_Integration.Model;
using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Util
{
    class Merge
    {
        public List<object> FullParsedTestcade;
        public List<object> FullMergedTestcade;
        public List<object> ResaultList;

        public Merge()
        {

        }
        ~Merge()
        {

        }

        public void TestcaseToCANDatabase(List<object> TestcaseDictionary)
        {
            try
            {
                // 특정 시트의 Testcase를 담고 있는 리스트
                FullParsedTestcade = TestcaseDictionary;
                // Testcase와 DBC 파일을 병합할 리스트
                FullMergedTestcade = new List<object>();
                // TRACE32에서 읽은 값을 보관할 리스트
                ResaultList = new List<object>();

                // Testcase Data editing :: Merge Intermediate testcase and Database CAN

                for (int i = 0; i < FullParsedTestcade.Count; i++)
                {
                    Dictionary<string, string> TestDictionary = (Dictionary<string, string>)FullParsedTestcade[i];
                    if (TestDictionary["Input"].Equals(null) || TestDictionary["Input"].Equals("null"))
                    {
                        TestDictionary["TC[MIN]_Output1"] = "No Input Data";
                        FullMergedTestcade.Add(TestDictionary);
                        continue;
                    }

                    if (TestDictionary["Input"].Contains("::"))
                    {
                        // Input 데이터 중에 "::" 가 포함되어 있는면 CAN MSG 정보
                        // CAN MSG일 경우 DBC에서 매칭시켜 메시지 생성에 필요한 정보를 저장
                        bool FindFlag = false;

                        for (int z = 0; z < ViewModelLocator.MainVM.CANPropertyModelList.Count; z++)
                        {
                            string tempPath = "";
                            string convertPath = "";
                            if (ViewModelLocator.MainVM.CANPropertyModelList[z].CAN_DBC_Path.Contains("$WORKSPACE$"))
                                tempPath = ViewModelLocator.MainVM.CANPropertyModelList[z].CAN_DBC_Path.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                            else
                                tempPath = ViewModelLocator.MainVM.CANPropertyModelList[z].CAN_DBC_Path;

                            convertPath = Path.GetDirectoryName(tempPath) + "\\" + Path.GetFileNameWithoutExtension(tempPath);

                            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, convertPath);
                            List<object> FullDatabaseCAN_CH = CANlink.getInstance().MessageSignalLinking(convertPath + "_MSG.csv", convertPath + "_SIG.csv");

                            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, TestDictionary["Input"]);
                            string[] InputData = TestDictionary["Input"].Split(' ');

                            for (int j = 0; j < FullDatabaseCAN_CH.Count; j++)
                            {
                                Dictionary<string, string> DatabaseCANDictionary = (Dictionary<string, string>)FullDatabaseCAN_CH[j];

                                if (InputData[0].Trim().Equals(DatabaseCANDictionary["Message name"]) && InputData[2].Trim().Equals(DatabaseCANDictionary["Signal name"]))
                                {
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, "Find Data : " + TestDictionary["Input"] + "  Database : " + z.ToString());
                                    var MergeDictionary = TestDictionary.Union(DatabaseCANDictionary).ToDictionary(k => k.Key, v => v.Value);
                                    MergeDictionary["Database"] = Convert.ToString(z);
                                    FullMergedTestcade.Add(MergeDictionary);
                                    FindFlag = true;
                                    break;
                                }
                            }

                            if (FindFlag) break;
                        }

                        if (!FindFlag)
                        {
                            // 해당 메시지 정보가 DB 파일에 없을 경우
                            TestDictionary["TC[MIN]_Output1"] = "No signal found.";
                            FullMergedTestcade.Add(TestDictionary);
                        }

                    }
                    else if (TestDictionary["Output1"].Contains("::"))
                    {
                        bool FindFlag = false;

                        for (int z = 0; z < ViewModelLocator.MainVM.CANPropertyModelList.Count; z++)
                        {
                            string tempPath = "";
                            string convertPath = "";
                            if (ViewModelLocator.MainVM.CANPropertyModelList[z].CAN_DBC_Path.Contains("$WORKSPACE$"))
                                tempPath = ViewModelLocator.MainVM.CANPropertyModelList[z].CAN_DBC_Path.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                            else
                                tempPath = ViewModelLocator.MainVM.CANPropertyModelList[z].CAN_DBC_Path;

                            convertPath = Path.GetDirectoryName(tempPath) + "\\" + Path.GetFileNameWithoutExtension(tempPath);

                            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, convertPath);
                            List<object> FullDatabaseCAN_CH = CANlink.getInstance().MessageSignalLinking(convertPath + "_MSG.csv", convertPath + "_SIG.csv");
                            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, TestDictionary["Output1"]);

                            string[] InputData = TestDictionary["Output1"].Split(' ');

                            for (int j = 0; j < FullDatabaseCAN_CH.Count; j++)
                            {
                                Dictionary<string, string> DatabaseCANDictionary = (Dictionary<string, string>)FullDatabaseCAN_CH[j];

                                if (InputData[0].Trim().Equals(DatabaseCANDictionary["Message name"]) && InputData[2].Trim().Equals(DatabaseCANDictionary["Signal name"]))
                                {
                                    var MergeDictionary = TestDictionary.Union(DatabaseCANDictionary).ToDictionary(k => k.Key, v => v.Value);
                                    MergeDictionary["Database"] = Convert.ToString(z);
                                    FullMergedTestcade.Add(MergeDictionary);
                                    FindFlag = true;
                                    break;
                                }

                            }
                            if (FindFlag) break;
                        }

                        if (!FindFlag)
                        {
                            // 해당 메시지 정보가 DB 파일에 없을 경우
                            TestDictionary["TC[MIN]_Output1"] = "No signal found.";
                            FullMergedTestcade.Add(TestDictionary);
                        }
                    }
                    else
                    {
                        // Input 데이터가 메시지가 아닐경우
                        FullMergedTestcade.Add(TestDictionary);
                    }
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(Merge).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }
    }
}
