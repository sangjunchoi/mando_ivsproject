﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Util
{
    class CANlink_Software
    {
        private bool isCANlinkStart = false;
        Process process;
        public void CANlinkConfigFileOpen(string Path)
        {
            process = new Process();
            process.StartInfo.FileName = Path;
            process.Start();
        }
        ~CANlink_Software()
        {
            //process.Kill();
        }

        public bool CANlinkStart()
        {
            try
            {
                if (!isCANlinkStart)
                {
                    Start();
                    isCANlinkStart = true;
                }
                else
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(CANlink_Software).Name + " :: " + ex.Message + " Line :: " + (ex));
                return false;
            }
        }

        public bool CANlinkStop()
        {
            if (isCANlinkStart)
            {
                Start();
                isCANlinkStart = false;
            }
            else
                return false;

            return true;
        }

        public bool CANlinkReplayCheck()
        {
            IntPtr nReturn = IntPtr.Zero;
            RunScriptFunction("ReplayCheckFunction", "", "", ref nReturn);

            // RunScriptFunction(Lua Script function name, input parameter type, Comma separated parameters, return value);
            // Order of parameters [Comma separated parameters]

            string msg = System.Runtime.InteropServices.Marshal.PtrToStringAuto(nReturn);
            Marshal.FreeBSTR(nReturn);

            if (msg == "0")
                return true;
            else
                return false;
        }

        public void CANlinkRecordFileSet(string Path)
        {
            IntPtr nReturn = IntPtr.Zero;
            RunScriptFunction("RecordFilePathSet", "s",Path, ref nReturn);

            // RunScriptFunction(Lua Script function name, input parameter type, Comma separated parameters, return value);
            // Order of parameters [Comma separated parameters]

            string msg = System.Runtime.InteropServices.Marshal.PtrToStringAuto(nReturn);

            Marshal.FreeBSTR(nReturn);
        }

        public string HWConnectionCheck()
        {
            IntPtr nReturn = IntPtr.Zero;
            RunScriptFunction("CheckCANlinkStart", "", "", ref nReturn);

            // RunScriptFunction(Lua Script function name, input parameter type, Comma separated parameters, return value);
            // Order of parameters [Comma separated parameters]

            string msg = System.Runtime.InteropServices.Marshal.PtrToStringAuto(nReturn);

            Marshal.FreeBSTR(nReturn);

            return msg;
        }

        public void CANlinkReset()
        {
            IntPtr nReturn = IntPtr.Zero;
            RunScriptFunction("OnResetCan", "", "", ref nReturn);

            // RunScriptFunction(Lua Script function name, input parameter type, Comma separated parameters, return value);
            // Order of parameters [Comma separated parameters]

            string msg = System.Runtime.InteropServices.Marshal.PtrToStringAuto(nReturn);

            Marshal.FreeBSTR(nReturn);
        }

        public void CANlinkRecordFilePlay()
        {
            IntPtr nReturn = IntPtr.Zero;
            RunScriptFunction("RecordFilePlay", "", "", ref nReturn);

            // RunScriptFunction(Lua Script function name, input parameter type, Comma separated parameters, return value);
            // Order of parameters [Comma separated parameters]

            string msg = System.Runtime.InteropServices.Marshal.PtrToStringAuto(nReturn);

            Marshal.FreeBSTR(nReturn);
        }

        [DllImport("COMServerDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        private extern static void Start();

        [DllImport("COMServerDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static void RunScriptFunction(string strFunctionName, string strTypes, string strParams, ref IntPtr pReturn);
        //strFunctionName       - Script Function Name
        //strTypes              - Parameter Type (ex : 'i' = 'int', 'f' = 'float', 'c' = 'char', 's' = 'string')
        //strParams             - Parameter
        //pReturn               - Return Value
    }
}

