﻿using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace PROST_Integration.Util
{
    class IVSLog
    {
        private static class LazyHolder
        {
            public static readonly IVSLog INSTANCE = new IVSLog();
        }

        public static IVSLog getInstance()
        {
            return LazyHolder.INSTANCE;
        }
        public void Log(string msg)
        {
            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                if (msg != null && !msg.Equals(""))
                {
                    //AttachConsole(-1);
                    string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                    Console.WriteLine(Logtime + " " + msg.TrimStart());
                }

            }
            else
            {
                Dispatcher dispatcher = System.Windows.Application.Current.Dispatcher;
                DispatcherOperation dispatcherOperation = dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Send, (ThreadStart)
                delegate ()
                {
                    if (msg != null && !msg.Equals(""))
                    {
                        string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                        ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + msg.TrimStart() });
                    }
                });
            }

        }
        public void Log(string msg, int module, ConsoleColor color)
        {
            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                if (msg != null && !msg.Equals(""))
                {
                    string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                    Console.ForegroundColor = color;
                    switch (module)
                    {
                        case Common.Common.MODULE_VIEW:
                            Console.WriteLine(Logtime + " [MAIN_GUI]" + " [INF] " + msg.TrimStart());
                            break;
                        case Common.Common.MODULE_PARSE:
                            Console.WriteLine(msg.TrimStart());
                            break;
                        case Common.Common.MODULE_REPORT:
                            Console.WriteLine(msg.TrimStart());
                            break;
                        case Common.Common.MODULE_TRACE32:
                            Console.WriteLine(Logtime + " [TRACE32]" + " [INF] " + msg.TrimStart());
                            break;
                        case Common.Common.MODULE_CANLINK:
                            Console.WriteLine(Logtime + " [CANINK]" + " [INF] " + msg.TrimStart());
                            break;
                    }

                }

            }
            else
            {
                Dispatcher dispatcher = System.Windows.Application.Current.Dispatcher;
                DispatcherOperation dispatcherOperation = dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Send, (ThreadStart)
                delegate ()
                {
                    if (msg != null && !msg.Equals(""))
                    {
                        string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                        switch (module)
                        {
                            case Common.Common.MODULE_VIEW:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " [MAIN_GUI]" + " [INF] " + msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_PARSE:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_REPORT:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_TRACE32:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " [TRACE32]" + " [INF] " + msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_CANLINK:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " [CANLINK]" + " [INF] " + msg.TrimStart() });
                                break;
                        }
                    }
                });
            }

        }
        public void Log(string msg, int module)
        {
            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                if (msg != null && !msg.Equals(""))
                {
                    string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                    switch (module)
                    {
                        case Common.Common.MODULE_VIEW:
                            Console.WriteLine(Logtime + " [MAIN_GUI]" + " [INF] " + msg.TrimStart());
                            break;
                        case Common.Common.MODULE_PARSE:
                            Console.WriteLine(msg.TrimStart());
                            break;
                        case Common.Common.MODULE_REPORT:
                            Console.WriteLine(msg.TrimStart());
                            break;
                        case Common.Common.MODULE_T1MANAGER:
                            Console.WriteLine(msg.TrimStart());
                            break;
                        case Common.Common.MODULE_TRACE32:
                            Console.WriteLine(Logtime + " [TRACE32]" + " [INF] " + msg.TrimStart());
                            break;
                        case Common.Common.MODULE_CANLINK:
                            Console.WriteLine(Logtime + " [CANINK]" + " [INF] " + msg.TrimStart());
                            break;
                    }

                }

            }
            else
            {
                Dispatcher dispatcher = System.Windows.Application.Current.Dispatcher;
                DispatcherOperation dispatcherOperation = dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Send, (ThreadStart)
                delegate ()
                {
                    if (msg != null && !msg.Equals(""))
                    {
                        string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                        switch (module)
                        {
                            case Common.Common.MODULE_VIEW:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " [MAIN_GUI]" + " [INF] " + msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_PARSE:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_REPORT:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_T1MANAGER:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_TRACE32:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " [TRACE32]" + " [INF] " + msg.TrimStart() });
                                break;
                            case Common.Common.MODULE_CANLINK:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " [CANLINK]" + " [INF] " + msg.TrimStart() });
                                break;
                        }
                    }
                });
            }

        }
        public void Log(int module, int logtype, string msg)
        {
            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                if (msg != null && !msg.Equals(""))
                {
                    string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                    string LogModuleName = "";

                    switch (module)
                    {
                        case Common.Common.MODULE_VIEW:
                            LogModuleName = "[MAIN_GUI]";
                            break;
                        case Common.Common.MODULE_TRACE32:
                            LogModuleName = "[TRACE32]";
                            break;
                        case Common.Common.MODULE_CANLINK:
                            LogModuleName = "[CANLINK]";
                            break;
                    }

                    switch (logtype)
                    {
                        case Common.Common.LOGTYPE_INF:
                            Console.WriteLine(Logtime + " " + LogModuleName + " [INF] " + msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_PFF:
                            if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 2)
                                Console.WriteLine(Logtime + " " + LogModuleName + " [PFF] " + msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_PGR:
                            if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                Console.WriteLine(Logtime + " " + LogModuleName + " [PRG] " + msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_ERR:
                            Console.WriteLine(Logtime + " " + LogModuleName + " [ERR] " + msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_CPL:
                            if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                Console.WriteLine(Logtime + " " + LogModuleName + " [CPL] " + msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_TINFO:
                            Console.WriteLine(Logtime + " " + LogModuleName + " [TINFO] " + msg.TrimStart());
                            break;
                    }
                }

            }
            else
            {
                Dispatcher dispatcher = System.Windows.Application.Current.Dispatcher;
                DispatcherOperation dispatcherOperation = dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Send, (ThreadStart)
                delegate ()
                {
                    if (msg != null && !msg.Equals(""))
                    {
                        string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                        string LogModuleName = "";

                        switch (module)
                        {
                            case Common.Common.MODULE_VIEW:
                                LogModuleName = "[MAIN_GUI]";
                                break;
                            case Common.Common.MODULE_TRACE32:
                                LogModuleName = "[TRACE32]";
                                break;
                            case Common.Common.MODULE_CANLINK:
                                LogModuleName = "[CANLINK]";
                                break;
                        }


                        switch (logtype)
                        {
                            case Common.Common.LOGTYPE_INF:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() {MainContent=Logtime + " " + LogModuleName + " [INF] " + msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_PFF:
                                if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 2)
                                    ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [PFF] " + msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_PGR:
                                if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                    ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [PRG] " + msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_ERR:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [ERR] " + msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_CPL:
                                if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                    ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [CPL] " + msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_TINFO:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [TINFO] " + msg.TrimStart() });
                                break;
                        }
                    }
                });
            }
        }

        public void Log(int module, int logtype, string _1_msg, string _2_msg, string _3_msg)
        {
            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                if (_1_msg != null && !_1_msg.Equals(""))
                {
                    string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                    string LogModuleName = "";

                    switch (module)
                    {
                        case Common.Common.MODULE_VIEW:
                            LogModuleName = "[MAIN_GUI]";
                            break;
                        case Common.Common.MODULE_TRACE32:
                            LogModuleName = "[TRACE32]";
                            break;
                        case Common.Common.MODULE_CANLINK:
                            LogModuleName = "[CANLINK]";
                            break;
                    }

                    switch (logtype)
                    {
                        case Common.Common.LOGTYPE_INF:
                            Console.WriteLine(Logtime + " " + LogModuleName + " [INF] " + _1_msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_PFF:
                            if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 2)
                                Console.WriteLine(Logtime + " " + LogModuleName + " [PFF] " + _1_msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_PGR:
                            if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                Console.WriteLine(Logtime + " " + LogModuleName + " [PRG] " + _1_msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_ERR:
                            Console.WriteLine(Logtime + " " + LogModuleName + " [ERR] " + _1_msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_CPL:
                            if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                Console.WriteLine(Logtime + " " + LogModuleName + " [CPL] " + _1_msg.TrimStart());
                            break;
                        case Common.Common.LOGTYPE_TINFO:
                            Console.WriteLine(Logtime + " " + LogModuleName + " [TINFO] " + _1_msg.TrimStart());
                            break;
                    }
                }

            }
            else
            {
                Dispatcher dispatcher = System.Windows.Application.Current.Dispatcher;
                DispatcherOperation dispatcherOperation = dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Send, (ThreadStart)
                delegate ()
                {
                    if (_1_msg != null && !_1_msg.Equals(""))
                    {
                        string Logtime = "[" + DateTime.Now.ToString(format: "HH:mm:ss.fff") + "]";
                        string LogModuleName = "";

                        switch (module)
                        {
                            case Common.Common.MODULE_VIEW:
                                LogModuleName = "[MAIN_GUI]";
                                break;
                            case Common.Common.MODULE_TRACE32:
                                LogModuleName = "[TRACE32]";
                                break;
                            case Common.Common.MODULE_CANLINK:
                                LogModuleName = "[CANLINK]";
                                break;
                        }


                        switch (logtype)
                        {
                            case Common.Common.LOGTYPE_INF:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [INF] " + _1_msg.TrimStart(), SecondContent=_2_msg.TrimStart(),ThirdContent=_3_msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_PFF:
                                if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 2)
                                    ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [PFF] " + _1_msg.TrimStart(), SecondContent = _2_msg.TrimStart(), ThirdContent = _3_msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_PGR:
                                if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                    ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [PRG] " + _1_msg.TrimStart(), SecondContent = _2_msg.TrimStart(), ThirdContent = _3_msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_ERR:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [ERR] " + _1_msg.TrimStart(), SecondContent = _2_msg.TrimStart(), ThirdContent = _3_msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_CPL:
                                if (ViewModelLocator.MainVM.MainModel.LogLevelIndex >= 1)
                                    ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [CPL] " + _1_msg.TrimStart(), SecondContent = _2_msg.TrimStart(), ThirdContent = _3_msg.TrimStart() });
                                break;
                            case Common.Common.LOGTYPE_TINFO:
                                ViewModelLocator.MainVM.MainModel.LogData.Add(new LogDataModel() { MainContent = Logtime + " " + LogModuleName + " [TINFO] " + _1_msg.TrimStart(), SecondContent = _2_msg.TrimStart(), ThirdContent = _3_msg.TrimStart() });
                                break;
                        }
                    }
                });
            }
        }
    }
}
