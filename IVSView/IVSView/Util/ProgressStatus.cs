﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Util
{
    class ProgressStatus
    {
        public double Value { get; set; }
        public string Content { get; set; }
        public string SubContent { get; set; }
    }
}
