﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Util
{
    public class LogDataModel
    {
        public string MainContent { get; set; }
        public string SecondContent { get; set; }
        public string ThirdContent { get; set; }
    }
}
