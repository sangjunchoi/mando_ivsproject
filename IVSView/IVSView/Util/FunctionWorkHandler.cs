﻿using HwDriverAPI;
using PROST_Integration.Dialog;
using PROST_Integration.Model;
using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace PROST_Integration.Util
{
    class FunctionWorkHandler
    {
        private object TestRunlockObject = new object();
        private object ParserlockObject = new object();
        private object PerformacelockObject = new object();
        public BackgroundWorker TestRunWorker;
        public BackgroundWorker InterfaceParserWorker;
        public BackgroundWorker PerformanceTestWorker;
        public CANlink_Software ReplayCANlink;
        Thread thread;
        DispatcherTimer timer;
        Timer ThreadTimer;
        string ConvertTestSpecPath;
        string ConvertTestCasePath;
        string ConvertELFPath;
        string ConvertPreHookPath;

        string BackgroundWorkerErrorMessage;
        

        private ManualResetEvent locker = new ManualResetEvent(true);

        private static class LazyHolder
        {
            public static readonly FunctionWorkHandler INSTANCE = new FunctionWorkHandler();
        }

        public static FunctionWorkHandler getInstance()
        {
            return LazyHolder.INSTANCE;
        }
        public FunctionWorkHandler()
        {
			try
			{
				foreach (CANDBPropertyModel model in ViewModelLocator.MainVM.CANPropertyModelList)
				{
					if (model.CAN_DBC_Path.Contains("$WORKSPACE$"))
					{
						string ConvertWorkspacePath = model.CAN_DBC_Path.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
						CANlink.getInstance().DBCtoCSV(ConvertWorkspacePath);
					}
					else
						CANlink.getInstance().DBCtoCSV(model.CAN_DBC_Path);
				}
			}
			catch(Exception ex)
			{
				IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(FunctionWorkHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
			}
        }

        public void TestStart()
        {
            IVSLog.getInstance().Log("Test Start..", Common.Common.MODULE_VIEW, ConsoleColor.Yellow);
            TRACE32TimeoutCheck();

        }


        // 만도 테스트 명세서 경로 변수 : ReadTestcaseFilePath
        //string ReadTestcaseFilePath = @"C:\CIVS\WorkSpace\TestCase\202009030848_Parsed_new_ADAS_DRV_Interface_ForMDS.XLSX";

        // 테스트 케이스 변수 수정 불필요
        //string[] TestcaseInfo = { "TC[MIN]", "TC[MAX]", "TC[MID]" };


        //Python TestcaseCreate = new Python();
        //TestcaseCreate.ReadTestSpecifications();

        // 테스트 케이스 파일을 복수 후 카피 파일에 레포트를 출력
        //ExcelParser ReadTestStatement = new ExcelParser();
        // CopyExcelFile(파일 이름, 현재 파일의 경로, 카피 파일의 경로)

        private void TestRunWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Exit Excel Program
            CommonUtil.ExitExcelProcess();
            ExcelParser.getInstance().WriteExcelStopSheetNameList.Clear();

            BackgroundWorkerErrorMessage = "";
            ConvertTestSpecPath = "";
            ConvertTestCasePath = "";
            ConvertELFPath = "";
            ConvertPreHookPath = "";


            if (ViewModelLocator.MainVM.MainModel.TestSpecificationPath.Contains("$WORKSPACE$"))
            {
                string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.TestSpecificationPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                ConvertTestSpecPath = ConvertWorkspacePath;
            }
            if (ViewModelLocator.MainVM.MainModel.TestCasePath.Contains("$WORKSPACE$"))
            {
                string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.TestCasePath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                ConvertTestCasePath = ConvertWorkspacePath;
            }

            if (ViewModelLocator.MainVM.MainModel.ELFFilePath.Contains("$WORKSPACE$"))
            {
                string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.ELFFilePath.Replace("$WORKSPACE$",Common.Common.WORKSPACE_PATH + "\\");
                ConvertELFPath = ConvertWorkspacePath;
            }

            if (ViewModelLocator.MainVM.MainModel.PreHookScriptPath.Contains("$WORKSPACE$"))
            {
                string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.PreHookScriptPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                ConvertPreHookPath = ConvertWorkspacePath;
            }

            FileInfo FlashDownCMM = new FileInfo("..\\src\\script\\tc39x.cmm");
            if (!FlashDownCMM.Exists)
            {
                IVSLog.getInstance().Log("Flash Download CMM not exists...", Common.Common.MODULE_VIEW);
            }

            // FLASH DOWNLOAD
                if (ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem == "MandoTestspecParser_15")
                {
                TRACE32.getInstance().FlashDownload("..\\src\\script\\tc39x.cmm", ConvertELFPath, ConvertPreHookPath);
                }

                else if (ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem != "MandoTestspecParser_10")
                {
                    if (ConvertELFPath == "")
                        TRACE32.getInstance().FlashDownload("..\\src\\script\\tc37x.cmm", ViewModelLocator.MainVM.MainModel.ELFFilePath,"");
                    else
                        TRACE32.getInstance().FlashDownload("..\\src\\script\\tc37x.cmm", ConvertELFPath,"");
                }

            if (Common.Common.IS_TESTMODE)
            {
                ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Clear();
                ViewModelLocator.MainVM.MainModel.TestCasePath = Common.Common.WORKSPACE_PATH + "\\TestCase\\AAAAA.xlsx";
                //ViewModelLocator.MainVM.MainModel.TestCasePath = @"C:\Users\CSA_DEV\Documents\Working\IVS\WorkSpace\TestCase\202009071548_Parsed_new_ADAS_DRV_Interface_ForMDS.xlsx";
            }


            if (ConvertTestCasePath == "")
                ExcelParser.getInstance().CopyExcelFile(Path.GetFileNameWithoutExtension(ViewModelLocator.MainVM.MainModel.TestCasePath), Common.Common.WORKSPACE_PATH + "\\TestCase", Common.Common.WORKSPACE_PATH + "\\Report");
            else
                ExcelParser.getInstance().CopyExcelFile(Path.GetFileNameWithoutExtension(ConvertTestCasePath), Common.Common.WORKSPACE_PATH + "\\TestCase", Common.Common.WORKSPACE_PATH + "\\Report");

            try
            {
                string[] TestcaseInfo = { "TC[MIN]", "TC[MAX]", "TC[MID]" };
                // DBC 파일을 CSV 파일로 변환

                foreach (CANDBPropertyModel model in ViewModelLocator.MainVM.CANPropertyModelList)
                {
                    if (model.CAN_DBC_Path.Contains("$WORKSPACE$"))
                    {
                        string ConvertWorkspacePath = model.CAN_DBC_Path.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                        CANlink.getInstance().DBCtoCSV(ConvertWorkspacePath);
                    }
                    else
                        CANlink.getInstance().DBCtoCSV(model.CAN_DBC_Path);
                }

                // DBC Data Editing :: Merge Message Data and Signal Data
                IVSLog.getInstance().Log("Finish Database to CSV File", Common.Common.MODULE_VIEW);

                //CSV로 변환 된 DBC 파일중 필요한 내용을 구문분석

                if (TestRunWorker.CancellationPending)
                {
                    BackgroundWorkerErrorMessage = "Test Stop";
                    e.Cancel = true;
                    return;
                }

/*                // Read testcase file (All data)
                if (ConvertTestCasePath == "")
                    ExcelParser.getInstance().ReadExcelData(ViewModelLocator.MainVM.MainModel.TestCasePath);
                else
                    ExcelParser.getInstance().ReadExcelData(ConvertTestCasePath);*/


                if (ExcelParser.getInstance().ReturnDataList == null)
                {
                    IVSLog.getInstance().Log("Problem reading Excel file.", Common.Common.MODULE_VIEW);
                    return;
                }
                else
                {
                    IVSLog.getInstance().Log("Excel file read complete.", Common.Common.MODULE_VIEW);
                    if (ExcelParser.getInstance().ErrorIdentifier)
                    {
                        IVSLog.getInstance().Log("Ending the program due to an error.", Common.Common.MODULE_VIEW);
                    }
                }
                if (TestRunWorker.CancellationPending)
                {
                    BackgroundWorkerErrorMessage = "Test Stop";
                    e.Cancel = true;
                    return;
                }
                // Change from Test Case to Dictionary<string, List<object>>               
                ExcelParser.getInstance().TestcaseExtraction();

                if (ExcelParser.getInstance().TestcaseDictonary == null)
                {
                    IVSLog.getInstance().Log("Problem during parsing process", Common.Common.MODULE_VIEW);
                }
                else
                {
                    IVSLog.getInstance().Log("Testcase file parsing complete.", Common.Common.MODULE_VIEW);
                    if (ExcelParser.getInstance().ErrorIdentifier)
                    {
                        IVSLog.getInstance().Log("Ending the program due to an error.", Common.Common.MODULE_VIEW);
                    }
                }


                // SHOW TEST DIALOG
                TestRunWorker.ReportProgress(Common.Common.TESTDIALOG_SHOW, new ProgressStatus { Value = 0, Content = "Wait..." });

                Merge MergedTestcase = new Merge();

                for (int SheetName = 0; SheetName < ExcelParser.getInstance().TestcaseDictonary.Count; SheetName++)
                {
                    if (!ViewModelLocator.MainVM.SelectTestcaseSheetNameList[SheetName])
                        continue;

                    ExcelParser.getInstance().WriteExcelStopSheetNameList.Add(ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetName],true);
                    double Progress = 0;
                    TestRunWorker.ReportProgress(0, new ProgressStatus { Value = 0, Content = "Mapping CANDB & TestCase" });

                    MergedTestcase.TestcaseToCANDatabase(ExcelParser.getInstance().TestcaseDictonary[ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetName]]);

                    // 테스트 시퀀스 시작
                    for (int Index = 0; Index < MergedTestcase.FullMergedTestcade.Count; Index++)
                    {

                        ReTest:
                        Progress = (double)(100.0 / MergedTestcase.FullMergedTestcade.Count) * Index;
                        TestRunWorker.ReportProgress(0, new ProgressStatus { Value = Progress, Content = "Running Test [" + ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetName] + "] Sheet...",SubContent= "[" + (Index + 1).ToString() + " / " + MergedTestcase.FullMergedTestcade.Count.ToString() + "]"});


                        locker.WaitOne();

                        if (TestRunWorker.CancellationPending)
                        {
                            BackgroundWorkerErrorMessage = "Test Stop";
                            e.Cancel = true;
                            return;
                        }



                        Dictionary<string, string> TestDictionary = (Dictionary<string, string>)MergedTestcase.FullMergedTestcade[Index];
                        IVSLog.getInstance().Log(Common.Common.MODULE_VIEW,Common.Common.LOGTYPE_PFF,"Testcase : " + (Index + 1).ToString() + " / " + MergedTestcase.FullMergedTestcade.Count.ToString());

                        Dictionary<string, string> ResaultDictionary = new Dictionary<string, string>();

                        if (TestDictionary["Input"].Equals("Input")) continue;

                        if (TestDictionary["Input"].Equals(null) || TestDictionary["Input"].ToString().Equals("null"))
                        {
                            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF,"Input value is null.");
                            for (int i = 0; i < 3; i++)
                            {
                                ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "No Input Data";
                                ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "No Input Data";
                            }
                            MergedTestcase.ResaultList.Add(ResaultDictionary);
                            continue;
                        }
                        if (TestDictionary["Output1"].Equals(null) || TestDictionary["Output1"].Equals("-") || TestDictionary["Output1"].Equals("x") || TestDictionary["Output1"].Equals("null"))
                        {
                            IVSLog.getInstance().Log("Output value is null.", Common.Common.MODULE_VIEW);
                            for (int i = 0; i < 3; i++)
                            {
                                ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Output value is null.";
                                ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "Output value is null.";
                            }
                            MergedTestcase.ResaultList.Add(ResaultDictionary);
                            continue;
                        }


                        if (TestDictionary["Input"].Contains("::"))
                        {
                            if (!TRACE32.getInstance().IsExistSymbolCheck(TestDictionary["Output1"]))
                            {
                                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, "Input value is symbol not found.");
                                for (int i = 0; i < 3; i++)
                                {
                                    ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Symbol Not Found";
                                    ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "Symbol Not Found";
                                }
                                MergedTestcase.ResaultList.Add(ResaultDictionary);
                                continue;
                            }

                            // Test point A to B
                            if (TestDictionary["TC[MIN]_Output1"] == "No signal found.")
                            {
                                for (int i = 0; i < 3; i++)
                                {
                                    IVSLog.getInstance().Log("No signal found.", Common.Common.MODULE_VIEW);
                                    ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "No signal found.";
                                    ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "No signal found.";
                                }
                                MergedTestcase.ResaultList.Add(ResaultDictionary);
                                continue;
                            }

                            for (int i = 0; i < 3; i++) // min, max, mid test
                            {
                                if (!TestDictionary[TestcaseInfo[i] + "_Input"].Equals(null) || !TestDictionary[TestcaseInfo[i] + "_Input"].Equals("null"))
                                {
                                    // TRACE32 Input variable setting
                                    string[] InputData = TestDictionary[TestcaseInfo[i] + "_Input"].Split('(');


                                    IVSLog.getInstance().Log(TestcaseInfo[i] + "_Input", Common.Common.MODULE_VIEW);

                                    //IVSLog.getInstance().Log("CANlink :: SetCANMessage(" + TestDictionary["Message name"] + " , " + TestDictionary["Message ID"] + "," + TestDictionary["DLC [Byte]"] + "," + TestDictionary["Signal name"] + "," + TestDictionary["Startbit"] + "," + TestDictionary["ID_Format"] + ")", Common.Common.MODULE_VIEW);
                                    IVSLog.getInstance().Log("Send CAN Message [ " + TestDictionary["Message name"] + " :: " + TestDictionary["Signal name"] + " - Send Data : " + InputData[0] + " ]", Common.Common.MODULE_VIEW);
                                    bool Overflow = CANlink.getInstance().SendCANMsg(Convert.ToInt32(TestDictionary["Message ID"], 16), Convert.ToInt32(TestDictionary["Startbit"]), Convert.ToInt32(TestDictionary["DLC [Byte]"]), Convert.ToInt32(InputData[0].Trim(), 16), TestDictionary["ID_Format"], Convert.ToInt32(TestDictionary["SignalLength"]), Convert.ToInt32(TestDictionary["Database"]), TestDictionary["Byte order"]);

                                    Thread.Sleep(ViewModelLocator.MainVM.MainModel.TestStepDelay);

                                    // CAN Message read
                                    //IVSLog.getInstance().Log("Test Point A");
                                    if (Overflow == true)
                                    {
                                        ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Input Data Overflow...";
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "Input Data Overflow...";
                                        continue;
                                    }

                                    string Output1 = TRACE32.getInstance().ReadVariable(TestDictionary["Output1"], InputData[0].Trim());
                                    IVSLog.getInstance().Log("Read Variable_Point A [ " + TestDictionary["Output1"] + " : " + Output1 + " ]", Common.Common.MODULE_VIEW);
                                    ResaultDictionary[TestcaseInfo[i] + "_Output1"] = Output1;

                                    Thread.Sleep(ViewModelLocator.MainVM.MainModel.TestStepDelay);

                                    if (!TestDictionary["Output2"].Equals("null"))
                                    {
                                        //IVSLog.getInstance().Log("Test Point B");
                                        string Output2 = TRACE32.getInstance().ReadVariable(TestDictionary["Output2"], InputData[0].Trim());
                                        IVSLog.getInstance().Log("Read Variable_Point B [ " + TestDictionary["Output2"] + " : " + Output2 + " ]", Common.Common.MODULE_VIEW);
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = Output2;
                                    }
                                    else if (TestDictionary["Output2"].Equals("null"))
                                    {
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = " ";
                                    }
                                    else
                                    {
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = " ";
                                    }
                                }
                                else
                                {
                                    IVSLog.getInstance().Log("Input value is null.", Common.Common.MODULE_VIEW);
                                    continue;
                                }
                            }
                            MergedTestcase.ResaultList.Add(ResaultDictionary);
                        }
                        else
                        {
                            // Test point C, D
                            if (TestDictionary["Output1"].Contains("::"))
                            { // Test point D
                                if (!TRACE32.getInstance().IsExistSymbolCheck(TestDictionary["Input"]))
                                {
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, "Input value is symbol not found.");
                                    for (int i = 0; i < 3; i++)
                                    {
                                        ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Symbol Not Found";
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "Symbol Not Found";
                                    }
                                    MergedTestcase.ResaultList.Add(ResaultDictionary);
                                    continue;
                                }


                                if (TestDictionary["TC[MIN]_Output1"] == "No signal found.")
                                {
                                    for (int i = 0; i < 3; i++)
                                    {
                                        IVSLog.getInstance().Log("No signal found.", Common.Common.MODULE_VIEW);
                                        ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "No signal found.";
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "No signal found.";
                                    }
                                    MergedTestcase.ResaultList.Add(ResaultDictionary);
                                    continue;
                                }

                                string[] SplitMessageName = TestDictionary["Message name"].Split('_');
                                string MessageCycle = SplitMessageName[SplitMessageName.Length - 1].Replace("ms", "").Trim();
                                UInt32 MessageCycleTime = Convert.ToUInt32(MessageCycle);
                                UInt32 MessageTick = MessageCycleTime / 10;

                                for (int i = 0; i < 3; i++) // min, max, mid test
                                {
                                    if (!TestDictionary[TestcaseInfo[i] + "_Input"].Equals(null) || !TestDictionary[TestcaseInfo[i] + "_Input"].Equals("null"))
                                    {
                                        string[] InputData = TestDictionary[TestcaseInfo[i] + "_Input"].Split('(');


                                        bool BreakCheck = TRACE32.getInstance().SetBrake(TestDictionary["Input"]);
                                        if (!BreakCheck)
                                        {
                                            ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Break point entry failed.";
                                            ResaultDictionary[TestcaseInfo[i] + "_Output2"] = " ";
                                            continue;
                                        }
                                        Thread.Sleep(10);

                                        IVSLog.getInstance().Log("Set Variable_Point D  [ " + TestDictionary["Input"] + " : " + InputData[0] + " ]", Common.Common.MODULE_VIEW);
                                        CANlink.getInstance().rxVariableReadInit();

                                        for (int j = 0; j < MessageTick * 2; j++)
                                        {
                                            TRACE32.getInstance().SetVariable(TestDictionary["Input"], InputData[0].Trim());
                                            //goto ReTest;

                                            CANlink.getInstance().rxThreadVariableSet(Convert.ToUInt16(TestDictionary["Message ID"], 16), Convert.ToUInt16(TestDictionary["Startbit"]), Convert.ToUInt16(TestDictionary["SignalLength"]), TestDictionary["Byte order"], true);
                                            CANlink.getInstance().ReceiveBufferClear();

                                            TRACE32.getInstance().TargetBoardRun();
                                            Thread.Sleep(30);
                                            CANlink.getInstance().ReceiveThread();

                                            if (InputData[0].Trim() == CANlink.getInstance().HexResultData)
                                                break;
                                        }

                                        IVSLog.getInstance().Log("Read CAN Message_Point D  [ " + TestDictionary["Output1"] + " : " + CANlink.getInstance().HexResultData + " ]", Common.Common.MODULE_VIEW);

                                        ResaultDictionary[TestcaseInfo[i] + "_Output1"] = CANlink.getInstance().HexResultData;
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = " ";

                                        TRACE32.getInstance().DeleteBrake(TestDictionary["Input"]);
                                        CANlink.getInstance().rxThreadVariableInit();
                                    }
                                }
                                MergedTestcase.ResaultList.Add(ResaultDictionary);
                            }
                            else
                            { // Test point C
                                if (!TRACE32.getInstance().IsExistSymbolCheck(TestDictionary["Output1"]))
                                {
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PFF, "Input value is symbol not found.");
                                    for (int i = 0; i < 3; i++)
                                    {
                                        ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Symbol Not Found";
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = "Symbol Not Found";
                                    }
                                    MergedTestcase.ResaultList.Add(ResaultDictionary);
                                    continue;
                                }

                                //IVSLog.getInstance().Log("Test Point C");
                                for (int i = 0; i < 3; i++) // min, max, mid test
                                {
                                    if (!TestDictionary[TestcaseInfo[i] + "_Input"].Equals(null) || !TestDictionary[TestcaseInfo[i] + "_Input"].Equals("null"))
                                    {

                                        bool BreakCheck = TRACE32.getInstance().SetBrake(TestDictionary["Input"], TestDictionary["Output1"], ViewModelLocator.MainVM.MainModel.TestStepDelay);
                                        if (!BreakCheck)
                                        {
                                            TRACE32.getInstance().DeleteBrake(TestDictionary["Input"], TestDictionary["Output1"]);
                                            ResaultDictionary[TestcaseInfo[i] + "_Output1"] = "Break point entry failed.";
                                            ResaultDictionary[TestcaseInfo[i] + "_Output2"] = " ";
                                            TRACE32.getInstance().TargetBoardRun();
                                            continue;
                                        }

                                        string[] InputData = TestDictionary[TestcaseInfo[i] + "_Input"].Split('(');
                                        TRACE32.getInstance().SetVariable(TestDictionary["Input"], InputData[0].Trim());
                                        IVSLog.getInstance().Log("Set Variable_Point C [ " + TestDictionary["Input"] + " : " + InputData[0] + " ]", Common.Common.MODULE_VIEW);
                                        TRACE32.getInstance().TargetBoardRun();

                                        Thread.Sleep(ViewModelLocator.MainVM.MainModel.TestStepDelay);

                                        string Output1 = TRACE32.getInstance().ReadVariable(TestDictionary["Output1"], InputData[0].Trim());

                                        int RepeatCnt = 0;
                                        while (string.Compare(InputData[0].Trim(), Output1.Trim(), true) != 0)
                                        {
                                            if (RepeatCnt > 5)
                                                break;

                                            RepeatCnt++;
                                            TRACE32.getInstance().TargetBoardRun();
                                            Thread.Sleep(20);
                                            TRACE32.getInstance().SetVariable(TestDictionary["Input"], InputData[0].Trim());
                                            TRACE32.getInstance().TargetBoardRun();

                                            Thread.Sleep(ViewModelLocator.MainVM.MainModel.TestStepDelay);

                                            Output1 = TRACE32.getInstance().ReadVariable(TestDictionary["Output1"], InputData[0].Trim());
                                        }

                                        IVSLog.getInstance().Log("Read Variable_Point C [ " + TestDictionary["Output1"] + " : " + Output1 + " ]", Common.Common.MODULE_VIEW);
                                        TRACE32.getInstance().DeleteBrake(TestDictionary["Input"], TestDictionary["Output1"]);
                                        TRACE32.getInstance().TargetBoardRun();

                                        ResaultDictionary[TestcaseInfo[i] + "_Output1"] = Output1;
                                        ResaultDictionary[TestcaseInfo[i] + "_Output2"] = " ";

                                    }
                                }
                                MergedTestcase.ResaultList.Add(ResaultDictionary);
                            }
                        }

                        if (!TRACE32.getInstance().CheckSystemDown())
                        {
                            PauseTest();
                            System.Windows.Application.Current.Dispatcher.Invoke(new System.Action(() =>
                            {
                                ViewModelLocator.MainVM.ShowContinueTestDialogTest();
                            }));
                        }

                        locker.WaitOne();

                        if (ViewModelLocator.MainVM.MainModel.IsReTestFlag)
                        {
                            Index = 0;
                            ViewModelLocator.MainVM.MainModel.IsReTestFlag = false;
                            goto ReTest;
                        }


                        ExcelParser.getInstance().ResaultDictionary[ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetName]] = MergedTestcase.ResaultList;
                    }

                    ExcelParser.getInstance().ResaultDictionary[ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetName]] = MergedTestcase.ResaultList;
                    TestRunWorker.ReportProgress(0, new ProgressStatus { Value = 100, Content = "Running Test [" + ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetName] + "] Sheet..." });
                }

                if (TestRunWorker.CancellationPending)
                {
                    BackgroundWorkerErrorMessage = "Test Stop";
                    e.Cancel = true;
                    return;
                }

                TestRunWorker.ReportProgress(0, new ProgressStatus { Value = 100, Content = "Save Report File..." });

                ExcelParser.getInstance().WriteExcelData(ViewModelLocator.MainVM.MainModel.ReportFilePath);

                TestRunWorker.ReportProgress(0, new ProgressStatus { Value = 100, Content = "Please wait a moment..." });
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(MainViewModel).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }
        private void TestWorkerStart()
        {
            ViewModelLocator.MainVM.MainModel.TransitionIndex = 1;

            lock (TestRunlockObject)
            {
                TestRunWorker = new BackgroundWorker();
                TestRunWorker.DoWork += TestRunWorker_DoWork;
                TestRunWorker.WorkerSupportsCancellation = true;
                TestRunWorker.RunWorkerCompleted += TestRunWorker_DoWorkComplete;
                TestRunWorker.WorkerReportsProgress = true;
                TestRunWorker.ProgressChanged += new ProgressChangedEventHandler(TestRunWorker_ProgressChanged);

                TestRunWorker.RunWorkerAsync();
            }
        }
        int checkcount;
        public void TRACE32TimeoutCheck()
        {
            checkcount = 0;

            if (thread != null)
                thread.Abort();

            thread = new Thread(new ThreadStart(
            delegate ()
            {
                TRACE32.getInstance().TRACE32_CheckConnect();
            }));
            thread.IsBackground = true;
            thread.Start();

            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                ThreadTimer = new Timer(ThreadTimerTick, 0, 0, 1000);
            }
            else
            {
                if (timer != null)
                    timer.Stop();

                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += (s, a) =>
                {
                    checkcount++;

                    if (checkcount > 30)
                    {
                        ViewModelLocator.MainVM.showTrackBarMessage("Please Check The TRACE32 Connection..");

                        timer.Stop();
                    }
                    else
                    {
                        IVSLog.getInstance().Log("TRACE32 is checking the connection status... " + checkcount + "sec", Common.Common.MODULE_VIEW);
                        if (ViewModelLocator.MainVM.MainModel.IsTrace32Connection)
                        {
                            timer.Stop();
                            CANLINKCheck();
                        }
                    }
                };
                timer.Start();
            }

        }
        public void TRACE32PerformanceTimeoutCheck()
        {
            try
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "TRACE32PerformanceTimeoutCheck Start");
                checkcount = 0;

                if (thread != null)
                    thread.Abort();

                thread = new Thread(new ThreadStart(
                delegate ()
                {
                    TRACE32.getInstance().TRACE32_CheckConnect();
                }));
                thread.IsBackground = true;
                thread.Start();

                if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
                {
                    ThreadTimer = new Timer(ThreadTimerTick, 0, 0, 1000);
                }
                else
                {
                    if (timer != null)
                        timer.Stop();

                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromSeconds(1);
                    timer.Tick += (s, a) =>
                    {
                        checkcount++;

                        if (checkcount > 30)
                        {
                            ViewModelLocator.MainVM.showTrackBarMessage("Please Check The TRACE32 Connection..");

                            timer.Stop();
                        }
                        else
                        {
                            IVSLog.getInstance().Log("TRACE32 is checking the connection status... " + checkcount + "sec", Common.Common.MODULE_VIEW);
                            if (ViewModelLocator.MainVM.MainModel.IsTrace32Connection)
                            {
                                timer.Stop();
                                FunctionWorkHandler.getInstance().PerformanceMeasurementTest();
                            }
                        }
                    };
                    timer.Start();
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, ex.Message + " " + CommonUtil.GetExceptionLineNumber(ex));
            }

        }
        private void ThreadTimerTick(object state)
        {
            checkcount++;
            if (checkcount > 30)
            {
                ViewModelLocator.MainVM.showTrackBarMessage("Please Check The TRACE32 Connection..");
                ThreadTimer.Dispose();
            }
            else
            {
                IVSLog.getInstance().Log("TRACE32 is checking the connection status... " + checkcount + "sec", Common.Common.MODULE_VIEW);
                if (ViewModelLocator.MainVM.MainModel.IsTrace32Connection)
                {
                    ThreadTimer.Dispose();
                    CANLINKCheck();
                }
            }
        }
        private void CANLINKCheck()
        {
            // CANlink DLL Open
            CANlink.getInstance().LibraryOpen();
            // CANlink CAN Setting
            bool checkresult = CANlink.getInstance().CANInterfaceConnect();
            Thread.Sleep(100);
            if (checkresult)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_CANLINK, Common.Common.LOGTYPE_INF, "CANlink Connection.");
                TestWorkerStart();
            }
            else
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_CANLINK, Common.Common.LOGTYPE_ERR, "CANlink Not Connection.");
                return;
            }
        }
        private void TestRunWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            if (e.ProgressPercentage == Common.Common.TESTDIALOG_SHOW && ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                ViewModelLocator.MainVM.ShowPopupTestStatus();
            }
            else
            {
                ProgressStatus state = e.UserState as ProgressStatus;
                if (state == null)
                    return;

                ViewModelLocator.MainVM.MainModel.TestProgress = state.Value;
                ViewModelLocator.MainVM.MainModel.TestStatusText = state.Content;
                ViewModelLocator.MainVM.MainModel.TestStatusCountText = state.SubContent;

                if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
                {
                    if (prev_progress != state.Value)
                    {
                        IVSLog.getInstance().Log(Common.Common.MODULE_VIEW,Common.Common.LOGTYPE_TINFO,state.SubContent + "[ " + string.Format("{0:F2}", ViewModelLocator.MainVM.MainModel.TestProgress) + "% ]");
                    }

                    prev_progress = state.Value;
                }
            }
        }
        private void TestRunWorker_DoWorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            
            if (ViewModelLocator.MainVM.MainModel.GUIStartup)
                ViewModelLocator.MainVM.ShowWarningDialogTest("TEST FINISH!!");
            else
                IVSLog.getInstance().Log("######### TEST FINISH!! #########");

            if (!e.Cancelled)
                ViewModelLocator.ToolBarVM.OpenReport();

            CANlink.getInstance().CANDisConnect();
        }
        public void ParseStart()
        {
            ViewModelLocator.MainVM.MainModel.ParserWorkState = Common.Common.WORKSTATE_NORMAL;

            if (ViewModelLocator.MainVM.MainModel.TestSpecificationPath.Contains("$WORKSPACE$"))
            {
                string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.TestSpecificationPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");

                if (ConvertWorkspacePath == "" || !File.Exists(ConvertWorkspacePath))
                {
                    ViewModelLocator.MainVM.ShowWarningDialog("TestSpec File NotFound");
                    return;
                }
            }
            else
            {
                if (ViewModelLocator.MainVM.MainModel.TestSpecificationPath == "" || !File.Exists(ViewModelLocator.MainVM.MainModel.TestSpecificationPath))
                {
                    ViewModelLocator.MainVM.ShowWarningDialog("TestSpec File NotFound");
                    return;
                }
            }

            if (ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                ViewModelLocator.MainVM.ShowPopupParsingStatus();
            }

            lock (ParserlockObject)
            {
                InterfaceParserWorker = new BackgroundWorker();

                InterfaceParserWorker.DoWork += InterfaceParserWorker_DoWork;
                InterfaceParserWorker.RunWorkerCompleted += InterfaceParserWorker_DoWorkComplete;
                InterfaceParserWorker.WorkerSupportsCancellation = true;
                InterfaceParserWorker.WorkerReportsProgress = true;
                InterfaceParserWorker.ProgressChanged += new ProgressChangedEventHandler(InterfaceParserWorker_ProgressChanged);
                InterfaceParserWorker.RunWorkerAsync();
            }
        }
        private void InterfaceParserWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ViewModelLocator.MainVM.MainModel.ParseTestSpecProgress = 0;
            ViewModelLocator.MainVM.MainModel.ParseTestSpecSheetName = "Parsing Ready...";
            PythonHandler.getInstance().MakeDictionaryData();
            PythonHandler.getInstance().ParserThread();

        }
        double prev_progress;
        private void InterfaceParserWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressStatus state = e.UserState as ProgressStatus;
            if (state == null)
                return;

            ViewModelLocator.MainVM.MainModel.ParseTestSpecProgress = state.Value;
            ViewModelLocator.MainVM.MainModel.ParseTestSpecSheetName = "Parsing " + state.Content + " Sheet...";

            if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
            {
                if (prev_progress != state.Value)
                {
                    IVSLog.getInstance().Log(ViewModelLocator.MainVM.MainModel.ParseTestSpecSheetName + "[ " + ViewModelLocator.MainVM.MainModel.ParseTestSpecProgress + "% ]", Common.Common.MODULE_PARSE, ConsoleColor.Green);
                }

                prev_progress = state.Value;
            }

            // ADD Sheet Name
            if (ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Any(p => p.ToString() == state.Content) == false)
                ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Add(state.Content);
        }
        private void InterfaceParserWorker_DoWorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        public void PauseTest()
        {
            locker.Reset();
        }
        public void RestartTest()
        {
            locker.Set();
        }
        public void PerformanceMeasurementTest()
        {
            try
            {
                ViewModelLocator.MainVM.MainModel.IsTestStatusDialogOpen = false;
                foreach (var process in Process.GetProcessesByName("HPManager"))
                    process.Kill();
                
                ViewModelLocator.MainVM.MainModel.IsT1ManagerReady = false;
                ViewModelLocator.MainVM.ShowPopupPerformanceTestStatus();

                lock (PerformacelockObject)
                {
                    if (PerformanceTestWorker != null)
                        PerformanceTestWorker = null;

                    PerformanceTestWorker = new BackgroundWorker();

                    PerformanceTestWorker.DoWork += PerformanceTestWorker_DoWork;
                    PerformanceTestWorker.RunWorkerCompleted += PerformanceTestWorker_DoWorkComplete;
                    PerformanceTestWorker.WorkerSupportsCancellation = true;
                    PerformanceTestWorker.WorkerReportsProgress = true;
                    PerformanceTestWorker.ProgressChanged += new ProgressChangedEventHandler(PerformanceTestWorker_ProgressChanged);
                    PerformanceTestWorker.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(FunctionWorkHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }
        int stop_scenario_index;
        private void PerformanceTestWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                stop_scenario_index = -1;
                int TimeOutCount = 0;
                ViewModelLocator.MainVM.MainModel.IsT1ManagerConstraintComplete = false;
                ViewModelLocator.MainVM.MainModel.IsT1ManagerReady = false;
                ViewModelLocator.MainVM.MainModel.IsT1ManagerCanLogAck = false;

                PerformanceTestWorker.ReportProgress(0, new ProgressStatus() { Value = 10, Content = "Flash Download..." });
                string ConvertELFPath = "";
                if (ViewModelLocator.MainVM.MainModel.ELFFilePath.Contains("$WORKSPACE$"))
                {
                    string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.ELFFilePath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                    ConvertELFPath = ConvertWorkspacePath;
                }
                string ConvertPreHookPath = "";
                if (ViewModelLocator.MainVM.MainModel.PreHookScriptPath.Contains("$WORKSPACE$"))
                {
                    string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.PreHookScriptPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                    ConvertPreHookPath = ConvertWorkspacePath;
                }

                FileInfo FlashDownCMM = new FileInfo("..\\src\\script\\tc39x.cmm");
                if (!FlashDownCMM.Exists)
                {
                    IVSLog.getInstance().Log("Flash Download CMM not exists...", Common.Common.MODULE_VIEW);
                }

                // FLASH DOWNLOAD
                if (ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem == "MandoTestspecParser_15")
                {
                    TRACE32.getInstance().FlashDownload("..\\src\\script\\tc39x.cmm", ConvertELFPath, ConvertPreHookPath);
                }

                else if (ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem != "MandoTestspecParser_10")
                {
                    if (ConvertELFPath == "")
                        TRACE32.getInstance().FlashDownload("..\\src\\script\\tc37x.cmm", ViewModelLocator.MainVM.MainModel.ELFFilePath, "");
                    else
                        TRACE32.getInstance().FlashDownload("..\\src\\script\\tc37x.cmm", ConvertELFPath, "");
                }
                PerformanceTestWorker.ReportProgress(0, new ProgressStatus() { Value = 20, Content = "Flash Download Complete.." });

                if (PerformanceTestWorker.CancellationPending)
                {
                    BackgroundWorkerErrorMessage = "Test Stop";
                    e.Cancel = true;
                    return;
                }

                ReplayCANlink = new CANlink_Software();

                // Check CANlink HW Connection

                try
                {
                    if (PerformanceTestWorker.CancellationPending)
                    {
                        BackgroundWorkerErrorMessage = "Test Stop";
                        e.Cancel = true;
                        return;
                    }

                    ReplayCANlink.CANlinkConfigFileOpen("RecordTest.hcf");
                    Thread.Sleep(5000);
                }
                catch (Exception)
                {
                    BackgroundWorkerErrorMessage = "CANlinkConfigFileOpen Error";
                    e.Cancel = true;
                    return;
                }

                try
                {
                    // CANlink Software Run
                    if (PerformanceTestWorker.CancellationPending)
                    {
                        BackgroundWorkerErrorMessage = "Test Stop";
                        e.Cancel = true;
                        return;
                    }

                    Console.WriteLine(ReplayCANlink.CANlinkStart());
                    PerformanceTestWorker.ReportProgress(0, new ProgressStatus() { Value = 33, Content = "CANLink Start.." });
                }
                catch (Exception)
                {
                    BackgroundWorkerErrorMessage = "CANlinkStart Error";
                    e.Cancel = true;
                    return;
                }

                string checkCANlink = ReplayCANlink.HWConnectionCheck();
                if (checkCANlink == null || checkCANlink == "")
                {
                    BackgroundWorkerErrorMessage = "CANlinkStart Error";
                    e.Cancel = true;
                    return;
                }

                Thread.Sleep(1500);

                ///////////////// T1 Manager Start & Check //////////////////////

                PerformanceTestWorker.ReportProgress(0, new ProgressStatus() { Value = 38, Content = "T1 Manager Setting.." });
                PythonHandler.getInstance().MakeDictionaryData();
                Thread thread = new Thread(new ThreadStart(delegate ()
                {
                    PythonHandler.getInstance().T1_APIThread();
                }));
                thread.Start();

                while (!ViewModelLocator.MainVM.MainModel.IsT1ManagerReady)
                {
                    if (PerformanceTestWorker.CancellationPending)
                    {
                        BackgroundWorkerErrorMessage = "Test Stop";
                        e.Cancel = true;
                        return;
                    }

                    Thread.Sleep(100);
                    if (TimeOutCount > 600)
                    {
                        BackgroundWorkerErrorMessage = "T1Manager Python Not Response..";
                        e.Cancel = true;
                        return;
                    }
                    TimeOutCount++;
                }

                //////////////////////////////////////////////////////////////

                double TestProgress = 0;
                PerformanceTestWorker.ReportProgress(0, new ProgressStatus() { Value = 40, Content = "Performance Measurment...." });
                for(int scenarioIndex=0; scenarioIndex < ViewModelLocator.MainVM.T1ScenarioModelList.Count; scenarioIndex++)
                {
                    T1ScenarioModel t1ScenarioModel;
                    XmlSerializer xs = new XmlSerializer(typeof(T1ScenarioModel));
                    if (!File.Exists(ViewModelLocator.MainVM.T1ScenarioModelList[scenarioIndex].ScenarioXmlPath))
                        continue;

                    using (Stream fstream = File.OpenRead(ViewModelLocator.MainVM.T1ScenarioModelList[scenarioIndex].ScenarioXmlPath))
                        t1ScenarioModel = xs.Deserialize(fstream) as T1ScenarioModel;

                    List<string> LogFiles = Directory.GetFiles(t1ScenarioModel.CanLogPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\"), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".asc") || s.EndsWith(".blf")).ToList();
                    LogFiles.Sort();

                    ////////////// Create ScenarioSet File    /////////////////////////////
                    File.Create(Common.Common.SRC_PATH + "\\t1manager\\SendScenarioSet_" + (scenarioIndex + 1)).Close();
                    if (!File.Exists(Common.Common.SRC_PATH + "\\t1manager\\SendScenarioSet_" + (scenarioIndex + 1)))
                        IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, "File Not Create [SendScenarioSet_" + (scenarioIndex + 1) + "]");

                    TimeOutCount = 0;
                    while (!ViewModelLocator.MainVM.MainModel.IsT1ManagerConstraintComplete)
                    {
                        if (PerformanceTestWorker.CancellationPending)
                        {
                            BackgroundWorkerErrorMessage = "Test Stop";
                            e.Cancel = true;
                            return;
                        }

                        Thread.Sleep(100);
                        if (TimeOutCount > 600)
                        {
                            BackgroundWorkerErrorMessage = "T1Manager Python Not Response..";
                            e.Cancel = true;
                            return;
                        }
                        TimeOutCount++;
                    }
                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "Receive from T1 Message [T1ManagerConstraintSetComplete]");
                    ViewModelLocator.MainVM.MainModel.IsT1ManagerConstraintComplete = false;

                    if (t1ScenarioModel.IsRepeatAll)
                    {
                        if (t1ScenarioModel.RepeatCount == 0)
                        {
                            while (true)
                            {
                                foreach (string file in LogFiles)
                                {
                                    ReplayCANlink.CANlinkRecordFileSet(file);
                                    Thread.Sleep(100);
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "Start CANlink Record Play");
                                    ReplayCANlink.CANlinkRecordFilePlay();

                                    // Replay Check
                                    bool state = false;

                                    state = ReplayCANlink.CANlinkReplayCheck();
                                    while (!state)
                                    {
                                        if (PerformanceTestWorker.CancellationPending)
                                        {
                                            BackgroundWorkerErrorMessage = "Test Stop";
                                            stop_scenario_index = scenarioIndex + 1;
                                            e.Cancel = true;
                                            return;
                                        }

                                        Thread.Sleep(200);
                                        state = ReplayCANlink.CANlinkReplayCheck();
                                    }
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "End CANlink Record Play");
                                    ReplayCANlink.CANlinkReset();

                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < t1ScenarioModel.RepeatCount; i++)
                            {
                                foreach (string file in LogFiles)
                                {
                                    ReplayCANlink.CANlinkRecordFileSet(file);
                                    Thread.Sleep(100);
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "Start CANlink Record Play");
                                    ReplayCANlink.CANlinkRecordFilePlay();

                                    // Replay Check
                                    bool state = false;

                                    state = ReplayCANlink.CANlinkReplayCheck();
                                    while (!state)
                                    {
                                        if (PerformanceTestWorker.CancellationPending)
                                        {
                                            BackgroundWorkerErrorMessage = "Test Stop";
                                            stop_scenario_index = scenarioIndex + 1;
                                            e.Cancel = true;
                                            return;
                                        }

                                        Thread.Sleep(200);
                                        state = ReplayCANlink.CANlinkReplayCheck();
                                    }
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "End CANlink Record Play");
                                    ReplayCANlink.CANlinkReset();

                                }
                            }
                        }
                    }
                    else if (t1ScenarioModel.IsRepeatOnebyOne)
                    {
                        foreach (string file in LogFiles)
                        {
                            if (t1ScenarioModel.RepeatCount == 0)
                            {
                                while (true)
                                {
                                    ReplayCANlink.CANlinkRecordFileSet(file);
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "Start CANlink Record Play");
                                    Thread.Sleep(100);
                                    ReplayCANlink.CANlinkRecordFilePlay();

                                    // Replay Check
                                    bool state = false;

                                    state = ReplayCANlink.CANlinkReplayCheck();
                                    while (!state)
                                    {
                                        if (PerformanceTestWorker.CancellationPending)
                                        {
                                            BackgroundWorkerErrorMessage = "Test Stop";
                                            stop_scenario_index = scenarioIndex + 1;
                                            e.Cancel = true;
                                            return;
                                        }

                                        Thread.Sleep(200);
                                        state = ReplayCANlink.CANlinkReplayCheck();
                                    }
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "End CANlink Record Play");
                                    ReplayCANlink.CANlinkReset();
                                }
                            }
                            else
                            {
                                for (int i = 0; i < t1ScenarioModel.RepeatCount; i++)
                                {
                                    ReplayCANlink.CANlinkRecordFileSet(file);
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "Start CANlink Record Play");
                                    Thread.Sleep(100);
                                    ReplayCANlink.CANlinkRecordFilePlay();

                                    // Replay Check
                                    bool state = false;

                                    state = ReplayCANlink.CANlinkReplayCheck();
                                    while (!state)
                                    {
                                        if (PerformanceTestWorker.CancellationPending)
                                        {
                                            BackgroundWorkerErrorMessage = "Test Stop";
                                            stop_scenario_index = scenarioIndex + 1;
                                            e.Cancel = true;
                                            return;
                                        }

                                        Thread.Sleep(200);
                                        state = ReplayCANlink.CANlinkReplayCheck();
                                    }
                                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "End CANlink Record Play");
                                    ReplayCANlink.CANlinkReset();
                                }
                            }
                        }
                    }
                    File.Create(Common.Common.SRC_PATH + "\\t1manager\\SendScenarioFinish_" + (scenarioIndex + 1)).Close();
                    if (!File.Exists(Common.Common.SRC_PATH + "\\t1manager\\SendScenarioFinish_" + (scenarioIndex + 1)))
                        IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, "File Not Create [SendScenarioFinish_" + (scenarioIndex + 1) + "]");

                    TimeOutCount = 0;
                    while (!ViewModelLocator.MainVM.MainModel.IsT1ManagerCanLogAck)
                    {
                        if (PerformanceTestWorker.CancellationPending)
                        {
                            BackgroundWorkerErrorMessage = "Test Stop";
                            e.Cancel = true;
                            return;
                        }

                        Thread.Sleep(100);
                        if (TimeOutCount > 600)
                        {
                            BackgroundWorkerErrorMessage = "T1Manager Python Not Response..";
                            e.Cancel = true;
                            return;
                        }
                        TimeOutCount++;
                    }
                    IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "Receive from T1 Message [T1ManagerCanLogFinishAck]");
                    ViewModelLocator.MainVM.MainModel.IsT1ManagerCanLogAck = false;

                    scenarioIndex++;

                    TestProgress += (double)(60.0 / ViewModelLocator.MainVM.T1ScenarioModelList.Count);
                    PerformanceTestWorker.ReportProgress(0, new ProgressStatus { Value = TestProgress, Content = "Performance Measurment...." });
                }

                Thread.Sleep(1000);

                // T1 Performance Measurement Start
                //
                if (PerformanceTestWorker.CancellationPending)
                {
                    BackgroundWorkerErrorMessage = "Test Stop";
                    e.Cancel = true;
                    return;
                }
                PerformanceTestWorker.ReportProgress(0, new ProgressStatus { Value = 100, Content = "Finish Performance Measurment" });

                ReplayCANlink.CANlinkStop();
                ReplayCANlink = null;

            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(FunctionWorkHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
            }

        }
        private void PerformanceTestWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressStatus state = e.UserState as ProgressStatus;
            if (state == null)
                return;

            ViewModelLocator.MainVM.MainModel.PerformanceTestStatusText = state.Content;
            ViewModelLocator.MainVM.MainModel.PerformanceTestProgress = state.Value;

        }
        private void PerformanceTestWorker_DoWorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if(stop_scenario_index != -1)
                    File.Create(Common.Common.SRC_PATH + "\\t1manager\\SendScenarioFinish_" + stop_scenario_index + "_stop").Close();

                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, BackgroundWorkerErrorMessage);
                ViewModelLocator.MainVM.ShowWarningDialogTest(BackgroundWorkerErrorMessage);
            }
            else
                ViewModelLocator.MainVM.ShowWarningDialogTest("TEST FINISH!!");

            TRACE32.getInstance().Exit();
            foreach (var process in Process.GetProcessesByName("CANlink"))
                process.Kill();

            if (ViewModelLocator.MainVM.MainModel.T1ReportPath != null && ViewModelLocator.MainVM.MainModel.T1ReportPath != "")
            {
                string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.T1ReportPath;
                if (ViewModelLocator.MainVM.MainModel.T1ReportPath.Contains("$WORKSPACE$"))
                    ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.T1ReportPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                Process.Start("explorer.exe", ConvertWorkspacePath);
            }
        }



        public void StepTest()
        {
            if (ViewModelLocator.MainVM.MainModel.TestStateMode == Common.Common.PAUSE || ViewModelLocator.MainVM.MainModel.TestStateMode == Common.Common.STEP_RUNNING)
            {
                locker.Set();
                Thread.Sleep(1);
                locker.Reset();
            }
        }
        public void StopTest()
        {
            TestRunWorker.CancelAsync();
            locker.Set();
        }

        public void Print(string strLog, bool IsAlway)
        {
            IVSLog.getInstance().Log(strLog, Common.Common.MODULE_CANLINK);
        }


    }
}
