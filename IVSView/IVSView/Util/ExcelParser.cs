﻿using PROST_Integration.Model;
using PROST_Integration.ViewModel;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Reflection;

namespace PROST_Integration.Util
{
    class ExcelParser
    {
        public Dictionary<string, object> ResaultDictionary;
        public Dictionary<string, object[,]> ReturnDataList;
        public Dictionary<string, List<object>> TestcaseDictonary;
        public bool ErrorIdentifier;
        public Dictionary<string, bool> WriteExcelStopSheetNameList;
        
        private static class LazyHolder
        {
            public static readonly ExcelParser INSTANCE = new ExcelParser();
        }

        public static ExcelParser getInstance()
        {
            return LazyHolder.INSTANCE;
        }
        public ExcelParser()
        {
            ResaultDictionary = new Dictionary<string, object>();
            ReturnDataList = new Dictionary<string, object[,]>();
            TestcaseDictonary = new Dictionary<string, List<object>>();
            WriteExcelStopSheetNameList = new Dictionary<string, bool>();

            ErrorIdentifier = false;
        }
        ~ExcelParser()
        {
            
        }

        public void ReadExcelData(string FilePath)
        {
            ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Clear();
            ViewModelLocator.MainVM.SelectTestcaseSheetNameList.Clear();
            try
            {
                Excel.Workbook wb = null;
                Excel.Worksheet ws = null;
                Excel.Application ap = null;

                object[,] data = null;
                string ExcelFileName = FilePath;
                FileInfo ExcelFileInfo = new FileInfo(ExcelFileName);

                if (ExcelFileInfo.Exists)
                {
                    ap = new Excel.Application();
                    wb = ap.Workbooks.Open(ExcelFileName);

                    ap.Visible = false;

                    for (int i = 1; i <= wb.Worksheets.Count; i++)
                    {
                        Excel.Worksheet workSheet = wb.Worksheets[i] as Excel.Worksheet;
                        ws = (Excel.Worksheet)wb.Worksheets.get_Item(workSheet.Name);
                        ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Add(workSheet.Name);
                        ViewModelLocator.MainVM.SelectTestcaseSheetNameList.Add(true);
                        Range range = ws.UsedRange;
                        data = (object[,])range.Value2;
                        ReturnDataList[workSheet.Name] = data;
                    }
                }
                else
                {
                    ErrorIdentifier = true;
                    Console.WriteLine("[ERR] : Excel file not exist.");
                }

                ap.Quit();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(ExcelParser).Name + " :: " + ex.Message + " Line :: " + (ex));
                CommonUtil.ExitExcelProcess();
            }
        }
        public void TestcaseExtraction()
        {
            try
            {
                object[,] Testcase = null;
                for (int i = 0; i < ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Count; i++)
                {
                    List<object> ResultList = new List<object>();
                    Testcase = ReturnDataList[ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[i]];

                    for (int row = 2; row <= Testcase.GetLength(0); row++)
                    {
                        Dictionary<string, string> ResultTestData = new Dictionary<string, string>();
                        for (int column = 1; column <= Testcase.GetLength(1); column++)
                        {
                            if (Testcase[row, column] == null || Testcase[row, column].ToString() == "")
                            {
                                Testcase[row, column] = "null";
                            }
                            //else
                            {
                                switch (column)
                                {
                                    case 1: ResultTestData["Index"] = Testcase[row, column].ToString(); break;
                                    case 2: ResultTestData["Input"] = Testcase[row, column].ToString(); break;
                                    case 3: ResultTestData["Output1"] = Testcase[row, column].ToString(); break;
                                    case 4: ResultTestData["Output2"] = Testcase[row, column].ToString(); break;
                                    //case 5: ResultTestData["Length"] = Testcase[row, column].ToString(); break;

                                    //case 7: ResultTestData["StartBit"] = Testcase[row, column].ToString(); break;
                                    //case 6: ResultTestData["Value Table"] = Testcase[row, column].ToString(); break;
                                    //case 9: ResultTestData["Factor / Offset"] = Testcase[row, column].ToString(); break;
                                    //case 10: ResultTestData["Space"] = Testcase[row, column].ToString(); break;

                                    case 7: ResultTestData["TC[MIN]_Input"] = Testcase[row, column].ToString(); break;
                                    case 8: ResultTestData["TC[MIN]_Output1"] = Testcase[row, column].ToString(); break;
                                    case 10: ResultTestData["TC[MIN]_Output2"] = Testcase[row, column].ToString(); break;

                                    case 13: ResultTestData["TC[MAX]_Input"] = Testcase[row, column].ToString(); break;
                                    case 14: ResultTestData["TC[MAX]_Output1"] = Testcase[row, column].ToString(); break;
                                    case 16: ResultTestData["TC[MAX]_Output2"] = Testcase[row, column].ToString(); break;

                                    case 19: ResultTestData["TC[MID]_Input"] = Testcase[row, column].ToString(); break;
                                    case 20: ResultTestData["TC[MID]_Output1"] = Testcase[row, column].ToString(); break;
                                    case 22:
                                        ResultTestData["TC[MID]_Output2"] = Testcase[row, column].ToString();
                                        ResultList.Add(ResultTestData); break;
                                }
                            }
                        }
                    }

                    TestcaseDictonary[ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[i]] = ResultList;
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(ExcelParser).Name + " :: " + ex.Message + " Line :: " + (ex));
                CommonUtil.ExitExcelProcess();
            }
        }

        public void CopyExcelFile(string CopyFile, string OriFilePath, string CopyFilePath)
        {
            if (!Directory.Exists(Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile))
                Directory.CreateDirectory(Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile);

            string fileName = CopyFile;
            string sourcePath = OriFilePath;
            string targetPath = CopyFilePath;

            string sourceFile = System.IO.Path.Combine(sourcePath, fileName + ".xlsx");
            string destFile;
            if (File.Exists(Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile + "\\Test_Report.xlsx"))
            {
                if (CommonUtil.GetFreeFileNumber(Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile + "\\Test_Report.xlsx") != "")
                    destFile = CommonUtil.GetFreeFileNumber(Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile + "\\Test_Report.xlsx");
                else
                    destFile = Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile + "\\Test_Report.xlsx";
            }
            else
                destFile = Common.Common.WORKSPACE_PATH + "\\Report\\" + CopyFile + "\\Test_Report.xlsx";

            //System.IO.Directory.CreateDirectory(targetPath);
            System.IO.File.Copy(sourceFile, destFile, true);
            ViewModelLocator.MainVM.MainModel.ReportFilePath = destFile;
        }

        public void WriteExcelData(string FilePath)
        {
            Excel.Workbook wb = null;
            Excel.Worksheet ws = null;
            Excel.Application ap = null;
            try
            {
                string ExcelFileName = FilePath;
                FileInfo ExcelFileInfo = new FileInfo(ExcelFileName);

                if (ExcelFileInfo.Exists)
                {
                    ap = new Excel.Application();
                    wb = ap.Workbooks.Open(ExcelFileName);
                    ap.Visible = false;

                    ws = (Excel.Worksheet)wb.Worksheets.Add(Before: wb.Sheets[1]);
                    ws.Name = "Summary";

                    Excel.Range range = ws.Range[ws.Cells[1, 1], ws.Cells[40, 11]];
                    range.Interior.Color = ColorTranslator.ToOle(Color.White);

                    range = ws.Range[ws.Cells[1, 1], ws.Cells[1, 1]];
                    range.Cells.Font.Bold = true;
                    range.Cells.Font.Size = 12;
                    ws.Cells[1, 1] = "1. Document Introduction";

                    range = ws.Range[ws.Cells[4, 2], ws.Cells[4, 10]];
                    range.Interior.Color = ColorTranslator.ToOle(Color.WhiteSmoke);

                    range = ws.Range[ws.Cells[3, 1], ws.Cells[3, 1]];
                    range.Cells.Font.Bold = true;
                    range.Cells.Font.Size = 12;
                    ws.Cells[3, 1] = "1.1 Test Date";
                    ws.Cells[4, 2] = ViewModelLocator.MainVM.MainModel.CommonTestDate;

                    range = ws.Range[ws.Cells[7, 2], ws.Cells[7, 10]];
                    range.Interior.Color = ColorTranslator.ToOle(Color.WhiteSmoke);

                    range = ws.Range[ws.Cells[6, 1], ws.Cells[6, 1]];
                    range.Cells.Font.Bold = true;
                    range.Cells.Font.Size = 12;
                    ws.Cells[6, 1] = "1.2 Tester";
                    ws.Cells[7, 2] = ViewModelLocator.MainVM.MainModel.CommonTester;


                    range = ws.Range[ws.Cells[10, 2], ws.Cells[10, 10]];
                    range.Interior.Color = ColorTranslator.ToOle(Color.WhiteSmoke);

                    range = ws.Range[ws.Cells[9, 1], ws.Cells[9, 1]];
                    range.Cells.Font.Bold = true;
                    range.Cells.Font.Size = 12;
                    ws.Cells[9, 1] = "1.3 Target Name";
                    ws.Cells[10, 2] = ViewModelLocator.MainVM.MainModel.CommonTargetName;


                    range = ws.Range[ws.Cells[13, 2], ws.Cells[13, 10]];
                    range.Interior.Color = ColorTranslator.ToOle(Color.WhiteSmoke);

                    range = ws.Range[ws.Cells[12, 1], ws.Cells[12, 1]];
                    range.Cells.Font.Bold = true;
                    range.Cells.Font.Size = 12;
                    ws.Cells[12, 1] = "1.4 S/W Version";
                    ws.Cells[13, 2] = ViewModelLocator.MainVM.MainModel.CommonSWVersion;

                    range = ws.Range[ws.Cells[16, 2], ws.Cells[16, 10]];
                    range.Interior.Color = ColorTranslator.ToOle(Color.WhiteSmoke);

                    range = ws.Range[ws.Cells[15, 1], ws.Cells[15, 1]];
                    range.Cells.Font.Bold = true;
                    range.Cells.Font.Size = 12;
                    ws.Cells[15, 1] = "1.5 Comment";
                    ws.Cells[16, 2] = ViewModelLocator.MainVM.MainModel.CommonComment;


                    Dictionary<string, string> ResaultDictionaryFunction;

                    for (int SheetNameCount = 0; SheetNameCount < ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList.Count; SheetNameCount++)
                    {
                        if (!ViewModelLocator.MainVM.SelectTestcaseSheetNameList[SheetNameCount])
                            continue;

                        if (!WriteExcelStopSheetNameList.ContainsKey(ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetNameCount]))
                            continue;

                        ws = (Excel.Worksheet)wb.Worksheets.get_Item(ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetNameCount]);
                        range = ws.UsedRange;

                        List<object> ResaultList = (List<object>)ResaultDictionary[ViewModelLocator.MainVM.MainModel.TestcaseSheetNameList[SheetNameCount]];
                        for (int i = 0; i < ResaultList.Count; i++)
                        {
                            ResaultDictionaryFunction = (Dictionary<string, string>)ResaultList[i];

                            ws.Cells[3 + i, 8] = ResaultDictionaryFunction["TC[MIN]_Output1"];
                            ws.Cells[3 + i, 10] = ResaultDictionaryFunction["TC[MIN]_Output2"];

                            ws.Cells[3 + i, 14] = ResaultDictionaryFunction["TC[MAX]_Output1"];
                            ws.Cells[3 + i, 16] = ResaultDictionaryFunction["TC[MAX]_Output2"];

                            ws.Cells[3 + i, 20] = ResaultDictionaryFunction["TC[MID]_Output1"];
                            ws.Cells[3 + i, 22] = ResaultDictionaryFunction["TC[MID]_Output2"];

                        }
                        ws.Columns.AutoFit();

                        for (int i = 0; i < 3; i++)
                        {
                            Excel.Range cellToSet = ws.get_Range(ws.Cells[3, 9 + i * 6], ws.Cells[ResaultList.Count + 3, 9 + i * 6]);
                            Excel.FormatConditions fcs = cellToSet.FormatConditions;
                            Excel.FormatCondition fc = (Excel.FormatCondition)fcs.Add(XlFormatConditionType.xlTextString, Type.Missing, Type.Missing, Type.Missing, "PASS", XlContainsOperator.xlContains, Type.Missing, Type.Missing);
                            Excel.Interior interior = fc.Interior;
                            interior.Color = ColorTranslator.ToOle(Color.LightGreen);

                            fc = (Excel.FormatCondition)fcs.Add(XlFormatConditionType.xlTextString, Type.Missing, Type.Missing, Type.Missing, "FAIL", XlContainsOperator.xlContains, Type.Missing, Type.Missing);
                            interior = fc.Interior;
                            interior.Color = ColorTranslator.ToOle(Color.LightPink);
                        }

                    }

                    wb.Save();
                    wb.Close(false, Type.Missing, Type.Missing);
                    wb = null;
                }
                else
                {
                    ErrorIdentifier = true;
                    Console.WriteLine("[ERR] : Excel file not exist.");
                }


                ap.Quit();
            }
            catch (Exception ex)
            {
                CommonUtil.ExitExcelProcess();
            }
        }
    }
}
