﻿using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace PROST_Integration.Util
{
    class CommonUtil
    {
        public static void ExitPythonProcess()
        {
            try
            {
                Process[] processList = Process.GetProcesses();
                foreach (Process proc in processList)
                {
                    if (proc.ProcessName.Equals("python"))
                        proc.Kill();
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, ex.Message);
            }
        }
        public static void ExitExcelProcess()
        {
            try
            {
                Process[] processList = Process.GetProcesses();
                foreach (Process proc in processList)
                {
                    /*if (proc.ProcessName.Equals("python"))
                        proc.Kill();*/
                    if (proc.ProcessName.Equals("EXCEL"))
                        proc.Kill();
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(CommonUtil).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }

        public static int GetExceptionLineNumber(Exception ex)
        {
            var st = new StackTrace(ex, true);
            var frame = st.GetFrame(st.FrameCount - 1);
            var line = frame.GetFileLineNumber();

            return (int)line;
        }

        public static bool FileSave_List(string filePath, List<string> lines)
        {
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);

                if (fs == null) return false;

                StreamWriter outputFile = new StreamWriter(fs, System.Text.Encoding.Default);

                foreach (string line in lines)
                {
                    outputFile.WriteLine(line);
                }

                outputFile.Flush();
                outputFile.Close();
                fs.Close();
                //outputFile.Close();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(CommonUtil).Name + " :: " + ex.Message + " Line :: " + (ex));
                return false;
            }
            return true;
        }
        public static string[] FileRead(string filePath)
        {
            string[] returnReadData = new string[] { };

            if (!File.Exists(filePath))
            {
                File.WriteAllText(filePath, "");
            }

            try
            {
                returnReadData = File.ReadAllLines(filePath,Encoding.Default);

            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(CommonUtil).Name + " :: " + ex.Message + " Line :: " + (ex));
            }

            return returnReadData;
        }
        public static void InitWorkspace()
        {
            try
            {
                string readxmlPath = @"workspace_Info.xml";

                XmlDocument XmlDoc = new XmlDocument();
                FileInfo fileInfo = new FileInfo(readxmlPath);
                XmlElement DefaultPathTemp;

                if (!fileInfo.Exists)
                {
                    Common.Common.WORKSPACE_PATH = Directory.GetParent(Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).FullName).FullName + "\\WorkSpace";
                    XmlDoc.AppendChild(XmlDoc.CreateXmlDeclaration("1.0", "utf-8", "yes"));

                    XmlNode newRoot = XmlDoc.CreateElement("", "WorkspaceList", "");
                    XmlDoc.AppendChild(newRoot);

                    XmlNode firstnode = XmlDoc.DocumentElement;
                   
                    DefaultPathTemp = XmlDoc.CreateElement("DefaultPath");
                    DefaultPathTemp.InnerText = Common.Common.WORKSPACE_PATH;

                    firstnode.AppendChild(DefaultPathTemp);

                    XmlDoc.Save(readxmlPath);
                }
                else
                {
                    string proj_list_path = System.IO.File.ReadAllText(readxmlPath);

                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(proj_list_path);
                    XmlElement root = xml.DocumentElement;

                    XmlNodeList xnlist = root.ChildNodes;

                    foreach (XmlNode item in xnlist)
                    {
                        if (item.Name == "DefaultPath")
                        {

                            if (Directory.Exists(item.InnerText))
                                Common.Common.WORKSPACE_PATH = item.InnerText;
                            else
                                ViewModelLocator.MenuBarVM.NewWorkSpace();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(ex.Message);
            }
        }
        public static string GenerateTimingParamName(string str)
        {
            if (str == "CET")
                return "CoreExecutionTime";
            else if (str == "DT")
                return "DeltaTime";
            else if (str == "GET")
                return "GrossExecutionTime";
            else if (str == "IPT")
                return "InitialPendingTime";
            else if (str == "RT")
                return "ResponseTime";
            else if (str == "ST")
                return "SlackTime";
            else if (str == "PER")
                return "Period";
            else
                return str;
        }
        public static void UpdateWorkspace()
        {
            string readxmlPath = @"workspace_Info.xml";
            XmlDocument XmlDoc = new XmlDocument();
            FileInfo fileInfo = new FileInfo(readxmlPath);
            XmlElement DefaultPathTemp;


            XmlDoc.AppendChild(XmlDoc.CreateXmlDeclaration("1.0", "utf-8", "yes"));

            XmlNode newRoot = XmlDoc.CreateElement("", "WorkspaceList", "");
            XmlDoc.AppendChild(newRoot);

            XmlNode firstnode = XmlDoc.DocumentElement;

            DefaultPathTemp = XmlDoc.CreateElement("DefaultPath");
            DefaultPathTemp.InnerText = Common.Common.WORKSPACE_PATH;

            firstnode.AppendChild(DefaultPathTemp);

            XmlDoc.Save(readxmlPath);
        }
        public static bool IsHex(string hexString)
        {
            var isHex = false;
            if ((hexString ?? string.Empty).Length == 0)
                return false;
            if (hexString.Length > 0 && hexString.Length <= 32)
            {
                hexString = hexString.PadLeft(32, '0');
                Guid guid;
                isHex = Guid.TryParse(hexString, out guid);
            }
            else
            {
                throw new NotImplementedException("Use some other way to check the hex string!");
            }
            return isHex;
        }
        public static string GetFreeFileNumber(string path)
        {
            string pathOnly = path.Substring(0, path.LastIndexOf('\\') + 1);
            string nameOnly = Path.GetFileNameWithoutExtension(path);
            string extOnly = Path.GetExtension(path);
            string[] files = Directory.GetFiles(pathOnly, nameOnly + "*" + extOnly);
            if (files.Length == 0)
                return "";
            int largest = files.Max(f => GetFileNumber(f));
            return string.Format("{0}{1}{2}{3}", pathOnly, nameOnly,largest + 1, extOnly);
        }
        /// <summary>
        /// Get the number (if any) at the end of a filename
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static Regex fileNumber = new Regex("\\d+$", RegexOptions.Compiled);
        public static int GetFileNumber(string file)
        {
            Match m = fileNumber.Match(Path.GetFileNameWithoutExtension(file));
            if (!m.Success) return 0;
            return int.Parse(m.Value);
        }
    }
}
