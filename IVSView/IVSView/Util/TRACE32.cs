﻿using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using static System.Net.Mime.MediaTypeNames;

namespace PROST_Integration.Util
{
    class TRACE32
    {
        IntPtr T32Channel;

        public class TRACE32_DLL
        {
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_Config([MarshalAs(UnmanagedType.LPStr)] String String1, [MarshalAs(UnmanagedType.LPStr)] String String2);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_Init();
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_Attach(int DeviceSpecifier);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_Cmd([MarshalAs(UnmanagedType.LPStr)] string Command);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void T32_Exit();
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void T32_SetChannel(IntPtr Params);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_ReadMemory(UInt32 Address, int Access, ref UInt16 pBuffer, UInt32 Size);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_GetMessage(StringBuilder AreaMessage, ref UInt16 MessageType);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_GetChannelSize();
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void T32_GetChannelDefaults(IntPtr Params);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern void T32_GetSymbol([MarshalAs(UnmanagedType.LPStr)] string symbol, ref UInt32 address, ref UInt32 size, ref UInt32 reserved);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_GetPracticeState(ref int pstate);
            [DllImport(@"..\src\lib\T32\lib\t32api.dll", CallingConvention = CallingConvention.Cdecl)]
            public static extern int T32_Nop();
        }
        private static class LazyHolder
        {
            public static readonly TRACE32 INSTANCE = new TRACE32();
        }

        public static TRACE32 getInstance()
        {
            return LazyHolder.INSTANCE;
        }
        public void Init(string PORT, string CH)
        {
            int nRetryCounter = 2;
            int err = 0;
            int errorCount = 0;
            //Console.WriteLine("Point 1");

            T32Channel = Marshal.AllocHGlobal(TRACE32_DLL.T32_GetChannelSize());
            TRACE32_DLL.T32_GetChannelDefaults(T32Channel);
            TRACE32_DLL.T32_SetChannel(T32Channel);

            TRACE32_DLL.T32_Config("PORT=", PORT);  // PowerView Port Connect
        retry:
            if ((err = TRACE32_DLL.T32_Init()) != 0)    // 초기화 예외 처리
            {
                TRACE32_DLL.T32_Exit();
                nRetryCounter = nRetryCounter--;
                if (nRetryCounter > 0)
                {
                    if (errorCount >= 13)
                    {
                        //ViewModelLocator.MainVM.MainModel.IsTrace32Error = true;
                        Thread.Sleep(150);
                        return;
                    }
                    goto retry;
                }
                /* we may need a second try if the software crashed before */

                Console.WriteLine("ERROR: could not initialize connection to T32-API server\n");
                //return false;
            }

            TRACE32_DLL.T32_Config("TIMEOUT=", "5");

            if (TRACE32_DLL.T32_Attach(1) != 0) // PowerView
            {
                TRACE32_DLL.T32_Exit();
                nRetryCounter = nRetryCounter--;
                if (nRetryCounter > 0)
                    goto retry; /* we may need a second try if the software crashed before */

                Console.WriteLine("ERROR: could not attach to T32-API server\n");
                //return false;
            }

            TRACE32_DLL.T32_Config("TIMEOUT=", "10");

            string temp = "PRINT " + "\"" + CH + "_successful connection\"";
            TRACE32_DLL.T32_Cmd(temp);
            Thread.Sleep(100);
        }

        public void FlashDownload(string FlashFilePath, string e, string p)
        {
            int check = 1;
            string ElfFilePath = "";
            string PreHookScript = "";
            if (e == "")
                ElfFilePath = ViewModelLocator.MainVM.MainModel.ELFFilePath;
            else
                ElfFilePath = e;

            if (p == "")
                PreHookScript = ViewModelLocator.MainVM.MainModel.PreHookScriptPath;
            else
                PreHookScript = p;


            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("SYSTEM.RESET");

            FileInfo FlashFileInfo = new FileInfo(FlashFilePath);
            FileInfo ElfFileInfo = new FileInfo(ElfFilePath);

            if (FlashFileInfo.Exists)
            {
                if (ElfFileInfo.Exists || ElfFilePath == "PREPAREONLY")
                {
                    TRACE32_DLL.T32_Cmd("CD.DO \"" + FlashFilePath + "\"" + " " + "\"" + ElfFilePath + "\"" + " " + "\"" + " " + "\"");
                    TRACE32_DLL.T32_GetPracticeState(ref check);
                    while (check != 0)
                    {
                        TRACE32_DLL.T32_GetPracticeState(ref check);
                    }
                }
                else
                {
                    Console.WriteLine("[ERR] : ELF file not exist.");
                }
            }
            else
            {
                Console.WriteLine("[ERR] : Flash download file not exist.");
            }

        }

        public void TargetBoardRun()
        {
            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("GO");
        }

        public string GetCurrentPC()
        {
            StringBuilder resultTemp = new StringBuilder("0", 255);
            UInt16 msg = 0;

            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("PRINT R(PC)");
            Thread.Sleep(2);
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);

            return resultTemp.ToString();
        }

        public bool SetBrake(string VariableName1)
        {
            StringBuilder resultTemp = new StringBuilder("0", 255);
            UInt16 msg = 0;

            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("PRINT Var.ADDRESS(" + VariableName1 + ")");
            Thread.Sleep(2);
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
            string VariableReturnData = resultTemp.ToString();

            if (VariableReturnData.Contains(":"))
            {
                string[] VariableAddr = VariableReturnData.Split(':');
                TRACE32_DLL.T32_Cmd("Break.Set " + VariableAddr[1] + " /Write");

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsExistSymbolCheck(string symbol)
        {
            UInt16 msg = 0;
            StringBuilder resultTemp = new StringBuilder("0", 256);

            TRACE32_DLL.T32_Cmd("print var.exist(" + symbol + ")");
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
            if (resultTemp.ToString().Contains("TRUE"))
                return true;
            else
                return false;
        }

        public bool SetBrake(string VariableName1, string VariableName2, int Sleep)
        {
            StringBuilder resultTemp = new StringBuilder("0", 255);
            UInt16 msg = 0;

            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("PRINT Var.ADDRESS(" + VariableName1 + ")");
            Thread.Sleep(2);
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
            string VariableReturnData = resultTemp.ToString();

            if (VariableReturnData.Contains(":"))
            {
                string[] VariableAddr = VariableReturnData.Split(':');
                TRACE32_DLL.T32_Cmd("Break.Set " + VariableAddr[1] + " /Write");
                Thread.Sleep(Sleep);
                TRACE32_DLL.T32_Cmd("PRINT Var.ADDRESS(" + VariableName2 + ")");
                Thread.Sleep(2);
                TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
                VariableReturnData = resultTemp.ToString();

                if (VariableReturnData.Contains(":"))
                {
                    VariableAddr = VariableReturnData.Split(':');
                    TRACE32_DLL.T32_Cmd("Break.Set " + VariableAddr[1] + " /Write");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool DeleteBrake(string VariableName1)
        {
            StringBuilder resultTemp = new StringBuilder("0", 255);
            UInt16 msg = 0;

            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("PRINT Var.ADDRESS(" + VariableName1 + ")");
            Thread.Sleep(2);
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
            string VariableReturnData = resultTemp.ToString();

            if (VariableReturnData.Contains(":"))
            {
                string[] VariableAddr = VariableReturnData.Split(':');
                TRACE32_DLL.T32_Cmd("Break.Delete " + VariableAddr[1]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteBrake(string VariableName1, string VariableName2)
        {
            StringBuilder resultTemp = new StringBuilder("0", 255);
            UInt16 msg = 0;

            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("PRINT Var.ADDRESS(" + VariableName1 + ")");
            Thread.Sleep(2);
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
            string VariableReturnData = resultTemp.ToString();

            if (VariableReturnData.Contains(":"))
            {
                string[] VariableAddr = VariableReturnData.Split(':');
                TRACE32_DLL.T32_Cmd("Break.Delete " + VariableAddr[1]);

                TRACE32_DLL.T32_Cmd("PRINT Var.ADDRESS(" + VariableName2 + ")");
                Thread.Sleep(2);
                TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
                VariableReturnData = resultTemp.ToString();

                if (VariableReturnData.Contains(":"))
                {
                    VariableAddr = VariableReturnData.Split(':');
                    TRACE32_DLL.T32_Cmd("Break.Delete " + VariableAddr[1]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public string ReadVariable(string VariableName, string inputData)
        {
            string ParsedVariableData = null;
            try
            {
                StringBuilder resultTemp = new StringBuilder("0", 255);
                UInt16 msg = 0;

                TRACE32_DLL.T32_SetChannel(T32Channel);
                TRACE32_DLL.T32_Cmd("PRINT %Hex Var.VALUE(" + VariableName + ")");
                Thread.Sleep(10);
                TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);
                string VariableReturnData = resultTemp.ToString();

                TRACE32_DLL.T32_Cmd("PRINT %Hex Var.SIZEOF(" + VariableName + ")");
                Thread.Sleep(10);
                TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);

                string VariableMaskData = resultTemp.ToString();

                //TRACE32_DLL.T32_GetSymbol(VariableName, ref address, ref size, ref reserved);
                //TRACE32_DLL.T32_Cmd("PRINT \"Read Variable\"");
                //TRACE32_DLL.T32_ReadMemory(address, 0x0, ref buffer, size);

                if (VariableReturnData.Contains("old"))
                {
                    string[] sp = VariableReturnData.Split('=');
                    string[] sp2 = sp[2].Split(',');
                    VariableReturnData = sp2[0];
                    VariableReturnData = VariableReturnData.Remove(0, 1);
                    VariableReturnData = "0x" + Convert.ToString(Convert.ToInt32(VariableReturnData.ToString(), 16));
                }

                if (!CommonUtil.IsHex(VariableReturnData))
                    return VariableReturnData;

                string Mask = "";
                for (int i = 0; i < Convert.ToInt32(VariableMaskData.Trim()); i++)
                {
                    Mask += "FF";
                }
               
                UInt64 value = Convert.ToUInt64(VariableReturnData, 16) & Convert.ToUInt64(Mask, 16);


                UInt64 calcValue = Convert.ToUInt64(inputData.Replace("0x",""),16) & value;

                //ParsedVariableData = "0x" + value.ToString("X");
                ParsedVariableData = "0x" + calcValue.ToString("X");

            }

            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_TRACE32, Common.Common.LOGTYPE_ERR, typeof(TRACE32).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
            return ParsedVariableData;

        }
        public void SetVariable(string VariableName, string Value)
        {
            string CMD = "";

            CMD = "Var.set " + VariableName + "=" + Value;
            TRACE32_DLL.T32_Cmd(CMD);

        }

        public void Exit()
        {
            ViewModelLocator.MainVM.MainModel.IsTrace32Connection = false;
            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("QUIT");
            TRACE32_DLL.T32_Exit();
            Thread.Sleep(10);

        }
        public void TRACE32Start()
        {
            //IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_TINFO, Common.Common.SRC_PATH + @"\lib\T32\PowerView.bat");
            Process proc = Process.Start(@"..\src\lib\T32\PowerView.bat", @"..\src\lib\T32 ");

        }

        public void TRACE32_CheckConnect()
        {
            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_PGR, "TRACE32_CheckConnect Start");
            if (!ViewModelLocator.MainVM.MainModel.IsTrace32Connection)
            {
                TRACE32Start();
                Thread.Sleep(3000);
                Init("20000", "CH1");

                if (TRACE32_DLL.T32_Nop() == 0)
                {
                    ViewModelLocator.MainVM.MainModel.IsTrace32Connection = true;
                    ViewModelLocator.MainVM.showTrackBarMessage("TRACE32 Connection");
                    IVSLog.getInstance().Log("TRACE32 Connection", Common.Common.MODULE_TRACE32);
                }
                else
                {
                    /*ViewModelLocator.MainVM.MainModel.IsTrace32Connection = false;
                    ViewModelLocator.MainVM.showTrackBarMessage("TRACE32 Not Connection");*/
                }

                return;
            }
            else
            {
                // TRACE32가 종료되었을 때 판별
                for (int i = 0; i < 6; i++)
                {
                    //Init("20000", "CH1");
                    TRACE32_DLL.T32_SetChannel(T32Channel);
                    if (TRACE32_DLL.T32_Nop() == 0)
                    {
                        ViewModelLocator.MainVM.MainModel.IsTrace32Connection = true;
                        ViewModelLocator.MainVM.showTrackBarMessage("TRACE32 Connection");
                        return;
                    }
                    else
                    {
                        if (i >= 5)
                        {
                            ViewModelLocator.MainVM.MainModel.IsTrace32Connection = false;
                            ViewModelLocator.MainVM.showTrackBarMessage("TRACE32 Not Connection");
                            return;
                        }
                    }
                }
            }
        }
        public bool CheckSystemDown()
        {
            StringBuilder resultTemp = new StringBuilder("0", 255);
            UInt16 msg = 0;

            TRACE32_DLL.T32_SetChannel(T32Channel);
            TRACE32_DLL.T32_Cmd("print SYSTEM.UP()");
            TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);

            if (resultTemp.ToString().Contains("TRUE"))
                return true;
            else
            {
                TRACE32_DLL.T32_Cmd("system.up");
                TRACE32_DLL.T32_Cmd("print SYSTEM.UP()");
                TRACE32_DLL.T32_GetMessage(resultTemp, ref msg);

                if (resultTemp.ToString().Contains("TRUE"))
                    return true;
                else 
                    return false;
            }
        }
    }
}
