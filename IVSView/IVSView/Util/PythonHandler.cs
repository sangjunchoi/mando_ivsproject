﻿using PROST_Integration.Dialog;
using PROST_Integration.Model;
using PROST_Integration.ViewModel;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace PROST_Integration.Util
{
    class PythonHandler
    {
        private static class LazyHolder
        {
            public static readonly PythonHandler INSTANCE = new PythonHandler();
        }

        public static PythonHandler getInstance()
        {
            return LazyHolder.INSTANCE;
        }

        public PythonHandler()
        {

        }
        public bool MakeDictionaryData()
        {
            try
            {
                IronPython.Runtime.PythonDictionary GUI_PARAMETER_DIC = new IronPython.Runtime.PythonDictionary();

                // Common Path DIctionary
                IronPython.Runtime.PythonDictionary Common_Path_DIC = new IronPython.Runtime.PythonDictionary();
                Common_Path_DIC.Add("WorkingProject_Path", Common.Common.WORKSPACE_PATH);
                Common_Path_DIC.Add("T15_Install_Path", Common.Common.INSTALL_PATH);

                GUI_PARAMETER_DIC.Add("Common_Path", Common_Path_DIC);

                // Test Env DIctionary
                IronPython.Runtime.PythonDictionary Test_Env_DIC = new IronPython.Runtime.PythonDictionary();
                Test_Env_DIC.Add("SW_Version", "1.0");
                Test_Env_DIC.Add("Tester", ViewModelLocator.MainVM.MainModel.CommonTester);
                Test_Env_DIC.Add("Test_SW_Version", ViewModelLocator.MainVM.MainModel.CommonTester);
                Test_Env_DIC.Add("Test_CPU", ViewModelLocator.MainVM.MainModel.CommonTargetName);

                if (ViewModelLocator.MainVM.MainModel.T1ProjectPath != null && ViewModelLocator.MainVM.MainModel.T1ProjectPath != "")
                {
                    if (ViewModelLocator.MainVM.MainModel.T1ProjectPath.Contains("$WORKSPACE$"))
                    {
                        string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.T1ProjectPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                        Test_Env_DIC.Add("T1P_FILE", ConvertWorkspacePath);
                    }
                    else
                        Test_Env_DIC.Add("T1P_FILE", ViewModelLocator.MainVM.MainModel.T1ProjectPath);
                }

                if (ViewModelLocator.MainVM.MainModel.T1ReportPath != null && ViewModelLocator.MainVM.MainModel.T1ReportPath != "")
                {
                    if (ViewModelLocator.MainVM.MainModel.T1ReportPath.Contains("$WORKSPACE$"))
                    {
                        string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.T1ReportPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                        Test_Env_DIC.Add("reportFilePath", ConvertWorkspacePath);
                    }
                    else
                        Test_Env_DIC.Add("reportFilePath", ViewModelLocator.MainVM.MainModel.T1ReportPath);
                }

                if (ViewModelLocator.MainVM.MainModel.T1InstallPath != null && ViewModelLocator.MainVM.MainModel.T1InstallPath != "")
                {
                    if (ViewModelLocator.MainVM.MainModel.T1InstallPath.Contains("$WORKSPACE$"))
                    {
                        string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.T1InstallPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                        Test_Env_DIC.Add("t1InstallPath", ConvertWorkspacePath);
                    }
                    else
                        Test_Env_DIC.Add("t1InstallPath", ViewModelLocator.MainVM.MainModel.T1InstallPath);
                }

                IronPython.Runtime.List scenario_list = new IronPython.Runtime.List();
                
                for(int i=0;i< ViewModelLocator.MainVM.T1ScenarioModelList.Count;i++)
                {
                    T1ScenarioModel t1ScenarioModel;
                    XmlSerializer xs = new XmlSerializer(typeof(T1ScenarioModel));
                    using (Stream fstream = File.OpenRead(ViewModelLocator.MainVM.T1ScenarioModelList[i].ScenarioXmlPath))
                        t1ScenarioModel = xs.Deserialize(fstream) as T1ScenarioModel;

                    IronPython.Runtime.List core_list = new IronPython.Runtime.List();
                    foreach (T1ScenarioCoreModel coremodel in t1ScenarioModel.T1ScenarioCoreModelList)
                    {
                        IronPython.Runtime.List const_list = new IronPython.Runtime.List();
                        foreach (ConstrateModel constmodel in coremodel.ConstrateModelList)
                        {
                            string TaskName = "";
                            string TypeName = "";
                            string MinMaxName = "";
                            string FuncType = "";

                            if (constmodel.SelectedTaskItem.DataValue != null)
                            {
                                TaskName = constmodel.SelectedTaskItem.DataValue;
                                FuncType = constmodel.SelectedTaskItem.DataType;
                            }
                            if (constmodel.SelectedTypeItem != null)
                                TypeName = constmodel.SelectedTypeItem;
                            if (constmodel.SelectedMinMaxItem != null)
                                MinMaxName = constmodel.SelectedMinMaxItem;

                            string str_data = TaskName + "$" + CommonUtil.GenerateTimingParamName(TypeName) + "$" + MinMaxName.Replace("Value", "").Trim() + "$" + constmodel.Value + "$" + FuncType;
                            const_list.Add(str_data);
                        }
                        core_list.Add(const_list);
                    }
                    if(core_list.Count != 0)
                        scenario_list.Add(core_list);
                }
                Test_Env_DIC.Add("T1ScenarioList", scenario_list);

                GUI_PARAMETER_DIC.Add("Test_Env", Test_Env_DIC);

                // Log DIctionary
                IronPython.Runtime.PythonDictionary Log_DIC = new IronPython.Runtime.PythonDictionary();
                Log_DIC.Add("Log_Level", ViewModelLocator.MainVM.MainModel.LogLevelIndex + 1);

                GUI_PARAMETER_DIC.Add("Log", Log_DIC);

                // Parser DIctionary
                IronPython.Runtime.PythonDictionary Parser_DIC = new IronPython.Runtime.PythonDictionary();

                if (ViewModelLocator.MainVM.MainModel.TestSpecificationPath.Contains("$WORKSPACE$"))
                {
                    string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.TestSpecificationPath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\"); 
                    Parser_DIC.Add("Test_Spec_Path", ConvertWorkspacePath);
                }
                else
                    Parser_DIC.Add("Test_Spec_Path", ViewModelLocator.MainVM.MainModel.TestSpecificationPath);



                IronPython.Runtime.PythonDictionary CANDB_Property_DIC = new IronPython.Runtime.PythonDictionary();
                for (int i = 0; i < ViewModelLocator.MainVM.CANPropertyModelList.Count(); i++)
                {
                    IronPython.Runtime.PythonDictionary CANDB_Variable_DIC = new IronPython.Runtime.PythonDictionary();

                    CANDB_Variable_DIC.Add("CAN_BaudRate", ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_BaudRate);
                    CANDB_Variable_DIC.Add("CAN_TSEG1", ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_TSEG1);
                    CANDB_Variable_DIC.Add("CAN_TSEG2", ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_TSEG2);
                    CANDB_Variable_DIC.Add("CAN_SJW", ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_SJW);
                    CANDB_Variable_DIC.Add("CANFD_BaudRate", ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_BaudRate);
                    CANDB_Variable_DIC.Add("CANFD_TSEG1", ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_TSEG1);
                    CANDB_Variable_DIC.Add("CANFD_TSEG2", ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_TSEG2);
                    CANDB_Variable_DIC.Add("CANFD_SJW", ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_SJW);
                    CANDB_Variable_DIC.Add("CANFD_BRS", ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_BRS);

                    if (ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_DBC_Path != null)
                    {
                        if (ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_DBC_Path.Contains("$WORKSPACE$"))
                        {
                            string ConvertWorkspacePath = ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_DBC_Path.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                            CANDB_Variable_DIC.Add("CAN_DBC_Path", ConvertWorkspacePath);
                        }
                        else
                            CANDB_Variable_DIC.Add("CAN_DBC_Path", ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_DBC_Path);
                    }

                    CANDB_Property_DIC.Add("CAN_PROPERTY_" + i + 1, CANDB_Variable_DIC);
                }
                Parser_DIC.Add("CANDB_Property", CANDB_Property_DIC);


                GUI_PARAMETER_DIC.Add("Parser", Parser_DIC);


                var engine = IronPython.Hosting.Python.CreateEngine();
                var scope = engine.CreateScope();

                ICollection<string> searchPaths = engine.GetSearchPaths();

                searchPaths.Add(@"..\src\lib\IronPython 2.7\Lib");
                searchPaths.Add(@"..\src\lib\common\Pickle");
                /*searchPaths.Add(@"..\src\parser\python");
                searchPaths.Add(@"..\src\parser\src\python\Lib");*/
                engine.SetSearchPaths(searchPaths);

                var source = engine.CreateScriptSourceFromFile(@"..\src\lib\common\Pickle\test_pickle.py");
                source.Execute(scope);



                var GUI_Parameter = scope.GetVariable<Func<IronPython.Runtime.PythonDictionary, string, string>>("GUI_Parameter");

                GUI_Parameter(GUI_PARAMETER_DIC, Common.Common.WORKSPACE_PATH + "\\Temp\\" + Common.Common.PROJECT_INFO_DATA);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public ProcessStartInfo myProcessStartInfo;
        public void ParserThread()
        {
            try
            {
                myProcessStartInfo = new ProcessStartInfo(@"..\src\lib\Python37\python.exe");

                myProcessStartInfo.UseShellExecute = false;
                myProcessStartInfo.RedirectStandardOutput = true;

                //쌍따옴표 추가
                myProcessStartInfo.Arguments = @"..\src" + Common.Common.PARSER_APP + " \"" + Common.Common.WORKSPACE_PATH + "\\Temp\\" + Common.Common.PROJECT_INFO_DATA + "\" " + ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem;

                Process myProcess = new Process();
                myProcess.StartInfo = myProcessStartInfo;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.RedirectStandardError = true;
                myProcess.OutputDataReceived += new DataReceivedEventHandler(InterfaceParserOutputHandler);
                myProcess.ErrorDataReceived += new DataReceivedEventHandler(InterfaceParserOutputHandler);

                myProcess.Start();
                myProcess.BeginOutputReadLine();
                myProcess.BeginErrorReadLine();
                myProcess.WaitForExit();


                // close the process 
                //myProcess.Close();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(PythonHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
                Console.WriteLine(ex.Message);
            }
        }

        private void InterfaceParserOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (outLine.Data == null)
                return;

            if (outLine.Data.Contains("[PROGRESS]"))
            {
                string[] str = outLine.Data.Replace("[PROGRESS]", "").Split(':');
                string[] value = str[1].Split('/');
                int m_progress = (int)((double)((100.0 / Int32.Parse(value[1]))) * Int32.Parse(value[0]));
                FunctionWorkHandler.getInstance().InterfaceParserWorker.ReportProgress(0, new ProgressStatus { Value = m_progress, Content = str[0] });
            }
            else if (outLine.Data.Contains("[ERR]"))
            {
                ViewModelLocator.MainVM.MainModel.ParserWorkState = Common.Common.WORKSTATE_ERROR;
                Console.ForegroundColor = ConsoleColor.Red;
                IVSLog.getInstance().Log(outLine.Data, Common.Common.MODULE_PARSE);
                Console.ForegroundColor = ConsoleColor.White;
            }
            else
            {
                if (outLine.Data.Equals("TestspecParser Done"))
                {
                    if (ViewModelLocator.MainVM.MainModel.ParserWorkState == Common.Common.WORKSTATE_ERROR)
                    {
                        System.Windows.Application.Current.Dispatcher.Invoke(new System.Action(() =>
                        {
                            ViewModelLocator.MainVM.CloseParsingStatusView();
                        }));
                        IVSLog.getInstance().Log("######## TEST CASE PARSE ERROR !!!! ########");
                        return;
                    }

                    IVSLog.getInstance().Log("######## TEST CASE PARSE COMPLETE !!!! ########");

                    System.Windows.Application.Current.Dispatcher.Invoke(new System.Action(() =>
                    {
                        ViewModelLocator.MainVM.CloseParsingStatusView();

                        string ConvertTestCasePath = "";
                        if (ViewModelLocator.MainVM.MainModel.TestCasePath.Contains("$WORKSPACE$"))
                        {
                            string ConvertWorkspacePath = ViewModelLocator.MainVM.MainModel.TestCasePath.Replace("$WORKSPACE$", Common.Common.WORKSPACE_PATH + "\\");
                            ConvertTestCasePath = ConvertWorkspacePath;
                        }
                        if (ConvertTestCasePath == "")
                            ExcelParser.getInstance().ReadExcelData(ViewModelLocator.MainVM.MainModel.TestCasePath);
                        else
                            ExcelParser.getInstance().ReadExcelData(ConvertTestCasePath);
                    }));

                    if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
                    {
                        if (ViewModelLocator.MainVM.MainModel.ConsoleAppMode == Common.Common.AUTO_START)
                            FunctionWorkHandler.getInstance().TestStart();
                        else
                            ViewModelLocator.MainVM.ConsoleMode_TestMode();
                    }
                    else
                    {
                        if (ViewModelLocator.MainVM.MainModel.GUIAppMode == Common.Common.AUTO_TEST_MODE)
                        {
                            System.Windows.Application.Current.Dispatcher.Invoke(new System.Action(() =>
                            {
                                ViewModelLocator.MainVM.CloseParsingStatusView();
                                ViewModelLocator.MenuBarVM.SaveConfigDialogOK();
                                FunctionWorkHandler.getInstance().TestStart();
                            }));
                        }
                    }
                    return;
                }
                else if (outLine.Data.Contains("TESTCASEPATH::"))
                {
                    string tempPath = outLine.Data.Replace("TESTCASEPATH::", "");
                    if (tempPath.Contains(Common.Common.WORKSPACE_PATH))
                        ViewModelLocator.MainVM.MainModel.TestCasePath = "$WORKSPACE$" + tempPath.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                    else
                        ViewModelLocator.MainVM.MainModel.TestCasePath = tempPath;
                }

                if (!ViewModelLocator.MainVM.MainModel.GUIStartup)
                    IVSLog.getInstance().Log(outLine.Data, Common.Common.MODULE_PARSE, ConsoleColor.White);
                else
                    IVSLog.getInstance().Log(outLine.Data, Common.Common.MODULE_PARSE);
            }

        }

        public void T1_APIThread()
        {
            try
            {
                myProcessStartInfo = new ProcessStartInfo(@"..\src\lib\Python37\python.exe");

                myProcessStartInfo.UseShellExecute = false;
                myProcessStartInfo.RedirectStandardOutput = true;

                //쌍따옴표 추가
                myProcessStartInfo.Arguments = @"..\src" + Common.Common.T1MANAGER_APP + " \"" + Common.Common.WORKSPACE_PATH + "\\Temp\\" + Common.Common.PROJECT_INFO_DATA + "\"";

                Process myProcess = new Process();
                myProcess.StartInfo = myProcessStartInfo;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.RedirectStandardError = true;
                myProcess.OutputDataReceived += new DataReceivedEventHandler(T1APIOutputHandler);
                myProcess.ErrorDataReceived += new DataReceivedEventHandler(T1APIOutputHandler);

                myProcess.Start();
                myProcess.BeginOutputReadLine();
                myProcess.BeginErrorReadLine();
                myProcess.WaitForExit();


                // close the process 
                //myProcess.Close();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(PythonHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
                Console.WriteLine(ex.Message);
            }
        }
        public void T1ProjectInfoThread()
        {
            try
            {
                myProcessStartInfo = new ProcessStartInfo(@"..\src\lib\Python37\python.exe");

                myProcessStartInfo.UseShellExecute = false;
                myProcessStartInfo.RedirectStandardOutput = true;

                //쌍따옴표 추가
                myProcessStartInfo.Arguments = @"..\src" + Common.Common.T1PROJECTINFO_APP + " \"" + Common.Common.WORKSPACE_PATH + "\\Temp\\" + Common.Common.PROJECT_INFO_DATA + "\"";

                Process myProcess = new Process();
                myProcess.StartInfo = myProcessStartInfo;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.RedirectStandardError = true;
                myProcess.OutputDataReceived += new DataReceivedEventHandler(T1APIOutputHandler);
                myProcess.ErrorDataReceived += new DataReceivedEventHandler(T1APIOutputHandler);

                myProcess.Start();
                myProcess.BeginOutputReadLine();
                myProcess.BeginErrorReadLine();
                myProcess.WaitForExit();


                // close the process 
                //myProcess.Close();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(PythonHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
                Console.WriteLine(ex.Message);
            }
        }
        private void T1APIOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            try
            {
                if (outLine.Data == null)
                    return;

                ViewModelLocator.MainVM.MainModel.isDialogOpen = false;
                if (outLine.Data.Contains("T1ManagerConstraintSetComplete"))
                    ViewModelLocator.MainVM.MainModel.IsT1ManagerConstraintComplete = true;
                else if (outLine.Data.Contains("T1ManagerCanLogFinishAck"))
                    ViewModelLocator.MainVM.MainModel.IsT1ManagerCanLogAck = true;
                else if (outLine.Data.Contains("T1ManagerReady"))
                    ViewModelLocator.MainVM.MainModel.IsT1ManagerReady = true;
                else if (outLine.Data.Contains("T1ProjectInfoFile$"))
                {
                    string[] T1ProjectInfoFileSplit = outLine.Data.Split(new string[] { "$$" }, StringSplitOptions.None);

                    if (T1ProjectInfoFileSplit.Length > 1)
                    {
                        ViewModelLocator.MainVM.MainModel.T1ProjectDictionaryFile = T1ProjectInfoFileSplit[1];
                        ParsingT1ProjectInfo();
                    }
                }

                IVSLog.getInstance().Log(outLine.Data, Common.Common.MODULE_T1MANAGER);
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(PythonHandler).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }

        public void GetT1ProjectInfo()
        {
            if (ViewModelLocator.MainVM.MainModel.IsPerformanceMeasurmentEnable)
            {
                ViewModelLocator.MainVM.MainModel.IsT1ProjectParsingComplete = false;
                ViewModelLocator.MainVM.ShowLoadT1ProjectInfoDialog();
                Thread thread = new Thread(new ThreadStart(delegate ()
                {
                    MakeDictionaryData();
                    T1ProjectInfoThread();
                }));
                thread.Start();
            }
        }
        public void ParsingT1ProjectInfo()
        {
            try
            {
                if (!File.Exists(ViewModelLocator.MainVM.MainModel.T1ProjectDictionaryFile))
                {
                    ViewModelLocator.MainVM.showTrackBarMessage("Failed to create T1ProjectFile");
                    return;
                }

                var engine = IronPython.Hosting.Python.CreateEngine();
                var scope = engine.CreateScope();

                ICollection<string> searchPaths = engine.GetSearchPaths();

                searchPaths.Add(@"..\src\lib\IronPython 2.7\Lib");
                searchPaths.Add(@"..\src\lib\common\Pickle");
                searchPaths.Add(@"..\src\compiler\python");
                searchPaths.Add(@"..\src\parser\python");
                searchPaths.Add(@"..\src\Parser\python\Lib");
                engine.SetSearchPaths(searchPaths);

                var source = engine.CreateScriptSourceFromFile(@"..\src\lib\common\Pickle\test_pickle.py");
                source.Execute(scope);

                var Origin_TC_Load_init = scope.GetVariable<Func<string, object>>("Origin_TC_Load");
                IronPython.Runtime.PythonDictionary InfoList = (IronPython.Runtime.PythonDictionary)Origin_TC_Load_init(ViewModelLocator.MainVM.MainModel.T1ProjectDictionaryFile);

                ViewModelLocator.MainVM.T1ProjectInfoList.Clear();

                foreach (string key in InfoList.Keys)
                {
                    string type = "";
                    string corenum = key;
                    ObservableCollection<T1ProjectInfoDataModel> tempList = new ObservableCollection<T1ProjectInfoDataModel>();

                    IronPython.Runtime.PythonDictionary TaskInfo = (IronPython.Runtime.PythonDictionary)InfoList[key];

                    foreach (string infokey in TaskInfo.Keys)
                    {
                        if (TaskInfo[infokey] == "")
                            continue;

                        IronPython.Runtime.List datalist = (IronPython.Runtime.List)TaskInfo[infokey];

                        foreach (string str in datalist)
                            tempList.Add(new T1ProjectInfoDataModel() { DataType = infokey, DataValue = str });

                    }
                    ViewModelLocator.MainVM.T1ProjectInfoList.Add(new T1ProjectInfoModel() { CoreName = "Core " + key, DataList = tempList });
                }
                ViewModelLocator.MainVM.MainModel.IsT1ProjectParsingComplete = true;
                ViewModelLocator.MainVM.showTrackBarMessage("T1 Project Info Parsing Complete");
            }
            catch (Exception ex)
            {
                ViewModelLocator.MainVM.MainModel.IsT1ProjectParsingComplete = false;
                ViewModelLocator.MainVM.showTrackBarMessage("T1 Project Info Parsing Failed");
            }
        }
    }
}
