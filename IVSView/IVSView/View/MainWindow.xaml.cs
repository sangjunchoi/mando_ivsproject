﻿using PROST_Integration.CustomControl;
using PROST_Integration.Model;
using PROST_Integration.Util;
using PROST_Integration.ViewModel;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PROST_Integration.View
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {

        [DllImport("shell32.dll")]
        static extern void DragAcceptFiles(IntPtr hwnd, bool fAccept);

        [DllImport("shell32.dll")]
        static extern uint DragQueryFile(IntPtr hDrop, uint iFile, [Out] StringBuilder filename, uint cch);

        [DllImport("shell32.dll")]
        static extern void DragFinish(IntPtr hDrop);

        private bool isMarquee_testspecification = false;
        private bool isMarquee_dbc_1 = false;
        private bool isMarquee_dbc_2 = false;
        private bool isMarquee_dbc_3 = false;
        private bool isMarquee_dbc_4 = false;
        private bool isMarquee_testcase = false;
        private bool isMarquee_elfpath = false;
        private bool isMarquee_t1installpath = false;
        private bool isMarquee_t1reportpath = false;
        private bool isMarquee_t1projectpath = false;
        private bool isMarquee_prehookscript = false;

        private int CANdisplayIndex = 0;
        private int CANFDdisplayIndex = 0;

        public MainWindow()
        {
            InitializeComponent();
            ((INotifyCollectionChanged)logview.Items).CollectionChanged += (s, e2) => ScrollToBottom(logview, e2.Action);

        }
        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        // Minimize
        private void CommandBinding_Executed_Minimize(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void CommandBinding_Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }

        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void TEXTBLOCK_TestSpecification_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_testspecification && TEXTBOX_TestSpecification.ActualWidth > CANVAS_TestSpecification.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_TestSpecification.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_TestSpecification.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_testspecification = true;
            }
        }

        private void TEXTBLOCK_TestSpecification_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_TestSpecification.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_TestSpecification.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_TestSpecification.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_testspecification = false;
        }

        private void TEXTBOX_TestcasePath_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_testcase && TEXTBOX_TestcasePath.ActualWidth > CANVAS_TestcasePath.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_TestcasePath.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_TestcasePath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_testcase = true;
            }
        }

        private void TEXTBOX_TestcasePath_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_TestcasePath.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_TestcasePath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_TestcasePath.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_testcase = false;
        }

        private void ListBoxItem_Selected(object sender, RoutedEventArgs e)
        {

        }
        private void ScrollToBottom(ListBox log, NotifyCollectionChangedAction action)
        {
            if (log == null || !log.IsInitialized || action != NotifyCollectionChangedAction.Add || log.Items.Count == 0) return;

            var items = log.Items;
            var last = items[items.Count - 1];

            Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                items.MoveCurrentTo(last);
                log.ScrollIntoView(last);
            }));
        }
        private void ListView_KeyDown(object sender, KeyEventArgs e)
        {
/*            if (sender != logview) return;

            if (e.Key == Key.C && Keyboard.IsKeyDown(Key.LeftCtrl))
                CopySelectedValuesToClipboard();*/
        }
        private void CopySelectedValuesToClipboard()
        {
/*            var builder = new StringBuilder();
            foreach (LogDataModel item in logview.SelectedItems)
                builder.AppendLine(item.MainContent + item.SecondContent + item.ThirdContent);

            Clipboard.SetText(builder.ToString());*/
        }

        private void CANSettings_Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            if (btn.Name == "CANSETTING_1")
                CANdisplayIndex = 0;
            else if (btn.Name == "CANSETTING_2")
                CANdisplayIndex = 1;
            else if (btn.Name == "CANSETTING_3")
                CANdisplayIndex = 2;
            else if (btn.Name == "CANSETTING_4")
                CANdisplayIndex = 3;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_BaudRate = ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_BaudRate;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_SJW = ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_SJW;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_TSEG1 = ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_TSEG1;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_TSEG2 = ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_TSEG2;

            ViewModelLocator.MainVM.MainModel.CANDialogVisible = Common.Common.TRUE;
            ViewModelLocator.MainVM.MainModel.CANFDDialogVisible = Common.Common.FALSE;
            ViewModelLocator.MainVM.MainModel.IsT1ScenarioSettingVisible = Visibility.Hidden;
        }

        private void CANFDSettings_Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            if (btn.Name == "CANFDSETTING_1")
                CANFDdisplayIndex = 0;
            else if (btn.Name == "CANFDSETTING_2")
                CANFDdisplayIndex = 1;
            else if (btn.Name == "CANFDSETTING_3")
                CANFDdisplayIndex = 2;
            else if (btn.Name == "CANFDSETTING_4")
                CANFDdisplayIndex = 3;
            
            ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_BaudRate = ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_BaudRate;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_SJW = ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_SJW;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_TSEG1 = ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_TSEG1;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_TSEG2 = ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_TSEG2;
            ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_BRS = ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_BRS;

            ViewModelLocator.MainVM.MainModel.CANDialogVisible = Common.Common.FALSE;
            ViewModelLocator.MainVM.MainModel.CANFDDialogVisible = Common.Common.TRUE;
            ViewModelLocator.MainVM.MainModel.IsT1ScenarioSettingVisible = Visibility.Hidden;
        }

        private void TEXTBOX_CANDB_1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_dbc_1 && TEXTBOX_CANDB_1.ActualWidth > CANVAS_CANDB_1.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_CANDB_1.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_CANDB_1.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_dbc_1 = true;
            }
        }

        private void TEXTBOX_CANDB_1_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_CANDB_1.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_CANDB_1.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_CANDB_1.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_dbc_1 = false;
        }

        private void TEXTBOX_CANDB_2_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_CANDB_2.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_CANDB_2.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_CANDB_2.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_dbc_2 = false;
        }

        private void TEXTBOX_CANDB_2_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_dbc_2 && TEXTBOX_CANDB_2.ActualWidth > CANVAS_CANDB_2.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_CANDB_2.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_CANDB_2.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_dbc_2 = true;
            }
        }

        private void TEXTBOX_CANDB_3_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_dbc_3 && TEXTBOX_CANDB_3.ActualWidth > CANVAS_CANDB_3.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_CANDB_3.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_CANDB_3.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_dbc_3 = true;
            }
        }

        private void TEXTBOX_CANDB_3_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_CANDB_3.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_CANDB_3.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_CANDB_3.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_dbc_3 = false;
        }

        private void TEXTBOX_CANDB_4_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_dbc_4 && TEXTBOX_CANDB_4.ActualWidth > CANVAS_CANDB_4.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_CANDB_4.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_CANDB_4.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_dbc_4 = true;
            }
        }

        private void TEXTBOX_CANDB_4_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_CANDB_4.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_CANDB_4.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_CANDB_4.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_dbc_4 = false;
        }

        private void CANDB_ADD_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.MainVM.CANPropertyModelList.Add(new Model.CANDBPropertyModel());
            ViewModelLocator.MainVM.MainModel.CANDB_item_count++;
            ViewModelLocator.MainVM.UpdateItemVisible();
        }

        private void CANDB_DELETE_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.MainVM.CANPropertyModelList.RemoveAt(ViewModelLocator.MainVM.CANPropertyModelList.Count - 1);
            ViewModelLocator.MainVM.MainModel.CANDB_item_count--;
            ViewModelLocator.MainVM.UpdateItemVisible();
        }

        private void CANFDSetting_Close_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_BaudRate = ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_BaudRate;
            ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_SJW = ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_SJW;
            ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_TSEG1 = ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_TSEG1;
            ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_TSEG2 = ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_TSEG2;
            ViewModelLocator.MainVM.CANPropertyModelList[CANFDdisplayIndex].CANFD_BRS = ViewModelLocator.MainVM.DisplayCANDBProperty.CANFD_BRS;
        }

        private void CANSetting_Close_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_BaudRate = ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_BaudRate;
            ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_SJW = ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_SJW;
            ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_TSEG1 = ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_TSEG1;
            ViewModelLocator.MainVM.CANPropertyModelList[CANdisplayIndex].CAN_TSEG2 = ViewModelLocator.MainVM.DisplayCANDBProperty.CAN_TSEG2;
        }

        private void TEXTBLOCK_ELFFile_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_elfpath && TEXTBOX_ELFFilePath.ActualWidth > CANVAS_ELFFilePath.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_ELFFilePath.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_ELFFilePath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_elfpath = true;
            }
        }

        private void TEXTBLOCK_ELFFile_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_ELFFilePath.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_ELFFilePath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_ELFFilePath.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_elfpath = false;
        }

        private void TEXTBLOCK_T1InstallPath_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_t1installpath && TEXTBOX_T1InstallPath.ActualWidth > CANVAS_T1InstallPath.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_T1InstallPath.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_T1InstallPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_t1installpath = true;
            }
        }

        private void TEXTBLOCK_T1InstallPath_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_T1InstallPath.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_T1InstallPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_T1InstallPath.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_t1installpath = false;
        }
        private void TEXTBLOCK_T1ProjectPath_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_t1projectpath && TEXTBOX_T1ProjectPath.ActualWidth > CANVAS_T1ProjectPath.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_T1ProjectPath.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_T1ProjectPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_t1projectpath = true;
            }
        }

        private void TEXTBLOCK_T1ProjectPath_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_T1ProjectPath.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_T1ProjectPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_T1ProjectPath.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_t1projectpath = false;
        }

        private void TEXTBLOCK_T1ReportPath_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_t1reportpath && TEXTBOX_T1ReportPath.ActualWidth > CANVAS_T1ReportPath.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_T1ReportPath.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_T1ReportPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_t1reportpath = true;
            }
        }

        private void TEXTBLOCK_T1ReportPath_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_T1ReportPath.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_T1ReportPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_T1ReportPath.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_t1reportpath = false;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            var helper = new WindowInteropHelper(this);
            var hwnd = helper.Handle;

            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            source.AddHook(WndProc);

            DragAcceptFiles(hwnd, true);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == 0x233)
            {
                handled = true;
                return HandleDropFiles(wParam);
            }

            return IntPtr.Zero;
        }

        private IntPtr HandleDropFiles(IntPtr hDrop)
        {
            
            const int MAX_PATH = 260;

            var count = DragQueryFile(hDrop, 0xFFFFFFFF, null, 0);

            for (uint i = 0; i < count; i++)
            {
                int size = (int)DragQueryFile(hDrop, i, null, 0);

                var filename = new StringBuilder(size + 1);
                DragQueryFile(hDrop, i, filename, MAX_PATH);

                Console.WriteLine("Dropped: " + filename.ToString());
            }

            DragFinish(hDrop);

            return IntPtr.Zero;
        }

        private void T1ScenarioSetting_Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModelLocator.MainVM.MainModel.CANDialogVisible = Common.Common.FALSE;
            ViewModelLocator.MainVM.MainModel.CANFDDialogVisible = Common.Common.FALSE;
            ViewModelLocator.MainVM.MainModel.IsT1ScenarioSettingVisible = Visibility.Visible;

            if (!ViewModelLocator.MainVM.MainModel.IsT1ProjectParsingComplete)
            {
                if (ViewModelLocator.MainVM.MainModel.T1ProjectPath == "")
                {
                    CommonOpenFileDialog dialog = new CommonOpenFileDialog();

                    //dialog.InitialDirectory = @"C:\";
                    dialog.IsFolderPicker = false;
                    dialog.RestoreDirectory = false;
                    CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

                    if (commonFileDialogResult == CommonFileDialogResult.Ok)
                    {
                        if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                            ViewModelLocator.MainVM.MainModel.T1ProjectPath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            ViewModelLocator.MainVM.MainModel.T1ProjectPath = dialog.FileName;

                        PythonHandler.getInstance().GetT1ProjectInfo();
                    }
                }
                else
                    PythonHandler.getInstance().GetT1ProjectInfo();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.IsKeyDown(Key.LeftCtrl))
                ViewModelLocator.MenuBarVM.ShowSaveConfigDialog(Common.Common.SAVEMODE_STANDARD);
            else if (e.Key == Key.S && !Keyboard.IsKeyDown(Key.LeftCtrl))
                return;
        }

        private void TEXTBLOCK_PreHookScriptPath_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_prehookscript && TEXTBOX_PreHookScriptPath.ActualWidth > CANVAS_PreHookScriptPath.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_PreHookScriptPath.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_PreHookScriptPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_prehookscript = true;
            }
        }

        private void TEXTBLOCK_PreHookScriptPath_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_PreHookScriptPath.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_PreHookScriptPath.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_PreHookScriptPath.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_prehookscript = false;
        }
    }
}
