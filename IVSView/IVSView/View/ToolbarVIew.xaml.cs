﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PROST_Integration.View
{
    /// <summary>
    /// ToolbarVIew.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ToolbarVIew : UserControl
    {
        private bool isMarquee_workspace = false;
        public static ToolbarVIew Instance
        {
            get; private set;
        }
        public ToolbarVIew()
        {
            InitializeComponent();
            Instance = this;
        }

        private void BtnLog_Click(object sender, RoutedEventArgs e)
        {
            BtnLog.Content = "";
            PackIcon packIcon = new PackIcon();
            packIcon.Foreground = new SolidColorBrush(Colors.Yellow);
            packIcon.Kind = PackIconKind.MathLog;
            BtnLog.Content = packIcon;

            BtnView.Content = "";
            PackIcon packIcon1 = new PackIcon();
            packIcon1.Foreground = new SolidColorBrush(Colors.White);
            packIcon1.Kind = PackIconKind.Monitor;
            BtnView.Content = packIcon1;
        }

        private void BtnView_Click(object sender, RoutedEventArgs e)
        {
            PackIcon packIcon = new PackIcon();
            packIcon.Foreground = new SolidColorBrush(Colors.White);
            packIcon.Kind = PackIconKind.MathLog;
            BtnLog.Content = packIcon;

            PackIcon packIcon1 = new PackIcon();
            packIcon1.Foreground = new SolidColorBrush(Colors.Yellow);
            packIcon1.Kind = PackIconKind.Monitor;
            BtnView.Content = packIcon1;
        }

        private void TEXTBLOCK_Workspace_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isMarquee_workspace && TEXTBOX_Workspace.ActualWidth > CANVAS_Workspace.ActualWidth)
            {

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = 0;
                doubleAnimation.To = -TEXTBOX_Workspace.ActualWidth;
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));
                TEXTBOX_Workspace.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
                isMarquee_workspace = true;
            }
        }

        private void TEXTBLOCK_Workspace_MouseLeave(object sender, MouseEventArgs e)
        {
            TEXTBOX_Workspace.BeginAnimation(Canvas.LeftProperty, null);
            DoubleAnimation doubleAnimation = new DoubleAnimation();
            doubleAnimation.BeginTime = null;
            TEXTBOX_Workspace.BeginAnimation(Canvas.LeftProperty, doubleAnimation);
            TEXTBOX_Workspace.Margin = new Thickness(0, 0, 0, 0);
            isMarquee_workspace = false;
        }
        public void updateWorkspacePath(string str)
        {
            TEXTBOX_Workspace.Text = str;
        }
    }
}
