using System;
using Microsoft.WindowsAPICodePack.Dialogs;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PROST_Integration.Model;
using PROST_Integration.Dialog;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using PROST_Integration.Util;
using System.Collections.ObjectModel;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Threading;
using PROST_Integration.Popup;
using System.Windows;
using PROST_Integration.View;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Serialization;
using static PROST_Integration.Common.Common;

namespace PROST_Integration.ViewModel
{
    public class MainViewModel : ViewModelBase
    {

        public RelayCommand T1InstallPathBrowserCommand
        {
            get; set;
        }
        public RelayCommand T1ProjectPathBrowserCommand
        {
            get;set;
        }
        public RelayCommand T1ReportPathBrowserCommand
        {
            get; set;
        }
        
        public RelayCommand ELFFileBrowserCommand { get; set; }
        public RelayCommand PreHookScriptBrowserCommand
        {
            get;set;
        }
        public RelayCommand<object> TestSpecificationBrowserCommand { get; set; }
        public RelayCommand<object> T1ScenarioLogPathBrowserCommand
        {
            get; set;
        }
        public RelayCommand<object> SelectT1ScenarioBrowserCommand
        {
            get;set;
        }
        public RelayCommand<object> EditT1ScenarioCommand
        {
            get;set;
        }
        public RelayCommand<object> DBCBrowserCommand { get; set; }
        public RelayCommand TestcaseBrowserCommand { get; set; }


        
        public RelayCommand TestContinueDialogOKCommand
        {
            get;set;
        }
        public RelayCommand TestContinueDialogNoCommand
        {
            get;set;
        }
        public RelayCommand<object> DialogCancelCommand { get; set; }
        
        public RelayCommand SelectTestSheetDialogOKCommand
        {
            get;set;
        }
        public RelayCommand TestCaseParseWarningDialogYESCommand { get; set; }
        public RelayCommand TestCaseParseWarningDialogNOCommand {get; set; }
        

        public RelayCommand TestDialogCancelCommand { get; set; }
        public RelayCommand ParsingDialogCancelCommand { get; set; }

        

        public RelayCommand StopParseCommand { get; set; }
        public RelayCommand StopTestCommand { get; set; }
        public RelayCommand StopPerformanceTestCommand
        {
            get;set;
        }
        public RelayCommand PauseTestCommand { get; set; }
        public RelayCommand StepTestCommand { get; set; }
        public RelayCommand RestartTestCommand { get; set; }

        public RelayCommand BtnDeleteLogAllCommand { get; set; }
        public RelayCommand BtnSaveLogDataCommand { get; set; }

        public RelayCommand TestDocumentSettingClickCommand
        {
            get;set;
        }
        public RelayCommand T1ConfigSettingClickCommand
        {
            get; set;
        }
        public RelayCommand ETCConfigSettingClickCommand
        {
            get; set;
        }
        public RelayCommand<object> DeleteCanLogDataCommand
        {
            get;set;
        
        }
        public RelayCommand<object> DeleteConstateCommand
        {
            get;set;
        }
        public RelayCommand<object> DeleteT1ScenarioCommand
        {
            get; set;
        }
        public RelayCommand<object> AddConstateCommand
        {
            get;set;
        }
        public RelayCommand OKConstateCommand
        {
            get; set;
        }
        public RelayCommand<object> ShowPopupConstraintStatusCommand
        {
            get;set;
        }
        public RelayCommand<object> CheckedChangedTimingParamCommand
        {
            get; set;
        }
        public RelayCommand NewScenarioCommand
        {
            get; set;
        }
        public RelayCommand<object> LoadScenarioCommand
        {
            get; set;
        }
        public RelayCommand InsertScenarioCommand
        {
            get;set;
        }
        public RelayCommand SaveScenarioCommand
        {
            get; set;
        }
        public RelayCommand SaveAsScenarioCommand
        {
            get;set;
        }
        public RelayCommand<object> RemoveScenarioCommand
        {
            get; set;
        }

        private MainModel _MainModel;
        public MainModel MainModel
        {
            get { return _MainModel; }
            set
            {
                if (_MainModel != value)
                {
                    _MainModel = value;
                    RaisePropertyChanged("MainModel");
                }
            }
        }
        private CANDBPropertyModel _DisplayCANDBProperty;
        public CANDBPropertyModel DisplayCANDBProperty
        {
            get { return _DisplayCANDBProperty; }
            set
            {
                if (_DisplayCANDBProperty != value)
                {
                    _DisplayCANDBProperty = value;
                    RaisePropertyChanged("DisplayCANDBProperty");
                }
            }
        }

        private ObservableCollection<CANDBPropertyModel> _CANPropertyModelList;
        public ObservableCollection<CANDBPropertyModel> CANPropertyModelList
        {
            get { return _CANPropertyModelList; }
            set
            {
                if (_CANPropertyModelList != value)
                {
                    _CANPropertyModelList = value;
                    RaisePropertyChanged("CANPropertyModelList");
                }
            }
        }
        private ObservableCollection<bool> _SelectTestcaseSheetNameList;
        public ObservableCollection<bool> SelectTestcaseSheetNameList
        {
            get
            {
                return _SelectTestcaseSheetNameList;
            }
            set
            {
                if (_SelectTestcaseSheetNameList != value)
                {
                    _SelectTestcaseSheetNameList = value;
                    RaisePropertyChanged("SelectTestcaseSheetNameList");
                }
            }
        }
        private ObservableCollection<T1ScenarioModel> _T1ScenarioModelList;
        public ObservableCollection<T1ScenarioModel> T1ScenarioModelList
        {
            get
            {
                return _T1ScenarioModelList;
            }
            set
            {
                if (_T1ScenarioModelList != value)
                {
                    _T1ScenarioModelList = value;
                    RaisePropertyChanged("T1ScenarioModelList");
                }
            }
        }

        private ObservableCollection<T1ProjectInfoModel> _T1ProjectInfoList;
        public ObservableCollection<T1ProjectInfoModel> T1ProjectInfoList
        {
            get
            {
                return _T1ProjectInfoList;
            }
            set
            {
                if (_T1ProjectInfoList != value)
                {
                    _T1ProjectInfoList = value;
                    RaisePropertyChanged("T1ProjectInfoList");
                }
            }
        }
        private ObservableCollection<string> _TypeList;
        public ObservableCollection<string> TypeList
        {
            get
            {
                return _TypeList;
            }
            set
            {
                if (_TypeList != value)
                {
                    _TypeList = value;
                    RaisePropertyChanged("TypeList");
                }
            }
        }
        private ObservableCollection<string> _MinMaxList_2;
        public ObservableCollection<string> MinMaxList_2
        {
            get
            {
                return _MinMaxList_2;
            }
            set
            {
                if (_MinMaxList_2 != value)
                {
                    _MinMaxList_2 = value;
                    RaisePropertyChanged("MinMaxList_2");
                }
            }
        }
        private ObservableCollection<string> _MinMaxList_1;
        public ObservableCollection<string> MinMaxList_1
        {
            get
            {
                return _MinMaxList_1;
            }
            set
            {
                if (_MinMaxList_1 != value)
                {
                    _MinMaxList_1 = value;
                    RaisePropertyChanged("MinMaxList_1");
                }
            }
        }

        public MainViewModel()
        {
            InitProgram();
            SelectTestcaseSheetNameList = new ObservableCollection<bool>();
            T1ScenarioModelList = new ObservableCollection<T1ScenarioModel>();
            T1ProjectInfoList = new ObservableCollection<T1ProjectInfoModel>();
            TypeList = new ObservableCollection<string>();
            MinMaxList_2 = new ObservableCollection<string>();
            MinMaxList_1 = new ObservableCollection<string>();
            MinMaxList_2.Add("Min Value");
            MinMaxList_2.Add("Max Value");
            MinMaxList_1.Add("Max Value");
            TypeList.Add("CET");
            TypeList.Add("DT");
            TypeList.Add("GET");
            TypeList.Add("IPT");
            TypeList.Add("RT");
            TypeList.Add("ST");
            TypeList.Add("PER");

        }
        
        private void InitProgram()
        {
            MainModel = new MainModel();
            TestSpecificationBrowserCommand = new RelayCommand<object>(TestSpecificationBrowser);
            T1ScenarioLogPathBrowserCommand = new RelayCommand<object>(T1ScenarioLogPathBrowser);
            ELFFileBrowserCommand = new RelayCommand(ELFFileBrowser);
            PreHookScriptBrowserCommand = new RelayCommand(PreHookScriptBrowser);
            DBCBrowserCommand = new RelayCommand<object>(DBCBrowser);
            TestcaseBrowserCommand = new RelayCommand(TestcaseBrowser);


            TestContinueDialogOKCommand = new RelayCommand(TestContinueDialogOK);
            TestContinueDialogNoCommand = new RelayCommand(TestContinueDialogNo);

            SelectTestSheetDialogOKCommand = new RelayCommand(SelectTestSheetDialogOK);
            TestCaseParseWarningDialogYESCommand = new RelayCommand(TestCaseParseWarningDialogYES);
            TestCaseParseWarningDialogNOCommand = new RelayCommand(TestCaseParseWarningDialogNO);
            DialogCancelCommand = new RelayCommand<object>(DialogCancel);
            

            TestDialogCancelCommand = new RelayCommand(TestDialogCancel);
            ParsingDialogCancelCommand = new RelayCommand(ParsingDialogCancel);


            BtnDeleteLogAllCommand = new RelayCommand(BtnDeleteLogAll);
            BtnSaveLogDataCommand = new RelayCommand(BtnSaveLogData);
            StopParseCommand = new RelayCommand(StopParse);
            StopPerformanceTestCommand = new RelayCommand(StopPerformanceTest);
            StopTestCommand = new RelayCommand(StopTest);
            PauseTestCommand = new RelayCommand(PauseTest);
            RestartTestCommand = new RelayCommand(RestartTest);
            StepTestCommand = new RelayCommand(StepTest);

            TestDocumentSettingClickCommand = new RelayCommand(TestDocumentSettingClick);
            T1ConfigSettingClickCommand = new RelayCommand(T1ConfigSettingClick);
            ETCConfigSettingClickCommand = new RelayCommand(ETCConfigSettingClick);

            T1InstallPathBrowserCommand = new RelayCommand(T1InstallPathBrowser);
            T1ProjectPathBrowserCommand = new RelayCommand(T1ProjectPathBrowser);
            T1ReportPathBrowserCommand = new RelayCommand(T1ReportPathBrowser);

            DeleteCanLogDataCommand = new RelayCommand<object>(DeleteCanLogData);
            DeleteConstateCommand = new RelayCommand<object>(DeleteConstate);
            DeleteT1ScenarioCommand = new RelayCommand<object>(DeleteT1Scenario);

            AddConstateCommand = new RelayCommand<object>(AddConstate);
            OKConstateCommand = new RelayCommand(OKConstateFunction);

            ShowPopupConstraintStatusCommand = new RelayCommand<object>(ShowPopupConstraintStatus);
            CheckedChangedTimingParamCommand = new RelayCommand<object>(CheckedChangedTimingParam);

            EditT1ScenarioCommand = new RelayCommand<object>(EditT1ScenarioFunction);
            InsertScenarioCommand = new RelayCommand(InsertScenarioFunction);
            NewScenarioCommand = new RelayCommand(NewScenarioFunction);
            LoadScenarioCommand = new RelayCommand<object>(LoadScenarioFunction);
            SaveScenarioCommand = new RelayCommand(SaveScenarioFunction);
            SaveAsScenarioCommand = new RelayCommand(SaveAsScenarioFunction);
            SelectT1ScenarioBrowserCommand = new RelayCommand<object>(SelectT1ScenarioBrowserFunction);

            RemoveScenarioCommand = new RelayCommand<object>(RemoveScenarioFunction);

            CreateFolder();

            MainModel.CommonTestDate = DateTime.Now.ToString("yyyy.MM.dd  HH:mm");

            CANPropertyModelList = new ObservableCollection<CANDBPropertyModel>();
            CANPropertyModelList.Add(new CANDBPropertyModel());
            

            DisplayCANDBProperty = new CANDBPropertyModel();
            UpdateItemVisible();
        }
        private void CreateFolder()
        {
            if (Common.Common.WORKSPACE_PATH == null)
                return;

            DirectoryInfo di = new DirectoryInfo(Common.Common.WORKSPACE_PATH);
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\TestCase");
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\Report");
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\T1Report");
            if (!di.Exists)
                di.Create();

            di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\Temp");
            if (!di.Exists)
                di.Create();
        }
        
        public bool ReadConfigFile(string path)
        {
            try
            {
                InitConfigData();

                IVSLog.getInstance().Log("Read Config File #" + path, Common.Common.MODULE_VIEW, ConsoleColor.Yellow);
                string[] Project_Cfg = CommonUtil.FileRead(path);
                CANPropertyModelList.Clear();
                MainModel.CanLogFileList.Clear();
                MainModel.CANDB_item_count = 1;

                if (Project_Cfg == null)
                    return false;

                for (int i = 0; i < Project_Cfg.Length; i++)
                {
                    string[] temp = Project_Cfg[i].Split(new string[] { "=$=" }, StringSplitOptions.None);

                    if (temp == null) continue;

                    if (temp[0].Equals("Tester"))
                        MainModel.CommonTester = temp[1];
                    else if (temp[0].Equals("SWVersion"))
                        MainModel.CommonSWVersion = temp[1];
                    else if (temp[0].Equals("Comment"))
                        MainModel.CommonComment = temp[1];
                    else if (temp[0].Equals("TargetName"))
                        MainModel.CommonTargetName = temp[1];
                    else if (temp[0].Equals("CAN_BaudRate"))
                        CANPropertyModelList[0].CAN_BaudRate = Convert.ToInt32(temp[1]);
                    else if (temp[0].Equals("TestCaseParser"))
                        MainModel.TestCaseParserSelectItem = temp[1];
                    else if (temp[0].Equals("CANProperty"))
                    {
                        string[] str = temp[1].Split(new string[] { "$$" }, StringSplitOptions.None);
                        CANDBPropertyModel model = new CANDBPropertyModel();
                        model.CAN_BaudRate = Int32.Parse(str[0]);
                        model.CAN_TSEG1 = Int32.Parse(str[1]);
                        model.CAN_TSEG2 = Int32.Parse(str[2]);
                        model.CAN_SJW = Int32.Parse(str[3]);
                        model.CANFD_BaudRate = Int32.Parse(str[4]);
                        model.CANFD_TSEG1 = Int32.Parse(str[5]);
                        model.CANFD_TSEG2 = Int32.Parse(str[6]);
                        model.CANFD_SJW = Int32.Parse(str[7]);
                        model.CANFD_BRS = Boolean.Parse(str[8]);
                        if (str[9].Contains(Common.Common.WORKSPACE_PATH))
                            model.CAN_DBC_Path = "$WORKSPACE$" + str[9].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            model.CAN_DBC_Path = str[9];

                        CANPropertyModelList.Add(model);
                    }
                    else if (temp[0].Equals("CANDB_item_count"))
                        MainModel.CANDB_item_count = Int32.Parse(temp[1]);
                    else if (temp[0].Equals("TestcasePath"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.TestCasePath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.TestCasePath = temp[1];
                    }
                    else if (temp[0].Equals("TestSpecificationPath"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.TestSpecificationPath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.TestSpecificationPath = temp[1];
                    }
                    else if (temp[0].Equals("ELFPath"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.ELFFilePath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.ELFFilePath = temp[1];
                    }
                    else if (temp[0].Equals("LogLevel"))
                        MainModel.LogLevelIndex = Convert.ToInt32(temp[1]);
                    else if (temp[0].Equals("TestDelay"))
                        MainModel.TestStepDelay = Convert.ToInt32(temp[1]);
                    else if (temp[0].Equals("IsELFDownload"))
                    {
                        if (temp[1] == "true")
                            MainModel.IsELFDownload = true;
                        else
                            MainModel.IsELFDownload = false;
                    }
                    else if (temp[0].Equals("TestCaseParserSelectItem"))
                        MainModel.TestCaseParserSelectItem = temp[1];
                    else if (temp[0].Equals("T1InstallPath"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.T1InstallPath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.T1InstallPath = temp[1];
                    }
                    else if (temp[0].Equals("T1ProjectPath"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.T1ProjectPath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.T1ProjectPath = temp[1];
                    }
                    else if (temp[0].Equals("T1ReportPath"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.T1ReportPath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.T1ReportPath = temp[1];
                    }
                    else if (temp[0].Equals("CanLogFileList"))
                    {
                        string[] str = temp[1].Split(new string[] { "$$" }, StringSplitOptions.None);
                        CanLogFileModel model = new CanLogFileModel();
                        model.Index = Int32.Parse(str[0]);
                        model.FileName = str[1];
                        model.FullPath = str[2];
                        MainModel.CanLogFileList.Add(model);
                    }
                    else if (temp[0].Equals("PreHookScript"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            MainModel.PreHookScriptPath = "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        else
                            MainModel.PreHookScriptPath = temp[1];
                    }
                    else if (temp[0].Equals("T1ScenarioList"))
                    {
                        if (temp[1].Contains(Common.Common.WORKSPACE_PATH))
                            T1ScenarioModelList.Add(new T1ScenarioModel() {ScenarioXmlPath= "$WORKSPACE$" + temp[1].Replace(Common.Common.WORKSPACE_PATH + @"\", "") });
                        else
                            T1ScenarioModelList.Add(new T1ScenarioModel() { ScenarioXmlPath = temp[1]});
                    }
                }

                if(CANPropertyModelList.Count == 0)
                    CANPropertyModelList.Add(new CANDBPropertyModel());
                UpdateItemVisible();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(MainViewModel).Name + " :: " + ex.Message + " Line :: " + (ex));
                return false;
            }
            return true;
        }
        private void InitConfigData()
        {
            MainModel.CommonTester = "";
            MainModel.CommonSWVersion = "";
            MainModel.CommonComment = "";
            MainModel.CommonTargetName = "";
            CANPropertyModelList.Clear();

            MainModel.CANDB_item_count = 1;
            MainModel.TestCasePath = "";
            MainModel.TestSpecificationPath = "";
            MainModel.ELFFilePath = "";
            MainModel.LogLevelIndex = 0;
            MainModel.TestStepDelay = 0;
            MainModel.TestCaseParserSelectItem = null;
            MainModel.IsELFDownload = false;
        }
        private void TestSpecificationBrowser(object e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = false;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                    MainModel.TestSpecificationPath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                else
                    MainModel.TestSpecificationPath = dialog.FileName;
            }
        }
        private void T1ScenarioLogPathBrowser(object e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = true;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (dialog.FileName.Equals(Common.Common.WORKSPACE_PATH))
                    MainModel.EditT1ScenarioModel.CanLogPath = "$WORKSPACE$";
                else if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                    MainModel.EditT1ScenarioModel.CanLogPath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                else
                    MainModel.EditT1ScenarioModel.CanLogPath = dialog.FileName;
            }
        }
        private void ELFFileBrowser()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = false;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                {
                    MainModel.ELFFilePath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                }
                else
                {
                    MainModel.ELFFilePath = dialog.FileName;
                }
            }
        }
        private void PreHookScriptBrowser()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = false;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                {
                    MainModel.PreHookScriptPath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                }
                else
                {
                    MainModel.PreHookScriptPath = dialog.FileName;
                }
            }
        }
        private void DBCBrowser(object e)
        {
            try
            {
                string index = e.ToString();

                CommonOpenFileDialog dialog = new CommonOpenFileDialog();

                //dialog.InitialDirectory = @"C:\";
                dialog.IsFolderPicker = false;
                dialog.RestoreDirectory = false;
                CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

                if (commonFileDialogResult == CommonFileDialogResult.Ok)
                {
                    if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                    {
                        if (index == "1")
                        {
                            CANPropertyModelList[0].CAN_DBC_Path = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        }
                        else if (index == "2")
                        {
                            CANPropertyModelList[1].CAN_DBC_Path = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        }
                        else if (index == "3")
                        {
                            CANPropertyModelList[2].CAN_DBC_Path = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        }
                        else if (index == "4")
                        {
                            CANPropertyModelList[3].CAN_DBC_Path = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                        }
                    }
                    else
                    {
                        if (index == "1")
                        {
                            CANPropertyModelList[0].CAN_DBC_Path = dialog.FileName;
                        }
                        else if (index == "2")
                        {
                            CANPropertyModelList[1].CAN_DBC_Path = dialog.FileName;
                        }
                        else if (index == "3")
                        {
                            CANPropertyModelList[2].CAN_DBC_Path = dialog.FileName;
                        }
                        else if (index == "4")
                        {
                            CANPropertyModelList[3].CAN_DBC_Path = dialog.FileName;
                        }
                    }


                }
            }
            catch(Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(MainViewModel).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }
        private void TestcaseBrowser()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = false;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                {
                    MainModel.TestCasePath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                }
                else
                {
                    MainModel.TestCasePath = dialog.FileName;
                }
            }
        }



        public void ShowWarningDialog(string message)
        {
            MainModel.WarningDialogMessage = message;
            MainModel.DialogContent = new WarningDialog();
            MainModel.isDialogOpen = true;
        }
        public void ShowLoadT1ProjectInfoDialog()
        {
            MainModel.DialogContent = new ProgressDialogView();
            MainModel.isDialogOpen = true;
        }
        public void ShowWarningDialogTest(string message)
        {
            MainModel.WarningDialogMessage = message;
            MainModel.TestDialogContent = new WarningDialog();
            MainModel.IsTestStatusDialogOpen = true;
        }
        public void ShowContinueTestDialogTest()
        {
            MainModel.TestDialogContent = new TestContinueDialogView();
            MainModel.IsTestStatusDialogOpen = true;
        }

        public void TestContinueDialogOK()
        {
            MainModel.IsTestStatusDialogOpen = false;
            Thread.Sleep(1000);

            MainModel.IsReTestFlag = true;
            FunctionWorkHandler.getInstance().RestartTest();
            
        }
        public void TestContinueDialogNo()
        {
            MainModel.IsTestStatusDialogOpen = false;
            StopTest();
        }
        public void ShowSelectTestSheetDialog()
        {
            MainModel.DialogContent = new SelectTestSheetDialogView();
            MainModel.isDialogOpen = true;
        }

        
        private void SelectTestSheetDialogOK()
        {
            MainModel.isDialogOpen = false;
            FunctionWorkHandler.getInstance().TestStart();
        }
        private void TestCaseParseWarningDialogYES()
        {
            TestcaseBrowser();
            MainModel.isDialogOpen = false;
        }
        private void TestCaseParseWarningDialogNO()
        {
            MainModel.GUIAppMode = Common.Common.MANUAL_TEST_MODE;
            FunctionWorkHandler.getInstance().ParseStart();
            MainModel.TestCasePath = "";
            //MainModel.isDialogOpen = false;
        }
        

        private void DialogCancel(object e)
        {
            if (e != null && e.ToString().Equals("Save"))
            {
                if (MainModel.CurrentSaveMode == Common.Common.SAVEMODE_BEFORE_END)
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            MainModel.isDialogOpen = false;
            MainModel.IsTestStatusDialogOpen = false;
            CloseTestStatusView();
            ClosePerformanceTestStatusView();
        }
        private void TestDialogCancel()
        {
            CloseTestStatusView();
            ClosePerformanceTestStatusView();
        }
        public void ParsingDialogCancel()
        {
            MainModel.IsParsingStatusDialogOpen = false;
            CloseParsingStatusView();
        }

        private void BtnDeleteLogAll()
        {
            if(MainModel.LogData !=null)
                MainModel.LogData.Clear();
        }
        private void BtnSaveLogData()
        {
            List<string> outputList = new List<string>();
            if (MainModel.LogData.Count == 0)
            {
                showTrackBarMessage("Log Message is Empty");
                return;
            }

            try
            {
                foreach (LogDataModel data in MainModel.LogData)
                    outputList.Add(data.MainContent + data.SecondContent + data.ThirdContent);

                using (TextWriter tw = new StreamWriter(Common.Common.WORKSPACE_PATH + "\\Temp\\LogData.txt"))
                {
                    foreach (String s in outputList)
                        tw.WriteLine(s);

                    tw.Close();
                }
                ViewModelLocator.MainVM.showTrackBarMessage("Log Message is Saved (LogData.txt)");
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, typeof(MainViewModel).Name + " :: " + ex.Message + " Line :: " + (ex));
            }
        }
        private void StopParse()
        {
            Process[] processList = Process.GetProcesses();
            foreach(Process proc in processList)
            {
                if (proc.ProcessName.Equals("python"))
                    proc.Kill();
                if (proc.ProcessName.Equals("EXCEL"))
                    proc.Kill();
            }

            CloseParsingStatusView();
            
        }
        public void StopTest()
        {
            MainModel.TestStateMode = Common.Common.STOP;
            FunctionWorkHandler.getInstance().StopTest();

            CloseTestStatusView();

            BackgroundWorker WriteExcelWorker = new BackgroundWorker();
            WriteExcelWorker.DoWork += WriteExcelWorker_DoWork;
            WriteExcelWorker.RunWorkerCompleted += WriteExcelWorker_DoWorkComplete;
            WriteExcelWorker.RunWorkerAsync();
        }
        private void WriteExcelWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ExcelParser.getInstance().WriteExcelData(ViewModelLocator.MainVM.MainModel.ReportFilePath);
        }
        private void WriteExcelWorker_DoWorkComplete(object sender,RunWorkerCompletedEventArgs e)
        {
            IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_TINFO, "Test Results Interim Save ");
            ViewModelLocator.ToolBarVM.OpenReport();
        }
        public void StopPerformanceTest()
        {
            FunctionWorkHandler.getInstance().PerformanceTestWorker.CancelAsync();
            CommonUtil.ExitPythonProcess();
            ClosePerformanceTestStatusView();
        }
        public void PauseTest()
        {
            FunctionWorkHandler.getInstance().PauseTest();
            MainModel.TestStateMode = Common.Common.PAUSE;
        }
        public void RestartTest()
        {
            FunctionWorkHandler.getInstance().RestartTest();
            MainModel.TestStateMode = Common.Common.RUNNING;
        }
        public void StepTest()
        {
            FunctionWorkHandler.getInstance().StepTest();
            MainModel.TestStateMode = Common.Common.STEP_RUNNING;
        }
        public void showTrackBarMessage(string message)
        {
            if(MainModel.GUIStartup)
            Task.Factory.StartNew(() => MainModel.TrackBarMessageQueue.Enqueue(message, null, null, null, false, true, TimeSpan.FromSeconds(1)));
        }

        public void ShowPopupTestStatus()
        {
            MainModel.IsTestStatusDialogOpen = false;

            //FunctionWorkHandler.getInstance().Process_TestRun();

            Common.Common.m_PopupTestStatusView = new PopupTestStatusView();

            Common.Common.m_PopupTestStatusView.Width = 550;
            Common.Common.m_PopupTestStatusView.Height = 200;
            Common.Common.m_PopupTestStatusView.Owner = App.Current.MainWindow;
            Common.Common.m_PopupTestStatusView.WindowStartupLocation = WindowStartupLocation.Manual;
            Common.Common.m_PopupTestStatusView.Top = App.Current.MainWindow.Top;
            Common.Common.m_PopupTestStatusView.Left = App.Current.MainWindow.Left + ((App.Current.MainWindow.Width - Common.Common.m_PopupTestStatusView.Width) / 2);

            Common.Common.m_PopupTestStatusView.Show();
            MainModel.TransitionIndex = 1;
        }
        private void CloseTestStatusView()
        {
            MainModel.IsTestStatusDialogOpen = false;
            if (Common.Common.m_PopupTestStatusView != null)
            {
                Common.Common.m_PopupTestStatusView.Close();
                Common.Common.m_PopupTestStatusView = null;
            }
        }

        public void ShowPopupParsingStatus()
        {
            /*MainModel.DialogContent = new TestCaseParsingProgressDialogView();
            MainModel.isDialogOpen = true;*/
            //MainModel.IsParsingStatusDialogOpen = false;
            MainModel.isDialogOpen = false;
             Common.Common.m_PopupParsingView = new PopupParsingStatusView();

            Common.Common.m_PopupParsingView.Width = 450;
            Common.Common.m_PopupParsingView.Height = 160;
            Common.Common.m_PopupParsingView.Owner = App.Current.MainWindow;
            Common.Common.m_PopupParsingView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            Common.Common.m_PopupParsingView.Show();

        }
        public void ShowPopupConstraintStatus(object e)
        {
            Common.Common.m_PopupConstraintSettingView = new PopupConstraintSettingView("Constraint Setting (" + MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.CoreName + ")", MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel);
            Common.Common.m_PopupConstraintSettingView.Owner = App.Current.MainWindow;
            Common.Common.m_PopupConstraintSettingView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            Common.Common.m_PopupConstraintSettingView.Show();

        }
        private void CheckedChangedTimingParam(object e)
        {
            TimingParamterModel CheckedModel = (TimingParamterModel)e;
            if (CheckedModel == null)
                return;

            string TimingTypeString = "";

            switch (CheckedModel.TimingType)
            {
                case Common.Common.CET:
                    TimingTypeString = "CET";
                    break;
                case Common.Common.DT:
                    TimingTypeString = "DT";
                    break;
                case Common.Common.GET:
                    TimingTypeString = "GET";
                    break;
                case Common.Common.IPT:
                    TimingTypeString = "IPT";
                    break;
                case Common.Common.PER:
                    TimingTypeString = "PER";
                    break;
                case Common.Common.ST:
                    TimingTypeString = "ST";
                    break;
                case Common.Common.RT:
                    TimingTypeString = "RT";
                    break;
            }


            int index = 0;
            for (int i = 0; i < ViewModelLocator.MainVM.T1ProjectInfoList.Count; i++)
            {
                if (ViewModelLocator.MainVM.T1ProjectInfoList[i].CoreName == ViewModelLocator.MainVM.MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.CoreName)
                {
                    index = i;
                    break;
                }
            }

            

            if (CheckedModel.IsChecked)
            {
                ObservableCollection<string> tempCoreList = new ObservableCollection<string>();
                foreach (T1ProjectInfoModel str in ViewModelLocator.MainVM.T1ProjectInfoList)
                    tempCoreList.Add(str.CoreName);

                for (int i = 0; i < ViewModelLocator.MainVM.T1ProjectInfoList[index].DataList.Count; i++)
                {

                    ConstrateModel insertModel = new ConstrateModel();
                    
                    
                    //insertModel.SelectedCoreItem = ViewModelLocator.MainVM.T1ProjectInfoList[index].CoreName;
                    insertModel.SelectedTaskItem = ViewModelLocator.MainVM.T1ProjectInfoList[index].DataList[i];
                    insertModel.SelectedTypeItem = TimingTypeString;

                    insertModel.Value = 0;
                    insertModel.TaskISRList = ViewModelLocator.MainVM.T1ProjectInfoList[index].DataList;
                    //insertModel.CoreList = tempCoreList;
                    if (!MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList.Contains(insertModel))
                        MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList.Add(insertModel);
            }
            }
            else
            {
                ObservableCollection<ConstrateModel> RemoveColl = new ObservableCollection<ConstrateModel>();
                for (int i = 0; i < MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList.Count; i++)
                {
                    if (MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.CoreName == ViewModelLocator.MainVM.MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.CoreName && MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList[i].SelectedTypeItem == TimingTypeString)
                        RemoveColl.Add(MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList[i]);
                }

                foreach(ConstrateModel removemodel in RemoveColl)
                {
                    MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList.Remove(removemodel);
                }
            }

            List<TimingParamterModel> CheckedList = new List<TimingParamterModel>();
            foreach (TimingParamterModel model in MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList)
            {
                if (model.IsChecked && model.IsEnable)
                    CheckedList.Add(model);
            }


            for (int i = 0; i < MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList.Count; i++)
            {
                bool TypeCheck = false;
                foreach(TimingParamterModel model in CheckedList)
                {
                    if (model.TimingType == MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList[i].TimingType)
                        TypeCheck = true;
                }

                if (TypeCheck) // 체크가 되지 않은 값만 확인
                    continue;

                MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList[i].IsEnable = true;

                foreach (TimingParamterModel model1 in CheckedList)
                {
                    if (model1.StartParamter == MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList[i].StartParamter || model1.EndParamter == MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList[i].EndParamter)
                        MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.TimingParamterModelList[i].IsEnable = false;
                }
            }
        }
        private void InsertScenarioFunction()
        {
            T1ScenarioModelList.Add(new T1ScenarioModel());
        }
        private void NewScenarioFunction()
        {
            loadscenario_path = "";
            ObservableCollection<T1ScenarioCoreModel> tempList = new ObservableCollection<T1ScenarioCoreModel>();
            for (int i = 0; i < ViewModelLocator.MainVM.T1ProjectInfoList.Count; i++)
            {
                ObservableCollection<TimingParamterModel> tempTimingParamList = new ObservableCollection<TimingParamterModel>();
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.CET, StartParamter = Common.Common.TIMING_PARAM_START, EndParamter = Common.Common.TIMING_PARAM_STOP });
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.DT, StartParamter = Common.Common.TIMING_PARAM_START, EndParamter = Common.Common.TIMING_PARAM_START });
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.GET, StartParamter = Common.Common.TIMING_PARAM_START, EndParamter = Common.Common.TIMING_PARAM_STOP });
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.IPT, StartParamter = Common.Common.TIMING_PARAM_ACTIVATION, EndParamter = Common.Common.TIMING_PARAM_START });
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.RT, StartParamter = Common.Common.TIMING_PARAM_ACTIVATION, EndParamter = Common.Common.TIMING_PARAM_STOP });
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.ST, StartParamter = Common.Common.TIMING_PARAM_STOP, EndParamter = Common.Common.TIMING_PARAM_ACTIVATION });
                tempTimingParamList.Add(new TimingParamterModel() { TimingType = Common.Common.PER, StartParamter = Common.Common.TIMING_PARAM_ACTIVATION, EndParamter = Common.Common.TIMING_PARAM_ACTIVATION });

                tempList.Add(new T1ScenarioCoreModel() { CoreName = "Core " + i, TimingParamterModelList = tempTimingParamList });
            }
            MainModel.EditT1ScenarioModel = new T1ScenarioModel() { CanLogPath = "", T1ScenarioCoreModelList = tempList };
            ShowEditScenarioPopup();
        }
        string loadscenario_path = "";
        private void LoadScenarioFunction(object e)
        {
            if (e != null)
            {
                T1ScenarioModel model = e as T1ScenarioModel;
                XmlSerializer xs = new XmlSerializer(typeof(T1ScenarioModel));
                using (Stream fstream = File.OpenRead(model.ScenarioXmlPath))
                {
                    MainModel.EditT1ScenarioModel = xs.Deserialize(fstream) as T1ScenarioModel;
                }
                ShowEditScenarioPopup();
            }
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "XML Files (*.xml)|*.xml";

            if (dialog.ShowDialog() == true)
            {
                loadscenario_path = dialog.FileName;
                XmlSerializer xs = new XmlSerializer(typeof(T1ScenarioModel));
                using (Stream fstream = File.OpenRead(dialog.FileName))
                {
                    MainModel.EditT1ScenarioModel = xs.Deserialize(fstream) as T1ScenarioModel;
                }
                ShowEditScenarioPopup();
            }
        }
        private void ShowEditScenarioPopup()
        {
            Common.Common.m_PopupScenarioSettingView = new PopupScenarioSettingView();
            Common.Common.m_PopupScenarioSettingView.Owner = App.Current.MainWindow;
            Common.Common.m_PopupScenarioSettingView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            Common.Common.m_PopupScenarioSettingView.Show();
        }
        private void SaveScenarioFunction()
        {
            if (loadscenario_path == "")
            {
                Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
                dialog.Filter = "XML Files (*.xml)|*.xml";

                if (dialog.ShowDialog() == true)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T1ScenarioModel));

                    using (var sw = new StreamWriter(dialog.FileName))
                    {
                        serializer.Serialize(sw, MainModel.EditT1ScenarioModel);
                        sw.Close();
                    }
                    MainModel.EditT1ScenarioModel.ScenarioXmlPath = dialog.FileName;
                }
            }
            else
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T1ScenarioModel));

                using (var sw = new StreamWriter(loadscenario_path))
                {
                    serializer.Serialize(sw, MainModel.EditT1ScenarioModel);
                    sw.Close();
                }
            }

        }
        private void SaveAsScenarioFunction()
        {
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.Filter = "XML Files (*.xml)|*.xml";

            if (dialog.ShowDialog() == true)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T1ScenarioModel));

                using (var sw = new StreamWriter(dialog.FileName))
                {
                    serializer.Serialize(sw, MainModel.EditT1ScenarioModel);
                    sw.Close();
                }
                MainModel.EditT1ScenarioModel.ScenarioXmlPath = dialog.FileName;
            }
        }
        public void SelectT1ScenarioBrowserFunction(object e)
        {
            if (e == null)
                return;

            T1ScenarioModel model = e as T1ScenarioModel;

            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "XML Files (*.xml)|*.xml";

            if (dialog.ShowDialog() == true)
            {
                T1ScenarioModelList.Where(p => p == model).FirstOrDefault().ScenarioXmlPath = dialog.FileName;
            }
        }
        private void RemoveScenarioFunction(object e)
        {
            if (e == null)
                return;

            T1ScenarioModel model = e as T1ScenarioModel;

            T1ScenarioModelList.Remove(model);
        }
        public void EditT1ScenarioFunction(object e)
        {
            if (e == null)
                return;

            T1ScenarioModel model = e as T1ScenarioModel;

            XmlSerializer xs = new XmlSerializer(typeof(T1ScenarioModel));

            if (!File.Exists(model.ScenarioXmlPath))
            {
                showTrackBarMessage("Not Exist Scenario XML File...");
                return;
            }
            using (Stream fstream = File.OpenRead(model.ScenarioXmlPath))
            {
                MainModel.EditT1ScenarioModel = xs.Deserialize(fstream) as T1ScenarioModel;
            }
            ShowEditScenarioPopup();
        }
        public void CloseParsingStatusView()
        {
            //MainModel.IsParsingStatusDialogOpen = false;
            if (Common.Common.m_PopupParsingView != null)
            {
                Common.Common.m_PopupParsingView.Close();
                Common.Common.m_PopupParsingView = null;
            }
        }

        public void ShowPopupPerformanceTestStatus()
        {
            MainModel.isDialogOpen = false;
            Common.Common.m_PopupPerformanceTestStatusView = new PopupPerformanceTestStatusView();

            Common.Common.m_PopupPerformanceTestStatusView.Width = 550;
            Common.Common.m_PopupPerformanceTestStatusView.Height = 140;
            Common.Common.m_PopupPerformanceTestStatusView.Owner = App.Current.MainWindow;
            Common.Common.m_PopupPerformanceTestStatusView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            Common.Common.m_PopupPerformanceTestStatusView.Show();

        }

        public void ClosePerformanceTestStatusView()
        {
            MainModel.IsTestStatusDialogOpen = false;
            if (Common.Common.m_PopupPerformanceTestStatusView != null)
            {
                Common.Common.m_PopupPerformanceTestStatusView.Close();
                Common.Common.m_PopupPerformanceTestStatusView = null;
            }
        }

        public void UpdateItemVisible()
        {
            MainModel.CANDB_Item_1_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Item_2_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Item_3_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Item_4_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Minus_Button_1_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Minus_Button_2_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Minus_Button_3_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Minus_Button_4_Visibility = Common.Common.FALSE;
            MainModel.CANDB_Plus_Item_Visibility = Common.Common.FALSE;

            switch (MainModel.CANDB_item_count)
            {
                case 1:
                    MainModel.CANDB_Item_1_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Plus_Item_Visibility = Common.Common.TRUE;
                    break;
                case 2:
                    MainModel.CANDB_Item_1_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Item_2_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Minus_Button_2_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Plus_Item_Visibility = Common.Common.TRUE;
                    break;
                case 3:
                    MainModel.CANDB_Item_1_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Item_2_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Item_3_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Minus_Button_3_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Plus_Item_Visibility = Common.Common.TRUE;
                    break;
                case 4:
                    MainModel.CANDB_Item_1_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Item_2_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Item_3_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Item_4_Visibility = Common.Common.TRUE;
                    MainModel.CANDB_Minus_Button_4_Visibility = Common.Common.TRUE;
                    break;
            }
        }

        public void ConsoleMode_ParseMode()
        {
            string inputData;
            inputData = "";
            IVSLog.getInstance().Log("Start Working On Parsing? [Y/N]", Common.Common.MODULE_VIEW, ConsoleColor.Cyan);
            inputData = Console.ReadLine();

            if (inputData == "N" || inputData == "n")
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
                return;
            }
            else
            {

                DirectoryInfo di = new DirectoryInfo(Common.Common.WORKSPACE_PATH);
                if (!di.Exists)
                    di.Create();

                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\TestCase");
                if (!di.Exists)
                    di.Create();

                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\Report");
                if (!di.Exists)
                    di.Create();

                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\T1Report");
                if (!di.Exists)
                    di.Create();

                di = new DirectoryInfo(Common.Common.WORKSPACE_PATH + "\\Temp");
                if (!di.Exists)
                    di.Create();


                FunctionWorkHandler.getInstance().ParseStart();
            }
        }
        public void ConsoleMode_TestMode()
        {
            string inputData;
            inputData = "";
            IVSLog.getInstance().Log("Start Interface Test? [Y/N]", Common.Common.MODULE_VIEW, ConsoleColor.Cyan);
            inputData = Console.ReadLine();

            if (inputData == "N" || inputData == "n")
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
                return;
            }
            else
            {
                FunctionWorkHandler.getInstance().TestStart();
            }
        }
        public void ConsoleMode_ConfigMode()
        {
            string inputData;
            IVSLog.getInstance().Log("Input Config File Path", Common.Common.MODULE_VIEW, ConsoleColor.Cyan);
            inputData = Console.ReadLine();
            if (!File.Exists(inputData))
            {
                ConsoleMode_ConfigMode();
                return;
            }

            ViewModelLocator.MainVM.ReadConfigFile(inputData);
            ConsoleMode_ParseMode();
        }
        private void TestDocumentSettingClick()
        {
            MainModel.SettingDrawerIsOpen = true;
            MainModel.IsTestDocumentVisible = Visibility.Visible;
            MainModel.IsT1ConfigSettingVisible = Visibility.Hidden;
            MainModel.IsT1ScenarioSettingVisible = Visibility.Hidden;
            MainModel.IsETCConfigSettingVisible = Visibility.Hidden;
        }
        private void T1ConfigSettingClick()
        {
            MainModel.SettingDrawerIsOpen = true;
            MainModel.IsTestDocumentVisible = Visibility.Hidden;
            MainModel.IsT1ConfigSettingVisible = Visibility.Visible;
            MainModel.IsT1ScenarioSettingVisible = Visibility.Hidden;
            MainModel.IsETCConfigSettingVisible = Visibility.Hidden;
        }
        private void ETCConfigSettingClick()
        {
            MainModel.SettingDrawerIsOpen = true;
            MainModel.IsTestDocumentVisible = Visibility.Hidden;
            MainModel.IsT1ConfigSettingVisible = Visibility.Hidden;
            MainModel.IsT1ScenarioSettingVisible = Visibility.Hidden;
            MainModel.IsETCConfigSettingVisible = Visibility.Visible;
        }
        private void T1InstallPathBrowser()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = true;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                MainModel.T1InstallPath = dialog.FileName;
            }
        }
        private void T1ProjectPathBrowser()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = false;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (dialog.FileName.Contains(Common.Common.WORKSPACE_PATH))
                    MainModel.T1ProjectPath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
                else
                    MainModel.T1ProjectPath = dialog.FileName;

                    PythonHandler.getInstance().GetT1ProjectInfo();
            }
        }
        private void T1ReportPathBrowser()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = true;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                MainModel.T1ReportPath = "$WORKSPACE$" + dialog.FileName.Replace(Common.Common.WORKSPACE_PATH + @"\", "");
            }
        }
        private void DeleteCanLogData(object e)
        {
            if (e == null)
                return;

            MainModel.CanLogFileList.RemoveAt(Convert.ToInt32(e.ToString()));
        
        }
        private void DeleteConstate(object e)
        {
            for (int i=0;i< T1ScenarioModelList.Count;i++)
            {
                if (MainModel.EditT1ScenarioModel.Equals(T1ScenarioModelList[i]))
                    T1ScenarioModelList[i].SelectedT1ScenarioCoreModel.ConstrateModelList.Remove((ConstrateModel)e);
            }
        }
        private void DeleteT1Scenario(object e)
        {
            T1ScenarioModelList.Remove((T1ScenarioModel)e);
        }
        private void AddConstate(object e)
        {
            T1ScenarioCoreModel model = (T1ScenarioCoreModel)e;
            int index = 0;

            for(int i=0;i< ViewModelLocator.MainVM.T1ProjectInfoList.Count; i++)
            {
                if (ViewModelLocator.MainVM.T1ProjectInfoList[i].CoreName == model.CoreName)
                {
                    index = i;
                    break;
                }
            
            }
            ViewModelLocator.MainVM.MainModel.EditT1ScenarioModel.SelectedT1ScenarioCoreModel.ConstrateModelList.Add(new ConstrateModel() { TaskISRList= ViewModelLocator.MainVM.T1ProjectInfoList[index].DataList});
        }
        private void OKConstateFunction()
        {
            if (Common.Common.m_PopupConstraintSettingView != null)
            {
                Common.Common.m_PopupConstraintSettingView.Close();
                Common.Common.m_PopupConstraintSettingView = null;
            }
        }

    }
}