﻿using GalaSoft.MvvmLight.Command;
using PROST_Integration.Dialog;
using PROST_Integration.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.ViewModel
{
    public class ToolBarViewModel
    {
        public RelayCommand MakeTestCaseCommand
        {
            get; set;
        }
        public RelayCommand TestStartCommand
        {
            get; set;
        }
        public RelayCommand TestStepStartCommand
        {
            get; set;
        }
        public RelayCommand AutoTestStartCommand
        {
            get; set;
        }
        public RelayCommand PerformanceTestStartCommand
        {
            get; set;
        }
        public RelayCommand OpenReportCommand
        {
            get; set;
        }
        public RelayCommand ShowLogCommand
        {
            get; set;
        }
        public RelayCommand ShowMenuCommand
        {
            get; set;
        }
        public ToolBarViewModel()
        {
            MakeTestCaseCommand = new RelayCommand(MakeTestCase);
            TestStartCommand = new RelayCommand(TestStart);
            TestStepStartCommand = new RelayCommand(TestStepStart);
            AutoTestStartCommand = new RelayCommand(AutoTestStart);
            PerformanceTestStartCommand = new RelayCommand(PerformanceTestStart);
            OpenReportCommand = new RelayCommand(OpenReport);
            ShowLogCommand = new RelayCommand(ShowLog);
            ShowMenuCommand = new RelayCommand(ShowMenu);
        }
        private void TestStart()
        {
            ViewModelLocator.MainVM.MainModel.CurrentSaveMode = Common.Common.SAVEMODE_STANDARD;
            ViewModelLocator.MenuBarVM.SaveConfigDialogOK();
            ViewModelLocator.MainVM.MainModel.GUIAppMode = Common.Common.MANUAL_TEST_MODE;
            ViewModelLocator.MainVM.ShowSelectTestSheetDialog();
        }
        private void TestStepStart()
        {
            ViewModelLocator.MainVM.MainModel.CurrentSaveMode = Common.Common.SAVEMODE_STANDARD;
            ViewModelLocator.MenuBarVM.SaveConfigDialogOK();
            ViewModelLocator.MainVM.MainModel.GUIAppMode = Common.Common.MANUAL_TEST_MODE;
            ViewModelLocator.MainVM.MainModel.TestStateMode = Common.Common.STEP_RUNNING;
            FunctionWorkHandler.getInstance().PauseTest();
            ViewModelLocator.MainVM.ShowSelectTestSheetDialog();
        }
        private void AutoTestStart()
        {
            ViewModelLocator.MainVM.MainModel.GUIAppMode = Common.Common.AUTO_TEST_MODE;
            FunctionWorkHandler.getInstance().ParseStart();
        }
        private void PerformanceTestStart() // T1 연동 테스트
        {
            ViewModelLocator.MainVM.MainModel.CurrentSaveMode = Common.Common.SAVEMODE_STANDARD;
            ViewModelLocator.MenuBarVM.SaveConfigDialogOK();
            ViewModelLocator.MainVM.MainModel.PerformanceTestStatusText = "Start Performance Test..";
            FunctionWorkHandler.getInstance().TRACE32PerformanceTimeoutCheck();
        }
        public void OpenReport()
        {
            if (ViewModelLocator.MainVM.MainModel.ReportFilePath != null && ViewModelLocator.MainVM.MainModel.ReportFilePath != "")
                Process.Start("explorer.exe", ViewModelLocator.MainVM.MainModel.ReportFilePath);
        }

        private void MakeTestCase()
        {
            ViewModelLocator.MainVM.MainModel.DialogContent = new TestCaseParseWarningDialogView();
            ViewModelLocator.MainVM.MainModel.isDialogOpen = true;
        }
        private void ShowLog()
        {
            ViewModelLocator.MainVM.MainModel.TransitionIndex = 1;
        }
        private void ShowMenu()
        {
            ViewModelLocator.MainVM.MainModel.TransitionIndex = 0;
        }
    }
}
