﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PROST_Integration.Dialog;
using PROST_Integration.Model;
using PROST_Integration.Util;
using PROST_Integration.View;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PROST_Integration.ViewModel
{
    public class MenuBarViewModel : ViewModelBase
    {
        public RelayCommand LoadConfigCommand
        {
            get; set;
        }
        public RelayCommand SaveConfigDialogOKCommand
        {
            get; set;
        }
        public RelayCommand<object> ShowSaveConfigDialogCommand
        {
            get; set;
        }
        public RelayCommand SaveAsConfigCommand
        {
            get; set;
        }
        public RelayCommand LoadWorkSpaceCommand
        {
            get; set;
        }
        public RelayCommand NewWorkSpaceCommand
        {
            get; set;
        }
        public RelayCommand EditParserCommand
        {
            get; set;
        }
        public RelayCommand ShowAboutDialogCommand
        {
            get; set;
        }
        public MenuBarViewModel()
        {
            LoadConfigCommand = new RelayCommand(LoadConfig);
            ShowSaveConfigDialogCommand = new RelayCommand<object>(ShowSaveConfigDialog);
            SaveConfigDialogOKCommand = new RelayCommand(SaveConfigDialogOK);
            SaveAsConfigCommand = new RelayCommand(SaveAsConfig);
            LoadWorkSpaceCommand = new RelayCommand(LoadWorkSpace);
            NewWorkSpaceCommand = new RelayCommand(NewWorkSpace);
            EditParserCommand = new RelayCommand(EditParser);
            ShowAboutDialogCommand = new RelayCommand(ShowAboutDialog);
        }

        private void LoadConfig()
        {

            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = Common.Common.WORKSPACE_PATH;
            dialog.RestoreDirectory = false;
            dialog.IsFolderPicker = false;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (ViewModelLocator.MainVM.ReadConfigFile(dialog.FileName))
                    ViewModelLocator.MainVM.showTrackBarMessage("Configuration file has been loaded");

            }
        }
        public void ShowSaveConfigDialog(object e)
        {
            ViewModelLocator.MainVM.MainModel.CurrentSaveMode = Int32.Parse(e.ToString());
            ViewModelLocator.MainVM.MainModel.DialogContent = new SaveConfigDialogView();
            ViewModelLocator.MainVM.MainModel.isDialogOpen = true;
        }
        public void SaveConfigDialogOK()
        {
            try
            {
                List<string> Project_Cfg = new List<string>();

                Project_Cfg.Add("Tester=$=" + ViewModelLocator.MainVM.MainModel.CommonTester);
                Project_Cfg.Add("SWVersion=$=" + ViewModelLocator.MainVM.MainModel.CommonSWVersion);
                Project_Cfg.Add("Comment=$=" + ViewModelLocator.MainVM.MainModel.CommonComment);
                Project_Cfg.Add("TargetName=$=" + ViewModelLocator.MainVM.MainModel.CommonTargetName);
                Project_Cfg.Add("TestCaseParser=$=" + ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem);

                for (int i = 0; i < ViewModelLocator.MainVM.MainModel.CANDB_item_count; i++)
                {
                    Project_Cfg.Add("CANProperty=$=" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_BaudRate + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_TSEG1 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_TSEG2 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_SJW
                        + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_BaudRate + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_TSEG1 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_TSEG2 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_SJW + "$$" +
                        ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_BRS + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_DBC_Path);
                }

                Project_Cfg.Add("CANDB_item_count=$=" + ViewModelLocator.MainVM.MainModel.CANDB_item_count);
                Project_Cfg.Add("TestcasePath=$=" + ViewModelLocator.MainVM.MainModel.TestCasePath);
                Project_Cfg.Add("TestSpecificationPath=$=" + ViewModelLocator.MainVM.MainModel.TestSpecificationPath);
                Project_Cfg.Add("ELFPath=$=" + ViewModelLocator.MainVM.MainModel.ELFFilePath);
                Project_Cfg.Add("LogLevel=$=" + ViewModelLocator.MainVM.MainModel.LogLevelIndex);
                Project_Cfg.Add("TestDelay=$=" + ViewModelLocator.MainVM.MainModel.TestStepDelay);
                Project_Cfg.Add("T1InstallPath=$=" + ViewModelLocator.MainVM.MainModel.T1InstallPath);
                Project_Cfg.Add("T1ProjectPath=$=" + ViewModelLocator.MainVM.MainModel.T1ProjectPath);
                Project_Cfg.Add("T1ReportPath=$=" + ViewModelLocator.MainVM.MainModel.T1ReportPath);
                Project_Cfg.Add("TestCaseParserSelectItem=$=" + ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem);

                for (int i = 0; i < ViewModelLocator.MainVM.MainModel.CanLogFileList.Count; i++)
                    Project_Cfg.Add("CanLogFileList=$=" + ViewModelLocator.MainVM.MainModel.CanLogFileList[i].Index + "$$" + ViewModelLocator.MainVM.MainModel.CanLogFileList[i].FileName + "$$" + ViewModelLocator.MainVM.MainModel.CanLogFileList[i].FullPath);


                if (ViewModelLocator.MainVM.MainModel.IsELFDownload == true)
                    Project_Cfg.Add("IsELFDownload=$=" + "true");
                else
                    Project_Cfg.Add("IsELFDownload=$=" + "false");

                foreach (T1ScenarioModel model in ViewModelLocator.MainVM.T1ScenarioModelList)
                {
                    Project_Cfg.Add("T1ScenarioList=$=" + model.ScenarioXmlPath);
                }

                if (CommonUtil.FileSave_List(filePath: Common.Common.WORKSPACE_PATH + "\\Temp\\IVSConfig.csa", lines: Project_Cfg))
                    ViewModelLocator.MainVM.showTrackBarMessage("Configuration file has been saved");

                ViewModelLocator.MainVM.MainModel.isDialogOpen = false;

                if (ViewModelLocator.MainVM.MainModel.CurrentSaveMode == Common.Common.SAVEMODE_BEFORE_END)
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch (Exception ex)
            {
                IVSLog.getInstance().Log(Common.Common.MODULE_VIEW, Common.Common.LOGTYPE_ERR, ex.Message + " " + CommonUtil.GetExceptionLineNumber(ex));
            }
        }
        private void SaveAsConfig()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = true;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                List<string> Project_Cfg = new List<string>();

                Project_Cfg.Add("Tester=$=" + ViewModelLocator.MainVM.MainModel.CommonTester);
                Project_Cfg.Add("SWVersion=$=" + ViewModelLocator.MainVM.MainModel.CommonSWVersion);
                Project_Cfg.Add("Comment=$=" + ViewModelLocator.MainVM.MainModel.CommonComment);
                Project_Cfg.Add("TargetName=$=" + ViewModelLocator.MainVM.MainModel.CommonTargetName);

                for (int i = 0; i < ViewModelLocator.MainVM.MainModel.CANDB_item_count; i++)
                {
                    Project_Cfg.Add("CANProperty=$=" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_BaudRate + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_TSEG1 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_TSEG2 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_SJW
                        + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_BaudRate + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_TSEG1 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_TSEG2 + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_SJW + "$$" +
                        ViewModelLocator.MainVM.CANPropertyModelList[i].CANFD_BRS + "$$" + ViewModelLocator.MainVM.CANPropertyModelList[i].CAN_DBC_Path);
                }

                Project_Cfg.Add("CANDB_item_count=$=" + ViewModelLocator.MainVM.MainModel.CANDB_item_count);
                Project_Cfg.Add("TestcasePath=$=" + ViewModelLocator.MainVM.MainModel.TestCasePath);
                Project_Cfg.Add("TestSpecificationPath=$=" + ViewModelLocator.MainVM.MainModel.TestSpecificationPath);
                Project_Cfg.Add("ELFPath=$=" + ViewModelLocator.MainVM.MainModel.ELFFilePath);
                Project_Cfg.Add("LogLevel=$=" + ViewModelLocator.MainVM.MainModel.LogLevelIndex);
                Project_Cfg.Add("TestDelay=$=" + ViewModelLocator.MainVM.MainModel.TestStepDelay);
                Project_Cfg.Add("T1InstallPath=$=" + ViewModelLocator.MainVM.MainModel.T1InstallPath);
                Project_Cfg.Add("T1ProjectPath=$=" + ViewModelLocator.MainVM.MainModel.T1ProjectPath);
                Project_Cfg.Add("T1ReportPath=$=" + ViewModelLocator.MainVM.MainModel.T1ReportPath);
                Project_Cfg.Add("TestCaseParserSelectItem=$=" + ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem);

                for (int i = 0; i < ViewModelLocator.MainVM.MainModel.CanLogFileList.Count; i++)
                    Project_Cfg.Add("CanLogFileList=$=" + ViewModelLocator.MainVM.MainModel.CanLogFileList[i].Index + "$$" + ViewModelLocator.MainVM.MainModel.CanLogFileList[i].FileName + "$$" + ViewModelLocator.MainVM.MainModel.CanLogFileList[i].FullPath);

                if (ViewModelLocator.MainVM.MainModel.IsELFDownload == true)
                    Project_Cfg.Add("IsELFDownload=$=" + "true");
                else
                    Project_Cfg.Add("IsELFDownload=$=" + "false");

                Project_Cfg.Add("PreHookScript=$=" + ViewModelLocator.MainVM.MainModel.PreHookScriptPath);


                if (CommonUtil.FileSave_List(filePath: dialog.FileName + "\\IVSConfig.csa", lines: Project_Cfg))
                    ViewModelLocator.MainVM.showTrackBarMessage("Configuration file has been saved");

                ViewModelLocator.MainVM.MainModel.isDialogOpen = false;
            }
        }
        public void NewWorkSpace()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.Title = "Workspace 폴더 선택";
            dialog.IsFolderPicker = true;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (!Directory.Exists(dialog.FileName + "\\TestCase"))
                    Directory.CreateDirectory(dialog.FileName + "\\TestCase");

                if (!Directory.Exists(dialog.FileName + "\\Report"))
                    Directory.CreateDirectory(dialog.FileName + "\\Report");

                if (!Directory.Exists(dialog.FileName + "\\T1Report"))
                    Directory.CreateDirectory(dialog.FileName + "\\T1Report");

                if (!Directory.Exists(dialog.FileName + "\\Temp")) // Temp Folder 없으면 생성
                    Directory.CreateDirectory(dialog.FileName + "\\Temp");
                else
                {
                    if (!File.Exists(dialog.FileName + "\\Temp\\IVSConfig.csa")) // Temp Folder가 있지만 csa파일이 없을 때
                        File.Create(dialog.FileName + "\\Temp\\IVSConfig.csa");
                }
                Common.Common.WORKSPACE_PATH = dialog.FileName;
                ViewModelLocator.MainVM.ReadConfigFile(Common.Common.WORKSPACE_PATH + "\\Temp\\IVSConfig.csa");
                CommonUtil.UpdateWorkspace();
                ToolbarVIew.Instance.updateWorkspacePath(Common.Common.WORKSPACE_PATH);

            }
        }
        private void LoadWorkSpace()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            //dialog.InitialDirectory = @"C:\";
            dialog.IsFolderPicker = true;
            dialog.RestoreDirectory = false;
            CommonFileDialogResult commonFileDialogResult = dialog.ShowDialog();

            if (commonFileDialogResult == CommonFileDialogResult.Ok)
            {
                if (!Directory.Exists(dialog.FileName + "\\TestCase") || !Directory.Exists(dialog.FileName + "\\Temp") || !File.Exists(dialog.FileName + "\\Temp\\IVSConfig.csa") || !Directory.Exists(dialog.FileName + "\\Report"))
                    ViewModelLocator.MainVM.showTrackBarMessage("Please Check the Workspace Folder..");
                else
                {
                    Common.Common.WORKSPACE_PATH = dialog.FileName;
                    ViewModelLocator.MainVM.ReadConfigFile(Common.Common.WORKSPACE_PATH + "\\Temp\\IVSConfig.csa");
                    CommonUtil.UpdateWorkspace();
                    ToolbarVIew.Instance.updateWorkspacePath(Common.Common.WORKSPACE_PATH);
                }

            }
        }
        private void EditParser()
        {
            if (File.Exists("..\\src\\parser\\src\\Mando\\MandoInterfaceTest\\specification\\" + ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem + ".py"))
            {
                Process.Start("notepad.exe", "..\\src\\parser\\src\\Mando\\MandoInterfaceTest\\specification\\" + ViewModelLocator.MainVM.MainModel.TestCaseParserSelectItem + ".py");
            }
        }
        public void ShowAboutDialog()
        {
            ViewModelLocator.MainVM.MainModel.DialogContent = new AboutDialogView();
            ViewModelLocator.MainVM.MainModel.isDialogOpen = true;
        }
    }
}
