﻿using GalaSoft.MvvmLight;
using PROST_Integration.Util;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows;

namespace PROST_Integration.Model
{
    public class MainModel : ObservableObject
    {
        private int _TransitionIndex;
        public int TransitionIndex
        {
            get
            {
                return _TransitionIndex;
            }
            set
            {
                if (_TransitionIndex != value)
                {
                    _TransitionIndex = value;
                    RaisePropertyChanged("TransitionIndex");
                }
            }
        }
        private object _DialogContent;
        public object DialogContent
        {
            get
            {
                return _DialogContent;
            }
            set
            {
                if (_DialogContent != value)
                {
                    _DialogContent = value;
                    RaisePropertyChanged("DialogContent");
                }
            }
        }
        private bool _isDialogOpen;
        public bool isDialogOpen
        {
            get
            {
                return _isDialogOpen;
            }
            set
            {

                if (_isDialogOpen != value)
                {
                    _isDialogOpen = value;
                    RaisePropertyChanged("isDialogOpen");
                }
                return;
            }
        }
        private bool _IsTestStatusDialogOpen;
        public bool IsTestStatusDialogOpen
        {
            get
            {
                return _IsTestStatusDialogOpen;
            }
            set
            {

                if (_IsTestStatusDialogOpen != value)
                {
                    _IsTestStatusDialogOpen = value;
                    RaisePropertyChanged("IsTestStatusDialogOpen");
                }
                return;
            }
        }
        private bool _IsParsingStatusDialogOpen;
        public bool IsParsingStatusDialogOpen
        {
            get
            {
                return _IsParsingStatusDialogOpen;
            }
            set
            {

                if (_IsParsingStatusDialogOpen != value)
                {
                    _IsParsingStatusDialogOpen = value;
                    RaisePropertyChanged("IsParsingStatusDialogOpen");
                }
                return;
            }
        }
        private string _TestSpecificationPath;
        public string TestSpecificationPath
        {
            get
            {
                return _TestSpecificationPath;
            }
            set
            {
                if (_TestSpecificationPath != value)
                {
                    _TestSpecificationPath = value;
                    RaisePropertyChanged("TestSpecificationPath");
                }
            }
        }
        private string _TestCasePath;
        public string TestCasePath
        {
            get
            {
                return _TestCasePath;
            }
            set
            {
                if (_TestCasePath != value)
                {
                    _TestCasePath = value;
                    RaisePropertyChanged("TestCasePath");
                }
            }
        }
        private string _ELFFilePath;
        public string ELFFilePath
        {
            get
            {
                return _ELFFilePath;
            }
            set
            {
                if (_ELFFilePath != value)
                {
                    _ELFFilePath = value;
                    RaisePropertyChanged("ELFFilePath");
                }
            }
        }
        private string _PreHookScriptPath;
        public string PreHookScriptPath
        {
            get
            {
                return _PreHookScriptPath;
            }
            set
            {
                if (_PreHookScriptPath != value)
                {
                    _PreHookScriptPath = value;
                    RaisePropertyChanged("PreHookScriptPath");
                }
            }
        }
        private string _T1InstallPath;
        public string T1InstallPath
        {
            get
            {
                return _T1InstallPath;
            }
            set
            {
                if (_T1InstallPath != value)
                {
                    _T1InstallPath = value;
                    RaisePropertyChanged("T1InstallPath");
                }
            }
        }
        private string _T1ProjectPath;
        public string T1ProjectPath
        {
            get
            {
                return _T1ProjectPath;
            }
            set
            {
                if (_T1ProjectPath != value)
                {
                    _T1ProjectPath = value;
                    RaisePropertyChanged("T1ProjectPath");
                }
            }
        }
        private string _T1ReportPath;
        public string T1ReportPath
        {
            get
            {
                return _T1ReportPath;
            }
            set
            {
                if (_T1ReportPath != value)
                {
                    _T1ReportPath = value;
                    RaisePropertyChanged("T1ReportPath");
                }
            }
        }
        private string _T1ProjectDictionaryFile;
        public string T1ProjectDictionaryFile
        {
            get
            {
                return _T1ProjectDictionaryFile;
            }
            set
            {
                if (_T1ProjectDictionaryFile != value)
                {
                    _T1ProjectDictionaryFile = value;
                    RaisePropertyChanged("T1ProjectDictionaryFile");
                }
            }
        }
        private string _CommonTestDate;
        public string CommonTestDate
        {
            get
            {
                return _CommonTestDate;
            }
            set
            {
                if (_CommonTestDate != value)
                {
                    _CommonTestDate = value;
                    RaisePropertyChanged("CommonTestDate");
                }
            }
        }
        private string _CommonTester;
        public string CommonTester
        {
            get
            {
                return _CommonTester;
            }
            set
            {
                if (_CommonTester != value)
                {
                    _CommonTester = value;
                    RaisePropertyChanged("CommonTester");
                }
            }
        }
        private string _CommonComment;
        public string CommonComment
        {
            get
            {
                return _CommonComment;
            }
            set
            {
                if (_CommonComment != value)
                {
                    _CommonComment = value;
                    RaisePropertyChanged("CommonComment");
                }
            }
        }
        private string _CommonSWVersion;
        public string CommonSWVersion
        {
            get
            {
                return _CommonSWVersion;
            }
            set
            {
                if (_CommonSWVersion != value)
                {
                    _CommonSWVersion = value;
                    RaisePropertyChanged("CommonSWVersion");
                }
            }
        }
        private string _CommonTargetName;
        public string CommonTargetName
        {
            get
            {
                return _CommonTargetName;
            }
            set
            {
                if (_CommonTargetName != value)
                {
                    _CommonTargetName = value;
                    RaisePropertyChanged("CommonTargetName");
                }
            }
        }


        private ObservableCollection<LogDataModel> _LogData;
        public ObservableCollection<LogDataModel> LogData
        {
            get
            {
                return _LogData;
            }
            set
            {
                if (_LogData != value)
                {
                    _LogData = value;
                    RaisePropertyChanged("LogData");
                }
            }
        }
        private string _WarningDialogMessage;
        public string WarningDialogMessage
        {
            get
            {
                return _WarningDialogMessage;
            }
            set
            {
                if (_WarningDialogMessage != value)
                {
                    _WarningDialogMessage = value;
                    RaisePropertyChanged("WarningDialogMessage");
                }
            }
        }
        private int _AddRulePageIndex;
        public int AddRulePageIndex
        {
            get
            {
                return _AddRulePageIndex;
            }
            set
            {
                if (_AddRulePageIndex != value)
                {
                    _AddRulePageIndex = value;
                    RaisePropertyChanged("AddRulePageIndex");
                }
            }
        }

        private ObservableCollection<string> _ConditionList;
        public ObservableCollection<string> ConditionList
        {
            get
            {
                return _ConditionList;
            }
            set
            {
                if (_ConditionList != value)
                {
                    _ConditionList = value;
                    RaisePropertyChanged("ConditionList");
                }
            }
        }
        private double _ParseTestSpecProgress;
        public double ParseTestSpecProgress
        {
            get
            {
                return _ParseTestSpecProgress;
            }
            set
            {
                if (_ParseTestSpecProgress != value)
                {
                    _ParseTestSpecProgress = value;
                    RaisePropertyChanged("ParseTestSpecProgress");
                }
            }
        }
        private double _TestProgress;
        public double TestProgress
        {
            get
            {
                return _TestProgress;
            }
            set
            {
                if (_TestProgress != value)
                {
                    _TestProgress = value;
                    RaisePropertyChanged("TestProgress");
                }
            }
        }
        private string _PerformanceTestStatusText;
        public string PerformanceTestStatusText
        {
            get
            {
                return _PerformanceTestStatusText;
            }
            set
            {
                if (_PerformanceTestStatusText != value)
                {
                    _PerformanceTestStatusText = value;
                    RaisePropertyChanged("PerformanceTestStatusText");
                }
            }
        }
        private double _PerformanceTestProgress;
        public double PerformanceTestProgress
        {
            get
            {
                return _PerformanceTestProgress;
            }
            set
            {
                if (_PerformanceTestProgress != value)
                {
                    _PerformanceTestProgress = value;
                    RaisePropertyChanged("PerformanceTestProgress");
                }
            }
        }
        private string _ParseTestSpecSheetName;
        public string ParseTestSpecSheetName
        {
            get
            {
                return _ParseTestSpecSheetName;
            }
            set
            {
                if (_ParseTestSpecSheetName != value)
                {
                    _ParseTestSpecSheetName = value;
                    RaisePropertyChanged("ParseTestSpecSheetName");
                }
            }
        }
        private string _TestStatusText;
        public string TestStatusText
        {
            get
            {
                return _TestStatusText;
            }
            set
            {
                if (_TestStatusText != value)
                {
                    _TestStatusText = value;
                    RaisePropertyChanged("TestStatusText");
                }
            }
        }
        private string _TestStatusCountText;
        public string TestStatusCountText
        {
            get
            {
                return _TestStatusCountText;
            }
            set
            {
                if (_TestStatusCountText != value)
                {
                    _TestStatusCountText = value;
                    RaisePropertyChanged("TestStatusCountText");
                }
            }
        }

        private SnackbarMessageQueue _TrackBarMessageQueue;
        public SnackbarMessageQueue TrackBarMessageQueue
        {
            get
            {
                return _TrackBarMessageQueue;
            }
            set
            {
                if (_TrackBarMessageQueue != value)
                {
                    _TrackBarMessageQueue = value;
                    RaisePropertyChanged("TrackBarMessageQueue");
                }
            }
        }

        private string _CANDialogVisible;
        public string CANDialogVisible
        {
            get
            {
                return _CANDialogVisible;
            }
            set
            {
                if (_CANDialogVisible != value)
                {
                    _CANDialogVisible = value;
                    RaisePropertyChanged("CANDialogVisible");
                }
            }
        }
        private string _CANFDDialogVisible;
        public string CANFDDialogVisible
        {
            get
            {
                return _CANFDDialogVisible;
            }
            set
            {
                if (_CANFDDialogVisible != value)
                {
                    _CANFDDialogVisible = value;
                    RaisePropertyChanged("CANFDDialogVisible");
                }
            }
        }

        private int _CANDB_item_count;
        public int CANDB_item_count
        {
            get
            {
                return _CANDB_item_count;
            }
            set
            {
                if (_CANDB_item_count != value)
                {
                    _CANDB_item_count = value;
                    RaisePropertyChanged("CANDB_item_count");
                }
            }
        }
        private string _CANDB_Plus_Item_Visibility;
        public string CANDB_Plus_Item_Visibility
        {
            get
            {
                return _CANDB_Plus_Item_Visibility;
            }
            set
            {
                if (_CANDB_Plus_Item_Visibility != value)
                {
                    _CANDB_Plus_Item_Visibility = value;
                    RaisePropertyChanged("CANDB_Plus_Item_Visibility");
                }
            }
        }
        private string _CANDB_Item_1_Visibility;
        public string CANDB_Item_1_Visibility
        {
            get
            {
                return _CANDB_Item_1_Visibility;
            }
            set
            {
                if (_CANDB_Item_1_Visibility != value)
                {
                    _CANDB_Item_1_Visibility = value;
                    RaisePropertyChanged("CANDB_Item_1_Visibility");
                }
            }
        }
        private string _CANDB_Item_2_Visibility;
        public string CANDB_Item_2_Visibility
        {
            get
            {
                return _CANDB_Item_2_Visibility;
            }
            set
            {
                if (_CANDB_Item_2_Visibility != value)
                {
                    _CANDB_Item_2_Visibility = value;
                    RaisePropertyChanged("CANDB_Item_2_Visibility");
                }
            }
        }
        private string _CANDB_Item_3_Visibility;
        public string CANDB_Item_3_Visibility
        {
            get
            {
                return _CANDB_Item_3_Visibility;
            }
            set
            {
                if (_CANDB_Item_3_Visibility != value)
                {
                    _CANDB_Item_3_Visibility = value;
                    RaisePropertyChanged("CANDB_Item_3_Visibility");
                }
            }
        }
        private string _CANDB_Item_4_Visibility;
        public string CANDB_Item_4_Visibility
        {
            get
            {
                return _CANDB_Item_4_Visibility;
            }
            set
            {
                if (_CANDB_Item_4_Visibility != value)
                {
                    _CANDB_Item_4_Visibility = value;
                    RaisePropertyChanged("CANDB_Item_4_Visibility");
                }
            }
        }
        private string _CANDB_Minus_Button_1_Visibility;
        public string CANDB_Minus_Button_1_Visibility
        {
            get
            {
                return _CANDB_Minus_Button_1_Visibility;
            }
            set
            {
                if (_CANDB_Minus_Button_1_Visibility != value)
                {
                    _CANDB_Minus_Button_1_Visibility = value;
                    RaisePropertyChanged("CANDB_Minus_Button_1_Visibility");
                }
            }
        }
        private string _CANDB_Minus_Button_2_Visibility;
        public string CANDB_Minus_Button_2_Visibility
        {
            get
            {
                return _CANDB_Minus_Button_2_Visibility;
            }
            set
            {
                if (_CANDB_Minus_Button_2_Visibility != value)
                {
                    _CANDB_Minus_Button_2_Visibility = value;
                    RaisePropertyChanged("CANDB_Minus_Button_2_Visibility");
                }
            }
        }
        private string _CANDB_Minus_Button_3_Visibility;
        public string CANDB_Minus_Button_3_Visibility
        {
            get
            {
                return _CANDB_Minus_Button_3_Visibility;
            }
            set
            {
                if (_CANDB_Minus_Button_3_Visibility != value)
                {
                    _CANDB_Minus_Button_3_Visibility = value;
                    RaisePropertyChanged("CANDB_Minus_Button_3_Visibility");
                }
            }
        }
        private string _CANDB_Minus_Button_4_Visibility;
        public string CANDB_Minus_Button_4_Visibility
        {
            get
            {
                return _CANDB_Minus_Button_4_Visibility;
            }
            set
            {
                if (_CANDB_Minus_Button_4_Visibility != value)
                {
                    _CANDB_Minus_Button_4_Visibility = value;
                    RaisePropertyChanged("CANDB_Minus_Button_4_Visibility");
                }
            }
        }
        private bool _GUIStartup;
        public bool GUIStartup
        {
            get
            {
                return _GUIStartup;
            }
            set
            {
                if (_GUIStartup != value)
                {
                    _GUIStartup = value;
                    RaisePropertyChanged("GUIStartup");
                }
            }
        }
        private int _LogLevelIndex;
        public int LogLevelIndex
        {
            get
            {
                return _LogLevelIndex;
            }
            set
            {
                if (_LogLevelIndex != value)
                {
                    _LogLevelIndex = value;
                    RaisePropertyChanged("LogLevelIndex");
                }
            }
        }

        private int _TestStepDelay;
        public int TestStepDelay
        {
            get
            {
                return _TestStepDelay;
            }
            set
            {
                if (_TestStepDelay != value)
                {
                    _TestStepDelay = value;
                    RaisePropertyChanged("TestStepDelay");
                }
            }
        }

        private bool _IsTrace32Connection;
        public bool IsTrace32Connection
        {
            get
            {
                return _IsTrace32Connection;
            }
            set
            {
                if (_IsTrace32Connection != value)
                {
                    _IsTrace32Connection = value;
                    RaisePropertyChanged("IsTrace32Connection");
                }
            }
        }

        private ObservableCollection<string> _TestcaseSheetNameList;
        public ObservableCollection<string> TestcaseSheetNameList
        {
            get
            {
                return _TestcaseSheetNameList;
            }
            set
            {
                if (_TestcaseSheetNameList != value)
                {
                    _TestcaseSheetNameList = value;
                    RaisePropertyChanged("TestcaseSheetNameList");
                }
            }
        }


        private int _TestStateMode;
        public int TestStateMode
        {
            get
            {
                return _TestStateMode;
            }
            set
            {
                if (_TestStateMode != value)
                {
                    _TestStateMode = value;
                    RaisePropertyChanged("TestStateMode");
                }
            }
        }
        private int _ConsoleAppMode;
        public int ConsoleAppMode
        {
            get
            {
                return _ConsoleAppMode;
            }
            set
            {
                if (_ConsoleAppMode != value)
                {
                    _ConsoleAppMode = value;
                    RaisePropertyChanged("ConsoleAppMode");
                }
            }
        }
        private int _GUIAppMode;
        public int GUIAppMode
        {
            get
            {
                return _GUIAppMode;
            }
            set
            {
                if (_GUIAppMode != value)
                {
                    _GUIAppMode = value;
                    RaisePropertyChanged("GUIAppMode");
                }
            }
        }
        private bool _IsELFDownload;
        public bool IsELFDownload
        {
            get
            {
                return _IsELFDownload;
            }
            set
            {
                if (_IsELFDownload != value)
                {
                    _IsELFDownload = value;
                    RaisePropertyChanged("IsELFDownload");
                }
            }
        }
        private ObservableCollection<string> _TestCaseParserList;
        public ObservableCollection<string> TestCaseParserList
        {
            get
            {
                return _TestCaseParserList;
            }
            set
            {
                if (_TestCaseParserList != value)
                {
                    _TestCaseParserList = value;
                    RaisePropertyChanged("TestCaseParserList");
                }
            }
        }
        private string _TestCaseParserSelectItem;
        public string TestCaseParserSelectItem
        {
            get
            {
                return _TestCaseParserSelectItem;
            }
            set
            {
                if (_TestCaseParserSelectItem != value)
                {
                    _TestCaseParserSelectItem = value;
                    RaisePropertyChanged("TestCaseParserSelectItem");
                }
            }
        }
        private int _ParserWorkState;
        public int ParserWorkState
        {
            get
            {
                return _ParserWorkState;
            }
            set
            {
                if (_ParserWorkState != value)
                {
                    _ParserWorkState = value;
                    RaisePropertyChanged("ParserWorkState");
                }
            }
        }
        private bool _SettingDrawerIsOpen;
        public bool SettingDrawerIsOpen
        {
            get
            {
                return _SettingDrawerIsOpen;
            }
            set
            {
                if (_SettingDrawerIsOpen != value)
                {
                    _SettingDrawerIsOpen = value;
                    RaisePropertyChanged("SettingDrawerIsOpen");
                }
            }
        }
        private Visibility _IsTestDocumentVisible;
        public Visibility IsTestDocumentVisible
        {
            get
            {
                return _IsTestDocumentVisible;
            }
            set
            {
                if (_IsTestDocumentVisible != value)
                {
                    _IsTestDocumentVisible = value;
                    RaisePropertyChanged("IsTestDocumentVisible");
                }
            }
        }
        private Visibility _IsT1ConfigSettingVisible;
        public Visibility IsT1ConfigSettingVisible
        {
            get
            {
                return _IsT1ConfigSettingVisible;
            }
            set
            {
                if (_IsT1ConfigSettingVisible != value)
                {
                    _IsT1ConfigSettingVisible = value;
                    RaisePropertyChanged("IsT1ConfigSettingVisible");
                }
            }
        }
        private Visibility _IsT1ScenarioSettingVisible;
        public Visibility IsT1ScenarioSettingVisible
        {
            get
            {
                return _IsT1ScenarioSettingVisible;
            }
            set
            {
                if (_IsT1ScenarioSettingVisible != value)
                {
                    _IsT1ScenarioSettingVisible = value;
                    RaisePropertyChanged("IsT1ScenarioSettingVisible");
                }
            }
        }

        private Visibility _IsETCConfigSettingVisible;
        public Visibility IsETCConfigSettingVisible
        {
            get
            {
                return _IsETCConfigSettingVisible;
            }
            set
            {
                if (_IsETCConfigSettingVisible != value)
                {
                    _IsETCConfigSettingVisible = value;
                    RaisePropertyChanged("IsETCConfigSettingVisible");
                }
            }
        }
        private int _CurrentSaveMode;
        public int CurrentSaveMode
        {
            get
            {
                return _CurrentSaveMode;
            }
            set
            {
                if (_CurrentSaveMode != value)
                {
                    _CurrentSaveMode = value;
                    RaisePropertyChanged("CurrentSaveMode");
                }
            }
        }
        private ObservableCollection<CanLogFileModel> _CanLogFileList;
        public ObservableCollection<CanLogFileModel> CanLogFileList
        {
            get
            {
                return _CanLogFileList;
            }
            set
            {
                if (_CanLogFileList != value)
                {
                    _CanLogFileList = value;
                    RaisePropertyChanged("CanLogFileList");
                }
            }
        }
        private string _ReportFilePath;
        public string ReportFilePath
        {
            get
            {
                return _ReportFilePath;
            }
            set
            {

                if (_ReportFilePath != value)
                {
                    _ReportFilePath = value;
                    RaisePropertyChanged("ReportFilePath");
                }
                return;
            }
        }
        private bool _IsT1ManagerReady;
        public bool IsT1ManagerReady
        {
            get
            {
                return _IsT1ManagerReady;
            }
            set
            {
                if (_IsT1ManagerReady != value)
                {
                    _IsT1ManagerReady = value;
                    RaisePropertyChanged("IsT1ManagerReady");
                }
            }
        }
        private bool _IsT1ManagerConstraintComplete;
        public bool IsT1ManagerConstraintComplete
        {
            get
            {
                return _IsT1ManagerConstraintComplete;
            }
            set
            {
                if (_IsT1ManagerConstraintComplete != value)
                {
                    _IsT1ManagerConstraintComplete = value;
                    RaisePropertyChanged("IsT1ManagerConstraintComplete");
                }
            }
        }
        private bool _IsT1ManagerCanLogAck;
        public bool IsT1ManagerCanLogAck
        {
            get
            {
                return _IsT1ManagerCanLogAck;
            }
            set
            {
                if (_IsT1ManagerCanLogAck != value)
                {
                    _IsT1ManagerCanLogAck = value;
                    RaisePropertyChanged("IsT1ManagerCanLogAck");
                }
            }
        }


        private object _TestDialogContent
        {
            get; set;
        }
        public object TestDialogContent
        {
            get
            {
                return _TestDialogContent;
            }
            set
            {
                if (_TestDialogContent != value)
                {
                    _TestDialogContent = value;
                    RaisePropertyChanged("TestDialogContent");
                }
            }
        }
        private ObservableCollection<int> _ConstateCountList;
        public ObservableCollection<int> ConstateCountList
        {
            get
            {
                return _ConstateCountList;
            }
            set
            {
                if (_ConstateCountList != value)
                {
                    _ConstateCountList = value;
                    RaisePropertyChanged("ConstateCountList");
                }
            }
        }
        private bool _IsReTestFlag;
        public bool IsReTestFlag
        {
            get
            {
                return _IsReTestFlag;
            }
            set
            {
                if (_IsReTestFlag != value)
                {
                    _IsReTestFlag = value;
                    RaisePropertyChanged("IsReTestFlag");
                }
            }
        }
        private bool _IsPerformanceMeasurmentEnable;
        public bool IsPerformanceMeasurmentEnable
        {
            get
            {
                return _IsPerformanceMeasurmentEnable;
            }
            set
            {
                if (_IsPerformanceMeasurmentEnable != value)
                {
                    _IsPerformanceMeasurmentEnable = value;
                    RaisePropertyChanged("IsPerformanceMeasurmentEnable");
                }
            }
        }
        private T1ScenarioModel _EditT1ScenarioModel;
        public T1ScenarioModel EditT1ScenarioModel
        {
            get
            {
                return _EditT1ScenarioModel;
            }
            set
            {
                if (_EditT1ScenarioModel != value)
                {
                    _EditT1ScenarioModel = value;
                    RaisePropertyChanged("EditT1ScenarioModel");
                }
            }
        }
        private bool _IsT1ProjectParsingComplete;
        public bool IsT1ProjectParsingComplete
        {
            get
            {
                return _IsT1ProjectParsingComplete;
            }
            set
            {
                if (_IsT1ProjectParsingComplete != value)
                {
                    _IsT1ProjectParsingComplete = value;
                    RaisePropertyChanged("IsT1ProjectParsingComplete");
                }
            }
        }
        public MainModel()
        {
            ConditionList = new ObservableCollection<string>();
            LogData = new ObservableCollection<LogDataModel>();
            TestcaseSheetNameList = new ObservableCollection<string>();
            TestCaseParserList = new ObservableCollection<string>();
            CanLogFileList = new ObservableCollection<CanLogFileModel>();
            ConstateCountList = new ObservableCollection<int>();
            EditT1ScenarioModel = new T1ScenarioModel();

            UpdateParserIndex();

            TrackBarMessageQueue = new SnackbarMessageQueue();
            CANDB_Plus_Item_Visibility = Common.Common.TRUE;

            CANDB_item_count = 1;
            LogLevelIndex = 0;
            TestStepDelay = 50;

            IsPerformanceMeasurmentEnable = true;
            PreHookScriptPath = "";
        }

        public void UpdateParserIndex()
        {
            if (Directory.Exists("..\\src\\parser\\src\\Mando\\MandoInterfaceTest\\specification"))
            {
                string[] files = Directory.GetFiles("..\\src\\parser\\src\\Mando\\MandoInterfaceTest\\specification", "*.py");
                foreach (string f in files)
                {
                    TestCaseParserList.Add(Path.GetFileNameWithoutExtension(f));
                }
            }
        }
    }
}
