﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class CanLogFileModel : ObservableObject
    {
        private int _Index;
        public int Index
        {
            get
            {
                return _Index;
            }
            set
            {
                if (_Index != value)
                {
                    _Index = value;
                    RaisePropertyChanged("Index");
                }
            }
        }
        private string _FileName;
        public string FileName
        {
            get
            {
                return _FileName;
            }
            set
            {
                if (_FileName != value)
                {
                    _FileName = value;
                    RaisePropertyChanged("FileName");
                }
            }
        }
        private string _FullPath;
        public string FullPath
        {
            get
            {
                return _FullPath;
            }
            set
            {
                if (_FullPath != value)
                {
                    _FullPath = value;
                    RaisePropertyChanged("FullPath");
                }
            }
        }
    }
}
