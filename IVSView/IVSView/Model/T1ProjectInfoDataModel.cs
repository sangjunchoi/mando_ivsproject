﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class T1ProjectInfoDataModel : ObservableObject
    {

        private string _DataType;
        public string DataType
        {
            get
            {
                return _DataType;
            }
            set
            {
                if (_DataType != value)
                {
                    _DataType = value;
                    RaisePropertyChanged("DataType");
                }

            }
        }
        private string _DataValue;
        public string DataValue
        {
            get
            {
                return _DataValue;
            }
            set
            {
                if (_DataValue != value)
                {
                    _DataValue = value;
                    RaisePropertyChanged("DataValue");
                }

            }
        }
    }
}