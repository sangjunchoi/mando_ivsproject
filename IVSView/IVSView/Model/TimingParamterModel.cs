﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PROST_Integration.Model
{
    public class TimingParamterModel : ObservableObject
    {
        public TimingParamterModel()
        {
            IsEnable = true;
        }
        private int _TimingType;
        public int TimingType
        {
            get
            {
                return _TimingType;
            }
            set
            {
                if (_TimingType != value)
                {
                    _TimingType = value;
                    RaisePropertyChanged("TimingType");
                }
            }
        }
        private int _StartParamter;
        public int StartParamter
        {
            get
            {
                return _StartParamter;
            }
            set
            {
                if (_StartParamter != value)
                {
                    _StartParamter = value;
                    RaisePropertyChanged("StartParamter");
                }
            }
        }
        private int _EndParamter;
        public int EndParamter
        {
            get
            {
                return _EndParamter;
            }
            set
            {
                if (_EndParamter != value)
                {
                    _EndParamter = value;
                    RaisePropertyChanged("EndParamter");
                }
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get
            {
                return _IsChecked;
            }
            set
            {
                if (_IsChecked != value)
                {
                    _IsChecked = value;
                    RaisePropertyChanged("IsChecked");
                }
            }
        }
        private bool _IsEnable;
        public bool IsEnable
        {
            get
            {
                return _IsEnable;
            }
            set
            {
                if (_IsEnable != value)
                {
                    _IsEnable = value;
                    RaisePropertyChanged("IsEnable");
                }
            }
        }
    }
}
