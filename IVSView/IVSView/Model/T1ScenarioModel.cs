﻿using GalaSoft.MvvmLight;
using PROST_Integration.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class T1ScenarioModel : ObservableObject
    {
        private string _CanLogPath;
        public string CanLogPath
        {
            get
            {
                return _CanLogPath;
            }
            set
            {
                if (_CanLogPath != value)
                {
                    _CanLogPath = value;
                    RaisePropertyChanged("CanLogPath");
                }
            }
        }
        private int _RepeatCount;
        public int RepeatCount
        {
            get
            {
                return _RepeatCount;
            }
            set
            {
                if (_RepeatCount != value)
                {
                    _RepeatCount = value;
                    RaisePropertyChanged("RepeatCount");
                }
            }
        }

        private bool _IsRepeatAll;
        public bool IsRepeatAll
        {
            get
            {
                return _IsRepeatAll;
            }
            set
            {
                if (_IsRepeatAll != value)
                {
                    _IsRepeatAll = value;
                    RaisePropertyChanged("IsRepeatAll");
                }
            }
        }
        private bool _IsRepeatOnebyOne;
        public bool IsRepeatOnebyOne
        {
            get
            {
                return _IsRepeatOnebyOne;
            }
            set
            {
                if (_IsRepeatOnebyOne != value)
                {
                    _IsRepeatOnebyOne = value;
                    RaisePropertyChanged("IsRepeatOnebyOne");
                }
            }
        }
        private ObservableCollection<T1ScenarioCoreModel> _T1ScenarioCoreModelList;
        public ObservableCollection<T1ScenarioCoreModel> T1ScenarioCoreModelList
        {
            get
            {
                return _T1ScenarioCoreModelList;
            }
            set
            {
                if (_T1ScenarioCoreModelList != value)
                {
                    _T1ScenarioCoreModelList = value;
                    RaisePropertyChanged("T1ScenarioCoreModelList");
                }
            }
        }
        private T1ScenarioCoreModel _SelectedT1ScenarioCoreModel;
        public T1ScenarioCoreModel SelectedT1ScenarioCoreModel
        {
            get
            {
                return _SelectedT1ScenarioCoreModel;
            }
            set
            {
                if (_SelectedT1ScenarioCoreModel != value)
                {
                    _SelectedT1ScenarioCoreModel = value;
                    RaisePropertyChanged("SelectedT1ScenarioCoreModel");
                }
            }
        }
        private string _ScenarioXmlPath;
        public string ScenarioXmlPath
        {
            get
            {
                return _ScenarioXmlPath;
            }
            set
            {
                if (_ScenarioXmlPath != value)
                {
                    _ScenarioXmlPath = value;
                    RaisePropertyChanged("ScenarioXmlPath");
                }
            }
        }


        public T1ScenarioModel()
        {
            IsRepeatAll = true;
            T1ScenarioCoreModelList = new ObservableCollection<T1ScenarioCoreModel>();

            /*for (int i = 0; i < ViewModelLocator.MainVM.T1ProjectInfoList.Count; i++)
                T1ScenarioCoreModelList.Add(new T1ScenarioCoreModel() { CoreName = "Core " + i });*/
        }
    }
}
