﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class T1ProjectInfoModel : ObservableObject
    {

        private string _CoreName;
        public string CoreName
        {
            get
            {
                return _CoreName;
            }
            set
            {
                if (_CoreName != value)
                {
                    _CoreName = value;
                    RaisePropertyChanged("CoreName");
                }
            }
        }

        private ObservableCollection<T1ProjectInfoDataModel> _DataList;
        public ObservableCollection<T1ProjectInfoDataModel> DataList
        {
            get
            {
                return _DataList;
            }
            set
            {
                if (_DataList != value)
                {
                    _DataList = value;
                    RaisePropertyChanged("_DataList");
                }
            }
        }
        public T1ProjectInfoModel()
        {
            DataList = new ObservableCollection<T1ProjectInfoDataModel>();
        }


    }
}
