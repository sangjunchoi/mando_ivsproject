﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class CANDBPropertyModel : ObservableObject
    {
        private int _CANFD_BaudRate;
        public int CANFD_BaudRate
        {
            get { return _CANFD_BaudRate; }
            set
            {
                if (_CANFD_BaudRate != value)
                {
                    _CANFD_BaudRate = value;
                    RaisePropertyChanged("CANFD_BaudRate");
                }
            }
        }
        private int _CANFD_TSEG1;
        public int CANFD_TSEG1
        {
            get { return _CANFD_TSEG1; }
            set
            {
                if (_CANFD_TSEG1 != value)
                {
                    _CANFD_TSEG1 = value;
                    RaisePropertyChanged("CANFD_TSEG1");
                }
            }
        }
        private int _CANFD_TSEG2;
        public int CANFD_TSEG2
        {
            get { return _CANFD_TSEG2; }
            set
            {
                if (_CANFD_TSEG2 != value)
                {
                    _CANFD_TSEG2 = value;
                    RaisePropertyChanged("CANFD_TSEG2");
                }
            }
        }
        private int _CANFD_SJW;
        public int CANFD_SJW
        {
            get { return _CANFD_SJW; }
            set
            {
                if (_CANFD_SJW != value)
                {
                    _CANFD_SJW = value;
                    RaisePropertyChanged("CANFD_SJW");
                }
            }
        }
        private bool _CANFD_BRS;
        public bool CANFD_BRS
        {
            get { return _CANFD_BRS; }
            set
            {
                if (_CANFD_BRS != value)
                {
                    _CANFD_BRS = value;
                    RaisePropertyChanged("CANFD_BRS");
                }
            }
        }
        private int _CAN_BaudRate;
        public int CAN_BaudRate
        {
            get { return _CAN_BaudRate; }
            set
            {
                if (_CAN_BaudRate != value)
                {
                    _CAN_BaudRate = value;
                    RaisePropertyChanged("CAN_BaudRate");
                }
            }
        }
        private int _CAN_TSEG1;
        public int CAN_TSEG1
        {
            get { return _CAN_TSEG1; }
            set
            {
                if (_CAN_TSEG1 != value)
                {
                    _CAN_TSEG1 = value;
                    RaisePropertyChanged("CAN_TSEG1");
                }
            }
        }
        private int _CAN_TSEG2;
        public int CAN_TSEG2
        {
            get { return _CAN_TSEG2; }
            set
            {
                if (_CAN_TSEG2 != value)
                {
                    _CAN_TSEG2 = value;
                    RaisePropertyChanged("CAN_TSEG2");
                }
            }
        }
        private int _CAN_SJW;
        public int CAN_SJW
        {
            get { return _CAN_SJW; }
            set
            {
                if (_CAN_SJW != value)
                {
                    _CAN_SJW = value;
                    RaisePropertyChanged("CAN_SJW");
                }
            }
        }
        private string _CAN_DBC_Path;
        public string CAN_DBC_Path
        {
            get { return _CAN_DBC_Path; }
            set
            {
                if (_CAN_DBC_Path != value)
                {
                    _CAN_DBC_Path = value;
                    RaisePropertyChanged("CAN_DBC_Path");
                }
            }
        }
    }
}
