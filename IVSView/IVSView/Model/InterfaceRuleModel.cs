﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class InterfaceRuleModel : ObservableObject
    {
        public string SelectTestPoint { set; get; }
        public ObservableCollection<string> TestPointList { set; get; }
        public string InformationType { set; get; }
        public string EnvironmentVariable { set; get; }
        public string ProcessingRules { set; get; }

        public InterfaceRuleModel()
        {
            TestPointList = new ObservableCollection<string>();
        }
        
    }
}
