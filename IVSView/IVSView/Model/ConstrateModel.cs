﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using static PROST_Integration.Common.Common;

namespace PROST_Integration.Model
{
    public class ConstrateModel : ObservableObject
    {
        private T1ProjectInfoDataModel _SelectedTaskItem;
        public T1ProjectInfoDataModel SelectedTaskItem
        {
            get
            {
                return _SelectedTaskItem;
            }
            set
            {
                if (_SelectedTaskItem != value)
                {
                    _SelectedTaskItem = value;
                    
                    RaisePropertyChanged("SelectedTaskItem");
                }
            }
        }
        private string _SelectedTypeItem;
        public string SelectedTypeItem
        {
            get
            {
                return _SelectedTypeItem;
            }
            set
            {
                if (_SelectedTypeItem != value)
                {
                    _SelectedTypeItem = value;
                    RaisePropertyChanged("SelectedTypeItem");
                }
            }
        }
        private string _SelectedMinMaxItem;
        public string SelectedMinMaxItem
        {
            get
            {
                return _SelectedMinMaxItem;
            }
            set
            {
                if (_SelectedMinMaxItem != value)
                {
                    _SelectedMinMaxItem = value;
                    RaisePropertyChanged("SelectedMinMaxItem");
                }
            }
        }
        private int _Value;
        public int Value
        {
            get
            {
                return _Value;
            }
            set
            {
                if (_Value != value)
                {
                    _Value = value;
                    RaisePropertyChanged("Value");
                }
            }
        }


        private ObservableCollection<T1ProjectInfoDataModel> _TaskISRList;
        public ObservableCollection<T1ProjectInfoDataModel> TaskISRList
        {
            get
            {
                return _TaskISRList;
            }
            set
            {
                if (_TaskISRList != value)
                {
                    _TaskISRList = value;
                    RaisePropertyChanged("TaskISRList");
                }
            }
        }

        public ConstrateModel()
        {
            TaskISRList = new ObservableCollection<T1ProjectInfoDataModel>();
        }


    }
}
