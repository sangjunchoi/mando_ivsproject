﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROST_Integration.Model
{
    public class T1ScenarioCoreModel : ObservableObject
    {
        private string _CoreName;
        public string CoreName
        {
            get
            {
                return _CoreName;
            }
            set
            {
                if (_CoreName != value)
                {
                    _CoreName = value;
                    RaisePropertyChanged("CoreName");
                }
            }
        }
        private ObservableCollection<ConstrateModel> _ConstrateModelList;
        public ObservableCollection<ConstrateModel> ConstrateModelList
        {
            get
            {
                return _ConstrateModelList;
            }
            set
            {
                if (_ConstrateModelList != value)
                {
                    _ConstrateModelList = value;
                    RaisePropertyChanged("ConstrateModelList");
                }
            }
        }
        private ObservableCollection<TimingParamterModel> _TimingParamterModelList;
        public ObservableCollection<TimingParamterModel> TimingParamterModelList
        {
            get
            {
                return _TimingParamterModelList;
            }
            set
            {
                if (_TimingParamterModelList != value)
                {
                    _TimingParamterModelList = value;
                    RaisePropertyChanged("TimingParamterModelList");
                }
            }
        }
    
        public T1ScenarioCoreModel()
        {
            ConstrateModelList = new ObservableCollection<ConstrateModel>();
            TimingParamterModelList = new ObservableCollection<TimingParamterModel>();

            /*TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.CET, StartParamter = Common.Common.TIMING_PARAM_START, EndParamter = Common.Common.TIMING_PARAM_STOP });
            TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.DT, StartParamter = Common.Common.TIMING_PARAM_START, EndParamter = Common.Common.TIMING_PARAM_START });
            TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.GET, StartParamter = Common.Common.TIMING_PARAM_START, EndParamter = Common.Common.TIMING_PARAM_STOP });
            TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.IPT, StartParamter = Common.Common.TIMING_PARAM_ACTIVATION, EndParamter = Common.Common.TIMING_PARAM_START });
            TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.RT, StartParamter = Common.Common.TIMING_PARAM_ACTIVATION, EndParamter = Common.Common.TIMING_PARAM_STOP });
            TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.ST, StartParamter = Common.Common.TIMING_PARAM_STOP, EndParamter = Common.Common.TIMING_PARAM_ACTIVATION });
            TimingParamterModelList.Add(new TimingParamterModel() { TimingType = Common.Common.PER, StartParamter = Common.Common.TIMING_PARAM_ACTIVATION, EndParamter = Common.Common.TIMING_PARAM_ACTIVATION });*/
        }
    }
}
