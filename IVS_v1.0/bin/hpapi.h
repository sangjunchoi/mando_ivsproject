
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
//@@ File:
//@@   hpapi.h
//@@ Project:
//@@   HanilProtech Interface Driver Toolkit
//@@ Description:
//@@   Used for HanilProtech Hardware Config and Application made by user
//@@////////////////////////////////////////////////////////////////////////////////////////////@@//
//@@ Author:    juyeong park(juyeong.park@hanilprotech.com)
//@@ Date:      2019-07-26 
//@@ version:   1.0.1
//@@ Source:    http://hanilprotech.com/
//@@////////////////////////////////////////////////////////////////////////////////////////////@@//
//@@ Copyright (c) 2012 by Hanilprotech  All rights reserved.
//@@////////////////////////////////////////////////////////////////////////////////////////////@@//


#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <atlconv.h>
#include <atlstr.h>
#include <devguid.h>
#include <regstr.h>
#include <comdef.h>
#include <queue>
#include <deque>
#include <intrin.h>

#ifndef _HPAPI_H_                                        
#define _HPAPI_H_


#ifdef __cplusplus
extern "C" {
#endif

#define SOFT_SUPPORT		1

#define _HP_EXPORT_API   __stdcall

#define _EXPORT_API      __stdcall

  #if defined (DYNAMIC_CANDRIVER_DLL) || defined (DYNAMIC_HPDRIVER_DLL)
    #define _HP_EXPORT_DECL   _HP_EXPORT_API
    #define _HP_EXPORT_DEF    _HP_EXPORT_API
  #else
    #define _HP_EXPORT_DECL  __declspec(dllexport) _HP_EXPORT_API
    #define _HP_EXPORT_DEF   __declspec(dllexport) _HP_EXPORT_API
  #endif

#ifdef DYNAMIC_HPDRIVER_DLL

  #ifdef DO_NOT_DEFINE_EXTERN_DECLARATION

    #define DECL_STDHP_FUNC(apiname, apitype, args)    \
      typedef hpStatus (_HP_EXPORT_API *apitype) args

  #else

    #define DECL_STDHP_FUNC(apiname, apitype, args)    \
      typedef hpStatus (_HP_EXPORT_API *apitype) args; \
      extern apitype apiname

  #endif

#else

  #define DECL_STDHP_FUNC(apiname, apitype, args)      \
      hpStatus _HP_EXPORT_DECL apiname args
  #define IMPL_STDHP_FUNC(apiname, args)               \
     hpStatus _HP_EXPORT_DEF apiname args 

#endif 

#if SOFT_SUPPORT		
#pragma pack(push,1) 
#else
#include <pshpack1.h> 
#endif

//------------------------------------------------------------------------------
// Valuable Type definition section
//------------------------------------------------------------------------------

// Type definition for Error type
typedef short hpStatus;

// Type definition for convenience
typedef unsigned __int64 HPuint64;

// 불필요
typedef char *HPstringType;

// accessmask using 64bits
typedef HPuint64 HPaccess;

// Type definition for convenience 
typedef void *HANDLE;
typedef HANDLE HPhandle;

//------------------------------------------------------------------------------
// Transceiver types
//------------------------------------------------------------------------------
// CAN
#define HP_TRANSCEIVER_TYPE_NONE				    0x0000
#define HP_TRANSCEIVER_TYPE_CAN_1055_MAG			0x0090 
#define HP_TRANSCEIVER_TYPE_CAN_1051_MAG			0x0091
#define HP_TRANSCEIVER_TYPE_CAN_5790_MAG			0x0092
#define HP_TRANSCEIVER_TYPE_CAN_VIRTUAL				0x0001
#define HP_TRANSCEIVER_TYPE_MANUAL					0x0002
// ETC
#define HP_TRANSCEIVER_TYPE_DAIO					0x0093 
#define HP_TRANSCEIVER_TYPE_LIN_7269_MAG			0x0027  


//------------------------------------------------------------------------------
// Error section types
//------------------------------------------------------------------------------

//Error type definition
#define HP_MAX_ERR_SECTION                  20
#define HP_ERR_SECTION_FILE_OPEN            0
#define HP_ERR_SECTION_FILE_READ            1
#define HP_ERR_SECTION_FILE_WRITE           2
#define HP_ERR_SECTION_VERSION              3 
#define HP_ERR_SECTION_USB_INIT             4
#define HP_ERR_SECTION_USB_ENUM             5
#define HP_ERR_SECTION_USB_PIPECONF         6
#define HP_ERR_SECTION_USB_BULKIN           7
#define HP_ERR_SECTION_USB_BULKOUT          8
#define HP_ERR_SECTION_CALLBACK_APP         9
#define HP_ERR_SECTION_CALLBACK_CONFIG      10
#define HP_ERR_SECTION_CALLBACK_MANAGER     11
#define HP_ERR_SECTION_THREAD_TX            12
#define HP_ERR_SECTION_THREAD_RX            13
#define HP_ERR_SECTION_QUEFULL_TXCMD        14
#define HP_ERR_SECTION_QUEFULL_TXEVT        15
#define HP_ERR_SECTION_QUEFULL_RXEVT        16
#define HP_ERR_SECTION_WRONG_PARAMETER      17
#define HP_ERR_SECTION_WRONG_BUSTYPE        18
// driver status
#define HP_SUCCESS							0  
#define HP_PENDING							1  

#define HP_ERR_QUEUE_IS_EMPTY			    10 
#define HP_ERR_QUEUE_IS_FULL				11 
#define HP_ERR_TX_NOT_POSSIBLE				12 
#define HP_ERR_NO_LICENSE					14 
#define HP_ERR_QUEUE_PROTECT_LOOP			15
#define HP_ERR_VERSION_NOT_PRESENTED        16
// Application 
#define HP_ERR_APP_NOT_PRESENTED            20
#define HP_ERR_APP_NOT_READ                 21
#define HP_ERR_APP_NOT_WRITE                22
// USB
#define HP_ERR_USB_INIT                     30
#define HP_ERR_USB_ENUM                     31
#define HP_ERR_USB_PIPE_CONF                32
#define HP_ERR_USB_BULK_OUT                 33
#define HP_ERR_USB_BULK_IN                  34
#define HP_ERR_USB_PIPE_POLICY              35
// Callback
#define HP_ERR_CONFIG_CALLBACK              40
#define HP_ERR_MANAGER_CALLBACK             41
// thread
#define HP_ERR_TXRXTHREAD_NOT_CREATE		50
#define HP_ERR_WRONG_PARAMETER				101 
#define HP_ERR_TWICE_REGISTER				110 
#define HP_ERR_INVALID_CHAN_INDEX			111 
#define HP_ERR_INVALID_ACCESS				112 
#define HP_ERR_PORT_IS_OFFLINE				113 
#define HP_ERR_CHAN_IS_ONLINE				116 
#define HP_ERR_NOT_IMPLEMENTED				117 
#define HP_ERR_INVALID_PORT					118 
#define HP_ERR_HW_NOT_READY					120 
#define HP_ERR_CMD_TIMEOUT			        121 
#define HP_ERR_HW_NOT_PRESENT				129 
#define HP_ERR_NOTIFY_ALREADY_ACTIVE		131 
#define HP_ERR_NO_RESOURCES					152 
#define HP_ERR_WRONG_CHIP_TYPE				153 
#define HP_ERR_WRONG_COMMAND				154 
#define HP_ERR_INVALID_HANDLE				155 
#define HP_ERR_RESERVED_NOT_ZERO			157 
#define HP_ERR_CANNOT_OPEN_DRIVER			201 
#define HP_ERR_WRONG_BUS_TYPE				202 
#define HP_ERR_DLL_NOT_FOUND				203 
#define HP_ERR_INVALID_CHANNEL_MASK			204 
// special stream defines
#define HP_ERR_CONNECTION_BROKEN			210 
#define HP_ERR_CONNECTION_CLOSED			211 
#define HP_ERR_INVALID_STREAM_NAME			212 
#define HP_ERR_CONNECTION_FAILED			213 
#define HP_ERR_STREAM_NOT_FOUND				214 
#define HP_ERR_STREAM_NOT_CONNECTED			215 
#define HP_ERR_QUEUE_OVERRUN				216 
#define HP_ERROR							255 



////////////////////////////////////////////////////////////////////////////////
// LIN lib

#define HP_LIN_MASTER								(unsigned int) 01 
#define HP_LIN_SLAVE								(unsigned int) 02 
#define HP_LIN_VERSION_1_3							(unsigned int) 0x01 
#define HP_LIN_VERSION_2_0							(unsigned int) 0x02 
#define HP_LIN_VERSION_2_1							(unsigned int) 0x03 

#define HP_LIN_CALC_CHECKSUM						(unsigned short) 0x100 
#define HP_LIN_CALC_CHECKSUM_ENHANCED				(unsigned short) 0x200 

#define HP_LIN_SET_SILENT							(unsigned int) 0x01
#define HP_LIN_SET_WAKEUPID							(unsigned int) 0x03

#define HP_LIN_CHECKSUM_CLASSIC						(unsigned char) 0x00
#define HP_LIN_CHECKSUM_ENHANCED					(unsigned char) 0x01
#define HP_LIN_CHECKSUM_UNDEFINED					(unsigned char) 0xff

#define HP_LIN_STAYALIVE							(unsigned char) 0x00
#define HP_LIN_SET_SLEEPMODE						(unsigned char) 0x01
#define HP_LIN_COMESFROM_SLEEPMODE					(unsigned char) 0x02 

#define HP_LIN_WAKUP_INTERNAL						(unsigned char) 0x01
#define HP_LIN_UNDEFINED_DLC						(unsigned char) 0xff

#define HP_LIN_SLAVE_ON								(unsigned char) 0xff
#define HP_LIN_SLAVE_OFF							(unsigned char) 0x00




//------------------------------------------------------------------------------
// structures for LIN
//------------------------------------------------------------------------------
typedef struct {
	unsigned int	LINMode;    
	int				baudrate;
	unsigned int	LINVersion;
	unsigned int	reserved; 
} HPlinStatPar;

//------------------------------------------------------------------------------
// structures for CANFD
//------------------------------------------------------------------------------
// structure for CANFD Configuration 
typedef struct {
	unsigned int	arbitrationBitRate;
	unsigned int	sjwAbr;
	unsigned int	tseg1Abr;
	unsigned int	tseg2Abr;
	unsigned int	dataBitRate;
	unsigned int	sjwDbr;
	unsigned int	tseg1Dbr; 
	unsigned int	tseg2Dbr; 
	unsigned int	reserved[2]; 
} HPcanFdConf; 

////////////////////////////////////////////////////////////////////////////////
//  
//------------------------------------------------------------------------------
#define HP_APP_TYPE_MAX					10

#define HP_APP_CANLINK					0
#define HP_APP_CANLINK_MOBILE			1


#define HP_MAX_CAN_HARDWARE				64
#define HP_MAX_CAN_HARDWARE_CHANNEL		64
#define HP_MAX_CAN_CHANNEL				64

#ifndef MAX_MSG_LEN
#define MAX_MSG_LEN 8
#endif


#define HP_INTERFACE_VERSION			1                                                     

#define HP_CAN_EXT_MSG_ID				0x80000000                                                 

//-------------------------------------------------------------------------
// CAN Message Flag 
//-------------------------------------------------------------------------
#define HP_CAN_MSG_FLAG_ERROR_FRAME				0x01	// = 1
#define HP_CAN_MSG_FLAG_OVERRUN					0x02	// = 2
#define HP_CAN_MSG_FLAG_NERR					0x04 	// = 4
#define HP_CAN_MSG_FLAG_WAKEUP					0x08 	// = 8
#define HP_CAN_MSG_FLAG_REMOTE_FRAME			0x10	// = 16
#define HP_CAN_MSG_FLAG_RESERVED_1				0x20	// = 32
#define HP_CAN_MSG_FLAG_TX_COMPLETED			0x40 	// = 64
#define HP_CANFD_MSG_FLAG_TX_COMPLETED			0x100 	// = 256
#define HP_CAN_MSG_FLAG_TX_REQUEST				0x80 	// = 128

#define HP_LIN_MSGFLAG_TX						HP_CAN_MSG_FLAG_TX_COMPLETED 
#define HP_LIN_MSGFLAG_CRCERROR					0x81    // = 129

//-------------------------------------------------------------------------
// LIN Message Flag
//-------------------------------------------------------------------------
#define LIN_IGNORE 0x0
#define LIN_TX 0x1
#define LIN_RX 0x2

#define HP_LIN_MSG_FLAG_PID_PARITY_ERROR		0x030
#define HP_LIN_MSG_FLAG_RXDATA_TIMEOUT_ERROR	0x040
#define HP_LIN_MSG_FLAG_TXDATA_TIMEOUT_ERROR	0x050
#define HP_LIN_MSG_FLAG_CRC_TIMEOUT_ERROR		0x060
#define HP_LIN_MSG_FLAG_CRC_DATA_ERROR			0x070
#define HP_LIN_MSG_FLAG_HEADER_TIMEOUT_ERROR	0x080
#define HP_LIN_MSG_FLAG_NO_ANSER_ERROR			0x090
#define HP_LIN_MSG_FLAG_RX_BREAK_ERROR			0x100
#define HP_LIN_MSG_FLAG_UNDEFINED_ID_ERROR		0x110
#define HP_LIN_MSG_FLAG_HEADER_BREAK_ERROR		0x120
#define HP_LIN_MSG_FLAG_SYNC_ERROR				0x130

//-------------------------------------------------------------------------
// DAIO Message Flag
//-------------------------------------------------------------------------
#define HP_DAIO_DATA_GET						0x8000
#define HP_DAIO_DATA_VALUE_DIGITAL				0x0001
#define HP_DAIO_DATA_VALUE_ANALOG				0x0002
#define HP_DAIO_DATA_PWM						0x0010
#define HP_DAIO_MODE_PULSE						0x0020  

#define HP_DAIO_PORT_TYPE_MASK_DIGITAL			0x01
#define HP_DAIO_PORT_TYPE_MASK_ANALOG			0x02

#define HP_DAIO_TRIGGER_TYPE_CYCLIC				0x01
#define HP_DAIO_TRIGGER_TYPE_PORT				0x02

#define HP_DAIO_PORT_MASK_DIGITAL_D0			0x01
#define HP_DAIO_PORT_MASK_DIGITAL_D1			0x02
#define HP_DAIO_PORT_MASK_DIGITAL_D2			0x04
#define HP_DAIO_PORT_MASK_DIGITAL_D3			0x08
#define HP_DAIO_PORT_MASK_DIGITAL_D4			0x10
#define HP_DAIO_PORT_MASK_DIGITAL_D5			0x20
#define HP_DAIO_PORT_MASK_DIGITAL_D6			0x40
#define HP_DAIO_PORT_MASK_DIGITAL_D7			0x80

#define HP_DAIO_TRIGGER_TYPE_RISING				0x01
#define HP_DAIO_TRIGGER_TYPE_FALLING			0x02
#define HP_DAIO_TRIGGER_TYPE_BOTH				0x03

//-------------------------------------------------------------------------
// ChipState Message Flag
//-------------------------------------------------------------------------
#define HP_CHIPSTAT_BUSOFF					0x01
#define HP_CHIPSTAT_ERROR_PASSIVE			0x02
#define HP_CHIPSTAT_ERROR_WARNING			0x04
#define HP_CHIPSTAT_ERROR_ACTIVE			0x08

//-------------------------------------------------------------------------
// SyncPulse Message Flag
//-------------------------------------------------------------------------
#define HP_SYNC_PULSE_EXTERNAL				0x00
#define HP_SYNC_PULSE_OUR					0x01
#define HP_SYNC_PULSE_OUR_SHARED			0x02

//-------------------------------------------------------------------------
// CANFD Message Flag
//-------------------------------------------------------------------------
//////////          CANFD Max Data Lengrth      //////////
#define HP_CAN_MAX_DATA_LEN                  64 

//////////          CANFD TX Msg Flag           //////////
#define HP_CAN_TXMSG_FLAG_EDL                0x0001    // extended data length
#define HP_CAN_TXMSG_FLAG_BRS                0x0002    // baud rate switch
#define HP_CAN_TXMSG_FLAG_RTR                0x0010    // remote transmission request
#define HP_CAN_TXMSG_FLAG_HIGHPRIO           0x0080    // high priority message - clears all send buffers - then transmits
#define HP_CAN_TXMSG_FLAG_EF                 0x0200
#define HP_CAN_TXMSG_FLAG_WAKEUP             0x2000    // generate a wakeup message

//////////          CANFD RX Msg Flag           //////////
#define HP_CAN_RXMSG_FLAG_EDL                0x0001    // extended data length
#define HP_CAN_RXMSG_FLAG_BRS                0x0002    // baud rate switch
#define HP_CAN_RXMSG_FLAG_ESI                0x0004    // error state indicator
#define HP_CAN_RXMSG_FLAG_RTR				 0x0010		//Remote frame	
#define HP_CAN_RXMSG_FLAG_EF                 0x0200    // error frame (only posssible in XL_CAN_EV_TX_REQUEST/XL_CAN_EV_TX_REMOVED)
#define HP_CAN_RXMSG_FLAG_ARB_LOST           0x0400    // Arbitration Lost
                                                       // set if the receiving node tried to transmit a message but lost arbitration process
#define HP_CAN_RXMSG_FLAG_WAKEUP             0x2000    // high voltage message on single wire CAN
#define HP_CAN_RXMSG_FLAG_TE                 0x4000    // 1: transceiver error detected

/*----------------------------------/
	CAN event structure
/----------------------------------*/
struct s_hp_can_msg {  /* 16 Bytes */
	unsigned long		id;
	unsigned short		flags;
	unsigned short		dlc;
	unsigned char		data [MAX_MSG_LEN];
};


/*----------------------------------/
	LIN event structure
/----------------------------------*/
struct s_hp_lin_msg {
	unsigned char	id;
	unsigned char	dlc;
	unsigned short	flags;
	unsigned char	data[8];
	unsigned char	crc;
};
struct s_hp_lin_sleep { 
	unsigned char	flag;
};
struct s_hp_lin_no_ans {
	unsigned char	id;
};
struct s_hp_lin_wake_up {
	unsigned char	flag;
};
struct s_hp_lin_crc_info {
	unsigned char	id;
	unsigned char	flags;
};

union  s_hp_lin_msg_api {
	struct s_hp_lin_msg			linMsg; 
	struct s_hp_lin_no_ans		linNoAns;
	struct s_hp_lin_wake_up		linWakeUp;
	struct s_hp_lin_sleep		linSleep;
	struct s_hp_lin_crc_info	linCRCinfo;
};


/*----------------------------------/
	DAIO event structure
/----------------------------------*/
struct s_hp_daio_data {  /* 23 Bytes */
	unsigned short		flags;                 // 2
	unsigned int		timestamp_correction;  // 4
	unsigned char		mask_digital;          // 1
	unsigned char		value_digital;         // 1
	unsigned char		mask_analog;           // 1
	unsigned short		value_analog[4];       // 8
	unsigned int		pwm_frequency;         // 4
	unsigned short		pwm_value;             // 2
};


/*----------------------------------/
	ChipState event structure
/----------------------------------*/
struct s_hp_chip_state {
	unsigned char	busStatus;
	unsigned char	txErrorCounter;
	unsigned char	rxErrorCounter;
};


/*----------------------------------/
	Sync pulse event structure
/----------------------------------*/
struct s_hp_sync_pulse {
	unsigned char	pulseCode;
	HPuint64		time; 
};


#define HP_OUTPUT_MODE_SILENT						0 
#define HP_OUTPUT_MODE_NORMAL						1 
#define HP_OUTPUT_MODE_TX_OFF						2
#define HP_OUTPUT_MODE_SJA_1000_SILENT				3

#define HP_TRANSCEIVER_EVENT_ERROR					1
#define HP_TRANSCEIVER_EVENT_CHANGED				2


/*----------------------------------/
	CANFD event structure
/----------------------------------*/
//-----  CANFD RX Event Structure  -----//
typedef struct {
	unsigned int	canId;
	unsigned int	msgFlags;
	unsigned int	crc;
	unsigned char	reserved1[12];
	unsigned short	totalBitCnt;
	unsigned char	dlc;
	unsigned char	reserved[5];
	unsigned char	data[HP_CAN_MAX_DATA_LEN];
} HP_CAN_EV_RX_MSG; 
typedef struct {
	unsigned int	canId;
	unsigned int	msgFlags;
	unsigned char	dlc;
	unsigned char	reserved1;
	unsigned short	reserved;
	unsigned char	data[HP_CAN_MAX_DATA_LEN];
} HP_CAN_EV_TX_REQUEST; 

// to be used with XL_CAN_EV_TAG_CHIP_STATE
typedef struct {
	unsigned char	busStatus;
	unsigned char	txErrorCounter;
	unsigned char	rxErrorCounter;
	unsigned char	reserved;
	unsigned int	reserved0;
} HP_CAN_EV_CHIP_STATE; 

//to be used with XL_CAN_EV_TAG_RX_ERROR/XL_CAN_EV_TAG_TX_ERROR
typedef struct {
	unsigned char	errorCode;
	unsigned char	reserved[95];
} HP_CAN_EV_ERROR; 

typedef s_hp_sync_pulse HP_CAN_EV_SYNC_PULSE; 

//-----  CANFD TX Event Structure  -----//
typedef struct {
	unsigned int	canId;
	unsigned int	msgFlags;
	unsigned char	dlc;
	unsigned char	reserved[7];
	unsigned char	data[HP_CAN_MAX_DATA_LEN];
} HP_CAN_TX_MSG;  

/////////// Enumeration of HPevent ///////////
enum e_HPevent_type {
		HP_NO_COMMAND				= 0,
		HP_RECEIVE_MSG				= 1,
		HP_CHIP_STATE				= 4,
		HP_TRANSCEIVER				= 6,
		HP_TIMER					= 8,
		HP_RESET_TIMER				= 9,
		HP_TRANSMIT_MSG				= 10,
		HP_SYNC_PULSE				= 11,
		HP_APPLICATION_NOTIFICATION	= 15,
		HP_TXSTART_CMD				= 16,
		HP_TXSTOP_CMD				= 17,
		HP_BrokenData				= 18,
		HP_LoseData					= 19,
		
		//LIN
		HP_LIN_MSG					=20,
		HP_LIN_ERRMSG				=21,
		HP_LIN_SYNCERR				=22,
		HP_LIN_NOANS				=23,
		HP_LIN_WAKEUP				=24,
		HP_LIN_SLEEP				=25,
		HP_LIN_CRCINFO				=26,
		
		//DAIO
		HP_RECEIVE_DAIO_DATA		=32,  
		
		//CAN-FD
		HP_CAN_EV_TAG_RX_OK			=33, 
		HP_CAN_EV_TAG_RX_ERROR		=34, 
		HP_CAN_EV_TAG_TX_ERROR		=35, 
		HP_CAN_EV_TAG_TX_REQUEST	=36, 
		HP_CAN_EV_TAG_TX_OK			=37, 
		HP_CAN_EV_TAG_STATISTIC		=38, 
		HP_CAN_EV_TAG_CHIP_STATE	=39,
		HP_CAN_EV_TAG_TX_MSG		=40,
     };

//------------------------------------------------------------------------------
// HP_EVENT structures
//------------------------------------------------------------------------------
union s_hp_tag_data {
	struct s_hp_can_msg					msg;
	struct s_hp_chip_state				chipState;
	union  s_hp_lin_msg_api				linMsgApi;    
	struct s_hp_sync_pulse		        syncPulse;
	struct s_hp_daio_data				daioData; 
};
typedef unsigned char  HPeventTag;

// Old version Event structure
struct s_hp_event {
			char				start;			 // start check	
			HPeventTag			tag;             // 1                          
			unsigned char		chanIndex;       // 1
			unsigned short		transId;         // 2
			unsigned short		portHandle;      // 2 internal use only !!!!
			HPuint64			timeStamp;       // 8
			union s_hp_tag_data	tagData;         // 32 Bytes 
			char				end;			 // end check
};
typedef struct s_hp_event HPevent_old;                    
 
// Event Structure compatible with CANFD
struct s_hp_event_ex {
	char				start;			 // start check	
	HPeventTag			tag;             // 1                          
	unsigned char		chanIndex;       // 1
	unsigned short		transId;         // 2
	unsigned short		portHandle;      // 2 internal use only !!!!
	HPuint64			timeStamp;       // 8
	union {
		struct s_hp_can_msg			msg;
		struct s_hp_chip_state		chipState;
		union  s_hp_lin_msg_api		linMsgApi;    
		struct s_hp_sync_pulse		syncPulse;
		struct s_hp_daio_data		daioData; 
		HP_CAN_EV_RX_MSG			canTxOkMsg;
		HP_CAN_EV_ERROR				canError;
		HP_CAN_EV_CHIP_STATE		canChipState;
		HP_CAN_EV_SYNC_PULSE		canSyncPulse;
	} tagData;
	char				end;			 // end check
}; 
typedef struct s_hp_event_ex HPevent;  


#define HP_HWTYPE_NONE					0
#define HP_HWTYPE_CANbeep				1
#define HP_HWTYPE_CANlink_Mini			2
#define HP_HWTYPE_CANlink_FD			3
#define HP_HWTYPE_VIRTUAL				5

#define HP_MAX_HWTYPE					63
#define HP_HWTYPE_Res					255

//------------------------------------------------------------------------------
#define HP_CHANNEL_MASK(x)				(1I64<<(x))

#define HP_MAX_APPNAME					32

//------------------------------------------------------------------------------

#define HP_MAX_LENGTH					31
#define HP_CONFIG_MAX_CHANNELS			64
 
#define HP_ACTIVATE_NONE				0 
#define HP_ACTIVATE_RESET_CLOCK			8

#define HP_BUS_TYPE_NONE				0x00000000
#define HP_BUS_TYPE_CAN					0x00000001
#define HP_BUS_TYPE_LIN					0x00000002
#define HP_BUS_TYPE_DAIO				0x00000040 

#define HP_BUS_COMPATIBLE_CAN			HP_BUS_TYPE_CAN
#define HP_BUS_COMPATIBLE_LIN			HP_BUS_TYPE_LIN
#define HP_BUS_COMPATIBLE_DAIO			HP_BUS_TYPE_DAIO

#define HP_BUS_ACTIVE_CAP_CAN			(HP_BUS_COMPATIBLE_CAN<<16)
#define HP_BUS_ACTIVE_CAP_LIN			(HP_BUS_COMPATIBLE_LIN<<16)
#define HP_BUS_ACTIVE_CAP_DAIO			(HP_BUS_COMPATIBLE_DAIO<<16)


//------------------------------------------------------------------------------       

#define HP_CAN_STD 01    
#define HP_CAN_EXT 02                        

//------------------------------------------------------------------------------



#define HP_INVALID_PORTHANDLE (-1)
typedef long HPportHandle, *pHPportHandle;     

#define HP_FPGA_CORE_TYPE_NONE							0
#define HP_FPGA_CORE_TYPE_CAN							1
#define HP_FPGA_CORE_TYPE_LIN							2
#define HP_FPGA_CORE_TYPE_LIN_RX						3

#define HP_SPECIAL_DEVICE_STAT_FPGA_UPDATE_DONE			0x01 




//-------------------------------------------------------------------------
//  Driver, Channel Config structure
//-------------------------------------------------------------------------
typedef struct {                                                                         
  unsigned int		busType;
  union {
    struct {
		unsigned int	bitRate;
		unsigned char	sjw;
		unsigned char	tseg1;
		unsigned char	tseg2;
		unsigned char	sam;  // 1 or 3
		unsigned char	outputMode;
    } can;
    unsigned char	raw[32];
  }data;
} HPbusParams; 

typedef struct s_hp_license_info {
	unsigned int	ProgramLicense;
	unsigned int	OptionLicense;
} HP_LICENSE_INFO;
typedef HP_LICENSE_INFO HPlicenseInfo;

// structure used for hpCanSetChannelParams() function
typedef struct {
	unsigned long		bitRate;
	unsigned char		sjw;
	unsigned char		tseg1;
	unsigned char		tseg2;
	unsigned char		sam;  // 1 or 3
} HPchipParams; 

// structure used for hpGetDriverConfig
typedef struct s_hp_channel_config {
	char				name [HP_MAX_LENGTH + 1];
	unsigned char		hwType;      
	unsigned char		hwIndex;
	unsigned char		hwChannel;      
	unsigned short		transceiverType;
	unsigned short		transceiverState; 
	unsigned char		channelIndex;                    
	HPuint64			channelMask;                 
	unsigned int		hwTypeNum;               
	unsigned int		channelBusCapabilities;    
	unsigned char		isOnBus;   
	unsigned int		connectedBusType; 
	HPbusParams			busParams;                
	HPlicenseInfo		licenseInfo;                                              
	unsigned int		driverVersion;            
	unsigned int		interfaceVersion; 
	unsigned int		raw_data[10];                              
	unsigned int		serialNumber;                           
	char				transceiverName [HP_MAX_LENGTH + 1];
	unsigned int		reserved[3];
} HP_CHANNEL_CONFIG;
typedef HP_CHANNEL_CONFIG  HPchannelConfig;
typedef HP_CHANNEL_CONFIG  *pHPchannelConfig; 

typedef struct s_hp_driver_config {
	unsigned int		dllVersion;
	unsigned int		channelCount; 
	unsigned int		reserved[10];
	HPchannelConfig		channel[HP_CONFIG_MAX_CHANNELS];   
} HP_DRIVER_CONFIG;
typedef HP_DRIVER_CONFIG  HPdriverConfig;
typedef HP_DRIVER_CONFIG  *pHPdriverConfig;



//-------------------------------------------------------------------------
//  DAIO Info structure and define
//-------------------------------------------------------------------------
#define HP_DAIO_DIGITAL_ENABLED						0x00000001
#define HP_DAIO_DIGITAL_INPUT						0x00000002 
#define HP_DAIO_DIGITAL_TRIGGER						0x00000004

#define HP_DAIO_ANALOG_ENABLED						0x00000001 
#define HP_DAIO_ANALOG_INPUT						0x00000002 
#define HP_DAIO_ANALOG_TRIGGER						0x00000004  
#define HP_DAIO_ANALOG_RANGE_32V					0x00000008  

#define HP_DAIO_TRIGGER_MODE_NONE					0x00000000
#define HP_DAIO_TRIGGER_MODE_DIGITAL				0x00000001
#define HP_DAIO_TRIGGER_MODE_ANALOG_ASCENDING		0x00000002 
#define HP_DAIO_TRIGGER_MODE_ANALOG_DESCENDING		0x00000004
#define HP_DAIO_TRIGGER_MODE_ANALOG					(HP_DAIO_TRIGGER_MODE_ANALOG_ASCENDING | \
													HP_DAIO_TRIGGER_MODE_ANALOG_DESCENDING)  

#define HP_DAIO_TRIGGER_LEVEL_NONE					0  

#define HP_DAIO_POLLING_NONE						0  

#define HP_DAIO_PIN_MAX								32

// DAIO Pin Type(Group)
#define HP_DAIO_PIN_TYPE_UNUSED						0
#define HP_DAIO_PIN_TYPE_AGND						1
#define HP_DAIO_PIN_TYPE_DGND						2
#define HP_DAIO_PIN_TYPE_DIGITAL					3
#define HP_DAIO_PIN_TYPE_ANALOG						4

// DAIO Pin Function( bit check )
#define HP_DAIO_PIN_FUNCTION_INPUT					( 1 << 0 )
#define HP_DAIO_PIN_FUNCTION_OUTPUT					( 1 << 1 )
#define HP_DAIO_PIN_FUNCTION_OUTPUT_PP				( 1 << 2 )
#define HP_DAIO_PIN_FUNCTION_OUTPUT_OC				( 1 << 3 )
#define HP_DAIO_PIN_FUNCTION_RELAY					( 1 << 4 )
#define HP_DAIO_PIN_FUNCTION_PWM_PP					( 1 << 5 )
#define HP_DAIO_PIN_FUNCTION_PWM_OC					( 1 << 6 )
#define HP_DAIO_PIN_FUNCTION_PWM_CAPTURE			( 1 << 7 )

// DAIO Pin Trigger Type( bit check )
#define HP_DAIO_PIN_TRIGGERTYPE_CYCLIC				( 1 << 0 )
#define HP_DAIO_PIN_TRIGGERTYPE_DIGITAL				( 1 << 1 )
#define HP_DAIO_PIN_TRIGGERTYPE_ANALOG				( 1 << 2 )

// DAIO Port Type
#define HP_DAIO_PORTTYPE_DSUB_MALE					0
#define HP_DAIO_PORTTYPE_DSUB_FEMALE				1

typedef struct{
	unsigned char	PinNumber;		// 0, 1, 2, 3, 4, 5, 6, 7, 8 ...
	unsigned char 	Type;			// Unused, Digital, Analog, Gnd
	unsigned char 	TypeNumber;		// D0, D1, D2, D3 ...
	unsigned int	Function;		// Input, Output(pp), Output(oc), PWM(pp), PWM(oc), Relay, Capture ...
	unsigned char 	ConnectedPin;  	// 0, 1, 2, 3, 4 ,5 ,6 ,7, 8 ...
	unsigned int	TriggerType;	// Cyclic, Digital Trigger, Analog Trigger ...
	unsigned int 	reserved[4];	// reserved
} s_hp_daio_pin_info;

typedef struct{
	char 				hwIndex;
	char				hwChannel;
	unsigned char		PortType;
    unsigned char		PinCount;
	s_hp_daio_pin_info	daio_pin_info[HP_DAIO_PIN_MAX];
} s_hp_daio_ch_info;

typedef struct{
	unsigned int portTypeMask;
	unsigned int triggerType;
	union triggerTypeParams {
		unsigned int cycleTime;
		struct {
			unsigned int portMask;
			unsigned int type;
		} digital;
	} param;
} HPdaioTriggerMode;

typedef struct{
	unsigned int portMask;
	unsigned int valueMask;
} HPdaioDigitalParams;

typedef struct
{
	unsigned char	nPinNumber;
	unsigned char	nType;				//Analog, Digital, Gnd..
	unsigned char	nTypeNumber;		//D1, D2, D3,.,,,
	unsigned int	nFunction;			//Input, Output,Relay.....
	unsigned char	nConnectedPin;
} hp_DAIO_Pin_Info; 

typedef struct
{
	int	nHWIndex;
	int	nChannelIndex;
	int	nPortType;
	int	nCycleTime;

	hp_DAIO_Pin_Info m_listPin[HP_DAIO_PIN_MAX];
} hp_DAIO_Ch_Info; 

//--------------------------------------------
//  Acceptance Filter structure
//--------------------------------------------
struct _HPacc_filt {
	unsigned char	isSet;
	unsigned long	code;
	unsigned long	mask; 
};
typedef struct _HPacc_filt  HPaccFilt;

struct _HPacceptance {
	HPaccFilt	std;
	HPaccFilt	xtd;
};
typedef struct _HPacceptance HPacceptance;

// defines for hpSetBlovalTimeSync
#define HP_SET_TIMESYNC_NO_CHANGE		(unsigned long)  0
#define HP_SET_TIMESYNC_ON				(unsigned long)  1
#define HP_SET_TIMESYNC_OFF				(unsigned long)  2

//------------------------------------------------------------------------------
//		Common Used Define
//------------------------------------------------------------------------------
#define HP_MAX_APPNAME_LENGTH			128
#define HP_MAX_RX_QUEUE_BUFFSIZE		4096
#define HP_MAX_QUEUE_COUNT				100
#define HP_HARDWARE_CHANNEL_MAX			HP_MAX_CAN_HARDWARE_CHANNEL
#define HPCOMMANDSTRING					"HPCMD"
#define DLL_VERSION						"1.0.1.0"
#define HP_APP_ADD						1
#define HP_APP_MODIFY					2
#define HP_APP_REMOVE					3
#define HP_DEVICE_NAME					"CANlink Interface"
#define PORTHANDLEINDEX					0x10
#define CMD_Q_MAX						64
#define TX_Q_MAX						2048	//4096 //2048
#define RX_Q_MAX						2048	//4096 //2048  
#define TX_EVENT_BUF_SIZE				4		//1 
#define RX_EVENT_BUF_SIZE				64		//64
#define RX_ONETIME_EVENT_BUF_SIZE		8       //8
#define RX_ONETIME_EVENT_BUF_SIZE_FD	5       //8

enum e_HP_ioctl_type
{
	HP_IOCTL_SET_APPLCONFIG			= 0,
	HP_IOCTL_MODIFY_APPLCONFIG,		//1
	HP_IOCTL_REMOVE_APPLCONFIG,		//2
	HP_IOCTL_GET_DRIVERL_CONFIG ,   //3
	HP_IOCTL_GET_CHANNEL_INDEX,     //4
	HP_IOCTL_GET_CHANNEL_MASK,      //5
	HP_IOCTL_SET_TIMERRATE,         //6
	HP_IOCTL_ACTIVE_CHANNEL,        //7
	HP_IOCTL_DEACTIVE_CHANNEL,      //8
	HP_IOCTL_GENERATE_SYNCPULSE,    //9
	HP_IOCTL_GET_LICENSEINFO,       //10
	HP_IOCTL_RESET_TIMER,           //11

	HP_IOCTL_CAN_SET_CHANNELOUTPUT,			//12
	HP_IOCTL_CAN_SET_CHANNELMODE,			//13
	HP_IOCTL_CAN_SET_RECEIVEMODE,			//14
	HP_IOCTL_CAN_SET_CHANNELTRAN,			//15
	HP_IOCTL_CAN_SET_CHANNELPARAMS,			//16
	HP_IOCTL_CAN_SET_CHANNELPARAMS_C200,	//17  0x11
	HP_IOCTL_CAN_SET_CHANNELBITRATE,		//18
	HP_IOCTL_CAN_SET_CHANNELACCEPTANCE,     //19
	HP_IOCTL_CAN_ADD_ACCEPTANCERANGE,       //20
	HP_IOCTL_CAN_REMOVE_ACCEPTANCERANGE,    //21
	HP_IOCTL_CAN_RESET_ACCEPTANCE,          //22
	HP_IOCTL_CAN_REQUEST_CHIPSTATE,         //23
	HP_IOCTL_CAN_GET_BUSLOAD,               //24

	HP_IOCTL_DAIO_SET_PWMOUTPUT,			//25
	HP_IOCTL_DAIO_SET_DIGTALOUTPUT,			//26
	HP_IOCTL_DAIO_SET_ANALOGOUTPUT,			//27
	HP_IOCTL_DAIO_REQUEST_MEASUREMENT,		//28
	HP_IOCTL_DAIO_SET_DIGTALPARAM,			//29
	HP_IOCTL_DAIO_SET_ANALOGPARAM,			//30
	HP_IOCTL_DAIO_SET_ANALOGTRIGGER,		//31
	HP_IOCTL_DAIO_SET_MEASUREMENTFREQ,		//32
	HP_IOCTL_DAIO_SET_DIGTALTRIGGER,		//33

	HP_IOCTL_LIN_SET_DLC,					//34
	HP_IOCTL_LIN_SET_SLAVE,					//35
	HP_IOCTL_LIN_SENDREQUEST,				//36
	HP_IOCTL_LIN_SET_SLEEPMODE,				//37
	HP_IOCTL_LIN_SET_WAKEUP,				//38
	HP_IOCTL_LIN_SET_CHECKSUM,				//39
	HP_IOCTL_LIN_SWITCHSLAVE,				//40

	HP_IOCTL_HANILPROTECH,					//41	//CheckLicense
	HP_IOCTL_READ_EEPROM,					//42
	HP_IOCTL_WRITE_EEPROM,					//43

	HP_IOCTL_LIN_SET_CHANNELPARAMS,			//44
	HP_IOCTL_CANFD_SET_CONFIGURATION,		//45

	HP_IOCTL_LIN_SET_MASTER_RESISTOR,		//46
	HP_IOCTL_IO_SET_TRIGGER_MODE,			//47
	HP_IOCTL_IO_SET_DIGTALOUTPUT,			//48

	HP_IOCTL_FPGA_VERSION_NUMBER			//49
}; 

typedef struct hp_ioctl_hwtype{
	char				CmdString[6];
	unsigned char		HpCmd;
    char				hwIndex;		// [-1,0,1]
    char				hwChannel;		// [-1,0,1]
	unsigned char		nParams;		// Counts of Params
	HPuint64			Param1;			// data 1
	HPuint64			Param2;			// data 2
	HPuint64			Param3;			// data 3
	HPuint64			Param4;			// data 4
	HPuint64			Param5;			// data 4
}; 

typedef struct hp_hwtype{
	int		hwIndex;		// [-1,0,1]
	int		hwChannel;		// [-1,0,1]
}; 


//------------------------------------------------------------------------------
//  Channel, Application Info structure
//------------------------------------------------------------------------------
typedef struct
{
	hp_hwtype				stHW;
	int						appchannel;
	int						accesschannel;
	unsigned long			timerrate;
	HPuint64				basetimestamp;
	bool					isOnBus;
	unsigned int			busType;
}hp_channelinfo_type;

typedef struct
{
	HPportHandle			PortHandle;
	char					userName[HP_MAX_APPNAME_LENGTH];
	HPaccess				accessMask;
	HPaccess				PermissionMask;
	unsigned int			rxQueueSize;
	int						queueLevel;
	unsigned int			hpInterfaceVersion;
	int						totalChannel;
	hp_channelinfo_type		channelinfo[HP_CONFIG_MAX_CHANNELS];
}hp_stAppPortType;

typedef struct {
	int     appChannel;
	int     hwIndex;
	int     hwChannel;
	int     busType;
	int     hwType;
	int     hwNum;	
	int     hwSerial;	
}hp_stAppConfigInfo; 

typedef struct 
{
	char					appName[HP_MAX_APPNAME_LENGTH];
	hp_stAppConfigInfo		ConfigInfo[HP_CONFIG_MAX_CHANNELS];
}hp_stAppConfigType;

typedef struct 
{
	HPportHandle		portHandle;
	unsigned int		hwIndex;
	unsigned int		hwChannel;
	void*				hp_event_cb; 
}hp_stSetNotiType;

typedef void(* hp_callback_type)(void);


//------------------------------------------------------------------------------
//  structure for USB handling
//------------------------------------------------------------------------------
typedef struct
{
          HANDLE							winUsbHandle;
		  HANDLE							winUsbHandle_1;
		  unsigned char						bulkInPipe;
		  unsigned char						bulkOutPipe;

}WinUsbDevice; 


#include <poppack.h>
#include <pshpack8.h>
#include <pshpack1.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////                                  Function calls                                  ////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------//
//                              Common Function Start                               //
//----------------------------------------------------------------------------------//
hpStatus _HP_EXPORT_DECL hpLoadLibrary(BOOL bCheck = TRUE);
hpStatus _HP_EXPORT_DECL hpUnloadLibrary(void);

/*
	Func name 	: hpGetAppSettings()
 	Description : Add an AppSetting using the appName parameter
*/ 
DECL_STDHP_FUNC ( hpSetAppAdd, HPSETAPPADD, (
					char* appName)
				);

/*
	Func name 	: hpSetAppRemove()
 	Description : Remove an AppSetting using the appName parameter
*/ 
DECL_STDHP_FUNC ( hpSetAppRemove, HPSETAPPREMOVE, (
				  char* appName)
				);

/*
	Func name 	: hpSetAppEdit()
 	Description : Modify an AppSetting with specific appName using BusType, Value Parameters
*/ 
DECL_STDHP_FUNC ( hpSetAppEdit, HPSETAPPEDIT, (
				  char* appName,
				  unsigned int BusType,
				  unsigned int Value)
				);

/*
	Func name 	: hpGetAppBusCount()
 	Description : Get Connected BusCount of Application using appName, BusType(CAN/LIN/DAIO/..) and
				  output to pValue parameter
*/ 
DECL_STDHP_FUNC ( hpGetAppBusCount, HPGETBUSCOUNT, (
				  char* appName	,
				  unsigned int BusType,
				  unsigned int *pValue)
				);

/*
	Func name 	: hpGetAppSettings()
 	Description : Function to get AppSettings by name and Channel
*/ 
DECL_STDHP_FUNC ( hpGetAppSettings, HPGETAPPSETTINGS, (
				  char            *appName,   
                  unsigned int     appChannel, 
                  unsigned int    *pHwIndex, 
                  unsigned int    *pHwChannel, 
				  unsigned int	 *pBusType,
                  unsigned int     busType)
				);

/*
	Func name 	: hpSetAppSettings()
 	Description : Function to Add or modify AppSettings using parameters
*/ 
DECL_STDHP_FUNC ( hpSetAppSettings, HPSETAPPSETTINGS, (
                  char*            appName,
                  unsigned int     appChannel,
                  unsigned int     hwIndex,
                  unsigned int     hwChannel,
                  unsigned int     busType,
                  unsigned int	   mode)
                );

/*
	Func name 	: hpGetLicense()
 	Description : Function to get license infomation currently connected using Appname, and
	 			  output to theplicenseInfo parameter
*/ 
DECL_STDHP_FUNC ( hpGetLicense, HPGETLICENSE, (
                  unsigned int App_Name,
                  HPlicenseInfo *plicenseInfo)
                );

/*
	Func name 	: hpGetLicenseALL()
 	Description : Function to output all license information currently connected to the parameter
*/ 
DECL_STDHP_FUNC ( hpGetLicenseALL, HPGETLICENSEALL, (
                  void* plicenseInfo)
                );

/*
	Func name 	: hpGetDriverConfig()
 	Description : Get connected driverConfiguration and output to pDriverConfig parameter
*/ 
DECL_STDHP_FUNC ( hpGetDriverConfig, HPGETDRIVERCONFIG, (
				  HPdriverConfig *pDriverConfig)
				);

/*
	Func name 	: hpGetChIndex()
 	Description : Get Channel Index through specific hardware index and channel parameters and
	 			  output to ChIndex parameter
*/ 
DECL_STDHP_FUNC ( hpGetChIndex, HPGETCHINDEX, (
				  int hwIndex,  
				  int hwChannel, 
				  int *ChIndex)
				);

/*
	Func name 	: hpGetChMask()
 	Description : Get ChannelMask through specific hardware index and channel parameters and
	 			  output to ChMask parameter
*/ 
DECL_STDHP_FUNC ( hpGetChMask, HPGETCHMASK, (
				  int hwIndex,    
				  int hwChannel, 
				  HPaccess *ChMask)
				);

/*
	Func name 	: hpCreateObject()
 	Description : Create PortHandle with name and accessMask, bus type
*/ 
DECL_STDHP_FUNC ( hpCreateObject, HPOPENPORT, (   
                  HPportHandle   *pObject,
                  char           *AppName,
                  HPaccess       accessMask,
                  HPaccess       *pPermissionMask,
                  unsigned int   busType)
                );

//Unnessasary
/*
	Func name 	: hpCreateObject()
 	Description : The timer of the port will be activated/deacticated and the
				  rate for cyclic timer events is set.
				  The resolution of the parameter 'timerRate' is 10us.
				  The accepted values for this parameter are 100, 200, 300,... resulting
				  in an effective timerrate of 1000us, 2000us, 3000us,...
*/ 
DECL_STDHP_FUNC ( hpSetTimerRate, HPSETTIMERRATE, (
                  HPportHandle   Object,
                  unsigned long  timerRate)
                  );

/*
	Func name 	: hpSetTimerRateAndChannel()
 	Description : This function sets the timerrate for timerbased-notify feature using out from
				  the specified channels the one which is best suitable for exact timestamps.
				  If only one channel is specified, this channel is used for timer event generation.
				  Only channels that are assigned to the port specified by parameter portHandle may be used. 
				  Parameter timerRate specifies the requested timer event's cyclic rate; passed back is
				  the timer rate used by the driver. The resolution of this parameter is 10us.
				  A value timerRate=0 would disable timer events.
  				  Returns in parameter timerChannelMask the channel that is best suitable 
				  for timer event generation out of the channels specified by paramter 
				  timerChannelMask. The timer rate value may be below 1ms, but is limited to the following
				  discrete values (with 'x' as unsigned decimal value):
				  CAN/LIN     : 250 us ... (250 us + x * 250 us)

				  Example: timerRate=25  ==> Used timerrate would be 250us. 
*/ 
DECL_STDHP_FUNC ( hpSetTimerRateAndChannel, HPSETTIMERRATEANDCHANNEL, (
                  HPportHandle   Object, 
                  HPaccess       accessMask, 
                  unsigned long  timerRate)
                  );

/*
	Func name 	: hpResetTimer()
 	Description : The clock generating timestamps for the port will be reset
*/ 
DECL_STDHP_FUNC ( hpResetTimer, HPRESETTIMER, (
				  HPportHandle Object)
				  );

/*
	Func name 	: hpSetNotification()
 	Description : Setup a event to notify the application if there are messages in the
				  ports receive queue.
				  queueLevel specifies the number of messages that triggeres the event.
				  Note that the event is triggered only once, when the queueLevel is
				  reached. An application should read all available messages by hpReceive
				  to be sure to reenable the event. The API generates the handle by
				  itself. For LIN the queueLevel is fix to one.
*/ 
DECL_STDHP_FUNC ( hpSetNotification, HPSETNOTIFICATION, (
                  HPportHandle  Object,
                  HPhandle      *handle,
                  int           queueLevel)
                );

/*
	Func name 	: hpSetTimerBasedNotifiy()
 	Description : Setup a event to notify the application based on the timerrate which can
				  be set by hpSetTimerRate()/hpSetTimerRateAndChannel().
*/ 
DECL_STDHP_FUNC ( hpSetTimerBasedNotify, HPSETTIMERBASEDNOTIFY, (
                  HPportHandle Object, 
                  HPhandle     *pHandle)
                  );

/*
	Func name 	: hpFlushReceiveQueue()
 	Description : The receive queue of the port will be flushed.
*/ 
DECL_STDHP_FUNC ( hpFlushReceiveQueue, HPFLUSHRECEIVEQUEUE, (
				  HPportHandle Object)
				  );

/*
	Func name 	: hpGetReceiveQueueLevel()
 	Description : The count of events in the receive queue of the port will be returned.
*/ 
DECL_STDHP_FUNC ( hpGetReceiveQueueLevel, HPGETRECEIVEQUEUELEVEL, (
                  HPportHandle Object,
                  int       *level)
                );

/*
	Func name 	: hpOpenCh()
 	Description : The selected channels go 'on the bus'. Type of the bus is specified by busType parameter.
				  Additional parameters can be specified by flags parameter.
*/ 
DECL_STDHP_FUNC ( hpOpenCh, HPOPENCHANNEL, (
                  HPportHandle  Object, 
                  HPaccess      accessMask, 
                  unsigned int  busType, 
                  unsigned int  ResetTimer)
                );

/*
	Func name 	: hpGetEvent()
 	Description : The driver is asked to retrieve burst of Events from the
				  application's receive queue. This function is optimized
				  for speed. pEventCount on start must contain size of the buffer in
				  messages, on return it sets number of realy received messages (messages
				  written to pEvents buffer).
				  Application must allocate pEvents buffer big enough to hold number of
				  messages requested by pEventCount parameter.
				  It returns VERR_QUEUE_IS_EMPTY and *pEventCount=0 if no event
				  was received.
				  The function only works for CAN, LIN, DAIO. For MOST there is a different function
*/
DECL_STDHP_FUNC ( hpGetEvent, HPGETEVENT, (
                  HPportHandle  Object,
                  unsigned int  *pEventCount,
                  HPevent       *pEvents)
                );

/*
	Func name 	: hpGetStatusInfo()
 	Description : Get Error string of hpStatus 
*/ 
DECL_STDHP_FUNC ( hpGetStatusInfo, HPGETSTATUSINFO, (
                  hpStatus status, 
                  char  *StatusInfo)
                );

/*
	Func name 	: hpGetEventInfo()
 	Description : Get Event Information of HPevent
*/ 
DECL_STDHP_FUNC ( hpGetEventInfo, HPGETEVENTINFO, (
                  HPevent *pEv, 
                  char  *EventInfo)
                );

/*
	Func name 	: hpOemContact()
 	Description : Connect to Covered Function only allow use to the Contractor
*/ 
DECL_STDHP_FUNC ( hpOemContact, HPOEMCONTACT, (
				  HPportHandle Object,
				  unsigned long Channel,
				  HPuint64 context1,
				  HPuint64 *context2)
				);
     
/*
	Func name 	: hpGetSyncTime()
 	Description : the Function reading high precision (1ns) card time used for time synchronization
				  of Party Line trigger (sync line).
*/ 
DECL_STDHP_FUNC ( hpGetSyncTime, HPGETSYNCTIME, (
                  HPportHandle        Object, 
                  HPuint64            *pTime )
				);

/*
	Func name 	: hpSendSyncPulse()
 	Description : Activates short sync pulse on desired channel. Channels mask should not
				  define two channels of one hardware.
*/ 
DECL_STDHP_FUNC ( hpSendSyncPulse, HPSENDSYNCPULSE, (
                  HPportHandle Object,
                  HPaccess    accessMask)
                 );

/*
	Func name 	: hpOpenConfigWindow()
 	Description : open "hwcfg.exe" execute File to config application and interface hardware
*/ 
DECL_STDHP_FUNC ( hpOpenConfigWindow, HPOPENCONFIGWINDOW, (void));

/*
	Func name 	: hpCloseCh()
 	Description : The selected channels go 'off the bus'.
				  Its now possible to initialize
*/ 
DECL_STDHP_FUNC ( hpCloseCh, HPCLOSECHANNEL, (
                  HPportHandle Object,
                  HPaccess    accessMask)
                );

/*
	Func name 	: hpCloseObject()
 	Description : The port is closed, channels are deactivated.
*/ 
DECL_STDHP_FUNC ( hpCloseObject, HPCLOSEPORT, (
                  HPportHandle Object)
                );

/*
	Func name 	: hpSetGlobalTimeSync()
 	Description : To query and change the globale time sync setting 
*/ 
DECL_STDHP_FUNC ( hpSetGlobalTimeSync, HPSETGLOBALTIMESYNC, (
                  unsigned long   newValue,
                  unsigned long  *previousValue)
                );

/*
	Func name 	: hpCheckLicense()
 	Description : use hpHanilprotech() Function to CheckLicense
*/ 
DECL_STDHP_FUNC ( hpCheckLicense, HPCHECKLICENSE, (
                  HPportHandle    Object,
                  HPaccess        accessMask,
                  unsigned long   protectionCode)
                );

/*
	Func name 	: hpHanilprotech()
 	Description : For all channels the port wants to use it is checked wether
				  the hardware is licensed for the type of application.
				  If not the application should terminate.
*/ 
DECL_STDHP_FUNC ( hpHanilprotech, HPHANILPROTECH, (
                  HPportHandle    Object,
                  HPaccess        accessMask,
                  char           *param0,
                  unsigned long   param1,
                  unsigned long   param2,
                  unsigned long   param3,
                  unsigned long   param4)
                );

/*
	Func name 	: hpGetLicenseInfo()
 	Description : brief Function to get available licenses from Hanil protech devices.
				  This function returns the available licenses in an array of HPlicenseInfo structures. This array contains all available licenses on
				  the queried channels. The position inside the array is defined by the license itself, e.g. the license for
				  the Advanced-Flexray-Library is always at the same array index.
*/ 
DECL_STDHP_FUNC ( hpGetLicenseInfo, HPGETLICENSEINFO, (
                  HPportHandle Object,
                  HPaccess       channelMask,
                  HPlicenseInfo *pLicInfoArray,
                  unsigned int   licInfoArraySize)
                );
//----------------------------------------------------------------------------------//
//                               Common Function End                                //
//----------------------------------------------------------------------------------//





//----------------------------------------------------------------------------------//
//                                CAN Function Start                                //
//----------------------------------------------------------------------------------//

/*
	Func name 	: hpCanFlushTransmitQueue()
 	Description : the transmit queue of the selected channel will be flushed
*/ 
DECL_STDHP_FUNC ( hpCanFlushTransmitQueue, HPCANFLUSHTRANSMITQUEUE, (
                  HPportHandle Object,
				  HPaccess    accessMask)
                );
                 
/*
	Func name 	: hpCanSetChannelOutput()
 	Description : The output mode for the CAN chips of the channels defined by
				  accessMask, is set to OUTPUT_MODE_NORMAL or OUTPUT_MODE_SILENT.
				  The port must have init access to the channels.
*/ 
DECL_STDHP_FUNC ( hpCanSetChannelOutput, HPCANSETCHANNELOUTPUT,  (
                  HPportHandle Object,
                  HPaccess   accessMask,
                  int        mode)
                );

/*
	Func name 	: hpCanSetChannelMode()
 	Description : For the CAN channels defined by AccessMask is set
				  whether the caller will get a TX and/or a TXRQ
				  receipt for transmitted messages.
				  The port must have init access to the channels.
*/ 
DECL_STDHP_FUNC ( hpCanSetChannelMode, HPCANSETCHANNELMODE, (
                  HPportHandle    Object,
                  HPaccess        accessMask,
                  int             tx,
                  int             txrq)
                );

/*
	Func name 	: hpCanSetReceiveMode()
 	Description : Suppresses error frames and chipstate events with ‘1’, but
	 			  allows those with ‘0’.  Error frames and chipstate events
				  are allowed by default.
*/ 
DECL_STDHP_FUNC ( hpCanSetReceiveMode, HPCANSETRECEIVEMODE, (
                  HPportHandle    Object,      
                  unsigned char   ErrorFrame, 
                  unsigned char   ChipState)
                );

//추후
/*
	Func name 	: hpCanSetChannelTransceiver()
 	Description : brief The transceiver mode is set for all channels defined by
	 			  accessMask. The port must have init access to the channels.
*/ 
DECL_STDHP_FUNC ( hpCanSetChannelTransceiver, HPCANSETCHANNELTRANSCEIVER, (
                  HPportHandle  Object,
                  HPaccess      accessMask,
                  int           type,
                  int           lineMode,
                  int           resNet)
                  );

/*
	Func name 	: hpCanSetChannelParams()
 	Description : The channels defined by accessMask will be initialized with
	 			  the given parameters.
				  The port must have init access to the channels.
*/ 
DECL_STDHP_FUNC ( hpCanSetChannelParams, HPCANSETCHANNELPARAMS, (
                  HPportHandle  Object,
                  HPaccess      accessMask,
                  HPchipParams* pChipParams)
                  );

/*
	Func name 	: hpCanSetChannelParamsC200()
 	Description : The channels defined by accessMask will be initialized with
	  			  the given parameters.
				  The port must have init access to the channels.
*/ 
DECL_STDHP_FUNC ( hpCanSetChannelParamsC200, HPCANSETCHANNELPARAMSC200, (
                  HPportHandle  Object,
                  HPaccess      accessMask,
                  unsigned char btr0,
                  unsigned char btr1)
                  );

/*
	Func name 	: hpCanSetChBitrate()
 	Description : The channels defined by accessMask will be initialized with
				  the given parameters.
				  The port must have init access to the channels.
*/ 
DECL_STDHP_FUNC ( hpCanSetChBitrate, HPCANSETCHANNELBITRATE, (
                  HPportHandle  Object,
                  HPaccess      accessMask,
                  unsigned long bitrate)
                  );

/*
	Func name 	: hpCanSetChAcceptance()
 	Description : Set the acceptance filter
				  Filters for std and ext ids are handled independant in the driver.
				  Use mask=0xFFFF,code=0xFFFF or mask=0xFFFFFFFF,
				  code=0xFFFFFFFF to fully close the filter.
*/ 
DECL_STDHP_FUNC ( hpCanSetChAcceptance, HPCANSETCHANNELACCEPTANCE, (
                  HPportHandle Object,
                  HPaccess        accessMask,
                  unsigned long   code, 
                  unsigned long   mask,
                  unsigned int    idRange)
                 );

//추후
/*
	Func name 	: hpCanAddAcceptanceRange()
 	Description : The filters are opened (all messages are received) by default.
	  			  hpCanAddAcceptanceRange opens the filters for the specified
				  range of standard IDs. The function can be called several times
				  to open multiple ID windows. Different ports may have different 
				  filters for a channel. If the CAN hardware cannot implement 
				  the filter, the driver virtualizes filtering.
*/ 
DECL_STDHP_FUNC ( hpCanAddAcceptanceRange,    HPCANADDACCEPTANCERANGE, (
                  HPportHandle    Object,
                  HPaccess        accessMask,
                  unsigned long   first_id,
                  unsigned long   last_id)
                  );

/*
	Func name 	: hpCanRemoveAcceptanceRange()
 	Description : The specified IDs will not pass the acceptance filter.
				  hpCanRemoveAcceptanceRange is only implemented for standard
				  identifier. The range of the acceptance filter can be removed
				  several times. Different ports may have different	filters for
				  a channel. If the CAN hardware cannot implement the filter,
				  the driver virtualizes filtering.
*/ 
DECL_STDHP_FUNC ( hpCanRemoveAcceptanceRange, HPCANREMOVEACCEPTANCERANGE, (
                  HPportHandle    Object,
                  HPaccess        accessMask,
                  unsigned long   first_id,
                  unsigned long   last_id)
                  );

/*
	Func name 	: hpCanResetAcceptance()
 	Description : Resets the acceptance filter. The selected filters
	 			  (depending on the idRange flag) are open.
*/ 
DECL_STDHP_FUNC ( hpCanResetAcceptance,       HPCANRESETACCEPTANCE, (
                  HPportHandle     Object,
                  HPaccess        accessMask,
                  unsigned int    idRange);
                  );

/*
	Func name 	: hpCanRequestChipState()
 	Description : The state of the selected channels is requested.
				  The answer will be received as an event (HP_CHIP_STATE).
*/ 
DECL_STDHP_FUNC ( hpCanRequestChipState, HPCANREQUESTCHIPSTATE, (
                  HPportHandle Object,
                  HPaccess    accessMask)
                );

//추후
/*
	Func name 	: hpCanGetBusLoad()
 	Description : Requeset BusLoadData to Hardware
*/ 
DECL_STDHP_FUNC ( hpCanGetBusLoad, HPCANGETBUSLOAD, (
                  HPportHandle Object,
                  HPaccess    accessMask)
                );

/*
	Func name 	: hpCanTransmit()
 	Description : This function is designed to send different messages to
				  supported bus. Usualy pEvents is a pointer to HPevent array.
				  pEvents points to variable which contains information about
				  how many messages should be transmitted to desired channels.
				  It must be less or same as pEventCount buffer size in messages.
				  On return function writes number of transmitted messages
				  (moved to device queue for transmitting). 
*/ 
DECL_STDHP_FUNC ( hpCanTransmit, HPCANTRANSMIT, (
                  HPportHandle  Object,
                  HPaccess      accessMask,
                  unsigned int  *pEventCount,
                  void          *pEvents)
                );
//----------------------------------------------------------------------------------//
//                                 CAN Function End                                 //
//----------------------------------------------------------------------------------//







//----------------------------------------------------------------------------------//
//                              CAN-FD Function Start                               //
//----------------------------------------------------------------------------------//

/*
	Func name 	: hpCanFdSetConfiguration()
 	Description : config CAN-FD using HPcanFdConf structure
*/ 
DECL_STDHP_FUNC ( hpCanFdSetConfiguration, HPCANFDSETCONFIGURATION, (
				  HPportHandle Object,
				  HPaccess accessMask,
				  HPcanFdConf *pCANFDConf)
                );                

/*
	Func name 	: hpCanTransmitEx()
 	Description : transmits a number of CAN / CAN-FD events
*/ 
DECL_STDHP_FUNC ( hpCanTransmitEx, HPCANTRANSMITEX, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned int  msgCnt,
                  unsigned int* pMsgCntSent,
                  void   *pCANFDTxEvt)
                );

/*
	Func name 	: hpCanReceive()
 	Description : receives a CAN/CAN-FD event from the applications receive queue
*/ 
DECL_STDHP_FUNC ( hpCanReceive, HPCANRECEIVE, (
                  HPportHandle  portHandle,
                  HPevent* pCANFDRxEvt)
                );

//----------------------------------------------------------------------------------//
//                               CAN-FD Function End                                //
//----------------------------------------------------------------------------------//



//----------------------------------------------------------------------------------//
//                                LIN Function Start                                //
//----------------------------------------------------------------------------------//

/*
	Func name 	: hpLinSetChannelParams()
 	Description : Set the channel parameters using HPlinStatPar structure
*/ 
DECL_STDHP_FUNC ( hpLinSetChannelParams, HPLINSETCHANNELPARAMS, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  HPlinStatPar vStatPar)
                );

/*
	Func name 	: hpLinSetDLC()
 	Description : Set LIN channel's DLC
*/ 
DECL_STDHP_FUNC ( hpLinSetDLC, HPLINSETDLC, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned char dlc[64])
                );

/*
	Func name 	: hpLinSetSlave()
 	Description : Set LIN slave node has specific ID with data, dlc, checksum
*/ 
DECL_STDHP_FUNC ( hpLinSetSlave, HPLINSETSLAVE, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned char linId,
                  unsigned char data[8],
                  unsigned char dlc,
                  unsigned short checksum)
                );

/*
	Func name 	: hpLinSendRequest()
 	Description : Send Master of LIN Request to Slave with linId parameter
	 			  flags sets to 0
*/ 
DECL_STDHP_FUNC ( hpLinSendRequest, HPLINSENDREQUEST, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned char linId,
                  unsigned int flags)
                );

/*
	Func name 	: hpLinSetSleepMode()
 	Description : Set Node with LINID on specific channel to sleepmode
*/ 
DECL_STDHP_FUNC ( hpLinSetSleepMode, HPLINSETSLEEPMODE, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned int flags,
                  unsigned char linId)
                );

/*
	Func name 	: hpLinWakeUp()
 	Description : Set Node on specific channel to wakeup
*/ 
DECL_STDHP_FUNC ( hpLinWakeUp, HPLINWAKEUP, (
                  HPportHandle Object,
                  HPaccess accessMask)
                );

/*
	Func name 	: hpLinSetChecksum()
 	Description : Set specific channels's checksum using parameter
*/ 
DECL_STDHP_FUNC ( hpLinSetChecksum, HPLINSETCHECKSUM, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned char checksum[60])
                );

/*
	Func name 	: hpLinSwitchSlave()
 	Description : Switch on/off a LIN slave during Running
*/ 
DECL_STDHP_FUNC ( hpLinSwitchSlave, HPLINSWITCHSLAVE, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned char linID,
                  unsigned char mode)
                );

/*
	Func name 	: hpLinSetMasterResistor()
 	Description : Set specific channel to Master resistor using parameter
				  Normally the state of Master terminating resistor on the
				  LIN hardware should depend on the current mode of the hardware
				  (i.e. it should be used when the Master mode is activated and
				  should not be used when Master mode is not activated).
*/ 
DECL_STDHP_FUNC ( hpLinSetMasterResistor, HPLINSETMASTERRESISTOR, (
                  HPportHandle Object,
                  HPaccess accessMask,
                  unsigned char mode)
                );
//----------------------------------------------------------------------------------//
//                                 LIN Function End                                 //
//----------------------------------------------------------------------------------//





//----------------------------------------------------------------------------------//
//                               DAIO Function Start                                //
//----------------------------------------------------------------------------------//

/*
	Func name 	: hpDAIOSetPWMOutput()
 	Description : Changes PWM output to defined frequency and value.
*/ 
DECL_STDHP_FUNC ( hpDAIOSetPWMOutput, HPDAIOSETPWMOUTPUT, (
	              HPportHandle Object,
                  HPaccess     accessMask,
	              unsigned int frequency,
	              unsigned int value)
	            ); 

/*
	Func name 	: hpDAIOSetDigitalOutput()
 	Description : Sets digital output line to desired logical level.
*/ 
DECL_STDHP_FUNC ( hpDAIOSetDigitalOutput, HPDAIOSETDIGITALOUTPUT, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int outputMask,
                  unsigned int valuePattern)
                );

/*
	Func name 	: hpDAIOSetAnalogOutput()
 	Description : Sets analog output line to voltage level as requested 
				  (specified in millivolts).Optionally, the flag
				  HP_DAIO_IGNORE_CHANNEL can be used not to change line’s
				  current level.
*/ 
DECL_STDHP_FUNC ( hpDAIOSetAnalogOutput, HPDAIOSETANALOGOUTPUT, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int analogLine1,
                  unsigned int analogLine2,
                  unsigned int analogLine3,
                  unsigned int analogLine4)
                );

/*
	Func name 	: hpDAIORequestMeasurement()
 	Description : Forces manual measurement of DAIO values.
*/ 
DECL_STDHP_FUNC ( hpDAIORequestMeasurement, HPDAIOREQUESTMEASUREMENT, (
                  HPportHandle Object,
                  HPaccess     accessMask)
                );

/*
	Func name 	: hpDAIOSetDigitalParameters()
 	Description : Configures the digital lines. All lines are set to input by
	 			  default. The bit sequence to access the physical pins
				  on the D-SUB15 connector is as follows:
				  ? DAIO0: 0b00000001
				  ? DAIO1: 0b00000010
				  ? DAIO2: 0b00000100
				  ? DAIO3: 0b00001000
				  ? DAIO4: 0b00010000
				  ? DAIO5: 0b00100000
				  ? DAIO6: 0b01000000
				  ? DAIO7: 0b10000000
*/ 
DECL_STDHP_FUNC ( hpDAIOSetDigitalParameters, HPDAIOSETDIGITALPARAMETERS, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int inputMask,
                  unsigned int outputMask)
                );

/*
	Func name 	: hpDAIOSetAnalogParameters()
 	Description : Configures the analog lines. All lines are set to input by default. The bit sequence to
				  access the physical pins on the D-SUB15 connector is as follows:
				  ? AIO0 = 0001 (0x01)
				  ? AIO1 = 0010 (0x02)
				  ? AIO2 = 0100 (0x04)
				  ? AIO3 = 1000 (0x08)
*/ 
DECL_STDHP_FUNC ( hpDAIOSetAnalogParameters, HPDAIOSETANALOGPARAMETERS, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int inputMask,
                  unsigned int outputMask,
                  unsigned int highRangeMask)
                );

/*
	Func name 	: hpDAIOSetAnalogTrigger()
 	Description : Sets analog trigger with parameters
*/ 
DECL_STDHP_FUNC ( hpDAIOSetAnalogTrigger, HPDAIOSETANALOGTRIGGER, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int triggerMask,
                  unsigned int triggerLevel,
                  unsigned int triggerEventMode)
                );

/*
	Func name 	: hpDAIOSetMeasurementFrequency()
 	Description : Sets the measurement frequency. Events will be automatically 
				  triggered, which can be received by GetEvent().
*/ 
DECL_STDHP_FUNC ( hpDAIOSetMeasurementFrequency, HPDAIOSETMEASUREMENTFREQUENCY, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int measurementInterval)
                );

/*
	Func name 	: hpDAIOSetDigitalTrigger()
 	Description : Set digital trigger of daio ports.
*/ 
DECL_STDHP_FUNC ( hpDAIOSetDigitalTrigger, HPDAIOSETDIGITALTRIGGER, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  unsigned int triggerMask)
                );

// new DAIO Interface Function Declarations
/*
	Func name 	: hpIoSetTriggerMode()
 	Description : Set modes of digital ports.
*/ 
DECL_STDHP_FUNC ( hpIoSetTriggerMode, HPIOSETTRIGGERMODE, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  HPdaioTriggerMode* phpDaioTriggerMode)
                );

/*
	Func name 	: hpIoSetDigitalOutput()
 	Description : Set digital output of digital ports.
*/ 
DECL_STDHP_FUNC ( hpIoSetDigitalOutput, HPIOSETDIGITALOUTPUT, (
                  HPportHandle Object,
                  HPaccess     accessMask,
                  HPdaioDigitalParams* phpDaioDigitalParams)
                );

/*
	Func name 	: hpDAIOGetChinfo()
 	Description : Get channel information with hwindex and hwchannel.
*/ 
DECL_STDHP_FUNC ( hpDAIOGetChinfo, HPDAIOGETCHINFO, (
                  char hwIndex,
                  char hwChannel,
                  s_hp_daio_ch_info*  pDaio_ch_info)
                );

/*
	Func name 	: hpDAIOGetCh()
 	Description : get hp_DAIO_Ch_Info Structure using s_hp_daio_ch_info 
	 			  structure.
*/ 
DECL_STDHP_FUNC ( hpDAIOGetCh, HPDAIOGETCH, (
                  s_hp_daio_ch_info pDaio_ch_info,
                  hp_DAIO_Ch_Info *pDAIOChInfo)
                );
//----------------------------------------------------------------------------------//
//                                DAIO Function End                                 //
//----------------------------------------------------------------------------------//






#if 1
#pragma pack(pop)
#else
#include <poppack.h>
#endif

#ifdef __cplusplus
}
#endif   // _cplusplus

#endif // _HPAPI_H_        




