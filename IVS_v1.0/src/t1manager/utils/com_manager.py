#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

"""
/*************************************************************************************
*   FILE:           com_manager.py
*
*   DESCRIPTION:    COM handler to start the T1.api from the script
*                   Requires pypiwin32
*
*   $Author: alexandrebau $
*
*   $Revision: 55971 $
*
*   COPYRIGHT:      GLIWA GmbH embedded systems
*                   Weilheim i.OB.
*                   All rights reserved
*
*   NOTICE:
*
*   GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
*   exclusively with GLIWA's T1 products. This file, with or without modification,
*   can be freely distributed, bundled with or included in software solutions that
*   are using such products. Redistributions of source code must retain the above
*   copyright, this notice and the following disclaimer.
*
*   DISCLAIMER:
*
*   THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
*   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*   DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
*   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
*   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*************************************************************************************/
"""

from utils import Singleton
import sys
try:
    import win32com.client
except:
    HAS_COM = False
else:
    HAS_COM = True


class ComManager():
    __metaclass__ = Singleton
    """
    Class used to start the T1.api using the COM broker.
    The package win32com is required.
    """
    def __init__(self):
        self.apiManager = None
        # indicates the successful usage of the COM broker
        self.com_usage = False

    def dispatch(self, type="User"):
        """
        Dispatch the COM broker and start the T1.api
        :return int: 0 if successful, else the last HRESULT
        """
        if not HAS_COM:
            print("win32com not found, the module pypiwin32 is necessary to start the T1.api using COM.")
            return 1
        if type != "System" and type != "User":
            print("In ComManager.dispatch, type argument must be 'System' or 'User', using 'User' as default.")
            type="User"
        try:
            self.apiManager = win32com.client.Dispatch("T1.ApiBroker.ApiManager"+type)
            if self.apiManager is not None:
                print("T1.ApiBroker.ApiManager"+type+" started")
                error_code = self.get_last_error() & 0xFFFF
                if error_code != 0:
                    if error_code == 3501:
                        print("Error " + str(error_code) + " when starting api with COM, T1.console or the GUI"
                                                           " is running")
                    elif error_code == 2811:
                        print("Error " + str(error_code) + " when starting api with COM, license not found")
                    else:
                        print("Error starting the api with COM, error code is " + str(error_code))
                else:
                    self.com_usage = True
                    print("T1.api Started")
                    hresult, processId, threadId = self.apiManager.GetProcessThreadId()
                    print('ProcessId: {0}, ThreadId: {1}'.format(processId, threadId))
                return error_code
            else:
                print("Impossible to create T1.ApiBroker.ApiManager"+type)
                return 1
        except Exception as e:
            print("Exception when starting T1.ApiBroker:")
            print(e)
            return 1

    def get_last_error(self):
        if self.apiManager:
            error_code = self.apiManager.GetLastError()
            return error_code
        else:
            return 1

    def start_api(self):
        """
        Deprecated:
        The COM broker automatically start the T1.api during dispatch.
        :return int: 0
        """
        return 0

    def stop_api(self):
        """
        Deprecated:
        The COM broker automatically stop the T1.api when the COM reference is deleted.
        :return int: 0
        """
        return 0

    def shutdown_api(self):
        """
        delete the reference to the COM broker
        :return int: 0
        """
        if self.apiManager:
            del self.apiManager
        return 0

    def com_used(self):
        """
        Return the usage of the COM broker
        :return bool: True if the T1.api was started with the COM broker, False otherwise
        """
        return self.com_usage
