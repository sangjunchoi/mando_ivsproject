#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

"""
/*************************************************************************************
*   FILE:           utility.py
*
*   DESCRIPTION:    utility functions in the T1.pySdk
*
*   $Author: alexandrebau $
*
*   $Revision: 55837 $
*
*   COPYRIGHT:      GLIWA GmbH embedded systems
*                   Weilheim i.OB.
*                   All rights reserved
*
*   NOTICE:
*
*   GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
*   exclusively with GLIWA's T1 products. This file, with or without modification,
*   can be freely distributed, bundled with or included in software solutions that
*   are using such products. Redistributions of source code must retain the above
*   copyright, this notice and the following disclaimer.
*
*   DISCLAIMER:
*
*   THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
*   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*   DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
*   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
*   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*************************************************************************************/
"""

import time
import sys
import swagger_client
from swagger_client.rest import ApiException
from utils import ComManager
from utils import Singleton

import subprocess
import log_mod

counter_function = None
if (sys.version_info > (3, 3)):
    # Python 3.3, use perf_counter
    counter_function = time.perf_counter
else:
    # Python 2 or less than 3.3, use clock
    counter_function = time.clock
if not counter_function:
    print("Cannot check Python version, contact support@gliwa.com")
    sys.exit(1)

class Utility():
    __metaclass__ = Singleton
    """
    This class contains utility functions used by T1Connection or which can be used in the user script.
    """
    def clean_exit(self, message=None, return_code=0, link_enabled=False, t1p_loaded=False, save_project=False,
                   com_manager=None, consoleProcess=None):
        """
        Helper function to exit cleanly the script with return code 0
        :param message: the message to display
        :param return_code: the code to return
        :param link_enabled: if true, try to disable the link
        :param t1p_loaded: if true, try to close the T1 project after optionally saving
        :param save_project: if true, try to save the T1 project file before closing
        """
        if link_enabled:
            guid = swagger_client.TargetApi().target_v2_controller_disable_link_async()
            self.poll_until_completed(guid)
        if t1p_loaded:
            guid = swagger_client.ProjectApi().project_v2_controller_close_async(save_project=save_project)
            self.poll_until_completed(guid)
        if com_manager and com_manager.com_used():
            com_manager.shutdown_api()
        if message:
            #print(message.encode('utf8').decode(sys.stdout.encoding))
            log_mod.set_log('T1', message.encode('utf8').decode(sys.stdout.encoding), log_mod.Logtype_PRG)
        if consoleProcess:
            consoleProcess.kill()
        sys.exit(return_code)

    def handle_job(self, job, check_states=True, ignore_bid_mismatch=True):
        """
        Check the statetype of the Job and the execution messages.
        In case of error, the job is cancelled.
        :param job: The Job object
        :param check_states: boolean to indicate if the T1 target state should be checked
        :return: True if the job can be handled, False otherwise
        """
        ret = True
        if job.job_state.statetype != "Completed":
            ret = False
        for message in job.job_state.executionmessages:
            if message.messagelevel == "Error":
                ret = False
                print("ERROR job with id " + job.id + ", message:\n  " + message.text + ", check logs")
        if not ret:
            print("Cancelling job with id " + job.id)
            swagger_client.JobApi().job_v2_controller_cancel(job.id)
            cancelled_job = self.poll_until_completed(job.id)
            if cancelled_job.job_state.statetype != "Completed":
                print("ERROR cancelling the job with id " + cancelled_job.id)
        if check_states:
            if self.check_states(job_guid=job.id, ignore_bid_mismatch=ignore_bid_mismatch) != 0:
                ret = False
        return ret

    def poll_until_completed(self, job_guid, wait_time_s=0.1, timeout_s=10, print_progress=True):
        """
        Poll and wait until a specific T1.api job is completed/faulted/cancelled.
        This method will return when the job has finished running or after timeout.
        :param job_guid: The GUID of the job to be waited on.
        :param wait_time_s: Time waited between two consecutive calls, default 100ms.
        :param timeout_s: Maximum polling time limit of the function, default 10s.
        :return: The Job object.
        """
        job_api_instance = swagger_client.JobApi()
        try:
            job = job_api_instance.job_v2_controller_get_job(job_guid)
            start_time = counter_function()
            while job.job_state.statetype == "InProgress":
                if print_progress:
                    for system in job.job_state.per_system:
                        if system.progress_message is not None:
                            print("System {}, job progress message: {}".format(system.system_id, system.progress_message))
                time.sleep(wait_time_s)
                job = job_api_instance.job_v2_controller_get_job(job_guid)
                if start_time + timeout_s < counter_function():
                    print("Timeout on polling job with id " + job_guid)
                    # update the job state after timeout in case of script being blocked
                    job = job_api_instance.job_v2_controller_get_job(job_guid)
                    break
            if job.job_state.statetype != "Completed":
                print("WARNING job with id " + job_guid + " finished in state " + job.job_state.statetype)

            return job
        except ApiException as e:
            print("Exception when calling job_v2_controller_get_job:\n")
            print(e.encode('utf8').decode(sys.stdout.encoding))
            return None

    def poll_until_started(self, job_guid, wait_time_s=0.1, timeout_s=10):
        """
        Poll and wait until a specific T1.api job is running.
        This method will return when the job has started.
        :param job_guid: The GUID of the job to be waited on.
        :param wait_time_s: Time waited between two consecutive calls, default 100 ms
        :param timeout_s: Maximum polling time limit of the function, default 10 seconds.
        :return: The Job object.
        """
        job_api_instance = swagger_client.JobApi()
        try:
            start_time = counter_function()
            init_completed = False

            while init_completed == False:
                job = job_api_instance.job_v2_controller_get_job(job_guid)
                # work-around when not initialized yet
                if len(job.job_state.per_system) != 0:
                    init_completed = True
                    for per_system in job.job_state.per_system:
                        # if one system has not finish the init, set it back to False for another loop
                        if per_system.initialization_completed != True:
                            init_completed = False
                            break
                if not init_completed:
                    if job.job_state.statetype == 'Completed':
                        print("Warning, initialization error for job with id "+job_guid)
                        break
                    time.sleep(wait_time_s)
                    if start_time + timeout_s < counter_function():
                        print("Timeout on polling start of job with id " + job_guid)
                        break

            #going out of the loop, the job should be started on all systems and InProgress
            return job
        except ApiException as e:
            print("Exception when calling job_v2_controller_get_job:\n")
            print(e.encode('utf8').decode(sys.stdout.encoding))
            return None

    def check_states(self, job_guid=None, ignore_bid_mismatch=True, download_if_halted=True, polling_time=30, save_path=None):
        """
        Helper function to check states.
        This routine tries to restart the tracing if halted or enable the link if not enabled.
        :param job_guid: the last job executed if any
        :return: 0 with no problem found, 1 if tracing was halted or link was disabled
        """
        ret = 0
        tracing_test_failed = False
        tracing_enabled = False

        measurement_states = swagger_client.TargetApi().target_v2_controller_get_measurement_state()
        for tracing_state in measurement_states.target_measurement_states_per_system:
            if tracing_test_failed or tracing_enabled:
                break
            if tracing_state.target_measurement_state != "Tracing":
                print("------")
                if job_guid is not None:
                    print("After job " + job_guid)
                print(time.ctime()+" Tracing is in state " + tracing_state.target_measurement_state + " on system " + str(
                    tracing_state.system_id))
                if tracing_state.target_measurement_state == "TracingHalted":
                    if download_if_halted:
                        print("Downloading T1.scope trace on system " + str(tracing_state.system_id))
                        # the T1.scope traces will be downloaded in the default location alongside the project file
                        guid = swagger_client.TracesApi().traces_v2_controller_download_async(
                            system_ids=[tracing_state.system_id], save_traces=True,folder_path=save_path, keep_in_memory=True)
                        job = self.poll_until_completed(guid, timeout_s=60)
                        if job.job_state.statetype != "Completed":
                            print("ERROR downloading traces, job finished in state " + job.job_state.statetype)
                        else:
                            download_results = swagger_client.JobResultApi().job_v2_controller_get_download_result(
                                job.id)
                            for download_result in download_results.traces:
                                print("  Trace on system id " + str(
                                    download_result.system_id) + " saved in " + download_result.file_path)
                    elif not tracing_enabled:
                        guid = swagger_client.TargetApi().target_v2_controller_start_measurement_async()
                        job = self.poll_until_completed(guid, timeout_s=30)
                        if job.job_state.statetype != "Completed":
                            print("ERROR enabling the tracing, job finished in state " + job.job_state.statetype)
                        tracing_enabled = True
                    ret = 1
                else:
                    tracing_test_failed = True
                    break
        if tracing_test_failed:
            link_states = swagger_client.TargetApi().target_v2_controller_get_link_state()
            for link_state in link_states.target_link_states_per_system:
                if link_state.target_link_state != "Connected":
                    ret = 1
                    test_link = False
                    if job_guid is not None:
                        print("After job " + job_guid)
                    print(
                        "Link is in state " + link_state.target_link_state + " on system " + str(
                            link_state.system_id))

                    if link_state.target_link_state == "Connecting":
                        start_time = counter_function()
                        current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                            system_ids=[link_state.system_id])
                        while current_state.target_link_states_per_system[0].target_link_state == "Connecting":
                            time.sleep(1)
                            current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                                system_ids=[link_state.system_id])
                            if start_time + polling_time < counter_function():
                                print("Timeout on waiting to connect, disabling the link.")
                                guid = swagger_client.TargetApi().target_v2_controller_disable_link_async()
                                job = self.poll_until_completed(guid, timeout_s=30)
                                if job.job_state.statetype != "Completed":
                                    print("ERROR disabling the link, job finished in state " + job.job_state.statetype)
                                    ret=1
                                break
                        current_states = swagger_client.TargetApi().target_v2_controller_get_link_state()
                        test_connected = True
                        for current_state in current_states.target_link_states_per_system:
                            if current_state.target_link_state != "Connected":
                                test_connected = False
                        if test_connected:
                            print("Link enabled")
                        else:
                            test_link = True

                    if link_state.target_link_state == "Disconnecting":
                        start_time = counter_function()
                        current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                            system_ids=[link_state.system_id])
                        while current_state.target_link_states_per_system[0].target_link_state != "Disconnected":
                            time.sleep(1)
                            current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                                system_ids=[link_state.system_id])
                            if start_time + polling_time < counter_function():
                                print("Timeout on waiting for Disconnecting")
                                return 1
                        test_link = True

                    if link_state.target_link_state == "Startup":
                        start_time = counter_function()
                        time.sleep(1)
                        current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                                system_ids=[link_state.system_id])
                        while current_state.target_link_states_per_system[0].target_link_state == "Startup":
                            time.sleep(1)
                            current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                                system_ids=[link_state.system_id])
                            if start_time + 60 < counter_function():
                                print("Timeout on waiting for link on Startup state, trying to disable link")
                                guid = swagger_client.TargetApi().target_v2_controller_disable_link_async()
                                job = self.poll_until_completed(guid, timeout_s=30)
                                if job.job_state.statetype != "Completed":
                                    print("ERROR disabling the link, job finished in state " + job.job_state.statetype)
                                    ret = 1

                        current_states = swagger_client.TargetApi().target_v2_controller_get_link_state()
                        test_connected = True
                        for current_state in current_states.target_link_states_per_system:
                            if current_state.target_link_state != "Connected":
                                test_connected = False
                        if test_connected:
                            print("Link enabled")
                        else:
                            test_link = True

                    if test_link or link_state.target_link_state == "Disconnected":
                        print("Enabling link")

                        guid = swagger_client.TargetApi().target_v2_controller_enable_link_async(
                            ignore_bid_mismatch=ignore_bid_mismatch)
                        job = self.poll_until_completed(guid)
                        if job.job_state.statetype != "Completed":
                            print("Failed enabling the link")
                            ret = 1
                        else:
                            current_state = swagger_client.TargetApi().target_v2_controller_get_link_state(
                                    system_ids=[link_state.system_id])
                            if current_state.target_link_states_per_system[0].target_link_state != "Connected":
                                ret = 1
                            else:
                                ret = 0
                                # check that the tracing is restarted after enabling the link
                                measurement_states = swagger_client.TargetApi().target_v2_controller_get_measurement_state()
                                for tracing_state in measurement_states.target_measurement_states_per_system:
                                    if tracing_state.target_measurement_state != "Tracing":
                                        print("------")
                                        print(
                                            "Tracing is in state " + tracing_state.target_measurement_state + " on system " + str(
                                                tracing_state.system_id))

                                        guid = swagger_client.TargetApi().target_v2_controller_start_measurement_async()
                                        job = self.poll_until_completed(guid, timeout_s=30)
                                        if job.job_state.statetype != "Completed":
                                            print("ERROR enabling the tracing, job finished in state " +
                                                  job.job_state.statetype)
                                            ret = 1
                        break
                    else:
                        print("Link in state " + link_state.target_link_state)
                        guid = swagger_client.TargetApi().target_v2_controller_disable_link_async()
                        job = self.poll_until_completed(guid, timeout_s=30)
                        if job.job_state.statetype != "Completed":
                            print("ERROR disabling the link, job finished in state " + job.job_state.statetype)
                        ret = 1
        return ret


    def load_and_link(self, t1p_file, ignore_bid_mismatch=False, ignore_missing_elf=False, polling_time=30):
        """
        Helper function to load the T1 project file and link with the target.
        :param t1p_file: The T1 project file path
        :param ignore_bid_mismatch: Ignore build id mismatch during T1P loading and linking, default False
        :param ignore_missing_elf: Ignore missing ELF file during T1P loading, default False
        :param polling_time: Polling time when waiting for T1P to be loaded and link to be established, default 30sec
        :return: 0 with no problem found, 1 otherwise
        """
        guid = swagger_client.ProjectApi().project_v2_controller_load_async(t1p_file, ignore_bid_mismatch=ignore_bid_mismatch, ignore_missing_elf=ignore_missing_elf)
        if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=polling_time), check_states=False):
            return 1

        guid = swagger_client.TargetApi().target_v2_controller_enable_link_async(ignore_bid_mismatch=ignore_bid_mismatch)
        if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=polling_time), check_states=False):
            return 1
        # Check if tracing was halted during startup phase.
        # If it is halted, download traces and start tracing again.
        Utility().check_states()
        return 0