#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

"""
/*************************************************************************************
*   FILE:           singleton.py
*
*   DESCRIPTION:    metaclass used in Utility, T1Connection and ComManager
*
*   $Author: alexandrebau $
*
*   $Revision: 47819 $
*
*   COPYRIGHT:      GLIWA GmbH embedded systems
*                   Weilheim i.OB.
*                   All rights reserved
*
*   NOTICE:
*
*   GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
*   exclusively with GLIWA's T1 products. This file, with or without modification,
*   can be freely distributed, bundled with or included in software solutions that
*   are using such products. Redistributions of source code must retain the above
*   copyright, this notice and the following disclaimer.
*
*   DISCLAIMER:
*
*   THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
*   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*   DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
*   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
*   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*************************************************************************************/
"""

class Singleton(type):
    """
    Metaclass for singletons. Any instantiation of a Singleton class yields
    the exact same object.
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    @classmethod
    def __instancecheck__(mcs, instance):
        if instance.__class__ is mcs:
            return True
        else:
            return isinstance(instance.__class__, mcs)
