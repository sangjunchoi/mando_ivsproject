import pickle
import time, datetime, numpy as np

def object_file_dump(filename, data_dump, mode):
    try:
        with open(filename, mode) as f:
            pickle.dump(data_dump, f, protocol=2)
    except Exception as ex:
        raise
    
    return True

def object_file_load(filename, mode):
    data_dump=[]

    try:
        with open(filename, mode) as f:
            data_dump = pickle.load(f)
    except Exception as ex:
        raise
    
    return data_dump

def data_pickle_save(title, data, save_path, is_save):
    try:
        if super().is_empty(data):
            set_log('PARSE', title + " data is empty!", log_mod.Logtype_ERR)
            raise

        if is_save:
            if not object_file_dump(save_path, data, "wb"):
                set_log('PARSE', title + ' Pickle File Save Error', log_mod.Logtype_ERR)
                raise
    except Exception as ex:
        raise