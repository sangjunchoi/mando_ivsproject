#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

"""
/*************************************************************************************
*   FILE:           T1_api_example.py
*
*   DESCRIPTION:    example demonstrating usage of T1.api with Python
*
*   $Author: alexandrebau $
*
*   $Revision: 55882 $
*
*   COPYRIGHT:      GLIWA GmbH embedded systems
*                   Weilheim i.OB.
*                   All rights reserved
*
*   NOTICE:
*
*   GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
*   exclusively with GLIWA's T1 products. This file, with or without modification,
*   can be freely distributed, bundled with or included in software solutions that
*   are using such products. Redistributions of source code must retain the above
*   copyright, this notice and the following disclaimer.
*
*   DISCLAIMER:
*
*   THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
*   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*   DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
*   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
*   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*************************************************************************************/
"""

from utils.utility import *
from pprint import pprint
from swagger_client import *

import os.path
import time
import sys
import Pickle_Api
import log_mod
import FileManager
import subprocess
import time, datetime, numpy as np
import psutil
import copy

# for test. Please remove comment
import pickle

"""
USER DEFINED VARIABLES
The following variables need to be correctly created depending on the user environment and the location of the t1p file
"""

# Path of the T1 project file
T1P_FILE = r"D:\00.Company\11.workspace\MANDOIVS\WorkSpace\Temp\T1_project.t1p"

# the file path to confirm that CAN log is finished
tempFileForReportBase = r"\SendScenarioFinish"

scenarioSetReadyForConstraintConfigureBase = r"\SendScenarioSet"

# Path of T1 system report file to be created
reportFilePathBase = r"C:\Users\CSA_DEV\Documents\Working\MANDOIVS\IVS_v1.0\src\t1manager"

# Path of T1 installation for executing T1.Console.exe
t1InstallPath = r"C:\Program Files (x86)\GLIWA\T1_3.4.0-preview.1.13927.18+75af4276"

# usage of COM to start T1.api
USE_COM = False

# output for the results, None for the console, can be replaced by an opened file
output = None


def is_empty(data):
    if isinstance(data, np.ndarray):
        if not data.any():
            return True
    else:
        if not data:
            return True
    return False

def data_pickle_load(title, load_path):
    try:
        data = Pickle_Api.object_file_load(load_path, "rb")
    
        if is_empty(data):
            log_mod.set_log('T1', title + ' data is empty!', log_mod.Logtype_ERR)
            raise
    
        return data
    except Exception as ex:
        raise

log_mod.set_log('T1', 'T1 Automation Start!', log_mod.Logtype_PRG)


## for test. Please remove comment
configureT1EnvFromDicPath = r"C:\Users\CSA_DEV\Documents\Working\MANDOIVS\WorkSpace\Temp\C_PROJECT_INFO_DATA_dictionary.txt"
#dicFilePath = open(configureT1EnvFromDicPath, "rb")
#readDicContents = pickle.load(dicFilePath)
#configureT1EnvFromDic = readDicContents["Test_Env"]
#gLogLevel = readDicContents["Log"]["Log_Level"]

# for release. Please remove comment
#configureT1EnvFromDicPath = sys.argv[1]
readDicContents = data_pickle_load(title="[Dictionary Log]", load_path=configureT1EnvFromDicPath)
configureT1EnvFromDic = readDicContents["Test_Env"]
gLogLevel = readDicContents["Log"]["Log_Level"]
T1P_FILE = configureT1EnvFromDic["T1P_FILE"]
t1InstallPath = configureT1EnvFromDic["t1InstallPath"]
reportFilePathBase = configureT1EnvFromDic["reportFilePath"]
gLogLevel = readDicContents["Log"]["Log_Level"]
install_path = readDicContents["Common_Path"]["T15_Install_Path"]

# the file path to confirm that CAN log is finished
tempFileForReportBase = install_path + r"\src\t1manager\SendScenarioFinish"

scenarioSetReadyForConstraintConfigureBase = install_path + r"\src\t1manager\SendScenarioSet"

T1ScenarioLists = configureT1EnvFromDic["T1ScenarioList"]
scenarioIndex=1

scope_save_path = install_path + r"\src\t1manager\ScopeFile"


"""
/*************************************************************************************
*
*   SETUP PHASE
*   Here the project file is loaded and the link is enabled
*
/*************************************************************************************
"""

# 현재 HP 노트북에서 port 9001는 DPAgent 사용 중이어서 프로그램 실행 시 DPAgent 종료
# T1.Console이 이미 실행 시 재실행 안 됨. 따라서 종료
for proc in psutil.process_iter():
    try:
        #get process name and PID value
        processName = proc.name()
        processID = proc.pid
        if processName == "T1.Console.exe" or processName == "DPAgent.exe":
            parent_pid = processID  #PID
            parent = psutil.Process(parent_pid) # PID 찾기
            for child in parent.children(recursive=True):  #자식-부모 종료
                child.kill()
            parent.kill()
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):   #예외처리
        pass

com_manager = None
if USE_COM:
    com_manager = ComManager()
    if com_manager.dispatch() != 0:
        Utility().clean_exit("ERROR : Could not start T1.api using COM broker", return_code=1, com_manager=com_manager)
else:
    try:
        """
        Set T1.Console
        The following variables are setting the T1.console path and log file
        """
        # Path of this script
        pythonApiPath = os.path.dirname(os.path.realpath(__file__))        
        consoleLogFile = os.path.join(pythonApiPath, "consoleLog.txt")
        if os.path.isfile(consoleLogFile):
            os.remove(consoleLogFile)
        with open('consoleLog.txt', 'w') as f:
            t1ConsolePath = t1InstallPath+"\T1.Console.exe"
            T1_console_process = subprocess.Popen(t1ConsolePath, stdout=f)
            time.sleep(20)
    
    except Exception as e:
        print("Exception occured", e)

try:
    CommonApi().logging_v2_controller_print_message("Verbose",
                                                    "Start of T1_api_example with the project " +
                                                    T1P_FILE)
except Exception as e:
    log_mod.set_log('T1', 'Connection error', log_mod.Logtype_ERR)
    Utility().clean_exit("ERROR : Could not print info message, check that T1.api is running\n"
                         "Error: %s\n" % e, return_code=1, com_manager=com_manager, consoleProcess=T1_console_process)

guid=ProjectApi().project_v2_controller_load_async(T1P_FILE, ignore_bid_mismatch=True, ignore_missing_elf=True)
if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=60), check_states=False):
    Utility().clean_exit("ERROR loading the project and linking, verify logs.", return_code=1,
                    com_manager=com_manager)
    
log_mod.set_log('T1', 'T1 project file load success', log_mod.Logtype_PRG)

guid=TargetApi().target_v2_controller_set_hardware_interface_async(hw_interface="CANlink FD Interface Channel 1")
if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=10), check_states=False):
    Utility().clean_exit("Can not find a hardware interface. Please connect CANlink interface", return_code=1,
                    com_manager=com_manager)

guid=TargetApi().target_v2_controller_enable_link_async(ignore_bid_mismatch=True)
if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=60), check_states=False):
    Utility().clean_exit("ERROR linking", return_code=1,
                    com_manager=com_manager)                  

guid=TargetApi().target_v2_controller_start_measurement_async()
if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=60), check_states=False):
    Utility().clean_exit("ERROR start measure", return_code=1,
                    com_manager=com_manager)

# send 'T1ManagerReady' to IVSView for alrerting T1 ready
log_mod.set_log('T1', 'T1ManagerReady', log_mod.Logtype_PRG)

"""
/*************************************************************************************
*
*   MEASUREMENT PHASE
*
/*************************************************************************************
"""

initConstranits = SystemsApi().systems_v2_controller_get_constraints()
for initConstranit in initConstranits.included_constraints:
    guid = SystemsApi().systems_v2_controller_delete_constraint_async(constraint_key=initConstranit.constraint_key, remove_event_chain_from_target=True)
    job = Utility().poll_until_completed(guid)
    if not Utility().handle_job(job):
        print("Fail to delete a constraint")

# 시나리오 순차적으로 시작
for T1ScenarioList in T1ScenarioLists:
    
    # SendScenarioSet_N 파일 생성 확인
    scenarioSetReadyForConstraintConfigure = scenarioSetReadyForConstraintConfigureBase+"_"+str(scenarioIndex)
    time.sleep(1)
    while os.path.exists(scenarioSetReadyForConstraintConfigure) == False :
        time.sleep(1)
    if os.path.isfile(scenarioSetReadyForConstraintConfigure):
        time.sleep(3)
        os.remove(scenarioSetReadyForConstraintConfigure)
        
    # 제약조건 초기화를 위한 변수 선언
    resetT1ScenarioList = copy.deepcopy(T1ScenarioList)
    #curConstranits = SystemsApi().systems_v2_controller_get_constraints()
    #print(str(curConstranits))
    # 코어 별로 제약조건 설정
    for system in SystemsApi().systems_v2_controller_get_systems().values:
        T1ScenarioList.reverse()
        systemCore = T1ScenarioList.pop()
        for constraintList in systemCore:
            splitTemp = constraintList.split("$")
            print(str(splitTemp))
            if splitTemp[0] == "CPU Load":
                cpuLoadThresholdRaw = int(splitTemp[3])*256/100+0.5
                cont_measurement_property = ContMeasurementProperties(cpu_load_threshold=int(cpuLoadThresholdRaw))
                guid = T1contApi().cont_measurements_v2_controller_set_cont_measurement_properties_async(system_ids=[system.sid],
                                                                                                       cont_measurement_properties=cont_measurement_property)
                job = Utility().poll_until_completed(guid)
                if not Utility().handle_job(job):
                    print("Warning: failed to run measurement")
                test = T1contApi().cont_measurements_v2_controller_get_cont_measurement_properties(system_ids=[system.sid])
            else:
                if splitTemp[1] == "":
                    Utility().clean_exit(message="Configure Timing Parameter for "+ splitTemp[0], return_code=0, link_enabled=True, t1p_loaded=True,
                                         com_manager=com_manager, consoleProcess=T1_console_process)                    
                if splitTemp[2] == "Max":
                    splitTemp[2] = "MaxThreshold"
                elif splitTemp[2] == "Min":
                    splitTemp[2] = "MinThreshold"
                if splitTemp[4] == "ISR":
                    splitTemp[4] = "Interrupt"
                constraint_key=ConstraintKey(system_id=system.sid, 
                                             system_element_name_or_id=splitTemp[0], 
                                             timing_result_type=splitTemp[1], 
                                             constraint_result_sub_type=splitTemp[2], 
                                             system_element_type=splitTemp[4])
                guid2 = SystemsApi().systems_v2_controller_add_constraint_async(constraint_key=constraint_key, value=int(splitTemp[3]))
                job = Utility().poll_until_completed(guid2)
                if not Utility().handle_job(job):
                    print("Warning: failed to run measurement")
    # 제약조건 설정 완료에 대한 메시지 전달
    #time.sleep(10)
    log_mod.set_log('T1', 'T1ManagerConstraintSetComplete', log_mod.Logtype_PRG)
    curConstranits = SystemsApi().systems_v2_controller_get_constraints()
    idx_c=1
    print(str(len(curConstranits.included_constraints)))
    for curConstranit in curConstranits.included_constraints:
        print(str(curConstranit))
        print(idx_c)
        idx_c=idx_c+1
    # 현재 상태 가져옴
    #cont_state = TargetApi().target_v2_controller_get_state()
    # Scope 다운로드 진행
    #scope_down = TracesApi().traces_v2_controller_download_async()
    # Scope 다운로드 체크
    #download_check = JobResultApi().job_v2_controller_get_download_result()

    # SendScenarioFinish_N 파일 생성 확인
    log_mod.set_log('T1', 'Waiting to end the CAN log', log_mod.Logtype_PRG)    
    tempFileForReport=tempFileForReportBase+"_"+str(scenarioIndex)
    while os.path.exists(tempFileForReport)==False :
        scope_down = Utility().check_states(save_path=scope_save_path + "\\" + str(scenarioIndex))
        time.sleep(1)
    if os.path.isfile(tempFileForReport):
        time.sleep(3)
        os.remove(tempFileForReport)
        
    # T1 리포트 생성
    log_mod.set_log('T1', 'Generate system report', log_mod.Logtype_PRG)
    reportFilePath=reportFilePathBase+"\T1_project_"+str(scenarioIndex)+".html"
    # export System report with Html format
    if os.path.exists(reportFilePath)==True :
        os.remove(reportFilePath)
    ProjectApi().project_v2_controller_create_and_save_system_report(path=reportFilePath, report_type="Html")
    
    idx_c = 1

    # 제약조건 초기화
    resetConstranits = SystemsApi().systems_v2_controller_get_constraints()
    for resetConstranit in resetConstranits.included_constraints:
        print(str(idx_c))
        idx_c = idx_c + 1
        guid=SystemsApi().systems_v2_controller_delete_constraint_async(constraint_key=resetConstranit.constraint_key, remove_event_chain_from_target=True)    
        job = Utility().poll_until_completed(guid)
        if not Utility().handle_job(job):
            print("Fail to delete a constraint")
    
    # 다음 시나리오 시작을 위한 Index 증가
    scenarioIndex=scenarioIndex+1
    
    log_mod.set_log('T1', 'T1ManagerCanLogFinishAck', log_mod.Logtype_PRG)
    
# consoleLog.txt 파일 종료
if output is not None:
    output.close()

# 스크립트 종료
Utility().clean_exit(message="*** Script finished ***", return_code=0, link_enabled=True, t1p_loaded=True,
                     com_manager=com_manager, consoleProcess=T1_console_process)


# Loop through each system (each core) retrieving tasks
# Launch a 1 second focus CET measurement on each task retrieved, except task with "init" or "background" in their name

#guid=T1contApi().cont_measurements_v2_controller_add_result_async(element_name_or_id="OsTask_ASW_FG1_10ms", 
                                                             #result_type='DeltaTime')
#job = Utility().poll_until_completed(guid)
#if not Utility().handle_job(job):
    #print("Warning: failed to run measurement on {}".format("OsTask_ASW_FG1_10ms"))

#time.sleep(3)
  

#guid=T1contApi().cont_measurements_v2_controller_remove_result_async(element_name_or_id, result_type, **kwargs)
#job = Utility().poll_until_completed(guid)
#if not Utility().handle_job(job):
    #print("Warning: failed to run measurement on {}".format("OsTask_ASW_FG1_10ms"))


#for system in SystemsApi().systems_v2_controller_get_systems().values:
    #print("Starting on {}".format(system.name))
    ## get the tasks of the system
    #tasks = SystemsApi().systems_v2_controller_get_tasks(system_ids=[system.sid])
    #for task in tasks.values:
        ## do a focus measurement on task
        #if "init" in task.name.lower() or "background" in task.name.lower():
            #continue
        #'''---------------------------------------------------------------------------
        #| T1.cont example, tasks timing results                                      |
        #---------------------------------------------------------------------------'''
        #print("Measuring Task {}".format(task.name))

        #guid=T1contApi().cont_measurements_v2_controller_add_result_async(element_name_or_id=task.name, 
                                                                     #result_type='CoreExecutionTime',
                                                                     #system_ids=[system.sid])
        #job = Utility().poll_until_completed(guid)
        #if not Utility().handle_job(job):
            #print("Warning: failed to run measurement on {}".format(task.name))
        #pprint("Task {} on System {}".format(task.name, system.name), stream=output)
        
        ##timing_results = ResultsApi().results_v2_controller_get_timing(name_type="SystemElement",
                                                                       ##name=task.name,
                                                                       ##system_ids=[system.sid])
        ##if timing_results is not None and len(timing_results.values) > 0:
            ##for timing_result in timing_results.values:
                ##pprint(timing_result, stream=output, indent=4)
        ##pprint("---", stream=output)           
        ##job = Utility().poll_until_completed(guid)
        ##if not Utility().handle_job(job):
            ##print("Warning: failed to run measurement on {}".format(task.name))
        ##pprint("Task {} on System {}".format(task.name, system.name), stream=output)
        ##timing_results = ResultsApi().results_v2_controller_get_timing(name_type="SystemElement",
                                                                       ##name=task.name,
                                                                       ##system_ids=[system.sid])
        ##if timing_results is not None and len(timing_results.values) > 0:
            ##for timing_result in timing_results.values:
                ##pprint(timing_result, stream=output, indent=4)
        ##pprint("---", stream=output)


#for T1ScenarioList in T1ScenarioLists:
    #CoreNum=0
    #for systemCore in T1ScenarioList:
        #for constraintList in systemCore:
            #splitTemp=constraintList.split("$")
            #print(splitTemp)
            #if splitTemp[0] == "CPU Load":
                ##cpuLoadThresholdRaw=int(splitTemp[3])*256/100+0.5
                #cpuLoadThresholdRaw=240
                #print(cpuLoadThresholdRaw)
                #cont_measurement_property=ContMeasurementProperties(cpu_load_threshold=int(cpuLoadThresholdRaw))
                #guid=T1contApi().cont_measurements_v2_controller_set_cont_measurement_properties_async(system_ids=[CoreNum+2], cont_measurement_properties=cont_measurement_property)
                #job = Utility().poll_until_completed(guid)
                #if not Utility().handle_job(job):
                    #print("Warning: failed to run measurement")
                #test=T1contApi().cont_measurements_v2_controller_get_cont_measurement_properties(system_ids=[CoreNum+2])
            #else:
                #constraint_key=ConstraintKey(system_id=CoreNum+2, 
                                             #system_element_name_or_id=splitTemp[0], 
                                             #timing_result_type=splitTemp[1], 
                                             #constraint_result_sub_type=splitTemp[2], 
                                             #system_element_type="Task")
                #SystemsApi().systems_v2_controller_add_constraint_async(constraint_key=constraint_key, value=int(splitTemp[3]))
                #job = Utility().poll_until_completed(guid)
                #if not Utility().handle_job(job):
                    #print("Warning: failed to run measurement on {}".format("OsTask_BSW_FG1_10ms_Sub1"))                
                
                ##guid=T1contApi().cont_measurements_v2_controller_add_result_async(element_name_or_id="OsTask_BSW_FG1_10ms_Sub1", 
                                                                             ##result_type='DeltaTime')
                ##job = Utility().poll_until_completed(guid)
                ##if not Utility().handle_job(job):
                    ##print("Warning: failed to run measurement on {}".format("OsTask_BSW_FG1_10ms_Sub1"))                
                
        #++CoreNum
