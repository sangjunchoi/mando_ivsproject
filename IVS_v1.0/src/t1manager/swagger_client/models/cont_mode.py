# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ContMode(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'transmission_enabled': 'bool',
        'analysis_enabled': 'bool',
        'constraints_enabled': 'bool'
    }

    attribute_map = {
        'transmission_enabled': 'transmissionEnabled',
        'analysis_enabled': 'analysisEnabled',
        'constraints_enabled': 'constraintsEnabled'
    }

    def __init__(self, transmission_enabled=None, analysis_enabled=None, constraints_enabled=None):
        """
        ContMode - a model defined in Swagger
        """

        self._transmission_enabled = None
        self._analysis_enabled = None
        self._constraints_enabled = None

        if transmission_enabled is not None:
          self.transmission_enabled = transmission_enabled
        if analysis_enabled is not None:
          self.analysis_enabled = analysis_enabled
        if constraints_enabled is not None:
          self.constraints_enabled = constraints_enabled

    @property
    def transmission_enabled(self):
        """
        Gets the transmission_enabled of this ContMode.

        :return: The transmission_enabled of this ContMode.
        :rtype: bool
        """
        return self._transmission_enabled

    @transmission_enabled.setter
    def transmission_enabled(self, transmission_enabled):
        """
        Sets the transmission_enabled of this ContMode.

        :param transmission_enabled: The transmission_enabled of this ContMode.
        :type: bool
        """

        self._transmission_enabled = transmission_enabled

    @property
    def analysis_enabled(self):
        """
        Gets the analysis_enabled of this ContMode.

        :return: The analysis_enabled of this ContMode.
        :rtype: bool
        """
        return self._analysis_enabled

    @analysis_enabled.setter
    def analysis_enabled(self, analysis_enabled):
        """
        Sets the analysis_enabled of this ContMode.

        :param analysis_enabled: The analysis_enabled of this ContMode.
        :type: bool
        """

        self._analysis_enabled = analysis_enabled

    @property
    def constraints_enabled(self):
        """
        Gets the constraints_enabled of this ContMode.

        :return: The constraints_enabled of this ContMode.
        :rtype: bool
        """
        return self._constraints_enabled

    @constraints_enabled.setter
    def constraints_enabled(self, constraints_enabled):
        """
        Sets the constraints_enabled of this ContMode.

        :param constraints_enabled: The constraints_enabled of this ContMode.
        :type: bool
        """

        self._constraints_enabled = constraints_enabled

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ContMode):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
