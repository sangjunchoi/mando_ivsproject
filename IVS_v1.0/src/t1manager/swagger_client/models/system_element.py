# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class SystemElement(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'system_id': 'int',
        'id': 'int',
        'name': 'str',
        'symbol_reference': 'str',
        'type': 'str',
        'priority': 'int',
        'period': 'int',
        'offset': 'int',
        'max_activations': 'int',
        'is_data_flow': 'bool',
        'functional_group': 'str',
        'exclude_from_cpu_load': 'bool'
    }

    attribute_map = {
        'system_id': 'systemId',
        'id': 'id',
        'name': 'name',
        'symbol_reference': 'symbolReference',
        'type': 'type',
        'priority': 'priority',
        'period': 'period',
        'offset': 'offset',
        'max_activations': 'maxActivations',
        'is_data_flow': 'isDataFlow',
        'functional_group': 'functionalGroup',
        'exclude_from_cpu_load': 'excludeFromCpuLoad'
    }

    def __init__(self, system_id=None, id=None, name=None, symbol_reference=None, type=None, priority=None, period=None, offset=None, max_activations=None, is_data_flow=None, functional_group=None, exclude_from_cpu_load=None):
        """
        SystemElement - a model defined in Swagger
        """

        self._system_id = None
        self._id = None
        self._name = None
        self._symbol_reference = None
        self._type = None
        self._priority = None
        self._period = None
        self._offset = None
        self._max_activations = None
        self._is_data_flow = None
        self._functional_group = None
        self._exclude_from_cpu_load = None

        if system_id is not None:
          self.system_id = system_id
        if id is not None:
          self.id = id
        if name is not None:
          self.name = name
        if symbol_reference is not None:
          self.symbol_reference = symbol_reference
        if type is not None:
          self.type = type
        if priority is not None:
          self.priority = priority
        if period is not None:
          self.period = period
        if offset is not None:
          self.offset = offset
        if max_activations is not None:
          self.max_activations = max_activations
        if is_data_flow is not None:
          self.is_data_flow = is_data_flow
        if functional_group is not None:
          self.functional_group = functional_group
        if exclude_from_cpu_load is not None:
          self.exclude_from_cpu_load = exclude_from_cpu_load

    @property
    def system_id(self):
        """
        Gets the system_id of this SystemElement.
        System Id

        :return: The system_id of this SystemElement.
        :rtype: int
        """
        return self._system_id

    @system_id.setter
    def system_id(self, system_id):
        """
        Sets the system_id of this SystemElement.
        System Id

        :param system_id: The system_id of this SystemElement.
        :type: int
        """

        self._system_id = system_id

    @property
    def id(self):
        """
        Gets the id of this SystemElement.
        Id

        :return: The id of this SystemElement.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this SystemElement.
        Id

        :param id: The id of this SystemElement.
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """
        Gets the name of this SystemElement.
        Name

        :return: The name of this SystemElement.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this SystemElement.
        Name

        :param name: The name of this SystemElement.
        :type: str
        """

        self._name = name

    @property
    def symbol_reference(self):
        """
        Gets the symbol_reference of this SystemElement.
        Symbol reference

        :return: The symbol_reference of this SystemElement.
        :rtype: str
        """
        return self._symbol_reference

    @symbol_reference.setter
    def symbol_reference(self, symbol_reference):
        """
        Sets the symbol_reference of this SystemElement.
        Symbol reference

        :param symbol_reference: The symbol_reference of this SystemElement.
        :type: str
        """

        self._symbol_reference = symbol_reference

    @property
    def type(self):
        """
        Gets the type of this SystemElement.
        Type

        :return: The type of this SystemElement.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this SystemElement.
        Type

        :param type: The type of this SystemElement.
        :type: str
        """
        allowed_values = ["Unknown", "Task", "Interrupt", "Runnable", "RunnableGlobal", "Stopwatch", "UserEvent", "EventChain", "PosixThread", "PosixAppDefinedId", "PosixCpu"]
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"
                .format(type, allowed_values)
            )

        self._type = type

    @property
    def priority(self):
        """
        Gets the priority of this SystemElement.

        :return: The priority of this SystemElement.
        :rtype: int
        """
        return self._priority

    @priority.setter
    def priority(self, priority):
        """
        Sets the priority of this SystemElement.

        :param priority: The priority of this SystemElement.
        :type: int
        """

        self._priority = priority

    @property
    def period(self):
        """
        Gets the period of this SystemElement.

        :return: The period of this SystemElement.
        :rtype: int
        """
        return self._period

    @period.setter
    def period(self, period):
        """
        Sets the period of this SystemElement.

        :param period: The period of this SystemElement.
        :type: int
        """

        self._period = period

    @property
    def offset(self):
        """
        Gets the offset of this SystemElement.

        :return: The offset of this SystemElement.
        :rtype: int
        """
        return self._offset

    @offset.setter
    def offset(self, offset):
        """
        Sets the offset of this SystemElement.

        :param offset: The offset of this SystemElement.
        :type: int
        """

        self._offset = offset

    @property
    def max_activations(self):
        """
        Gets the max_activations of this SystemElement.

        :return: The max_activations of this SystemElement.
        :rtype: int
        """
        return self._max_activations

    @max_activations.setter
    def max_activations(self, max_activations):
        """
        Sets the max_activations of this SystemElement.

        :param max_activations: The max_activations of this SystemElement.
        :type: int
        """

        self._max_activations = max_activations

    @property
    def is_data_flow(self):
        """
        Gets the is_data_flow of this SystemElement.

        :return: The is_data_flow of this SystemElement.
        :rtype: bool
        """
        return self._is_data_flow

    @is_data_flow.setter
    def is_data_flow(self, is_data_flow):
        """
        Sets the is_data_flow of this SystemElement.

        :param is_data_flow: The is_data_flow of this SystemElement.
        :type: bool
        """

        self._is_data_flow = is_data_flow

    @property
    def functional_group(self):
        """
        Gets the functional_group of this SystemElement.

        :return: The functional_group of this SystemElement.
        :rtype: str
        """
        return self._functional_group

    @functional_group.setter
    def functional_group(self, functional_group):
        """
        Sets the functional_group of this SystemElement.

        :param functional_group: The functional_group of this SystemElement.
        :type: str
        """

        self._functional_group = functional_group

    @property
    def exclude_from_cpu_load(self):
        """
        Gets the exclude_from_cpu_load of this SystemElement.

        :return: The exclude_from_cpu_load of this SystemElement.
        :rtype: bool
        """
        return self._exclude_from_cpu_load

    @exclude_from_cpu_load.setter
    def exclude_from_cpu_load(self, exclude_from_cpu_load):
        """
        Sets the exclude_from_cpu_load of this SystemElement.

        :param exclude_from_cpu_load: The exclude_from_cpu_load of this SystemElement.
        :type: bool
        """

        self._exclude_from_cpu_load = exclude_from_cpu_load

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, SystemElement):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
