# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class TimingResult(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'element': 'ElementIdentification',
        'eventrestriction': 'EventRestriction',
        'source': 'str',
        'subtype': 'str',
        'type': 'str',
        'unit': 'str',
        'value': 'float'
    }

    attribute_map = {
        'element': 'element',
        'eventrestriction': 'eventrestriction',
        'source': 'source',
        'subtype': 'subtype',
        'type': 'type',
        'unit': 'unit',
        'value': 'value'
    }

    def __init__(self, element=None, eventrestriction=None, source=None, subtype=None, type=None, unit=None, value=None):
        """
        TimingResult - a model defined in Swagger
        """

        self._element = None
        self._eventrestriction = None
        self._source = None
        self._subtype = None
        self._type = None
        self._unit = None
        self._value = None

        if element is not None:
          self.element = element
        if eventrestriction is not None:
          self.eventrestriction = eventrestriction
        if source is not None:
          self.source = source
        if subtype is not None:
          self.subtype = subtype
        if type is not None:
          self.type = type
        if unit is not None:
          self.unit = unit
        if value is not None:
          self.value = value

    @property
    def element(self):
        """
        Gets the element of this TimingResult.
        Identifies the element that was measured.

        :return: The element of this TimingResult.
        :rtype: ElementIdentification
        """
        return self._element

    @element.setter
    def element(self, element):
        """
        Sets the element of this TimingResult.
        Identifies the element that was measured.

        :param element: The element of this TimingResult.
        :type: ElementIdentification
        """

        self._element = element

    @property
    def eventrestriction(self):
        """
        Gets the eventrestriction of this TimingResult.

        :return: The eventrestriction of this TimingResult.
        :rtype: EventRestriction
        """
        return self._eventrestriction

    @eventrestriction.setter
    def eventrestriction(self, eventrestriction):
        """
        Sets the eventrestriction of this TimingResult.

        :param eventrestriction: The eventrestriction of this TimingResult.
        :type: EventRestriction
        """

        self._eventrestriction = eventrestriction

    @property
    def source(self):
        """
        Gets the source of this TimingResult.
        Source

        :return: The source of this TimingResult.
        :rtype: str
        """
        return self._source

    @source.setter
    def source(self, source):
        """
        Sets the source of this TimingResult.
        Source

        :param source: The source of this TimingResult.
        :type: str
        """
        allowed_values = ["Unknown", "T1", "T1Scope", "T1Cont", "T1Flex"]
        if source not in allowed_values:
            raise ValueError(
                "Invalid value for `source` ({0}), must be one of {1}"
                .format(source, allowed_values)
            )

        self._source = source

    @property
    def subtype(self):
        """
        Gets the subtype of this TimingResult.
        SubType

        :return: The subtype of this TimingResult.
        :rtype: str
        """
        return self._subtype

    @subtype.setter
    def subtype(self, subtype):
        """
        Sets the subtype of this TimingResult.
        SubType

        :param subtype: The subtype of this TimingResult.
        :type: str
        """
        allowed_values = ["Max", "Min", "Average", "Mean", "SampleCount", "StandardDeviation"]
        if subtype not in allowed_values:
            raise ValueError(
                "Invalid value for `subtype` ({0}), must be one of {1}"
                .format(subtype, allowed_values)
            )

        self._subtype = subtype

    @property
    def type(self):
        """
        Gets the type of this TimingResult.
        Type

        :return: The type of this TimingResult.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this TimingResult.
        Type

        :param type: The type of this TimingResult.
        :type: str
        """
        allowed_values = ["BsfCpuLoad", "FocusCpuLoad", "CoreExecutionTime", "DeltaTime", "GrossExecutionTime", "InitialPendingTime", "Preemption", "ResponseTime", "SlackTime", "WaitTime", "Period", "GrossExecutionTimeStopwatch", "PreemptionStopwatch", "DataAgeStopwatch", "NetSlackTime", "Run", "SchedulingLatency", "Released", "RunT", "WaitT", "DataAge", "BsfWaitRatio", "BsfReadyRatio", "OccurrenceCount", "BsfCet"]
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"
                .format(type, allowed_values)
            )

        self._type = type

    @property
    def unit(self):
        """
        Gets the unit of this TimingResult.
        Unit

        :return: The unit of this TimingResult.
        :rtype: str
        """
        return self._unit

    @unit.setter
    def unit(self, unit):
        """
        Sets the unit of this TimingResult.
        Unit

        :param unit: The unit of this TimingResult.
        :type: str
        """

        self._unit = unit

    @property
    def value(self):
        """
        Gets the value of this TimingResult.
        Value

        :return: The value of this TimingResult.
        :rtype: float
        """
        return self._value

    @value.setter
    def value(self, value):
        """
        Sets the value of this TimingResult.
        Value

        :param value: The value of this TimingResult.
        :type: float
        """

        self._value = value

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, TimingResult):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
