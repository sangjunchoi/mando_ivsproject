# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class DistributionType(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'nof_classes': 'int',
        'min_value': 'float',
        'max_value': 'float',
        'element': 'list[DistributionElementType]',
        'type': 'str'
    }

    attribute_map = {
        'nof_classes': 'NofClasses',
        'min_value': 'MinValue',
        'max_value': 'MaxValue',
        'element': 'Element',
        'type': 'Type'
    }

    def __init__(self, nof_classes=None, min_value=None, max_value=None, element=None, type=None):
        """
        DistributionType - a model defined in Swagger
        """

        self._nof_classes = None
        self._min_value = None
        self._max_value = None
        self._element = None
        self._type = None

        if nof_classes is not None:
          self.nof_classes = nof_classes
        if min_value is not None:
          self.min_value = min_value
        if max_value is not None:
          self.max_value = max_value
        if element is not None:
          self.element = element
        if type is not None:
          self.type = type

    @property
    def nof_classes(self):
        """
        Gets the nof_classes of this DistributionType.

        :return: The nof_classes of this DistributionType.
        :rtype: int
        """
        return self._nof_classes

    @nof_classes.setter
    def nof_classes(self, nof_classes):
        """
        Sets the nof_classes of this DistributionType.

        :param nof_classes: The nof_classes of this DistributionType.
        :type: int
        """

        self._nof_classes = nof_classes

    @property
    def min_value(self):
        """
        Gets the min_value of this DistributionType.

        :return: The min_value of this DistributionType.
        :rtype: float
        """
        return self._min_value

    @min_value.setter
    def min_value(self, min_value):
        """
        Sets the min_value of this DistributionType.

        :param min_value: The min_value of this DistributionType.
        :type: float
        """

        self._min_value = min_value

    @property
    def max_value(self):
        """
        Gets the max_value of this DistributionType.

        :return: The max_value of this DistributionType.
        :rtype: float
        """
        return self._max_value

    @max_value.setter
    def max_value(self, max_value):
        """
        Sets the max_value of this DistributionType.

        :param max_value: The max_value of this DistributionType.
        :type: float
        """

        self._max_value = max_value

    @property
    def element(self):
        """
        Gets the element of this DistributionType.

        :return: The element of this DistributionType.
        :rtype: list[DistributionElementType]
        """
        return self._element

    @element.setter
    def element(self, element):
        """
        Sets the element of this DistributionType.

        :param element: The element of this DistributionType.
        :type: list[DistributionElementType]
        """

        self._element = element

    @property
    def type(self):
        """
        Gets the type of this DistributionType.

        :return: The type of this DistributionType.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this DistributionType.

        :param type: The type of this DistributionType.
        :type: str
        """
        allowed_values = ["BsfCpuLoad", "FocusCpuLoad", "CoreExecutionTime", "DeltaTime", "GrossExecutionTime", "InitialPendingTime", "Preemption", "ResponseTime", "SlackTime", "WaitTime", "Period", "GrossExecutionTimeStopwatch", "PreemptionStopwatch", "DataAgeStopwatch", "NetSlackTime", "Run", "SchedulingLatency", "Released", "RunT", "WaitT", "DataAge", "BsfWaitRatio", "BsfReadyRatio", "OccurrenceCount", "BsfCet"]
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"
                .format(type, allowed_values)
            )

        self._type = type

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, DistributionType):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
