# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class AccessResult(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'element': 'ElementIdentification',
        'source': 'str',
        'result_type': 'str',
        'address': 'int',
        'symbol_name': 'str',
        'symbol_offset': 'int'
    }

    attribute_map = {
        'element': 'element',
        'source': 'source',
        'result_type': 'resultType',
        'address': 'address',
        'symbol_name': 'symbolName',
        'symbol_offset': 'symbolOffset'
    }

    def __init__(self, element=None, source=None, result_type=None, address=None, symbol_name=None, symbol_offset=None):
        """
        AccessResult - a model defined in Swagger
        """

        self._element = None
        self._source = None
        self._result_type = None
        self._address = None
        self._symbol_name = None
        self._symbol_offset = None

        if element is not None:
          self.element = element
        if source is not None:
          self.source = source
        if result_type is not None:
          self.result_type = result_type
        if address is not None:
          self.address = address
        if symbol_name is not None:
          self.symbol_name = symbol_name
        if symbol_offset is not None:
          self.symbol_offset = symbol_offset

    @property
    def element(self):
        """
        Gets the element of this AccessResult.
        Identifies the element that was measured.

        :return: The element of this AccessResult.
        :rtype: ElementIdentification
        """
        return self._element

    @element.setter
    def element(self, element):
        """
        Sets the element of this AccessResult.
        Identifies the element that was measured.

        :param element: The element of this AccessResult.
        :type: ElementIdentification
        """

        self._element = element

    @property
    def source(self):
        """
        Gets the source of this AccessResult.
        Source

        :return: The source of this AccessResult.
        :rtype: str
        """
        return self._source

    @source.setter
    def source(self, source):
        """
        Sets the source of this AccessResult.
        Source

        :param source: The source of this AccessResult.
        :type: str
        """

        self._source = source

    @property
    def result_type(self):
        """
        Gets the result_type of this AccessResult.
        Result type

        :return: The result_type of this AccessResult.
        :rtype: str
        """
        return self._result_type

    @result_type.setter
    def result_type(self, result_type):
        """
        Sets the result_type of this AccessResult.
        Result type

        :param result_type: The result_type of this AccessResult.
        :type: str
        """
        allowed_values = ["Data_R", "Data_W", "Data_Rw", "Code_Prev", "Code_Next"]
        if result_type not in allowed_values:
            raise ValueError(
                "Invalid value for `result_type` ({0}), must be one of {1}"
                .format(result_type, allowed_values)
            )

        self._result_type = result_type

    @property
    def address(self):
        """
        Gets the address of this AccessResult.
        Measured address

        :return: The address of this AccessResult.
        :rtype: int
        """
        return self._address

    @address.setter
    def address(self, address):
        """
        Sets the address of this AccessResult.
        Measured address

        :param address: The address of this AccessResult.
        :type: int
        """

        self._address = address

    @property
    def symbol_name(self):
        """
        Gets the symbol_name of this AccessResult.
        Name of the symbol the address is located in. Empty if not available

        :return: The symbol_name of this AccessResult.
        :rtype: str
        """
        return self._symbol_name

    @symbol_name.setter
    def symbol_name(self, symbol_name):
        """
        Sets the symbol_name of this AccessResult.
        Name of the symbol the address is located in. Empty if not available

        :param symbol_name: The symbol_name of this AccessResult.
        :type: str
        """

        self._symbol_name = symbol_name

    @property
    def symbol_offset(self):
        """
        Gets the symbol_offset of this AccessResult.
        Byte offset to the start address of the symbol the address is located in. null, if not available

        :return: The symbol_offset of this AccessResult.
        :rtype: int
        """
        return self._symbol_offset

    @symbol_offset.setter
    def symbol_offset(self, symbol_offset):
        """
        Sets the symbol_offset of this AccessResult.
        Byte offset to the start address of the symbol the address is located in. null, if not available

        :param symbol_offset: The symbol_offset of this AccessResult.
        :type: int
        """

        self._symbol_offset = symbol_offset

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, AccessResult):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
