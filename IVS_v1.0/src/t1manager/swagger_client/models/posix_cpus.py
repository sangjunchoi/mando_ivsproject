# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class PosixCpus(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'cpus': 'list[PosixCpu]',
        'messages': 'list[Message]',
        'links': 'list[Link]'
    }

    attribute_map = {
        'cpus': 'cpus',
        'messages': '_messages',
        'links': '_links'
    }

    def __init__(self, cpus=None, messages=None, links=None):
        """
        PosixCpus - a model defined in Swagger
        """

        self._cpus = None
        self._messages = None
        self._links = None

        if cpus is not None:
          self.cpus = cpus
        if messages is not None:
          self.messages = messages
        if links is not None:
          self.links = links

    @property
    def cpus(self):
        """
        Gets the cpus of this PosixCpus.

        :return: The cpus of this PosixCpus.
        :rtype: list[PosixCpu]
        """
        return self._cpus

    @cpus.setter
    def cpus(self, cpus):
        """
        Sets the cpus of this PosixCpus.

        :param cpus: The cpus of this PosixCpus.
        :type: list[PosixCpu]
        """

        self._cpus = cpus

    @property
    def messages(self):
        """
        Gets the messages of this PosixCpus.
        contains all messages, warnings and errors that are returned by the command execution

        :return: The messages of this PosixCpus.
        :rtype: list[Message]
        """
        return self._messages

    @messages.setter
    def messages(self, messages):
        """
        Sets the messages of this PosixCpus.
        contains all messages, warnings and errors that are returned by the command execution

        :param messages: The messages of this PosixCpus.
        :type: list[Message]
        """

        self._messages = messages

    @property
    def links(self):
        """
        Gets the links of this PosixCpus.

        :return: The links of this PosixCpus.
        :rtype: list[Link]
        """
        return self._links

    @links.setter
    def links(self, links):
        """
        Sets the links of this PosixCpus.

        :param links: The links of this PosixCpus.
        :type: list[Link]
        """

        self._links = links

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, PosixCpus):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
