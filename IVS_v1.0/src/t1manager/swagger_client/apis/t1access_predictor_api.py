# coding: utf-8

"""
    T1.api v2

    NOTICE:
     
    GLIWA GmbH embedded systems (GLIWA) is supplying this file for use
    exclusively with GLIWA's T1 products. This file, with or without modification,
    can be freely distributed, bundled with or included in software solutions that
    are using such products. Redistributions of source code must retain the above
    copyright, this notice and the following disclaimer.
 
    DISCLAIMER:
 
    THIS SOFTWARE IS PROVIDED BY GLIWA ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL GLIWA BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    OpenAPI spec version: v2
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class T1accessPredictorApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def access_predictor_v2_controller_create_and_save_t1_stack_report_async(self, path, **kwargs):
        """
        Perform T1.accessPredictor analysis on the specified Systems and export the
        results in an OT1 file.
        <div>
        This API call is asynchronous and creates a new job which will do the work.
        The call returns a GUID for the new job.
        You should wait for the job's completion by polling the endpoint /api/jobs/{id}
        until the job state is either 'Completed', 'Faulted' or 'Canceled'.
        If you decide to stop waiting for the job's completion (e.g.
        if you are using a scripted timeout), it is highly recommended to cancel the
        running job by calling /api/cancel/{id}.
        Otherwise, the old job may run forever, potentially blocking resources.
        The response of the endpoint /api/jobs/{id} also contains any warnings and
        errors that happen during the execution of the job.
        </div><div>
        If the job goes into state 'Completed', the result of the operation can be
        obtained via the endpoint </div> /api/jobs/results/getstring.
        It represents the absolute file path of the stored export.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.access_predictor_v2_controller_create_and_save_t1_stack_report_async(path, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str path: The desired output file path (required)
        :param list[int] system_ids: A comma separated list of system
            identifiers. If not specified, all existing systems are used.
        :param T1AccessPredictorExportProperties properties: A set of options
            for the export. The individual parameters in the properties model
            are optional. Their default value is false.
        :param str query_name: The name of a T1.stack query that is to be
            applied on the analysis result before exporting the results.
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.access_predictor_v2_controller_create_and_save_t1_stack_report_async_with_http_info(path, **kwargs)
        else:
            (data) = self.access_predictor_v2_controller_create_and_save_t1_stack_report_async_with_http_info(path, **kwargs)
            return data

    def access_predictor_v2_controller_create_and_save_t1_stack_report_async_with_http_info(self, path, **kwargs):
        """
        Perform T1.accessPredictor analysis on the specified Systems and export the
        results in an OT1 file.
        <div>
        This API call is asynchronous and creates a new job which will do the work.
        The call returns a GUID for the new job.
        You should wait for the job's completion by polling the endpoint /api/jobs/{id}
        until the job state is either 'Completed', 'Faulted' or 'Canceled'.
        If you decide to stop waiting for the job's completion (e.g.
        if you are using a scripted timeout), it is highly recommended to cancel the
        running job by calling /api/cancel/{id}.
        Otherwise, the old job may run forever, potentially blocking resources.
        The response of the endpoint /api/jobs/{id} also contains any warnings and
        errors that happen during the execution of the job.
        </div><div>
        If the job goes into state 'Completed', the result of the operation can be
        obtained via the endpoint </div> /api/jobs/results/getstring.
        It represents the absolute file path of the stored export.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.access_predictor_v2_controller_create_and_save_t1_stack_report_async_with_http_info(path, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str path: The desired output file path (required)
        :param list[int] system_ids: A comma separated list of system
            identifiers. If not specified, all existing systems are used.
        :param T1AccessPredictorExportProperties properties: A set of options
            for the export. The individual parameters in the properties model
            are optional. Their default value is false.
        :param str query_name: The name of a T1.stack query that is to be
            applied on the analysis result before exporting the results.
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['path', 'system_ids', 'properties', 'query_name']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method access_predictor_v2_controller_create_and_save_t1_stack_report_async" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'path' is set
        if ('path' not in params) or (params['path'] is None):
            raise ValueError("Missing the required parameter `path` when calling `access_predictor_v2_controller_create_and_save_t1_stack_report_async`")


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'system_ids' in params:
            query_params.append(('systemIds', params['system_ids']))
            collection_formats['systemIds'] = 'multi'
        if 'path' in params:
            query_params.append(('path', params['path']))
        if 'query_name' in params:
            query_params.append(('queryName', params['query_name']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'properties' in params:
            body_params = params['properties']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/vnd.t1api.v2+json'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api('/api/accessPredictor/ot1/export/async', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='str',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)
