
from Pickle_Api import *




def is_empty(data):
    if isinstance( data, np.ndarray):
        if not data.any():
            return True
    else:
        if not data:
            return True
    return False
