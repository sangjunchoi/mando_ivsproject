#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

from utils.utility import *
from pprint import pprint
from swagger_client import *

import os
import sys
import log_mod
import Pickle_Api
import subprocess
import time, datetime, numpy as np
import psutil

# for test. Please remove comment
import pickle

"""
USER DEFINED VARIABLES
The following variables need to be correctly created depending on the user environment and the location of the t1p file
"""

# Path of the T1 project file
T1P_FILE = r"D:\00.Company\11.workspace\MANDO_ADAS_DCU-10_NE1_TC297_ODIN_Multi\trunk\T1\T1_project.t1p"

# Path of T1 installation for executing T1.Console.exe
t1InstallPath = r"C:\Program Files (x86)\GLIWA\T1_3.4.0-preview.1.13927.18+75af4276"

# usage of COM to start T1.api
USE_COM = False

def is_empty(data):
    if isinstance( data, np.ndarray):
        if not data.any():
            return True
    else:
        if not data:
            return True
    return False

def data_pickle_load(title, load_path):
    try:
        data = Pickle_Api.object_file_load(load_path, "rb")
    
        if is_empty(data):
            log_mod.set_log('T1', title + ' data is empty!', log_mod.Logtype_ERR)
            raise
    
        return data
    except Exception as ex:
        raise

def data_pickle_save(title, data, save_path, is_save):
    try:
        if is_empty(data):
            set_log('PARSE', title + " data is empty!", log_mod.Logtype_ERR)
            raise

        if is_save:
            if not Pickle_Api.object_file_dump(save_path, data, "wb"):
                set_log('PARSE', title + ' Pickle File Save Error', log_mod.Logtype_ERR)
                raise
    except Exception as ex:
        raise


## for test. Please remove comment
#configureT1EnvFromDicPath = r"D:\00.Company\11.workspace\MANDOIVS\WorkSpace\Temp\C_PROJECT_INFO_DATA_dictionary.txt"
#dicFilePath = open(configureT1EnvFromDicPath, "rb")
#readDicContents = pickle.load(dicFilePath)
#configureT1EnvFromDic = readDicContents["Test_Env"]
#gLogLevel = readDicContents["Log"]["Log_Level"]

# for release. Please remove comment
configureT1EnvFromDicPath = sys.argv[1]
readDicContents = data_pickle_load(title="[Dictionary Log]", load_path=configureT1EnvFromDicPath)
configureT1EnvFromDic = readDicContents["Test_Env"]
gLogLevel = readDicContents["Log"]["Log_Level"]
T1P_FILE = configureT1EnvFromDic["T1P_FILE"]
t1InstallPath = configureT1EnvFromDic["t1InstallPath"]



"""
/*************************************************************************************
*
*   SETUP PHASE
*   Here the project file is loaded and the link is enabled
*
/*************************************************************************************
"""

for proc in psutil.process_iter():
    try:
        #get process name and PID value
        processName = proc.name()
        processID = proc.pid

        if processName == "T1.Console.exe" or processName == "DPAgent.exe":
            parent_pid = processID  #PID
            parent = psutil.Process(parent_pid) # PID 찾기
            for child in parent.children(recursive=True):  #자식-부모 종료
                child.kill()
            parent.kill()
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):   #예외처리
        pass

com_manager = None
if USE_COM:
    com_manager = ComManager()
    if com_manager.dispatch() != 0:
        Utility().clean_exit("ERROR : Could not start T1.api using COM broker", return_code=1, com_manager=com_manager)
else:
    try:
        """
        SET T1.console
        The following variables are setting the T1.console path and log file
        """
        # Path of this script
        pythonApiPath = os.path.dirname(os.path.realpath(__file__))        
        consoleLogFile = os.path.join(pythonApiPath, "consoleLog.txt")
        if os.path.isfile(consoleLogFile):
            os.remove(consoleLogFile)
        with open('consoleLog.txt', 'w') as f:
            t1ConsolePath = t1InstallPath+"\T1.Console.exe"
            T1_console_process = subprocess.Popen(t1ConsolePath, stdout=f)
            time.sleep(20)
    except Exception as e:
        print("Exception occured", e)

#try:
    #CommonApi().logging_v2_controller_print_message("Verbose",
                                                    #"Start of T1_api_example with the project " +
                                                    #T1P_FILE)
#except Exception as e:
    #log_mod.set_log('T1', 'Connection error', log_mod.Logtype_ERR)
    #Utility().clean_exit("ERROR : Could not print info message, check that T1.api is running\n"
                         #"Error: %s\n" % e, return_code=1, com_manager=com_manager, consoleProcess=T1_console_process)

guid=ProjectApi().project_v2_controller_load_async(T1P_FILE, ignore_bid_mismatch=True, ignore_missing_elf=True)
if not Utility().handle_job(Utility().poll_until_completed(guid, timeout_s=60), check_states=False):
    Utility().clean_exit("ERROR loading the project and linking, verify logs.", return_code=1,
                    com_manager=com_manager, consoleProcess=T1_console_process)

"""
/*************************************************************************************
*
*   PARSE PHASE
*   Here the project file is loaded and parsed for sid, task, isr
*
/*************************************************************************************
"""

project = {}

for system in SystemsApi().systems_v2_controller_get_systems().values:
    project[str(system.sid-2)]={}
    project[str(system.sid-2)]["CPU Load"]=["CPU Load"]
        
    # get the tasks of the system
    tasks = SystemsApi().systems_v2_controller_get_tasks(system_ids=[system.sid])
    projectTask=[]
    for task in tasks.values:
        projectTask.append(str(task.name))
    project[str(system.sid-2)]["Task"]=projectTask
    
    interrupts = SystemsApi().systems_v2_controller_get_interrupts(system_ids=[system.sid])
    projectInterrupt=[]
    for interrupt in interrupts.values:
        projectInterrupt.append(str(interrupt.name))
    project[str(system.sid-2)]["ISR"]=projectInterrupt

currentPath=os.getcwd()
T1pInfofilePath=currentPath+"\T1pInfo.txt"
data_pickle_save(title="[Parse T1 project file]", data=project, save_path=T1pInfofilePath, is_save=True)
log_mod.set_log('T1', 'T1ProjectInfoFile$$'+T1pInfofilePath, log_mod.Logtype_PRG)

Utility().clean_exit(message="*** Script finished ***", return_code=0, link_enabled=True, t1p_loaded=True,
                     com_manager=com_manager, consoleProcess=T1_console_process)