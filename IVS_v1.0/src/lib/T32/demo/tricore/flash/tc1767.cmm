; --------------------------------------------------------------------------------
; @Title: Generic Flash script file for TC1767 devices
; @Description:
;   Example script for flash declaration and programming of Infineon
;   TriCore TC1767 internal flash.
; @Keywords: Flash, Infineon, TriCore
; @Author: WRD
; @Chip: TC1767*
; @Copyright: (C) 1989-2014 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Rev: 2935 $
; $Id: tc1767.cmm 2935 2014-08-05 07:49:32Z mobermeir $


; Script arguments:
;
;   DO tc1767 [PREPAREONLY]
;
;     PREPAREONLY only declares flash but does not execute flash programming
;
; Example:
;
;   DO ~~/demo/tricore/flash/tc1767 PREPAREONLY
;
; Flash:
;   2 MByte onchip program flash at 0x80000000--0x801fffff (cached)
;                                or 0xa0000000--0xa01fffff (non cached)
;     Sectors (logical):  8 x 16kB, 1 x 128kB, 7 x 256kB
;     Sectors (physical): 2 x 64kB, 1 x 128kB, 7 x 256kB
;
;   64 kByte onchip data flash at 0x8fe00000--0x8fe07fff (cached)
;                             and 0x8fe10000--0x8fe17fff (cached)
;                              or 0xafe00000--0xafe07fff (non cached)
;                             and 0xafe10000--0xafe17fff (non cached)
;     Sectors: 2 x 32kB
;
; RAM:
;   72 kByte DMI Scratch-Pad RAM at 0xd0000000--0xd0011fff for data
;   24 kByte PMI Scratch-Pad RAM at 0xd4000000--0xd4005fff for flash algorithm
;

LOCAL &parameters &param_prepareonly
ENTRY %LINE &parameters
&param_prepareonly=(STRing.SCAN(STRing.UPpeR("&parameters"),"PREPAREONLY",0)!=-1)

; --------------------------------------------------------------------------------
; Initialize

IF SYStem.MODE()<5.
(
  SYStem.RESet
  SYStem.CPU TC1767
  SYStem.Up
)

Data.Set 0xF00005F4 %Long 0x00000008     ; disable watchdog for programming
MAP.DenyAccess 0xC8000000++0x07FFFFFF    ; workaround for DMA_TC.013

; --------------------------------------------------------------------------------
; Flash Declaration
FLASH.RESet
; Program flash
FLASH.Create 1. 0xa0000000--0xa000ffff 0x04000 TARGET Long
FLASH.Create 2. 0xa0010000--0xa001ffff 0x04000 TARGET Long
FLASH.Create 3. 0xa0020000--0xa003ffff 0x20000 TARGET Long
FLASH.Create 3. 0xa0040000--0xa01fffff 0x40000 TARGET Long
; Data flash
FLASH.Create 4. 0xafe00000--0xafe07fff 0x08000 TARGET Long
FLASH.Create 5. 0xafe10000--0xafe17fff 0x08000 TARGET Long
FLASH.TARGET 0xd4000000 0xd0000000 0x1000 ~~/demo/tricore/flash/long/tc1797.bin
FLASH.CreateALIAS 0x80000000--0x8fffffff 0xa0000000

; Flash script ends here if called with parameter PREPAREONLY
IF &param_prepareonly
  ENDDO PREPAREDONE

; --------------------------------------------------------------------------------
; Example for download

DIALOG.YESNO "Program flash memory?"
LOCAL &progflash
ENTRY &progflash
IF &progflash
(
  FLASH.ReProgram ALL /Erase
  Data.LOAD.auto *
  FLASH.ReProgram.off
)

ENDDO
