/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2012  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation,
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

#include "shared.h"
#include <math.h>

#define Ns 1024
#define N 20

float x[Ns];
float y[Ns];
static int LEDstate = 0;

#define ASSERT_LENGTH 20U
EE_TYPEASSERTVALUE EE_assertions[ASSERT_LENGTH];

static int assert_count;
void test_assert(int test)
{
  register int next_assert;
  next_assert = (assert_count == EE_ASSERT_NIL) ? 1 : assert_count + 1;
  EE_assert(next_assert, test, EE_ASSERT_NIL);
  assert_count = next_assert;
}

#define ERROR_LENGTH 10U
static EE_UINT32 error_counter;

static OSServiceIdType  const test_as_service_error_expected[ERROR_LENGTH] = {
  OSServiceId_ActivateTask, OSServiceId_SetRelAlarm
};

static StatusType       const test_as_errors_expected[ERROR_LENGTH] = {
  E_OS_LIMIT, E_OS_STATE
};

static EE_UREG          const test_as_param_error_expected[ERROR_LENGTH] = {
	cpu1_Task, cpu2_Alarm
};

static StatusType       test_as_errors[ERROR_LENGTH];
static OSServiceIdType  test_as_service_error[ERROR_LENGTH];
static EE_UREG          test_as_param_error[ERROR_LENGTH];

#define TEST_WAIT 50000

TASK(cpu0_Task)
{
  EE_UREG volatile stop;
  TickType    ticks, elapsed;
  EE_UREG     i, flag;
  StatusType  status = ActivateTask(cpu1_Task);
  test_assert(status == E_OK);
  status = ActivateTask(cpu1_Task);
  test_assert(status == E_OS_LIMIT);
  status = SetEvent(cpu1_Task, cpu1_Event);
  test_assert(status == E_OK);
  for( stop = 0; stop < TEST_WAIT; ++stop )
    ;
  status = ActivateTask(cpu1_Task);
  test_assert(status == E_OK);
  status = SetAbsAlarm(cpu2_Alarm, 1, 0);
  test_assert(status == E_OK);
  status = SetRelAlarm(cpu2_Alarm, 1, 0);
  test_assert(status == E_OS_STATE);
  status = ActivateTask(cpu2_Task);
  test_assert(status == E_OK);
  for( stop = 0; stop < TEST_WAIT; ++stop )
    ;
  status = GetCounterValue(cpu2_Counter, &ticks);
  test_assert(status == E_OK);
  test_assert(ticks == 1);
  status = ActivateTask(cpu2_Task);
  test_assert(status == E_OK);
  for( stop = 0; stop < TEST_WAIT; ++stop )
    ;
  status = GetElapsedValue(cpu2_Counter, &ticks, &elapsed);
  test_assert(status == E_OK);
  test_assert(elapsed == 1);
  status = GetCounterValue(cpu2_Counter, &ticks);
  test_assert(status == E_OK);
  test_assert(ticks == 0);

  /* Check if the errors have been raised as expected */
  for(i=0U, flag=1U; (i < ERROR_LENGTH) && (flag != 0U); ++i)
  {
    flag &= (test_as_errors[i]        == test_as_errors_expected[i]);
    flag &= (test_as_service_error[i] == test_as_service_error_expected[i]);
    flag &= (test_as_param_error[i]   == test_as_param_error_expected[i]);
  }
  test_assert(flag);

  EE_assert_range(0U, 1U, assert_count);

  EE_assert_last();

  /* Cleanly terminate the Task */
  assert_count = EE_ASSERT_NIL;
  error_counter = 0U;
  TerminateTask();
}

TASK(cpu0_LED)
{
  if(LEDstate==0)
  {
    lb_tc2x5_turn_led(EE_TRIBOARD_2X5_LED_1, EE_TRIBOARD_2X5_LED_ON);
    LEDstate=1;
  } else {
    lb_tc2x5_turn_led(EE_TRIBOARD_2X5_LED_1, EE_TRIBOARD_2X5_LED_OFF);
    LEDstate=0;
  }

  TerminateTask();
}





int lfsr_state = 0xACE1u;  /* Any nonzero start state will work. */

int lfsr(void)
{
  int lsb = lfsr_state & 1;  /* Get LSB (i.e., the output bit). */

  lfsr_state >>= 1;               /* Shift register */
  if (lsb == 1)             /* Only apply toggle mask if output bit is 1. */
	  lfsr_state ^= 0xB400u;      /* Apply toggle mask, value has 1 at bits corresponding
                                   * to taps, 0 elsewhere. */
  return lfsr_state;
}



float noise;
TASK(cpu0_generateData)
{
  int i;

  const float k=2*3.14*1/Ns;

  for(i=0;i < Ns;++i)
  {
	noise = 0.2*lfsr()/65535;
    x[i] = sin(k*10*i) + 0.09*sin(k*100*i) + 0.14*sin(k*300*i)+ 0.29*sin(k*400*i) + noise;
  }

  TerminateTask();
}

TASK(cpu0_processData)
{
  int i;
  int j;
  float b[N+1]={0.0141, 0.0210, 0.0286, 0.0366, 0.0447, 0.0523, 0.0593, 0.0651, 0.0695, 0.0722, 0.0732, 0.0722, 0.0695, 0.0651, 0.0593, 0.0523, 0.0447, 0.0366, 0.0286, 0.0210, 0.0141};

  y[0]=b[0]*x[0];
  for(i=1;i<=N;i++)
  {
    y[i]=0;
    for(j=0;j<=i;j++)
	  y[i]=y[i]+b[j]*x[i-j];
  }
  for(i=N+1;i<=Ns;i++)
  {
    y[i]=0;
    for(j=0;j<=N;j++)
	  y[i]=y[i]+b[j]*x[i-j];
  }

  TerminateTask();
}

void ErrorHook( StatusType Error ) {
  OSServiceIdType const service_id = OSErrorGetServiceId();
  CoreIdType      const core_id    = GetCoreID();

  if( core_id != OS_CORE_ID_MASTER )
  {
    /* This should never happened */
    EE_tc_debug();
  }

  test_as_service_error[error_counter]  = service_id;
  test_as_errors[error_counter]         = Error;
  /* Trick to get the first parameter independently from service
     (IT MUST NOT BE USED IN PRODUCTION CODE) */
  test_as_param_error[error_counter]    = EE_oo_get_errorhook_data()->param1.
    value_param;

  ++error_counter;
}

/*
 * MAIN TASK
 */
int main(void)
{
  StatusType status;

  lb_tc2x5_leds_init();

  StartCore(OS_CORE_ID_1, &status);
  StartCore(OS_CORE_ID_2, &status);

  StartOS(OSDEFAULTAPPMODE);
  return 0;
}
