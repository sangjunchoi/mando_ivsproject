ERIKA Enterprise Demo Project

This demo and all of its individual components are provided 'as is'.
Lauterbach explicitly excludes any liability resulting from use of
the demo project or parts of it.

The demo is created for the TRACE32 Instruction Set Simulator,
simulating a TC275TE CPU. You can download an evaluation version
of the TriCore Instruction Set Simulator at:
http://www.lauterbach.com/download.html
Execute the Simulator within the Debug directory of your project to
automatically start the "work-settings.cmm" script therin, or explicitely
start the script with File -> Run Batchfile...

This demo executable is based on the open-source OSEK/VDX Kernel
ERIKA Enterprise. For more information about the ERIKA Enterprise
project, please refer to the web site http://erika.tuxfamily.org

ERIKA Enterprise is free software distributed under the GPL2+Linking
Exception license. This demo executable is using revision 2896
of ERIKA Enterprise. The complete software can be downloaded directly
from the ERIKA Enterprise website, http://erika.tuxfamily.org,
following the instructions on the page
http://erika.tuxfamily.org/wiki/index.php?title=ERIKA_Enterprise_and_RT-Druid_SVN_Access

As an alternative, you can require a zipped version of the source code
used in this project by writing to support@lauterbach.com
