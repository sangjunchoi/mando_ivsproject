/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2012  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation,
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

 /** @file   ee_tc2x5_board.h
  *  @brief  Board-dependent part of API
  *  @author Errico Guidieri
  *  @date 2012
  */

#include <ee.h>
#include "lb_tc2x5_board.h"

void lb_tc2x5_turn_led(enum EE_tc2x5_led_id
    led_id, enum EE_tc2x5_led_status onoff)
{
  switch(led_id) {
    case EE_TRIBOARD_2X5_LED_8:
      P33_OUT.B.P13 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_7:
      P33_OUT.B.P12 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_6:
      P33_OUT.B.P11 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_5:
      P33_OUT.B.P10 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_4:
      P33_OUT.B.P9 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_3:
      P33_OUT.B.P8 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_2:
      P33_OUT.B.P7 = (EE_UINT8)onoff;
    break;
    case EE_TRIBOARD_2X5_LED_1:
      P33_OUT.B.P6 = (EE_UINT8)onoff;
    break;
    default:
      P33_OUT.B.P13 = (EE_UINT8)onoff;
      P33_OUT.B.P12 = (EE_UINT8)onoff;
      P33_OUT.B.P11 = (EE_UINT8)onoff;
      P33_OUT.B.P10 = (EE_UINT8)onoff;
      P33_OUT.B.P9 = (EE_UINT8)onoff;
      P33_OUT.B.P8 = (EE_UINT8)onoff;
      P33_OUT.B.P7 = (EE_UINT8)onoff;
      P33_OUT.B.P6 = (EE_UINT8)onoff;
    break;
  }
}

 void lb_tc2x5_leds_on( void )
{
  lb_tc2x5_turn_led(EE_TRIBOARD_2X5_ALL_LEDS, EE_TRIBOARD_2X5_LED_ON);
}

 void  lb_tc2x5_leds_off( void )
{
  lb_tc2x5_turn_led(EE_TRIBOARD_2X5_ALL_LEDS, EE_TRIBOARD_2X5_LED_OFF);
}

 void  lb_tc2x5_leds_init( void )
{
  lb_tc2x5_leds_off();

  P33_IOCR12.B.PC13  =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR12.B.PC12  =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR8.B.PC11   =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR8.B.PC10   =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR8.B.PC9    =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR8.B.PC8    =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR4.B.PC7    =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
  P33_IOCR4.B.PC6    =   EE_TC2YX_OUTPUT_PUSH_PULL_GP;
}

