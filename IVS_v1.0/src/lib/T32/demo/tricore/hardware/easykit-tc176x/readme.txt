; --------------------------------------------------------------------------------
; @Title: README for Easy Kit with TC1767-based processors
; @Description: -
; @Author: MAX
; @Board: EasyKit-TC176?
; @Chip: TC1767
; @Copyright: (C) 1989-2014 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: readme.txt 6937 2014-04-23 13:19:22Z mobermeir $


The following demos are available for each CPU:
demo   Loads the Lauterbach sieve demo into RAM and sets up a demo
       debug scenario.
flash  Configures flash programming for available flash memories, and
       offers the possibility to erase and program.


The following demos applications are available:
easykit-tc1767_sieve_intmem.elf
    Lauterbach sieve demo for running from internal RAM.
easykit-tc1767_sieve_intflash.elf
    Lauterbach sieve demo for running from internal FLASH.
    Demo needs to be flashed first.
