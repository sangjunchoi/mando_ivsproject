; --------------------------------------------------------------------------------
; @Title: Demo script for TC275TF on TriBoard-TC2x5 (SMP, multisieve app)
; @Description:
;   Loads the multisieve demo application (multi-core) into RAM and sets up a
;   demo debug scenario for SMP debugging. Further information is available in
;   in the Tricore Processor Architecture Manual (pdf/debugger_tricore.pdf),
;   chapter "SMP Debugging - Quick Start".
; @Keywords: AURIX, Infineon, multi-core, multisieve, TriCore
; @Author: MOB
; @Board: TriBoard-TC2x5
; @Chip: TC275TF
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: tc275tf_smp_demo_multisieve.cmm 15579 2020-01-27 15:08:53Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC275TF
IF ((ID.CABLE()==0x29)||(ID.CABLE()==0x4155)||(ID.CABLE()==0x00D1)||(ID.CABLE()==0x4150))
(
  ; Emulation Devices in LQFP packages (except Fusion Quad and TC27x A-Step)
  ; do not support JTAG (TDI pin is used as VDDPSB)
  ; DAP only supported by
  ; - Bi-directional OCDS Debug Cable (0x29)
  ; - Automotive Debug Cable (0x4155)
  ; See tricore_app_ocds.pdf for details
  SYStem.CONFIG.DEBUGPORTTYPE DAP2
)
ELSE
(
  DIALOG.OK "TC275TF not supported by "+CABLE.NAME() "For details see TriCore FAQ"
  ENDDO
)
CORE.ASSIGN 1. 2. 3. // assign cores to the SMP system
SYStem.Up

; optional settings:
SYStem.Option DUALPORT ON
SETUP.Var %SpotLight
MAP.BOnchip 0x0--0xffffffff // force onchip-breakpoints

; --------------------------------------------------------------------------------
; Load elf file
; - uses internal RAM only
; - uses global addressing (core-local addressing of SPRAM is not recommended)
Data.LOAD.Elf "~~~~/triboard-tc275_multisieve_intmem.elf"

; --------------------------------------------------------------------------------
; select trace method
Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; enable TriCore core 1 for flow trace (core 0 is enabled by default)

MCDS.SOURCE.Set CpuMux1.Core TriCore1
MCDS.SOURCE.Set CpuMux1.Program ON
MCDS.SOURCE.Set CpuMux1.PTMode FlowTrace

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 33% 33%
TargetSystem.state
WinPOS 33% 0% 33% 33%
List.auto
WinPOS 66% 0% 33% 33%
Var.Watch
WinPOS 0% 33% 33% 33%
List.auto /CORE 0.
WinPOS 0% 66% 33% 33%
Register.view /CORE 0.
IF (CORENUMBER()>1.)
(
  WinPOS 33% 33% 33% 33%
  List.auto /CORE 1.
  WinPOS 33% 66% 33% 33%
  Register.view /CORE 1.
  IF (CORENUMBER()>2.)
  (
    WinPOS 66% 33% 33% 33%
    List.auto /CORE 2.
    WinPOS 66% 66% 33% 33%
    Register.view /CORE 2.
  )
)

Var.AddWatch %Hex flags flagsc flags0 flags1 flags2

ENDDO
