; --------------------------------------------------------------------------------
; @Title: Demo script for TC277TE on TriBoard-TC2x7 (sieve app)
; @Description:
;   Loads the sieve demo application (single-core) into RAM and sets up a demo
;   debug scenario. Use this script for getting started.
; @Keywords: AURIX, Infineon, TriCore
; @Author: MOB
; @Board: TriBoard-TC2x7
; @Chip: TC277TE
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: tc277te_demo.cmm 15579 2020-01-27 15:08:53Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC277TE
SYStem.Up

; --------------------------------------------------------------------------------
; load demo program (uses internal RAM only)
Data.LOAD.Elf "~~~~/triboard-tc275_sieve_intmem.elf" /SingleLineAdjacent

; --------------------------------------------------------------------------------
; select trace method
IF !Analyzer()
  Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; --------------------------------------------------------------------------------
; start program execution
Go.direct main

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 100% 50%
List.auto
WinPOS 0% 50% 50% 50%
Frame.view /Locals /Caller
WinPOS 50% 50% 50% 50%
Var.Watch
Var.AddWatch ast flags

ENDDO
