; --------------------------------------------------------------------------------
; @Title: Demo script for TC277TE-Astep on TriBoard-TC2x7 (sieve app)
; @Description:
;   Loads the sieve demo application (single-core) into RAM and sets up a demo
;   debug scenario. Use this script for getting started.
; @Keywords: AURIX, Infineon, TriCore
; @Author: MOB
; @Board: TriBoard-TC2x7
; @Chip: TC277TE
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: tc277te-astep_demo.cmm 15579 2020-01-27 15:08:53Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC277TE-Astep
SYStem.Up

; --------------------------------------------------------------------------------
; load demo program (uses internal RAM only)
Data.LOAD.Elf "~~~~/triboard-tc275_sieve_intmem.elf" /SingleLineAdjacent

IF Analyzer()
(
  ; Workaround for AGBT "RLINE repetition" issue AGBT_TC.006:
  ; - AURIX TC27*-Astep devices only (fixed with TC27*-Bstep)
  ; - only required when PLL is not configured
  ; - when configuring PLL make sure that this workaround is included
  ; Desription: issue will cause corrupted trace data
  ; Workaround: f(MAX) = f(MCDS) = f(BBB)
  ; For 100MHz: MAXDIV = 0x2, BBBDIV = 0x1, MCDSDIV = 0x1
  ; For 50MHz:  MAXDIV = 0x4, BBBDIV = 0x1, MCDSDIV = 0x1
  PER.Set D:0xF0036034 %Long (Data.Long(D:0xF0036034)&~0x00F)|0x002   ; MAXDIV = 0x2, for 100 MHz
  // PER.Set D:0xF0036034 %Long (Data.Long(D:0xF0036034)&~0x00F)|0x004 ; MAXDIV = 0x4, for 50 MHz
  PER.Set D:0xF0036040 %Long (Data.Long(D:0xF0036040)&~0xF0F)|0x101   ; BBBDIV = 0x1, MCDSDIV = 0x1
)
; --------------------------------------------------------------------------------
; select trace method
IF !Analyzer()
  Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; --------------------------------------------------------------------------------
; start program execution
Go.direct main

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 100% 50%
List.auto
WinPOS 0% 50% 50% 50%
Frame.view /Locals /Caller
WinPOS 50% 50% 50% 50%
Var.Watch
Var.AddWatch ast flags

ENDDO
