; --------------------------------------------------------------------------------
; @Title: Demo script for TC297T on TriBoard-TC2x7 (SMP, multisieve app)
; @Description:
;   Loads the multisieve demo application (multi-core) into RAM and sets up a
;   demo debug scenario for SMP debugging. Further information is available in
;   in the Tricore Processor Architecture Manual (pdf/debugger_tricore.pdf),
;   chapter "SMP Debugging - Quick Start".
; @Keywords: AURIX, Infineon, multi-core, multisieve, TriCore
; @Author: MOB
; @Board: TriBoard-TC2x7
; @Chip: TC297T
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: tc297t_smp_demo_multisieve.cmm 15579 2020-01-27 15:08:53Z sltaief $


; --------------------------------------------------------------------------------
; hardware setup
;   TC297T* A-Step requires connection of pin P23.5 to 3.3 V.
;   See INFINEON device status sheet.

; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC297T
CORE.ASSIGN 1. 2. 3. // assign cores to the SMP system
SYStem.Up

; optional settings:
SYStem.Option DUALPORT ON
SETUP.Var %SpotLight
MAP.BOnchip 0x0--0xffffffff // force onchip-breakpoints

; --------------------------------------------------------------------------------
; Load elf file
; - uses internal RAM only
; - uses global addressing (core-local addressing of SPRAM is not recommended)
Data.LOAD.Elf "~~~~/triboard-tc29x_multisieve_intmem.elf"

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 33% 33%
TargetSystem.state
WinPOS 33% 0% 33% 33%
List.auto
WinPOS 66% 0% 33% 33%
Var.Watch
WinPOS 0% 33% 33% 33%
List.auto /CORE 0.
WinPOS 0% 66% 33% 33%
Register.view /CORE 0.
IF (CORENUMBER()>1.)
(
  WinPOS 33% 33% 33% 33%
  List.auto /CORE 1.
  WinPOS 33% 66% 33% 33%
  Register.view /CORE 1.
  IF (CORENUMBER()>2.)
  (
    WinPOS 66% 33% 33% 33%
    List.auto /CORE 2.
    WinPOS 66% 66% 33% 33%
    Register.view /CORE 2.
  )
)

Var.AddWatch %Hex flags flagsc flags0 flags1 flags2

ENDDO
