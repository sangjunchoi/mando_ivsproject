; --------------------------------------------------------------------------------
; @Title: Demo script for TC357TA on TriBoard-TC3x7-ADAS (sieve app)
; @Description:
;   Loads the sieve demo application (single-core) into RAM and sets up a demo
;   debug scenario. Use this script for getting started.
; @Keywords: AURIX2G, Infineon, TriCore
; @Author: MOB
; @Board: TriBoard-TC3x7-ADAS
; @Chip: TC357TA
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: demo.cmm 15580 2020-01-27 15:36:09Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC357TA
; The TriBoard-TC3x7-ADAS comes with a TLF35584 power supply
; This power supply has integrated watchdog functionality which will cut all
; power if not served correctly. By default this demo will disable this
; watchdog functionality. This includes configuring the QSPI controller as
; well as writing data to the QSPI.

IF !STATE.POWER()
(
  DO ~~~~/disable_tlf35584.cmm
)
SYStem.Up

; --------------------------------------------------------------------------------
; load demo program (uses internal RAM only)
Data.LOAD.Elf "~~~~/tc35x_sieve_intmem.elf" /SingleLineAdjacent

; --------------------------------------------------------------------------------
; select trace method
IF !Analyzer()
  Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; --------------------------------------------------------------------------------
; start program execution
Go.direct main

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 100% 50%
List.auto
WinPOS 0% 50% 50% 50%
Frame.view /Locals /Caller
WinPOS 50% 50% 50% 50%
Var.Watch
Var.AddWatch ast flags

ENDDO
