; --------------------------------------------------------------------------------
; @Title: Demo script for TC357TA on TriBoard-TC3x7-ADAS (Flash, sieve app)
; @Description:
;   Programs the sieve demo application (single-core) into the processor
;   internal flash and sets up a demo debug scenario. This script can be used
;   as a template for flashing an application.
; @Keywords: AURIX2G, flash, Infineon, TriCore
; @Author: MOB
; @Board: TriBoard-TC3x7-ADAS
; @Chip: TC357TA
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: demo_flash.cmm 15580 2020-01-27 15:36:09Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC357TA
; The TriBoard-TC3x7-ADAS comes with a TLF35584 power supply
; This power supply has integrated watchdog functionality which will cut all
; power if not served correctly. By default this demo will disable this
; watchdog functionality. This includes configuring the QSPI controller as
; well as writing data to the QSPI.

IF !STATE.POWER()
(
  DO ~~~~/disable_tlf35584.cmm
)
SYStem.Up

; --------------------------------------------------------------------------------
; Flash programming
LOCAL &elfFile &progFlash &bmhdResult
&elfFile="~~~~/tc35x_sieve_intflash.elf"

; prepare flash programming (declarations)
DO ~~/demo/tricore/flash/tc35x.cmm CPU=TC357TA PREPAREONLY

; check if application is already in flash and load symbols
Data.LOAD.Elf "&elfFile" /DIFF /SingleLineAdjacent
IF FOUND()
(
  ; ==== Step 1: Program TriCore code ====

  DIALOG.YESNO "Program Lauterbach demo into flash memory?"
  ENTRY &progFlash
  IF (&progFlash)
  (
    ; enable flash programming
    FLASH.ReProgram ALL

    ; load demo application
    Data.LOAD.Elf "&elfFile"

    FLASH.ReProgram OFF
  )

  ; ==== Step 2: Write boot mode header ====

  PRIVATE &progUcb &ucbOrigDiff &ucbCopyDiff &supported &DMU_HF_CONFIRM0 &result
  &progUcb=FALSE()
  ; Check if both UCBs are in state UNLOCKED or UNREAD. Other states are not supported by this script
  &DMU_HF_CONFIRM0=Data.Long(ED:0xF8040020)
  &supported=(((&DMU_HF_CONFIRM0)&(0x00020002))==0x00000000)
  IF !&supported
  (
	  DIALOG.MESSAGE "UCB_BMHD0_ORIG or UCB_BMHD0_COPY are not in state unlocked or unread"
  )
  ; Check if we need to program the BMHD
  &ucbOrigDiff=FALSE()
  &ucbCopyDiff=FALSE()
  IF &supported
  (
  	Data.LOAD.Elf "&elfFile" 0xAF400000++0x1FF /DIFF
  	&ucbOrigDiff=FOUND()
  	Data.LOAD.Elf "&elfFile" 0xAF401000++0x1FF /DIFF
  	&ucbCopyDiff=FOUND()
  )
  IF (&ucbOrigDiff)||(&ucbCopyDiff)
  (
    DIALOG.YESNO "Configure UCB_BMHD0_ORIG and UCB_BMHD0_COPY to start demo application?"
    ENTRY &progUcb
  )

  IF (&progUcb)&&(&ucbCopyDiff)
  (
    ; enable programming of UCB_BMHD0_COPY
    DO ~~/demo/tricore/flash/tc3xx-ucb.cmm UCB=BMHD0_COPY PREPAREONLY
    FLASH.AUTO 0xAF401000++0x1FF

    ; load data of UCB_BMHD0_COPY
    Data.LOAD.Elf "&elfFile" 0xAF401000++0x1FF

    ; check if we loaded a valid UCB
    DO ~~/demo/tricore/flash/tc3xx-ucb.cmm UCB=BMHD0_COPY CHECKUCB
    ENTRY &result

    IF ("&result"=="UCBOK")
    (
      FLASH.AUTO.off
    )
    ELSE
    (
      FLASH.AUTO.CANCEL
    )
    ; protect UCB sector against unwanted modification
    FLASH.CHANGETYPE 0xAF401000++0x1FF NOP
  )

  IF (&progUcb)&&(&ucbOrigDiff)
  (
    ; enable programming of UCB_BMHD0_ORIG
    DO ~~/demo/tricore/flash/tc3xx-ucb.cmm UCB=BMHD0_ORIG PREPAREONLY
    FLASH.AUTO 0xAF400000++0x1FF

    ; load data of UCB_BMHD0_ORIG
    Data.LOAD.Elf "&elfFile" 0xAF400000++0x1FF

    ; check if we loaded a valid UCB
    DO ~~/demo/tricore/flash/tc3xx-ucb.cmm UCB=BMHD0_ORIG CHECKUCB
    ENTRY &result

    IF ("&result"=="UCBOK")
    (
      FLASH.AUTO.off
    )
    ELSE
    (
      FLASH.AUTO.CANCEL
    )
    ; protect UCB sector against unwanted modification
    FLASH.CHANGETYPE 0xAF400000++0x1FF NOP
  )

  ; ==== Step 3: Verify programming ====

  Data.LOAD.Elf "&elfFile" /DIFF
  IF FOUND()
  (
    ; maybe some sections are still declared as NOP?
    PRIVATE &diffAddress
    &diffAddress=TRACK.ADDRESS()
    DIALOG.MESSAGE "File &elfFile has not been fully flashed, difference found at address &diffAddress (check flash declaration)"
  )
)

; --------------------------------------------------------------------------------
; select trace method
IF !Analyzer()
  Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; --------------------------------------------------------------------------------
; start program execution
Go.direct main

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 100% 50%
List.auto
WinPOS 0% 50% 50% 50%
Frame.view /Locals /Caller
WinPOS 50% 50% 50% 50%
Var.Watch
Var.AddWatch ast flags

ENDDO
