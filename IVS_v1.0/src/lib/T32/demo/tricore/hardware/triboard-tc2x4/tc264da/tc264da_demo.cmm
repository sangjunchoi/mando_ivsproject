; --------------------------------------------------------------------------------
; @Title: Demo script for TC264DA on TriBoard-TC2x4 (sieve app)
; @Description:
;   Loads the sieve demo application (single-core) into RAM and sets up a demo
;   debug scenario. Use this script for getting started.
; @Keywords: AURIX, Infineon, TriCore
; @Author: MOB
; @Board: TriBoard-TC2x4
; @Chip: TC264DA
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: tc264da_demo.cmm 15579 2020-01-27 15:08:53Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC264DA
IF ((ID.CABLE()==0x29)||(ID.CABLE()==0x4155)||(ID.CABLE()==0x00D1)||(ID.CABLE()==0x4150))
(
  ; Emulation Devices in LQFP packages (except Fusion Quad and TC27x A-Step)
  ; do not support JTAG (TDI pin is used as VDDPSB)
  ; DAP only supported by
  ; - Bi-directional OCDS Debug Cable (0x29)
  ; - Automotive Debug Cable (0x4155)
  ; See tricore_app_ocds.pdf for details
  SYStem.CONFIG.DEBUGPORTTYPE DAP2
)
ELSE
(
  DIALOG.OK "TC264DA not supported by "+CABLE.NAME() "For details see TriCore FAQ"
  ENDDO
)
SYStem.Up

; --------------------------------------------------------------------------------
; load demo program (uses internal RAM only)
Data.LOAD.Elf "~~~~/triboard-tc26x_sieve_intmem.elf" /SingleLineAdjacent

; --------------------------------------------------------------------------------
; select trace method
Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; --------------------------------------------------------------------------------
; start program execution
Go.direct main

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 100% 50%
List.auto
WinPOS 0% 50% 50% 50%
Frame.view /Locals /Caller
WinPOS 50% 50% 50% 50%
Var.Watch
Var.AddWatch ast flags

ENDDO
