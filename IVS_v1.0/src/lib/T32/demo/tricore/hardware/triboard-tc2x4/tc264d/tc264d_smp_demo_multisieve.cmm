; --------------------------------------------------------------------------------
; @Title: Demo script for TC264D on TriBoard-TC2x4 (SMP, multisieve app)
; @Description:
;   Loads the multisieve demo application (multi-core) into RAM and sets up a
;   demo debug scenario for SMP debugging. Further information is available in
;   in the Tricore Processor Architecture Manual (pdf/debugger_tricore.pdf),
;   chapter "SMP Debugging - Quick Start".
; @Keywords: AURIX, Infineon, multi-core, multisieve, TriCore
; @Author: MOB
; @Board: TriBoard-TC2x4
; @Chip: TC264D
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: tc264d_smp_demo_multisieve.cmm 15579 2020-01-27 15:08:53Z sltaief $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC264D
CORE.ASSIGN 1. 2. // assign cores to the SMP system
SYStem.Up

; optional settings:
SYStem.Option DUALPORT ON
SETUP.Var %SpotLight
MAP.BOnchip 0x0--0xffffffff // force onchip-breakpoints

; --------------------------------------------------------------------------------
; Load elf file
; - uses internal RAM only
; - uses global addressing (core-local addressing of SPRAM is not recommended)
Data.LOAD.Elf "~~~~/triboard-tc26x_multisieve_intmem.elf"

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 33% 33%
TargetSystem.state
WinPOS 33% 0% 33% 33%
List.auto
WinPOS 66% 0% 33% 33%
Var.Watch
WinPOS 0% 33% 50% 33%
List.auto /CORE 0.
WinPOS 0% 66% 50% 33%
Register.view /CORE 0.
IF (CORENUMBER()>1.)
(
  WinPOS 50% 33% 50% 33%
  List.auto /CORE 1.
  WinPOS 50% 66% 50% 33%
  Register.view /CORE 1.
)

Var.AddWatch %Hex flags flagsc flags0 flags1

ENDDO
