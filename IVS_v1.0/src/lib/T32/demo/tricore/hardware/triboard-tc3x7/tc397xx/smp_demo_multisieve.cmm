; --------------------------------------------------------------------------------
; @Title: Demo script for TC397XX on TriBoard-TC3x7 (SMP, multisieve app)
; @Description:
;   Loads the multisieve demo application (multi-core) into RAM and sets up a
;   demo debug scenario for SMP debugging. Further information is available in
;   in the Tricore Processor Architecture Manual (pdf/debugger_tricore.pdf),
;   chapter "SMP Debugging - Quick Start".
; @Keywords: AURIX2G, Infineon, multi-core, multisieve, TriCore
; @Author: MOB
; @Board: TriBoard-TC3x7
; @Chip: TC397XX
; @Copyright: (C) 1989-2020 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: smp_demo_multisieve.cmm 15539 2020-01-17 12:22:53Z meick $


; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.CPU TC397XX
CORE.ASSIGN 1. 2. 3. 4. 5. 6. // assign cores to the SMP system
SYStem.Up

; optional settings:
SYStem.Option DUALPORT ON
SETUP.Var %SpotLight
MAP.BOnchip 0x0--0xffffffff // force onchip-breakpoints

; --------------------------------------------------------------------------------
; Load elf file
; - uses internal RAM only
; - uses global addressing (core-local addressing of SPRAM is not recommended)
Data.LOAD.Elf "~~~~/tc39x_multisieve_intmem.elf"

; --------------------------------------------------------------------------------
; select trace method
IF !Analyzer()
  Trace.Method Onchip

; --------------------------------------------------------------------------------
; set up MCDS trace
Trace.OFF

; enable TriCore core 1 for flow trace (core 0 is enabled by default)

MCDS.SOURCE.Set CpuMux1.Core TriCore1
MCDS.SOURCE.Set CpuMux1.Program ON
MCDS.SOURCE.Set CpuMux1.PTMode FlowTrace

; --------------------------------------------------------------------------------
; open some windows
WinCLEAR
WinPOS 0% 0% 33% 33%
TargetSystem.state
WinPOS 33% 0% 33% 33%
List.auto
WinPOS 66% 0% 33% 33%
Var.Watch
WinPOS 0% 33% 16% 33%
List.auto /CORE 0.
WinPOS 0% 66% 16% 33%
Register.view /CORE 0.
IF (CORENUMBER()>1.)
(
  WinPOS 16% 33% 16% 33%
  List.auto /CORE 1.
  WinPOS 16% 66% 16% 33%
  Register.view /CORE 1.
  IF (CORENUMBER()>2.)
  (
    WinPOS 32% 33% 16% 33%
    List.auto /CORE 2.
    WinPOS 32% 66% 16% 33%
    Register.view /CORE 2.
  )
)
  IF (CORENUMBER()>3.)
  (
    WinPOS 48% 33% 16% 33%
    List.auto /CORE 3.
    WinPOS 48% 66% 16% 33%
    Register.view /CORE 3.
  )

  IF (CORENUMBER()>4.)
  (
    WinPOS 64% 33% 16% 33%
    List.auto /CORE 4.
    WinPOS 64% 66% 16% 33%
    Register.view /CORE 4.
  )

  IF (CORENUMBER()>5.)
  (
    WinPOS 80% 33% 16% 33%
    List.auto /CORE 5.
    WinPOS 80% 66% 16% 33%
    Register.view /CORE 5.
  )

Var.AddWatch %Hex flags flagsc flags0 flags1 flags2 flags3 flags4  flags5

ENDDO
