; --------------------------------------------------------------------------------
; @Title: README for OCDS-L2 data trace example for TC1765 on TriBoard-TC1765
; @Description: -
; @Author: MAX
; @Board: TriBoard-TC1765
; @Chip: TC1765
; @Copyright: (C) 1989-2014 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: readme.txt 6937 2014-04-23 13:19:22Z mobermeir $


OCDS-L2 data trace example for TC1765 on TriBoard-TC1765

Warning:

This demo uses an older version of tracedata.asm which might be incompatible
with recent compiler versions. For new projects, use the version from the
parent directory instead.